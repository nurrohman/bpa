﻿
##############################################################################
#
#    Odoo
#    Copyright (C) 2014-2016 CodUP (<http://codup.com>).
#
##############################################################################

from . import repeat_work_order
from . import see_photo

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
