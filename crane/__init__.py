﻿
##############################################################################
#
#    Odoo
#    Copyright (C) 2014-2016 CodUP (<http://codup.com>).
#
##############################################################################

from . import crane
from . import users
from . import wizard
from . import report

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: