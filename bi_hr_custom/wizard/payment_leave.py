from odoo import models, fields, api, _


class PaymentLeave(models.TransientModel):
    _name = 'payment.leave'
    
    cuti_id = fields.Many2one('pengganti.cuti', 'Pengganti Cuti')
    journal_id = fields.Many2one('account.journal', string='Payment Journal')
    amount = fields.Float('Amount')
    payment_date = fields.Date('Payment Date', default=fields.Date.today)
    tax_account_id = fields.Many2one('account.account', 'Tax Account')
    leave_account_id = fields.Many2one('account.account', string='Leave Account')
    
    @api.multi
    def action_validate(self):
        self.action_move_create()

    @api.multi
    def _convert_amount(self, amount):
        for voucher in self:
            return self.env.user.company_id.currency_id.compute(amount, self.env.user.company_id.currency_id)

    @api.multi
    def first_move_line_get(self, move_id, company_currency, current_currency, amount, account_id):
        amount = self._convert_amount(amount)
        debit = 0.0
        credit = amount
        if amount < 0.0: 
            debit = -amount
            credit = 0.0
        sign = debit - credit < 0 and -1 or 1
        # set the first line of the voucher
        move_line = {
                'name': self.cuti_id.name or '/',
                'debit': debit,
                'credit': credit,
                'account_id': account_id,
                'move_id': move_id,
                'journal_id': self.journal_id.id,
                'currency_id': company_currency != current_currency and current_currency or False,
                'amount_currency': (sign * abs(self.amount)
                    if company_currency != current_currency else 0.0),
                'date': self.payment_date,
            }
        return move_line

    @api.multi
    def account_move_get(self):
        if self.journal_id.sequence_id:
            if not self.journal_id.sequence_id.active:
                raise UserError(_('Please activate the sequence of selected journal !'))
            name = self.journal_id.sequence_id.with_context(ir_sequence_date=self.payment_date).next_by_id()
        else:
            raise UserError(_('Please define a sequence on the journal.'))

        move = {
            'name': name,
            'journal_id': self.journal_id.id,
            'date': self.payment_date,
        }
        return move

    @api.multi
    def action_move_create(self):
        '''
        Confirm the vouchers given in ids and create the journal entries for each of them
        ''' 
        company_currency = self[0].journal_id.company_id.currency_id.id
        current_currency = self[0].journal_id.company_id.id or company_currency
        local_context = dict(self._context, force_company=self[0].journal_id.company_id.id)
        ctx = local_context.copy()
        ctx['date'] = self[0].payment_date
        ctx['check_move_validity'] = False
        move = self.env['account.move'].create(self[0].account_move_get())
        move_line = self.env['account.move.line']
        mvl1 = self.env['account.move.line'].with_context(ctx).create(self[0].with_context(ctx).first_move_line_get(move.id, company_currency, current_currency, \
                                                                                                    self.amount, self[0].journal_id.default_credit_account_id.id))
        mvl2 = self.env['account.move.line'].with_context(ctx).create(self[0].with_context(ctx).first_move_line_get(move.id, company_currency, current_currency, \
                                                                                                    -(self.amount - self.cuti_id.pph), self[0].leave_account_id.id))
        
        tax_line = {
            'name': 'PPH',
            'debit': self.cuti_id.pph,
            'credit': 0,
            'account_id': self.tax_account_id.id,
            'move_id': move.id,
            'journal_id': self.journal_id.id,
            'currency_id': company_currency != current_currency and current_currency or False,
            'date': self.payment_date,
        }
        mvl3 = self.env['account.move.line'].with_context(ctx).create(tax_line)
        
        move.post()
        self.cuti_id.write({
            'move_id': move.id,
            'state': 'paid',
        })
        
        self.env['hr.payslip.input'].create({
            'payslip_id': self.cuti_id.payslip_reference.id,
            'name': 'Pengganti Cuti',
            'code': 'PC',
            'amount': self.cuti_id.payment_amount,
            'contract_id': self.cuti_id.employee_id.contract_ids and self.cuti_id.employee_id.contract_ids[-1].id or False,
        })
        for line in self.cuti_id.expense_line:
            self.env['hr.payslip.input'].create({
                'payslip_id': self.cuti_id.payslip_reference.id,
                'name': line.product_id.name,
                'code': line.product_id.default_code or '/',
                'amount': line.amount,
                'contract_id': self.cuti_id.employee_id.contract_ids and self.cuti_id.employee_id.contract_ids[-1].id or False,
            })
        return True
    
