# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.
import logging
from datetime import datetime
from odoo.exceptions import Warning, UserError, ValidationError
from odoo import models, fields, exceptions, api, _

_logger = logging.getLogger(__name__)


class HrContract(models.Model):
    _inherit = 'hr.contract'

    job_level_id = fields.Many2one('hr.employee.level', 'Level')

    job_level_1 = fields.Many2one('hr.employee.level', 'Job Level 1')
    job_level_2 = fields.Many2one('hr.employee.level', 'Job Level 2')
    job_level_3 = fields.Many2one('hr.employee.level', 'Job Level 3')
    job_level_4 = fields.Many2one('hr.employee.level', 'Job Level 4')

    def check_nik_sama(self, rec, nik_draft):
        count_nik = self.env['hr.employee'].search([('barcode', '=', nik_draft)]).ids
        if len(count_nik) < 1:
            return nik_draft
        else:
            rec.type_id.urutan = rec.type_id.urutan + 1
            urutan_selanjutnya = rec.type_id.urutan
            nik_draft = rec.type_id.code_contract + '.' + datetime.today().strftime('%y') + '.' + datetime.today().strftime('%m') + '.' + "{0:0=3d}".format(urutan_selanjutnya)
            self.check_nik_sama(self, rec, nik_draft)

    @api.onchange('job_level_1', 'job_level_2', 'job_level_3', 'job_level_4')
    def onchange_job_level_number(self):
        # hitunng meal
        list_meal = [self.job_level_1.meal, self.job_level_2.meal, self.job_level_3.meal, self.job_level_4.meal]
        max_list_meal = max(list_meal)
        self.meal = max_list_meal

        # hitung allowance
        allowance_level_1 = self.job_level_1.allowance
        allowance_level_2 = self.job_level_2.allowance * 1 / 2
        allowance_level_3 = self.job_level_3.allowance * 1 / 4
        allowance_level_4 = self.job_level_4.allowance * 1 / 8
        total_allowance = allowance_level_1 + allowance_level_2 + allowance_level_3 + allowance_level_4
        self.allowance = total_allowance

    nama_type_id = fields.Char('Nama Tipe ID', compute='on_status_karyawan_change')

    @api.model
    def run_employee_contract_expired_alert(self):
        # Reminder H- 10 end contract via email 
        import datetime
        today = fields.Date.today()
        sepuluh_hari_lalu = fields.Date.to_string(fields.Date.from_string(today) - datetime.timedelta(days=10))
        obj_status_expired = self.search([('date_end', '>=', sepuluh_hari_lalu)])
        for contract in obj_status_expired:
            # mulai kirim email
            mail_content = 'Contract anda akan habis'
            main_content = {
                'subject': 'Remainder contract habis',
                'author_id': self.env.user.partner_id.id,
                'body_html': mail_content,
                'email_to': contract.employee_id.work_email,
            }
            self.env['mail.mail'].create(main_content).send()            

    @api.onchange('employee_id')
    def onchange_employee_id(self):
        for lines in self:
            lines.work_location = lines.employee_id.project_id.id
            lines.job_level_id = lines.employee_id.job_level_id
            lines.allowance = lines.employee_id.job_level_id.allowance
            lines.meal = lines.employee_id.job_level_id.meal
            """
            jika belom punya contract mana new, jika sudah sama dengan barcode
            """
            employee_id = lines.employee_id.id
            if len(self.env['hr.contract'].search([('employee_id', '=', employee_id)]).ids) < 1:
                lines.nik = 'New'
            else:
                lines.nik = lines.employee_id.barcode

    schedule_pay = fields.Selection([
        ('monthly', 'Monthly'),
        ('weekly', 'Weekly'),
    ], string='Scheduled Pay', index=True, default='monthly',
    help="Defines the frequency of the wage payment.")

    work_location = fields.Many2one('project.project', compute='onchange_employee_id')

    nomor_kontrak = fields.Char('Contract Number', default='New')

    urutan = fields.Integer('Urutan')

    urutan_bk = fields.Integer('Urutan BK')
    urutan_non_bk = fields.Integer('Urutan Percobaan Tetap')

    hide_start_end_date = fields.Boolean(compute='on_status_karyawan_change')

    @api.onchange('type_id')
    def on_status_karyawan_change(self):
        for i in self:
            i.hide_start_end_date = i.type_id.display_start_end_date
            i.nama_type_id = i.type_id.name    

    nik = fields.Char('NIK', default="New")

    @api.model
    def create(self, vals):
        vals['name'] = 'New'
        obj_emp = self.env['hr.employee'].search([('id', '=', vals['employee_id'])])
        if obj_emp.contract_ids.filtered(lambda c: c.state == 'open'):
            raise ValidationError(_("%s masih mempunyai Kontrak yang belum berahir !") % (obj_emp.name))
            
        rec = super(HrContract, self).create(vals)          
        # jika sudah punya contract sebelumnya , maka jangan buat nik baru
        if rec.nik != 'New':
            rec.nik = obj_emp.barcode
        else:
            urutan_terakhir = rec.type_id.urutan
            urutan_selanjutnya = urutan_terakhir + 1
            rec.type_id.urutan = urutan_selanjutnya

            if rec.type_id.code_contract == False:
                raise ValidationError(_("Code di Contract Type belum di isi."))

            nik_draft = rec.type_id.code_contract + '.' + datetime.today().strftime('%y') + '.' + datetime.today().strftime('%m') + '.' + "{0:0=3d}".format(urutan_selanjutnya)

            nik_draft = self.check_nik_sama(rec, nik_draft)

            rec.nik = nik_draft

            # cek apakah sudah ada nomor NIK yang sama
            obj_emp.barcode = rec.nik   

        if rec.employee_id.city.id == False:
            # rec.work_location = "Work Location Belum Di isi"
            raise ValidationError(_("Home based belum di isi!"))

        else:
            rec.nomor_kontrak = "{0:0=3d}".format(rec.type_id.urutan) + '/' + rec.type_id.format + '/' + rec.employee_id.city.name + '/' + datetime.now().strftime("%m") + '/' + datetime.now().strftime("%Y")        
            rec.name = rec.nomor_kontrak
        return rec

    @api.multi
    def confirm(self):
        for i in self:
            i.state = 'open'

    @api.multi
    def cancel(self):
        for i in self:
            i.state = 'cancel'   

    @api.multi
    def to_renew(self):
        for i in self:
            i.state = 'pending'   

    @api.onchange('state')
    def _calculate_expired_state(self):                                      
        if self.status_expired == 'Akan Expired' or self.status_expired == 'Expired':
            self.state = 'close'

    state = fields.Selection([
        ('draft', 'New'),
        ('open', 'Running'),
        ('pending', 'To Renew'),
        ('close', 'Expired'),
        ('cancel', 'Cancelled')
    ], string='Status', default='draft', compute="_calculate_expired_state", store=True)

    @api.multi
    def _get_status_expired(self):
        for i in self:
            from datetime import date
            hari_ini = date.today()
            qq = 0
            if i.date_end == False:
                break

            selisih_hari_ray = i.date_end - hari_ini
            selisih_hari = selisih_hari_ray.days

            # update ke staging jika sudah h-30 atau expired

            if i.kategori_karyawan == 'probation' and selisih_hari < 1:

                i.status_expired = 'Expired'
                query = "update hr_contract set state = 'close' where id = " + str(i.id)
                self._cr.execute(query)

            elif i.kategori_karyawan == 'probation' and selisih_hari < 30:

                i.status_expired = 'Akan Expired'
                query = "update hr_contract set state = 'close' where id = " + str(i.id)
                self._cr.execute(query)
            else:

                i.status_expired = 'Aman'

            self.env.cr.commit()

    status_expired = fields.Char(compute='_get_status_expired')                        

    allowance = fields.Float('Allowance')

    meal = fields.Monetary('Meal', digits=(16, 2))

    @api.multi
    def write(self, vals):
        res = super(HrContract, self).write(vals)
        for i in self:
            i.employee_id.barcode = i.nik
        return res


class HrContractType(models.Model):
    _inherit = 'hr.contract.type'

    code_contract = fields.Char('Code') 
    display_start_end_date = fields.Boolean('Start & end date')   
    format = fields.Char('Format', default='Format-nomor-kontrak')
    urutan = fields.Integer('Sequence', default=0)
