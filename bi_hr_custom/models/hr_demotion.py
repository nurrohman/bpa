# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

import logging
from datetime import datetime
from odoo.exceptions import Warning
from odoo import models, fields, exceptions, api, _

_logger = logging.getLogger(__name__)

class HrDemotions(models.Model):
    _name = 'hr.demotion'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'HR Demotions'
    _rec_name = 'employee_id'

    ##### sk demotion no START  ##############
    sk_demoti_no = fields.Char(string="Sk Demotion No", required=True, copy=False, default='New SK')

    ##### sk demotion no END ##############

    DEMOTION_STATES = [
        ('new', 'To Start'),
        ('pending', 'Pending'),
        ('done', 'Demoted'),
        ('cancel', "Cancelled"),
    ]

    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id)
    color = fields.Integer(string='Color Index', help='This color will be used in the kanban view.')

    employee_id = fields.Many2one('hr.employee', required=True, string='Nama', index=True)

    nik = fields.Char(string='NIK', compute='_onchange_employee' ,store=True )

    @api.onchange('employee_id')
    def _onchange_employee(self):
        if self.employee_id.id != False:
            self.nik = self.employee_id.barcode
            hr_employee = self.env['hr.employee'].search([('id', '=', self.employee_id.id)])
            id_employee = hr_employee.id
            # barcode = hr_employee.barcode
            self.namanya = id_employee

    job_level_id = fields.Many2one('hr.employee.level', 'Level')
    department_id = fields.Char(string='Department',compute='_get_external',readonly=True)
    job_id = fields.Char(string='Position',compute='_get_external',readonly=True)
    effective_date = fields.Date(string='Effective Date')
    reason = fields.Many2one(
        comodel_name = 'hr.reason',
        string = 'Reason'
    )







    coach_id = fields.Char(string="Coach", compute='_get_external')
    status = fields.Char(string='Status', compute='_get_external')
    job_level = fields.Char(string='Level',compute='_get_external')

    @api.multi
    @api.depends('employee_id')
    def _get_external(self):
        for promotion in self:
            qq=0
            promotion.coach_id = promotion.employee_id.coach_id.name
            promotion.status = promotion.employee_id.status_id
            promotion.department_id = promotion.employee_id.department_id.name
            promotion.job_level = promotion.employee_id.job_level_id.name

            if promotion.employee_id.job_id.name == False:
                promotion.job_id = ''
            else:
                promotion.job_id = promotion.employee_id.job_id.name

            # set new status position
            promotion.new_job_id = promotion.employee_id.job_id
            promotion.new_level_id = promotion.employee_id.job_level_id
            promotion.new_department_id = promotion.employee_id.department_id
            promotion.new_location = promotion.employee_id.work_location
            promotion.new_coach_id = promotion.employee_id.coach_id
            promotion.location = promotion.employee_id.work_location














    @api.multi
    def _attachment_count(self):
        attachment_obj = self.env['ir.attachment']
        for record in self:
            record.attachment_count = attachment_obj.search_count([('res_model', '=', 'hr.demotion'),
                                                                    ('res_id', '=', record.id)])    

    attachment_count = fields.Integer(compute='_attachment_count', string='Attachments')

    @api.multi
    def open_attachment(self):
        xml_id = 'mail.view_document_file_kanban'
        kanban_view_id = self.env.ref(xml_id).id
        xml_id = 'base.view_attachment_tree'
        tree_view_id = self.env.ref(xml_id).id
        xml_id = 'base.view_attachment_form'
        form_view_id = self.env.ref(xml_id).id
        return {
            'name': _('Attachments'),
            'view_type': 'form',
            'view_mode': 'kanban,tree,form',
            'views': [(kanban_view_id, 'kanban'),(tree_view_id, 'tree'), (form_view_id, 'form')],
            'res_model': 'ir.attachment',
            'domain': [('res_id', 'in', self._ids),
                       ('res_model', '=', 'hr.demotion')],
            'type': 'ir.actions.act_window',
        }




























    state = fields.Selection(DEMOTION_STATES, string='Status', track_visibility='onchange', required=True,readonly=True, copy=False, default='new', index=True)
    meeting_id = fields.Many2one('calendar.event', string='Meeting')
    date_demotion = fields.Datetime(string='Payment Date', required=True, default=lambda *a: fields.Datetime.now())
    new_job_id = fields.Many2one('hr.job', string='Job Position',store=True)
    new_department_id = fields.Many2one('hr.department', string='Department', store=True)
    new_coach_id = fields.Many2one('hr.employee', string='Coach',store=True)
    new_meeting_id = fields.Many2one('calendar.event', string='Meeting')
    image = fields.Binary(
        "Medium-sized photo", attachment=True,related='employee_id.image',
        help="Medium-sized photo of the employee. It is automatically "
             "resized as a 128x128px image, with aspect ratio preserved. "
             "Use this field in form views or some kanban views.")
    note = fields.Text('Manager Comments')
    # nik = fields.Char('NIK')
    employee_status = fields.Char('Employee Status')
    joint_date = fields.Date('Joint Date')
    year_of_service = fields.Char('Year Of Service')

    location = fields.Char('Location/Branch')
    periode_jabatan_start_date = fields.Date('Periode Jabatan')
    periode_jabatan_end_date = fields.Date('Periode Jabatan End Date')
    current_salary = fields.Float('Salary/Gross')

    new_job_id = fields.Many2one('hr.job', string='Position',store=True)
    new_level_id = fields.Many2one('hr.employee.level', 'Level')
    new_location = fields.Char('Location/Branch')
    new_salary = fields.Float('Salary/Gross')
    adjustment_salary = fields.Float('Adjustment')

    estimated_demotion_date = fields.Date('Estimated Demotion Date')

    proposed_manager_id = fields.Many2one('res.partner','Manager')
    proposed_date = fields.Date('Date')
    proposed_note = fields.Text('Note')

    direct_manager_id = fields.Many2one('res.partner','Direct Manager')
    direct_manager_date = fields.Date('Date')
    direct_manager_note = fields.Text('Note')

    recruitment_manager_id = fields.Many2one('res.partner','Recruitment Manager')
    recruitment_manager_date = fields.Date('Date')
    recruitment_manager_note = fields.Text('Note')

    hrd_manager_id = fields.Many2one('res.partner','HRD Manager')
    hrd_manager_date = fields.Date('Date')
    hrd_manager_note = fields.Text('Note')

    approved_manager_id = fields.Many2one('res.partner','Sr.Mgr/Gm')
    approved_manager_date = fields.Date('Date')
    approved_manager_note = fields.Text('Note')

    cod_manager_id = fields.Many2one('res.partner','COD/BOD')
    cod_manager_date = fields.Date('Date')
    cod_manager_note = fields.Text('Note')

    @api.onchange('employee_id')
    def onchange_employee(self):
        self.department_id = self.employee_id.department_id.id
        # self.job_id = self.employee_id.job_id
        # self.coach_id = self.employee_id.coach_id
        self.job_level_id = self.employee_id.job_level_id

    @api.model
    def create(self, vals):
        # sequence demotion no
        vals['sk_demoti_no'] = self.env['ir.sequence'].next_by_code('seq.demotion')

        vals['nik'] = self.env['hr.employee'].search([('id','=',vals['employee_id'])]).barcode
        # Add the followers at creation, so they can be notified
        employee_id = vals.get('employee_id')
        if vals.get('employee_id'):
            employee = self.env['hr.employee'].browse(vals['employee_id'])
            users = self._get_users_to_subscribe(employee=employee) - self.env.user
            vals['message_follower_ids'] = []
            for partner in users.mapped('partner_id'):
                vals['message_follower_ids'] += self.env['mail.followers']._add_follower_command(self._name, [], {partner.id: None}, {})[0]
        demotion = super(HrDemotions, self).create(vals)
        employee = self.env['hr.employee'].sudo().search([('id', '=', employee_id)])
        for emp in employee:
            managers = []
            managers.append(emp.parent_id.user_id.partner_id)
            users = self.env['res.users'].sudo().search([])
            for user in users:
                if user.has_group('bi_hr_custom.group_hr_demotion_manager'):
                    manager_employee = self.env['hr.employee'].sudo().search([('user_id', '=', user.id)])
                    for employeem in manager_employee:
                        demotion.add_follower(employeem.id)
                        managers.append(employeem.user_id.partner_id.id)
        return demotion

    @api.multi
    def button_start_demotions(self):
        for record in self:
            record.write({'state':'pending'})

    @api.multi
    def _get_users_to_subscribe(self, employee=False):
        users = self.env['res.users']
        for record in self:
            employee = employee or record.employee_id
            if employee.user_id:
                users |= employee.user_id
            if employee.parent_id:
                users |= employee.parent_id.user_id
            if employee.department_id and employee.department_id.manager_id and employee.parent_id != employee.department_id.manager_id:
                users |= employee.department_id.manager_id.user_id
        return users

    @api.model
    def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
        """ Override read_group to always display all states and order them appropriatly. """
        if groupby and groupby[0] == "state":
            states = [('new', _('To Start')), ('pending', _('Decisions Pending')), ('done', _('Demoted')), ('cancel', _('Cancelled'))]
            read_group_all_states = [{
                '__context': {'group_by': groupby[1:]},
                '__domain': domain + [('state', '=', state_value)],
                'state': state_value,
                'state_count': 0,
            } for state_value, state_name in states]
            # Get standard results
            read_group_res = super(HrDemotions, self).read_group(domain, fields, groupby, offset=offset, limit=limit, orderby=orderby)
            # Update standard results with default results
            result = []
            for state_value, state_name in states:
                res = [x for x in read_group_res if x['state'] == state_value]
                if not res:
                    res = [x for x in read_group_all_states if x['state'] == state_value]
                res[0]['state'] = state_value
                if res[0]['state'][0] == 'done' or res[0]['state'][0] == 'cancel':
                    res[0]['__fold'] = True
                result.append(res[0])
            return result
        else:
            return super(HrDemotions, self).read_group(domain, fields, groupby, offset=offset, limit=limit, orderby=orderby)

    @api.multi
    def add_follower(self, employee_id):
        employee = self.env['hr.employee'].browse(employee_id)
        if employee.user_id:
            self.message_subscribe(employee.user_id.ids)

    @api.multi
    def button_done_demotions(self):
        history_pool = self.env['demotion.history']
        for record in self:
            payslips = self.env['hr.payslip'].search([('employee_id','=', record.employee_id.id)], order="date_to desc, id desc", limit=1)
            if payslips:
                for payslip in payslips:
                    val =[(0, 0, {'employee_id': record.employee_id.id,
                             'date_demotion': record.date_demotion,
                             'job_position':  record.employee_id.job_id.name,
                             'department': record.employee_id.department_id.name,
                             'coach': record.employee_id.coach_id.name,
                             'job_level': record.employee_id.job_level_id.name,
                             'payslip_id': payslip.id})]
                    _logger.info("WIth Payslip")
                    _logger.info(val)
            else:
                val =[(0, 0, {'employee_id': record.employee_id.id,
                       'date_demotion': record.date_demotion,
                       'job_position': record.employee_id.job_id.name,
                       'department': record.employee_id.department_id.name,
                       'coach': record.employee_id.coach_id.name,
                       'job_level': record.employee_id.job_level_id.name,
                       'payslip_id': ''})]
                _logger.info("WIthout Payslip")
                _logger.info(val)

            history_pro = {'demotion_history_ids': val}

            record.employee_id.write(history_pro)
            # New Fields Value Set in Employee
            if record.new_department_id:
                record.employee_id.department_id = record.new_department_id
            if record.new_job_id:
                record.employee_id.job_id = record.new_job_id
            if record.new_coach_id:
                record.employee_id.coach_id = record.new_coach_id
            if record.new_level_id:
                record.employee_id.job_level_id = record.new_level_id

            body = (_("%s has been demoted to %s.") % (record.employee_id.name, record.employee_id.job_id.name))

            record.message_post(body=body, message_type='comment',
                    subtype='mail.mt_comment', author_id=self.env.user.partner_id.id, date=datetime.today(), partner_ids=record.message_follower_ids)

            record.write({'state':'done'})

    @api.multi
    def button_cancel_demotions(self):
        for record in self:
            record.write({'state':'cancel'})


class Employee(models.Model):
    _inherit = 'hr.employee'

    demotion_history_ids = fields.One2many('demotion.history', 'employee_id', 'Demotion History')

class DemotionHistory(models.Model):
    _name = 'demotion.history'
    _description = 'Demotion History'

    employee_id = fields.Many2one('hr.employee', 'Employee')
    date_demotion = fields.Date('Demotion Date')
    job_position = fields.Char('Job Position')
    department = fields.Char('Department')
    coach = fields.Char('Coach')
    job_level = fields.Char('Level')
    payslip_id = fields.Many2one('hr.payslip','Last Payslip')
