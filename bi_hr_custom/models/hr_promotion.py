# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.
import logging
from datetime import datetime
from odoo.exceptions import Warning
from odoo import models, fields, exceptions, api, _

_logger = logging.getLogger(__name__)

class HrPromotion(models.Model):
    _inherit = 'hr.promotion' 

    masa_kerja = fields.Char(
        string="Masa Kerja",
        readonly=True       
    )

    @api.multi
    def _attachment_count(self):
        attachment_obj = self.env['ir.attachment']
        for record in self:
            record.attachment_count = attachment_obj.search_count([('res_model', '=', 'hr.promotion'),
                                                                    ('res_id', '=', record.id)])    

    attachment_count = fields.Integer(compute='_attachment_count', string='Attachments')

    @api.multi
    def open_attachment(self):
        xml_id = 'mail.view_document_file_kanban'
        kanban_view_id = self.env.ref(xml_id).id
        xml_id = 'base.view_attachment_tree'
        tree_view_id = self.env.ref(xml_id).id
        xml_id = 'base.view_attachment_form'
        form_view_id = self.env.ref(xml_id).id
        return {
            'name': _('Attachments'),
            'view_type': 'form',
            'view_mode': 'kanban,tree,form',
            'views': [(kanban_view_id, 'kanban'),(tree_view_id, 'tree'), (form_view_id, 'form')],
            'res_model': 'ir.attachment',
            'domain': [('res_id', 'in', self._ids),
                       ('res_model', '=', 'hr.promotion')],
            'type': 'ir.actions.act_window',
        }


    @api.model
    def create(self, values):
        employee = self.env['hr.employee'].search([('id','=',values['employee_id'])])
        employee.write({
            'job_id':values['new_job_id'],
            'job_level_id':values['new_level_id'],
            'department_id':values['new_department_id'],
            'location':values['new_location'],
            'coach_id':values['new_coach_id'],
        })
        return super(HrPromotion, self).create(values)


    status = fields.Char(string='Status', compute='_get_external')

    employee_status = fields.Char('Employee Status')
    joint_date = fields.Date('Joint Date')
    year_of_service = fields.Char('Year Of Service')
    job_id = fields.Char(string='Position',compute='_get_external',readonly=True)
    # department_id = fields.Many2one('hr.department', string='Department')
    department_id = fields.Char(string='Department',compute='_get_external',readonly=True)

    reason = fields.Many2one(
        comodel_name = 'hr.reason',
        string = 'Reason'
    )



    effective_date = fields.Date(string='Effective Date')

    job_level_id = fields.Many2one('hr.employee.level', 'Level')
    job_level = fields.Char(string='Level',compute='_get_external')

    # coach_id = fields.Many2one('hr.employee', string='Coach')
    # coach_id = fields.Char(string="Coach", compute='_get_external')
    coach_id = fields.Many2one('hr.employee', 'Superior', compute='_get_external')


    @api.multi
    @api.depends('employee_id')
    def _get_external(self):
        for promotion in self:
            promotion.coach_id = promotion.employee_id.coach_id
            promotion.status = promotion.employee_id.status_id
            promotion.department_id = promotion.employee_id.department_id.name
            promotion.job_level = promotion.employee_id.job_level_id.name
            promotion.job_id = promotion.employee_id.job_id.name
            # set new status position
            promotion.new_job_id = promotion.employee_id.job_id
            promotion.new_level_id = promotion.employee_id.job_level_id
            promotion.new_department_id = promotion.employee_id.department_id
            promotion.new_location = promotion.employee_id.work_location
            promotion.new_coach_id = promotion.employee_id.coach_id



    # location = fields.Char('Location/Branch')
    location = fields.Char(string='Location',compute='_get_employee_location')

    @api.multi
    @api.depends('employee_id')
    def _get_employee_location(self):
        self.location = self.employee_id.work_location

    periode_jabatan_start_date = fields.Date('Periode Jabatan')
    periode_jabatan_end_date = fields.Date('Periode Jabatan End Date')
    current_salary = fields.Float('Salary/Gross')

    new_job_id = fields.Many2one('hr.job', string='Position')
    new_level_id = fields.Many2one('hr.employee.level', 'Level')
    new_location = fields.Char('Location/Branch')
    new_salary = fields.Float('Salary/Gross')
    adjustment_salary = fields.Float('Adjustment')

    estimated_promotion_date = fields.Date('Estimated Promotion Date')

    proposed_manager_id = fields.Many2one('res.partner','Manager')
    proposed_date = fields.Date('Date')
    proposed_note = fields.Text('Note')

    direct_manager_id = fields.Many2one('res.partner','Direct Manager')
    direct_manager_date = fields.Date('Date')
    direct_manager_note = fields.Text('Note')

    recruitment_manager_id = fields.Many2one('res.partner','Recruitment Manager')
    recruitment_manager_date = fields.Date('Date')
    recruitment_manager_note = fields.Text('Note')

    hrd_manager_id = fields.Many2one('res.partner','HRD Manager')
    hrd_manager_date = fields.Date('Date')
    hrd_manager_note = fields.Text('Note')

    approved_manager_id = fields.Many2one('res.partner','Sr.Mgr/Gm')
    approved_manager_date = fields.Date('Date')
    approved_manager_note = fields.Text('Note')

    cod_manager_id = fields.Many2one('res.partner','COD/BOD')
    cod_manager_date = fields.Date('Date')
    cod_manager_note = fields.Text('Note')


    @api.onchange('employee_id')
    def onchange_employee(self):
        for line in self:
            if line.employee_id:
                contract_id = self.env['hr.contract'].search([('employee_id','=',line.employee_id.id)], limit=1)
                for contract in contract_id:
                    line.hr_contract_id = contract.id
                    line.current_salary = contract.wage
                line.department_id = line.employee_id.department_id.id
                line.job_id = line.employee_id.job_id
                line.coach_id = line.employee_id.coach_id
                line.job_level_id = line.employee_id.job_level_id
                if line.employee_id.joining_date == False:
                    line.masa_kerja = 'Join date di employee belum di isi'
                else:
                    from datetime import datetime
                    join_date = datetime.strptime(line.employee_id.joining_date, '%Y-%m-%d')
                    tanggal_now = datetime.today()

                    from dateutil.relativedelta import relativedelta
                    jumlah_tahun = relativedelta(tanggal_now, join_date).years
                    jumlah_bulan = relativedelta(tanggal_now, join_date).months
                    jumlah_hari = relativedelta(tanggal_now, join_date).days

                    str_masa_kerja = ''
                    if jumlah_tahun > 0:
                        str_masa_kerja = str_masa_kerja +str(jumlah_tahun)+ ' Tahun '
                    if jumlah_bulan > 0:
                        str_masa_kerja = str_masa_kerja + str(jumlah_bulan) + ' Bulan '
                    if jumlah_hari > 0:
                        str_masa_kerja = str_masa_kerja + str(jumlah_hari) + ' Hari'                        
                    line.masa_kerja = str_masa_kerja                                       


    @api.multi
    def button_done_promotions(self):
        history_pool = self.env['promotion.history']
        payslips = self.env['hr.payslip'].search([('employee_id','=', self.employee_id.id)], order="date_to desc, id desc", limit=1)
        if payslips:
            for payslip in payslips:
                val =[(0, 0, {'employee_id': self.employee_id.id,
                         'date_promotion': self.date_promotion,
                         'job_position':  self.employee_id.job_id.name,
                         'department': self.employee_id.department_id.name,
                         'coach': self.employee_id.coach_id.name,
                         'job_level': self.employee_id.job_level_id.name,
                         'payslip_id': payslip.id})]
                _logger.info("WIth Payslip")
                _logger.info(val)
        else:
            val =[(0, 0, {'employee_id': self.employee_id.id,
                   'date_promotion': self.date_promotion,
                   'job_position': self.employee_id.job_id.name,
                   'department': self.employee_id.department_id.name,
                   'coach': self.employee_id.coach_id.name,
                   'job_level': self.employee_id.job_level_id.name,
                   'payslip_id': ''})]
            _logger.info("WIthout Payslip")
            _logger.info(val)

        history_pro = {'promotion_history_id': val}

        self.employee_id.write(history_pro)
        # New Fields Value Set in Employee
        if self.new_department_id:
            self.employee_id.department_id = self.new_department_id
        if self.new_job_id:
            self.employee_id.job_id = self.new_job_id
        if self.new_coach_id:
            self.employee_id.coach_id = self.new_coach_id
        if self.new_level_id:
            self.employee_id.job_level_id = self.new_level_id

        body = (_("%s has been promoted to %s.") % (self.employee_id.name, self.employee_id.job_id.name))

        self.message_post(body=body, message_type='comment',
                subtype='mail.mt_comment', author_id=self.env.user.partner_id.id, date=datetime.today(), partner_ids=self.message_follower_ids)

        self.write({'state':'done'})

class PromotionHistory(models.Model):
    _inherit = 'promotion.history'

    job_level = fields.Char('Level')


