# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo.exceptions import Warning
from odoo import models, fields, exceptions, api, _

class HrResignation(models.Model):
	_inherit = 'hr.resignation'

	attachment_resign_letter_id = fields.Many2one('ir.attachment',string="Attachment Resign Letter")
	reason = fields.Many2one('resignation.reason', string='Reason', required=True)
	note = fields.Text('Note')

class ResignationReason(models.Model):
	_name = 'resignation.reason'

	name = fields.Char('Reason', required=True)
