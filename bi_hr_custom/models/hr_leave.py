# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo.exceptions import Warning
from odoo import models, fields, exceptions, api, _

class HrLeave(models.Model):
	_inherit = 'hr.holidays'

	nik = fields.Char('NIK')
	start_working = fields.Float('Start Working',default=0)
	last_remaining_leaves = fields.Char('Remaining Leaves - Last Year')
	remaining_leaves = fields.Char('Remaining Leaves - This Year')
	used_leaves = fields.Char('Used Leaves')
	total_remaining_leaves = fields.Char('Total Remaining Leaves')
	contactable_address = fields.Text('Contactable Address')
	work_location = fields.Many2one('project.project')


	city = fields.Many2one('res.country.city')

	keperluan = fields.Char('Keperluan')

	expense_line = fields.One2many('hr.leave.expenses', 'leave_id', 'Expense Lines')


	@api.onchange('employee_id')
	def onchange_employee_id(self):
		self.work_location = self.employee_id.project_id
		self.city = self.employee_id.city

	@api.model
	def tambah_jatah_cuti(self):

		for employee in  self.env['hr.employee'].search([]):
			id_employee = employee.id
			id_user = self.env.user.id
			import datetime
			now = datetime.datetime.now()
			self.env.cr.execute("select count(*) from hr_holidays where employee_id = "+str(id_employee)+" and date_part('year',date_from) = "+ str(now.year)+" and date_part('month',date_from) = " + str(now.month))
			hasil = self.env.cr.fetchone()[0]
			if hasil < 1:
				sql_script = "insert into hr_holidays (user_id,employee_id,holiday_status_id,name,number_of_days,number_of_days_temp,type,state,holiday_type,date_from,date_to) values ("+str(id_user)+","+str(id_employee)+",1,'Cuti Otomatis',1,1,'add','validate','employee',now(),now()) "
				self._cr.execute(sql_script)
				self._cr.commit()

	@api.model
	def tambah_cuti_project(self):
		qq=0
		"""
		- loop per employee transfer
		- setiap employee transfer, ambil status bonus holiday nya $status_bonus_bulanan
		- ambil beda jeda jumlah hari dari start date employee transfer, $jeda_hari
		- jika $status_bonus_bulanan = 1 bulan , $jeda_hari lebih dari 1 bulan, exsekusi tambah 1 hari
		- jika $status_bonus_bulanan = 3 bulan , $jeda_hari lebih dari 1 bulan, exsekusi tambah 3 hari
		"""
		e_transfer = self.env['employee.transfer'].search([])
		from datetime import datetime
		now = datetime.now()
		fmt = '%Y-%m-%d'
		for i in e_transfer:
			start = datetime.strptime(i.date_start, fmt)
			days_diff = (now-start).days

			status_cuti = i.new_work_location.cuti_per_bulan
			if (status_cuti and days_diff > 30 ): # jika true dan sudah lewat dari 1 bulan, maka perbulan tambah 1
				leave = self.env['hr.holidays'].create({
					'name': 'Tambah cuti per 1 bulan',
					'employee_id': i.employee_id.id,
					'holiday_status_id': 1,
					'type': 'add',
					'holiday_type': 'employee',
					'number_of_days_temp': 1
				})
			if (status_cuti == False and days_diff > 90 ): # jika false dan lewat dari 3 bulan
				leave = self.env['hr.holidays'].create({
					'name': 'Tambah cuti per 3 bulan',
					'employee_id': i.employee_id.id,
					'holiday_status_id': 1,
					'type': 'add',
					'holiday_type': 'employee',
					'number_of_days_temp': 3
				})






