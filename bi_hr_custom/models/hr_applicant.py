# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo.exceptions import Warning
from odoo import models, fields, exceptions, api, _

class JenisId(models.Model):
    _name = 'jenis.id'

    name = fields.Char('Jenis ID',required=True)

class ApplicantId(models.Model):
    _name = 'applicant.id'

    hr_applicant_id = fields.Many2one('hr.applicant', 'Applicant')
    jenis_id = fields.Many2one('jenis.id')

    masa_berlaku_start = fields.Date('Masa Beriaku Start')
    masa_berlaku_end = fields.Date('Masa Beriaku End')

    employee_id = fields.Many2one('hr.employee', 'Employee')
    file_data = fields.Binary('File')
    file_name = fields.Char('File Name')	

class EmergencyContact(models.Model):
    _name = 'emergency.contact'

    eme_contact_id = fields.Many2one('hr.applicant', 'Applicant')
    name = fields.Char('Nama')
    alamat = fields.Char('Alamat')	
    handphone = fields.Char('No. handphone')
    hubungan = fields.Char('Hubungan')

class HrApplicant(models.Model):
    _inherit = 'hr.applicant'

    @api.multi
    def reset_applicant(self):

        self.staging_applicant = 'initial_qualification'

    staging_applicant = fields.Selection([
        ('initial_qualification','Initial Qualification'),
        ('first_interview','Test'),
        ('interview','First Interview'),
        ('second_interview','Second Interview'),
        ('contract_proposal','Contract Proposal'),
        ('contract_signed','Approved By Direksi'),
        ('employee_created','Employees'),
        ('refused','Refused')
    ],required=True,readonly=True ,default='initial_qualification', group_expand='_expand_states')

    def _expand_states(self, states, domain, order):
        return [key for key, val in type(self).staging_applicant.selection]	

    @api.multi
    def refuse_applicant(self):
        for i in self:
            i.staging_applicant = 'refused'

    @api.multi
    def reset_applicant_new(self):
        for i in self:
            i.staging_applicant = 'initial_qualification'

    @api.multi
    def first_interview(self):
        for i in self:
            i.staging_applicant = 'first_interview'

    @api.multi
    def interview(self):
        for i in self:
            i.staging_applicant = 'interview'

    @api.multi
    def second_interview(self):
        for i in self:
            i.staging_applicant = 'second_interview'

    @api.multi
    def contract_proposal(self):
        for i in self:
            i.staging_applicant = 'contract_proposal'

    @api.multi
    def contract_signed(self):
        for i in self:
            i.staging_applicant = 'contract_signed'		

    # _name = 'master.placeofbirth'
    tempat_lahir = fields.Many2one('master.placeofbirth','Place Of Birth')

    passing_year_start = fields.Char('Year Start')
    passing_year_end = fields.Char('Year End')	
    
    npwp = fields.Char(string="NPWP")
    country_id = fields.Many2one(
        'res.country', 'Nationality (Country)', groups="hr.group_hr_user")
    identification_id = fields.Char(string='Identification No', groups="hr.group_hr_user")
    passport_id = fields.Char('Passport No', groups="hr.group_hr_user")

    bank_account_number = fields.Char(string='Bank Account Number', groups="hr.group_hr_user",help='Employee bank salary account')
    bank_account_name = fields.Char('Bank Account Name')
    nama_bank = fields.Many2one('res.bank','Bank Name')		

    #ANIF
    other_bank_account_number = fields.Char(string='Other Bank Account Number', groups="hr.group_hr_user",help='Employee bank salary account')
    other_bank_account_name = fields.Char('Other Bank Account Name')
    other_bank_name = fields.Many2one('res.bank', 'Other Bank Name')

    birthday = fields.Date('Date of Birth', groups="hr.group_hr_user")
    place_of_birth = fields.Char('Place of Birth', groups="hr.group_hr_user")
    country_of_birth = fields.Many2one('res.country', string="Country of Birth", groups="hr.group_hr_user")
    gender = fields.Selection([
        ('male', 'Male'),
        ('female', 'Female'),
    ], groups="hr.group_hr_user", string='Gender', default='')
    marital_status = fields.Selection([
                ('belum_menikah', 'Belum Menikah'),
                ('menikah', 'Menikah'),
                ('janda', 'Janda'),
                ('duda', 'Duda'),
    ], string='Marital Status', groups="hr.group_hr_user", default='')
    children = fields.Char(string='Number of Children', groups="hr.group_hr_user")
    spouse_complete_name = fields.Char(string="Spouse Complete Name", groups="hr.group_hr_user")
    spouse_birthdate = fields.Date(string="Spouse Birthdate", groups="hr.group_hr_user")
    religion_id = fields.Selection([
                                    ('islam','Islam'),
                                    ('kristen','Kristen'),
                                    ('khatolik','Khatolik'),
                                    ('budha','Budha'),
                                    ('hindu','Hindu'),
                                    ('konghucu','Konghucu'),
                                    ('lain','Lain lain')],'Religion')
    tinggi_badan = fields.Float('Tinggi Badan')
    berat_badan = fields.Float('Berat Badan')
    golongan_darah = fields.Selection([('a','A'),
                                      ('b','B'),
                                      ('ab','AB'),
                                      ('o','O')],'Golongan Darah', default='')
    alamat_domosili = fields.Text('Alamat Domisili')
    alamat_asai = fields.Text("Alamat Asal – Sesuai KTP")
    kepemillkan_rumah = fields.Selection([
        ('milik_sendiri', 'Milik Sendiri'),
        ('milik_orang_tua', 'Milik Orang Tua'),
        ('sewa', 'Sewa/Kontrak/Kost'),
        ('lain','Lain')
    ], string="Kepemilikan Rumah", default="milik_sendiri")
    email = fields.Char('Email')
    telepon_rumah = fields.Char('Telepon Rumah')
    handphone = fields.Char('No. handphone')
    facebook = fields.Char('Facebook')
    instagram = fields.Char('Instagram')
    emp_applicant_experience_ids = fields.One2many('hr.applicant.experience','applicant_experience_id','Experience')
    applicant_skill_ids = fields.One2many('hr.skills','skill_applicant_id','Applicant Skill')
    applicant_language_ids = fields.One2many('hr.language','lan_applicant_id','Applicant Language')
    applicant_training_ids = fields.One2many('hr.training','training_applicant_id','Applicant Training')
    applicant_reference_ids = fields.One2many('hr.reference','reference_applicant_id','Applicant Reference')
    applicant_ids = fields.One2many('applicant.id','hr_applicant_id','Applicant Id')
    emergency_contact_ids = fields.One2many('emergency.contact','eme_contact_id','Emergency Contact')

    @api.multi
    def create_employee_from_applicant(self):
        result = super(HrApplicant,self).create_employee_from_applicant()
        self.staging_applicant = 'employee_created'

        # create training dari applicant     self.applicant_training_ids
        employee_id = self.emp_id.id
        company_id = self.emp_id.company_id.id
        if self.emp_id:
            self.emp_id.write({
                'name': self.partner_name or self.name,
                'joining_date': self.create_date
            })

        for training in self.applicant_training_ids:  # training name , instansi tanggal
            qq=0
            self.env['training.request'].create({
                'employee_id': employee_id,
                'training_name': training.training_name,
                'instansi': training.instansi,
                'tanggal_training': training.tanggal_pelatihan,
            })
        for applicant in self:
            # set emp_applicant_experience_ids employee_id
            empployee_id = applicant.emp_id.id
            # set employee id ke hr experience bila ada
            if len(applicant.emp_applicant_experience_ids.ids) > 0:
                for exp in applicant.emp_applicant_experience_ids:
                    exp.employee_id = empployee_id

            vals = {
                'country_id': applicant.country_id.id or '',
                'department_id': applicant.department_id.id or '',
                'identification_id': applicant.identification_id or '',
                'passport_id': applicant.passport_id or '',

                'bank_account_number': applicant.bank_account_number or '',
                'bank_name':applicant.bank_account_name,
                'bank_id':applicant.nama_bank,

                'other_bank_account_number': applicant.other_bank_account_number or '',
                'other_bank_name':applicant.other_bank_account_name,
                'other_bank_id':applicant.other_bank_name,				

                'birthday': applicant.birthday,
                'place_of_birth': applicant.place_of_birth,
                'country_of_birth': applicant.country_of_birth,
                'religion_id': applicant.religion_id,
                'hr_height': applicant.tinggi_badan,
                'hr_weight': applicant.berat_badan,
                'hr_blood_type': applicant.golongan_darah,
                'gender': applicant.gender,
                'marital_status': applicant.marital_status,
                'children': applicant.children,
                'spouse_complete_name': applicant.spouse_complete_name,
                'spouse_birthdate': applicant.spouse_birthdate,
                'hr_email': applicant.email,
                'hr_phone': applicant.telepon_rumah,
                'hr_mobile': applicant.handphone,
                'hr_facebook': applicant.facebook,
                'hr_instagram': applicant.instagram,
                'job_title': applicant.job_id.name,
                'address_home': applicant.alamat_domosili,
                'id_card_address': applicant.alamat_asai,
                'npwp': applicant.npwp,
                'place_of_birth':applicant.tempat_lahir.name
                
            }


            for i in applicant.emergency_contact_ids:
                self.env['hr.emergency.contact'].create({
                    'name': i.name,
                    'address': i.alamat,
                    'number': i.handphone,
                    'relation': i.hubungan,
                    'employee_id': applicant.emp_id.id
                })
            for i in applicant.applicant_skill_ids:
                self.env['hr.skills'].create({
                    'skill_type': i.skill_type,
                    'skill_grade': i.skill_grade,
                    'skill_employee_id':applicant.emp_id.id,
                })
            for i in applicant.applicant_language_ids:
                self.env['hr.language'].create({
                    'language': i.language,
                    'lan_grade':i.lan_grade,
                    'lan_employee_id':applicant.emp_id.id,
                })




            """
            for i in applicant.applicant_training_ids:
                    self.env['hr.training'].create({
                        'training_name': i.training_name,
                        'training_organizer':i.training_organizer,
                        'training_year': i.training_year,
                        'training_cerificate': i.training_cerificate,
                        'training_employee_id':applicant.emp_id.id,
                    })
            """





            for i in applicant.applicant_reference_ids:
                self.env['hr.reference'].create({
                    'reference_name': i.reference_name,
                    'reference_mobile':i.reference_mobile,
                    'reference_relation': i.reference_relation,
                    'reference_position': i.reference_position,
                    'reference_employee_id':applicant.emp_id.id,
                })
            
            for i in applicant.applicant_ids:
                i.employee_id = empployee_id

            if len(applicant.applicant_education_ids.ids) > 0:
                for edu in applicant.applicant_education_ids:
                    edu.employee_id = empployee_id

            applicant_employeement = self.env['applicant.employeement'].search([('employee_id','=', applicant.emp_id.id)])
            if applicant_employeement:
                applicant_employeement.unlink()
            for i in applicant.applicant_employeement_ids:
                self.env['applicant.employeement'].create({
                    'organization_id': i.organization_id.id,
                    'start_date':i.start_date,
                    'end_date':i.end_date,
                    'role':i.role,
                    'supervisor':i.supervisor,
                    'employee_id':applicant.emp_id.id,
                })

            if applicant.applicant_family_ids:
                for family in applicant.applicant_family_ids:
                    family.employee_id = empployee_id


            applicant.emp_id.update(vals)
        return result

class MasterBank(models.Model):
    _name = 'master.bank'
    name = fields.Char('Bank Name', required=True)


class MasterBusiness(models.Model):    
    _name = 'master.business'
    name = fields.Char(string='Type Of Business', required=True)


class MasterMajor(models.Model):    
    _name = 'master.major'
    name = fields.Char(string='Major Subjects', required=True)
























