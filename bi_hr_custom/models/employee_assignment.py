# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo.exceptions import Warning
from odoo import models, fields, exceptions, api, _
from odoo.exceptions import UserError


class EmployeeAssignment(models.Model):
    _name = 'employee.assignment' 
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _order = 'date_assignment desc, id desc'
    _description = 'Assignment Letter of employee'
    
    @api.depends('state', 'advance_ids.state', 'voucher_ids.state')
    def _get_advanced(self):
        for rec in self:
            rec.advance_count = len(rec.advance_ids)
            rec.voucher_count = len(rec.voucher_ids)
            
    name = fields.Char(string="Assignment Letters", readonly=True, required=True, copy=False, default='New')
    date_assignment = fields.Date('Assignment date', default=fields.Date.today)

    employee_id = fields.Many2one('hr.employee', string="Employee" , required=True)
    
    nik = fields.Char("NIK")

    @api.onchange('employee_id')
    def onchange_employee(self):  
        for emp in self:
            emp.nik = emp.employee_id.barcode
            emp.job_id = emp.employee_id.job_id.id
            
    city_src_id = fields.Many2one('res.country.city', 'From')
    city_dest_ids = fields.Many2many('res.country.city', string='Destinations')

    job_id = fields.Many2one('hr.job', 'Job Position')
    user_id = fields.Many2one('res.users', 'Responsible', default=lambda self:self.env.user)
    company_id = fields.Many2one('res.company', 'Company', default=lambda self:self.env.user.company_id)
    note = fields.Text('Note')
    voucher_line = fields.One2many('employee.voucher.line', 'assignment_id', string="Assignment Reference")
    advance_amount = fields.Float('Cash Advance')
    
    advance_ids = fields.One2many('account.payment', 'payment_id', string='Advances', copy=False)
    advance_count = fields.Integer(compute='_get_advanced', string='Advance', default=0)
    voucher_ids = fields.One2many('account.voucher', 'voucher_id', string='Settlements', copy=False)
    voucher_count = fields.Integer(compute='_get_advanced', string='Settlements', default=0)

    state = fields.Selection([
        ('draft', 'Draft'),
        ('to_approve_manager', 'Waiting Manager Approval'),
        ('to_approve_director', 'Waiting Director Approval'),
        ('approved', 'Approved'),
        ('submit_advance', 'Submitted Advance'),
        ('to_settlement', 'Waiting Reimburse'),
        ('submit_settlement', 'Submitted Reimburse'),
        ('cancel', 'Cancel'),
        ('done', 'Done'),
    ], string="Status", index=True, copy=False, default='draft', track_visibility='onchange')
    
    @api.model
    def create(self, vals):
        if vals.get('name', 'New') == 'New':
            vals['name'] = self.env['ir.sequence'].next_by_code('employee.assignment') or '/'
        return super(EmployeeAssignment, self).create(vals)
    
    @api.multi
    def button_confirm(self):
        self.ensure_one()
        self.write({'state':'to_approve_manager'})

    @api.multi
    def button_approve_manager(self):
        self.ensure_one()
        self.write({'state':'to_approve_director'})

    @api.multi
    def button_approve_director(self):
        self.ensure_one()
        self.write({'state':'approved'}) 
    
    @api.multi
    def button_no_advance(self):
        self.ensure_one()
        if self.advance_amount > 0:
            raise UserError(_("Click button Submit Advance to continue or cancel & set cash advance to 0 !"))
        self.write({'state':'to_settlement'})

    @api.multi
    def button_advance_create(self):
        self.ensure_one()
        if self.advance_amount <= 0:
            raise UserError(_("Amount cash advance must greater than 0 ! \n Cancel & set cash amount or continue without advance !"))
        self.write({'state':'submit_advance'})
        journal_id = self.get_default_journal()
        payment_method_id = journal_id[0].outbound_payment_method_ids and journal_id[0].outbound_payment_method_ids[0].id
        partner = self.employee_id.user_id.partner_id or self.employee_id.address_id
        data = {
            'payment_type': 'outbound',
            'partner_type': 'employee',
            'partner_id': partner and partner.id or self.user_id.partner_id.id,
            'advance_type': 'advance',
            'payment_id': self.id,
            'journal_id': journal_id[0] and journal_id[0].id or False,
            'payment_date': fields.Date.today(),
            'payment_method_id': payment_method_id,
            'communication': 'Advance for ' + self.name,
            'amount': self.advance_amount}
        self.env['account.payment'].create(data)
        
    @api.multi
    def button_settlement_create(self):
        self.ensure_one()
        self.write({'state':'submit_settlement'})
        if not self.voucher_line:            
            raise UserError(_('Please input your expense !'))
        if any(amount <= 0 for amount in self.voucher_line.mapped('amount')):
            raise UserError(_("Amount for reimburse must greather than 0"))
        lines = []
        advances = []
        partner = self.employee_id.user_id.partner_id or self.employee_id.address_id
        part = partner or self.user_id.partner_id
        fpos = part.property_account_position_id
        journal_id = self.with_context({'settlement': True}).get_default_journal()
        for line in self.voucher_line:
            account = self._get_account(line.product_id, fpos, type=None)
            vals = (0, 0, {'product_id': line.product_id.id,
                           'name': line.product_id.display_name,
                           'account_id': account.id,
                           'quantity': 1,
                           'price_unit': line.amount})
            lines.append(vals)
        for adv in self.advance_ids:
            vals = (0, 0, {'name': adv.id,
                           'amount': adv.amount,
                           'tax_id': adv.tax_id.id,
                           'amount_total': adv.amount })
            advances.append(vals)
        data = {'partner_id': part.id,
                'voucher_id': self.id,
                'payment_type': 'cash',
                'voucher_type': 'purchase',
                'journal_id': journal_id[0] and journal_id[0].id or False,
                'date': fields.Date.today(),
                'name': 'Reimburse for ' + self.name, 'line_ids': lines, 'advance_ids': advances}
        self.env['account.voucher'].create(data)
    
    def _get_account(self, product, fpos, type):
        accounts = product.product_tmpl_id.get_product_accounts(fpos)
        if type == 'sale':
            return accounts['income']
        return accounts['expense']
    
    @api.one
    def get_default_journal(self):
        if self._context.get('settlement'):
            journal_id = self.env['account.journal'].search([('company_id', '=', self.company_id.id), ('type', '=', 'purchase')], limit=1)
        else:
            journal_id = self.env['account.journal'].search([('company_id', '=', self.company_id.id), ('type', '=', 'cash')], limit=1)
        return journal_id
        
    @api.multi
    def button_cancel(self):
        self.ensure_one()
        self.write({'state':'cancel'})

    @api.multi
    def button_draft(self):
        self.ensure_one()
        self.write({'state':'draft'})
    
    @api.multi
    def action_advance_paid(self):
        for rec in self:
            rec.write({'state': 'to_settlement'})
            
    @api.multi
    def action_voucher_paid(self):
        for rec in self:
            rec.write({'state': 'done'})

    @api.multi
    def unlink(self):
        for rec in self:
            if rec.state != 'draft': 
                raise UserError(_('Only draft document can be deleted !'))
        return super(EmployeeAssignment, self).unlink()
    
    @api.multi
    def action_view_advance(self):
        action = self.env.ref('account.view_account_supplier_payment_tree')
        result = action.read()[0]
        imd = self.env['ir.model.data']
        list_view_id = imd.xmlid_to_res_id('account.view_account_supplier_payment_tree')
        form_view_id = imd.xmlid_to_res_id('account.view_account_payment_form')
        result['domain'] = "[('id', 'in', " + str(self.advance_ids.ids) + ")]"
        return {
            'name': 'Advances',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'views': [[list_view_id, 'tree'], [form_view_id, 'form']],
            'domain' : result.get('domain'),
            'res_model': 'account.payment',
            'type': 'ir.actions.act_window',
        }
    
    @api.multi
    def action_view_settlement(self):
        action = self.env.ref('account_payment_request.view_voucher_tree')
        result = action.read()[0]
        imd = self.env['ir.model.data']
        list_view_id = imd.xmlid_to_res_id('account_payment_request.view_voucher_tree')
        form_view_id = imd.xmlid_to_res_id('account_payment_request.view_purchase_receipt_form')
        result['domain'] = "[('id', 'in', " + str(self.voucher_ids.ids) + ")]"
        return {
            'name': 'Reimburse',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'views': [[list_view_id, 'tree'], [form_view_id, 'form']],
            'domain' : result.get('domain'),
            'res_model': 'account.voucher',
            'type': 'ir.actions.act_window',
        }


class AssignmentVoucherLine(models.Model):
    _name = 'employee.voucher.line' 
    
    assignment_id = fields.Many2one('employee.assignment', 'Assignment Reference', readonly=True, ondelete='cascade')
    product_id = fields.Many2one('product.product', 'Product')
    name = fields.Char('Description')
    amount = fields.Float('Amount')
    
    @api.onchange('product_id')
    def change_product_id(self):
        if not self.product_id:
            return
        self.name = self.product_id.display_name

