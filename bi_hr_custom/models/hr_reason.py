from odoo import models, fields, exceptions, api, _

class HrReason(models.Model):
    _name = 'hr.reason'
    name = fields.Char(string='Reason', required=True)