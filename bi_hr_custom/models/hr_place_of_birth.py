from odoo import models, fields, exceptions, api, _

class PlaceOfBirth(models.Model):    
    _name = 'master.placeofbirth'
    name = fields.Char(string='Place Of Birth', required=True)