# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

import logging
from datetime import datetime
from odoo.exceptions import Warning, ValidationError, UserError
from odoo import models, fields, exceptions, api, _

_logger = logging.getLogger(__name__)


class MedicalRequest(models.Model):
    _name = 'medical.request'
    _inherit = ['medical.request', 'mail.thread', 'mail.activity.mixin']

    @api.model
    def create(self, vals):
        vals['name'] = self.env['ir.sequence'].next_by_code('medical.request') or _('New')
        res = super(MedicalRequest, self).create(vals)
        return res

    @api.multi
    def _attachment_count(self):
        attachment_obj = self.env['ir.attachment']
        for record in self:
            record.attachment_count = attachment_obj.search_count([('res_model', '=', 'medical.request'),
                                                                    ('res_id', '=', record.id)])

    employee_id = fields.Many2one('hr.employee', string="Employee", required=True, default=lambda self: self.env['hr.employee'].search([('user_id', '=', self.env.uid)], limit=1))
    bill_reference = fields.Char("Bill Reference")
    bill_date = fields.Date('Date')
    description = fields.Text("Description")
    notes = fields.Text("Notes")
    nik = fields.Char(related='employee_id.barcode', string="NIK")
    state = fields.Selection([
            ('draft', 'To Submit'),
            ('hr', 'Submitted'),
            ('cfo', 'Approved'),
            ('paid', 'Paid'),
            ('refund', 'Refund')], default='draft', string='State')
    attachment_count = fields.Integer(compute='_attachment_count', string='Attachments')
    journal_id = fields.Many2one('account.journal', string='Payment Method', copy=False)
    move_id = fields.Many2one('account.move', string='Journal Entries', copy=False)
    refund_move_id = fields.Many2one('account.move', string='Refund Entry', copy=False)

    @api.multi
    def send_to_hr(self):
        for record in self:
            if record.total_amount > record.employee_credit:
                raise ValidationError(_("Sorry, you don't have enough credit to process this"))
            if not record.lines_id:
                raise ValidationError(_("Sorry, A Medical details does not exist."))
            record.state = 'hr'

    @api.multi
    def open_attachment(self):
        xml_id = 'mail.view_document_file_kanban'
        kanban_view_id = self.env.ref(xml_id).id
        xml_id = 'base.view_attachment_tree'
        tree_view_id = self.env.ref(xml_id).id
        xml_id = 'base.view_attachment_form'
        form_view_id = self.env.ref(xml_id).id
        return {
            'name': _('Attachments'),
            'view_type': 'form',
            'view_mode': 'kanban,tree,form',
            'views': [(kanban_view_id, 'kanban'), (tree_view_id, 'tree'), (form_view_id, 'form')],
            'res_model': 'ir.attachment',
            'domain': [('res_id', 'in', self._ids),
                       ('res_model', '=', 'medical.request')],
            'type': 'ir.actions.act_window',
        }

    @api.multi
    def paid(self):
        company_id = self.env.user.company_id
#         if not company_id.bank_acc:
#             raise UserError(_('You have not selected a Medical Limit Bank'))
#         if not company_id.medical_bill_account:
#             raise UserError(_('You have not selected a Medical Limit Bank'))

        acc_move_line = self.env['account.move.line'].with_context(check_move_validity=False)

        for record in self:
            acc_move = self.env['account.move'].create({
                'journal_id': record.journal_id.id,
                'date': record.bill_date,
                'ref': record.name,
            })
            acc_move_line.create({
                'type': 'src',
                'name': 'Medical Bills',
                'quantity': 1,
                'credit': record.total_amount,
                'debit': 0.0,
                'account_id': record.journal_id.default_credit_account_id.id,
                'move_id': acc_move.id,
                'partner_id': record.employee_id.user_id.id
    
            })
            for line in self.lines_id:
                acc_move_line.create({
                    'type': 'src',
                    'name': 'Medical Bills for %s' % (line.patient_name),
                    'quantity': 1,
                    'debit': line.amount,
                    'credit': 0.0,
                    'account_id': line.account_id.id,
                    'move_id': acc_move.id,
                    'partner_id': record.employee_id.user_id.id
                })
            acc_move.post()
            record.move_id = acc_move.id
            record.employee_id.write({'medical_limit': record.employee_credit - record.total_amount})
            record.state = 'paid'

    @api.multi
    def action_medical_request_refund(self):
        for record in self:
            if record.move_id:
                ref_id = record.move_id.reverse_moves(record.bill_date, record.journal_id or False)
                refund_id = self.env['account.move'].browse(ref_id)
                record.refund_move_id = refund_id.id
                record.state = 'refund'


class MedicalRequestLine(models.Model):
    _inherit = 'medical.details.line'

    bill_date = fields.Date('Bill Date')
    cost_code = fields.Selection([
                    ('D', 'Dokter / Dokter Gigi'),
                    ('O', 'Binya Obat & Laboratorium'),
                    ('H', 'Kehamilan'),
                    ('R', 'Rumah Sakit'),
                    ('B', 'Bingkai kacamata'),
                    ('L', 'Lensa kacamata'),
                    ('A', 'Anak'),
                    ('K', 'Karyawan'),
                    ('I', 'Istri'), ], default='', string='Cost Code')
    account_id = fields.Many2one('account.account', string='Account')
    patient_name = fields.Char("Patient's Name")

    relation_id = fields.Many2one(
        'applicant.relation',
        string='Relation',
        required=False,
    )
    state = fields.Selection(related='medical_id.state', string='state', default='draft')
    afi_adjustment = fields.Float('AFI Adjustment')
    clean_reimbursement = fields.Float('Clean Reimbursement')
