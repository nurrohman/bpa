from odoo import models, fields, api, _
import odoo.addons.decimal_precision as dp
from odoo.exceptions import except_orm, Warning, RedirectWarning, UserError, ValidationError
import datetime


class PenggantiCuti(models.Model):
    _name = 'pengganti.cuti'
    _description = 'Pengganti Cuti'
    
    @api.one
    @api.depends('employee_id')
    def _compute_date(self):
        if self.project_id.date_contract:
            self.date_realize = datetime.datetime.strptime(self.project_id.date_contract, "%Y-%m-%d") + datetime.timedelta(days=107)
    
    @api.one
    @api.depends('pph', 'expense_line.amount')
    def _compute_amount(self):
        self.payment_amount = self.jumlah + sum(line.amount for line in self.expense_line) - self.pph
        
    name = fields.Char(string="Name", readonly=True, required=True, copy=False, default='New')
    employee_id = fields.Many2one('hr.employee', string="Employee" , required=True)
    job_id = fields.Many2one('hr.job', 'Job Position')
    job_level_id = fields.Many2one('hr.employee.level', 'Job Level')
    project_id = fields.Many2one('project.project', string='Work Location')
    remaining_leaves = fields.Float('Remaining Leaves')
    bank_account_number = fields.Integer('Bank Account Number')
    bank_account_name = fields.Char('Bank Account Name')
    bank_name = fields.Many2one('res.bank', 'Bank Name')
    date_reg = fields.Date('Date', default=fields.Date.today())
    jumlah = fields.Float('Jumlah')
    pph = fields.Float('PPH')
    payment_amount = fields.Float('Payment Amount', compute='_compute_amount')
    payment_reference = fields.Char('Payment Reference')
    payslip_reference = fields.Many2one('hr.payslip', string='Payslip Reference')
    date_realize = fields.Date('Realization Date', compute='_compute_date')
    
    expense_line = fields.One2many('pengganti.cuti.expense', 'cuti_id', string='Expense Line')
    move_id = fields.Many2one('account.move', 'Journal Entry', copy=False)
    move_line_ids = fields.One2many('account.move.line', 'Journal Item', related='move_id.line_ids')

    state = fields.Selection([
        ('draft', 'Draft'),
        ('waiting_approval_hrd', 'Waiting Approval HRD'),
        ('waiting_approval_direksi', 'Waiting Approval Direksi'),
        ('approved', 'Approved'),
        ('paid', 'Paid'),
    ], string="Status", index=True, copy=False, default='draft', track_visibility='onchange')

    @api.multi
    def submit(self):
        self.ensure_one()
        self.write({'state':'waiting_approval_hrd'})

    @api.multi
    def approve_hrd(self):
        self.ensure_one()
        self.write({'state':'waiting_approval_direksi'})

    @api.multi
    def approve_direksi(self):
        self.ensure_one()
        self.write({'state':'approved'})

    @api.multi
    def pay(self):
        self.ensure_one()
        if not self.date_realize:
            raise ValidationError(_("Please fill Realization Date ! \n Realization Date is computation from Tanggal Kontrak Project %s plus 107 days !") % self.project_id.name)
        if fields.Date.today() < self.date_realize:
            raise ValidationError(_("Unable to Register Payment...\n You can pay only after Realized Date !"))
        return {
            'name': _('Register Payments'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'payment.leave',
            'target': 'new',
            'context' : {
                'default_cuti_id': self.id,
                'default_amount': self.payment_amount
            }            
        }

    @api.onchange('employee_id')
    def onchange_employee(self):
        for emp in self:
            if not emp.employee_id:
                return {}
            emp.job_id = emp.employee_id.job_id
            emp.job_level_id = emp.employee_id.job_level_id
            emp.project_id = emp.employee_id.project_id
            emp.remaining_leaves = emp.employee_id.remaining_leaves - emp.employee_id.pengurang_cuti
            emp.bank_account_number = emp.employee_id.other_bank_account_number
            emp.bank_account_name = emp.employee_id.other_bank_name
            emp.bank_name = emp.employee_id.other_bank_id.id
            # hitung jumlah
            """
            ambil job location ho atau ngga , dari project.project ( cuti_per_bulan ) / 21 * contract wage, dari contract
            """
            is_ho = emp.employee_id.project_id.cuti_per_bulan  # jika head office = true
            wage = self.env['hr.contract'].search([('employee_id', '=', self.employee_id.id)], limit=1).wage
            if is_ho == True:  # jika HO
                emp.jumlah = emp.remaining_leaves / 21 * wage
            else:
                emp.jumlah = emp.remaining_leaves / 25 * wage

            payslip_id = self.env['hr.payslip'].search([('employee_id', '=', emp.employee_id.id), ('state', 'not in', ('done', 'cancel'))], limit=1).id
            if not payslip_id:
                raise ValidationError(_("This employee doesn't have open Payslip Reference! \n Please create payslip first !"))
            emp.payslip_reference = payslip_id

    @api.model
    def create(self, vals):
        if not self.env['hr.employee'].browse(vals.get('employee_id')).contract_ids:
            raise ValidationError(_("This employee doesn't have contract!"))
        
        if 'payslip_reference' not in vals:
            raise ValidationError(_("This employee doesn't have open Payslip Reference!"))

        if vals.get('name', 'New') == 'New':
            vals['name'] = self.env['ir.sequence'].next_by_code('sequence.pengganti.cuti') or 'New'
        result = super(PenggantiCuti, self).create(vals)
        return result
        
    @api.multi
    def unlink(self):
        for rec in self:
            if rec.state not in ('draft', 'cancel'):
                raise UserError(_('Cannot delete this documents, which are already opened or paid.'))
        return super(PenggantiCuti, self).unlink()


class PenggantiCutiExpense(models.Model):
    _name = 'pengganti.cuti.expense'
    _description = 'Pengganti Cuti Expense'
    
    @api.one
    @api.depends('qty', 'price_unit')
    def _compute_amount(self):
        self.amount = self.qty * self.price_unit
    
    cuti_id = fields.Many2one('pengganti.cuti', 'Penggganti Cuti Ref')
    name = fields.Char('Description')
    product_id = fields.Many2one('product.product', 'Product')
    qty = fields.Float('Quantity')
    price_unit = fields.Float('Unit Price')
    amount = fields.Float('Amount', compute='_compute_amount')
    
    @api.onchange('product_id')
    def change_product_id(self):
        if not self.product_id:
            return {}
        self.name = self.product_id.name
        self.qty = 1
        self.price_unit = self.product_id.list_price
    
