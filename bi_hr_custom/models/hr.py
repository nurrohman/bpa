# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from datetime import datetime, date
from odoo.exceptions import Warning, UserError, ValidationError
from odoo import models, fields, exceptions, api, _


class HrEmployeeContract(models.Model):
    _inherit = 'hr.emergency.contact' 

    name = fields.Char('Name') 
    address = fields.Char('Address')  
    number = fields.Char(string='Mobile', help='Contact Number')
    relation = fields.Char(string='Relation', help='Relation with employee') 
    employee_id = fields.Many2one('hr.employee', 'employee')


class HRApplicantEducation(models.Model):
    _inherit = 'applicant.education'

    tempat_lahir = fields.Many2one('master.placeofbirth', 'Place Of Birth')
    city_education = fields.Many2one('res.country.city', 'Place Of Birth')

    grade = fields.Selection([
                ('sd', 'SD'),
				('smp', 'SMP'),
                ('sltp', 'SLTP'),
				('smu', 'SMU'),
				('slta', 'SLTA'),
                ('d1', 'D1'),
                ('d2', 'D2'),
                ('d3', 'D3'),
                ('d4', 'D4'),
                ('s1', 'S1'),
                ('s2', 'S2'),
                ('s3', 'S3')], 'Grade')
    city = fields.Char('City')
    passing_year_start = fields.Char('Year Start')
    passing_year_end = fields.Char('Year End')
    pass_no_state = fields.Selection([
                                ('pass', 'Pass'),
                                ('no', 'No')], 'Pass/No')
    ir_attachment_id = fields.Many2one('ir.attachment', 'Attachment')
    attachment_ids = fields.Many2many('ir.attachment', string='Attachments')


class HRApplicantExperience(models.Model):
    _name = 'hr.applicant.experience'

    employee_id = fields.Many2one('hr.employee', 'Employee')
    applicant_experience_id = fields.Many2one('hr.applicant', 'Applicant Experience')
    year_of_service = fields.Char('Year Of Service')
    company_name = fields.Char('Company')
    company_address = fields.Char('Company Address')
    type_of_business = fields.Many2one('master.business', 'Type Of Business')
    position = fields.Char('Position')
    reason = fields.Char('Reason')
    ir_attachment_id = fields.Many2one('ir.attachment', 'Attachment')
    attachment_ids = fields.Many2many('ir.attachment', string='Attachments')
    year_of_service_start = fields.Char('Year Of Service Start')
    year_of_service_end = fields.Char('Year Of Service End')    


class HRSkills(models.Model):
    _name = 'hr.skills'
    
    skill_applicant_id = fields.Many2one('hr.applicant', 'Applicant')
    skill_employee_id = fields.Many2one('hr.employee', 'Employee')
    skill_type = fields.Char('Skill Type')
    skill_grade = fields.Selection([
        ('sangat_baik', 'Sangat Baik'),
        ('baik', ' Baik'),
        ('cukup', 'Cukup'),
        ('kurang', 'Kurang'),
        ('sangat_kurang', 'Sangat Kurang')
    ], 'Grade')


class HRLanguage(models.Model):
    _name = 'hr.language'

    lan_applicant_id = fields.Many2one('hr.applicant', 'Applicant')
    lan_employee_id = fields.Many2one('hr.employee', 'Employee')
    language = fields.Char('Language')
    lang_type = fields.Selection([
        ('mendengar', 'Mendengar'),
        ('membaca', 'Membaca'),
        ('menulis', 'Menulis'),
    ])

    lan_grade = fields.Selection([
        ('sangat_baik', 'Sangat Baik'),
        ('baik', ' Baik'),
        ('cukup', 'Cukup'),
        ('kurang', 'Kurang'),
        ('sangat_kurang', 'Sangat Kurang')
    ], 'Grade')


class HRTraining(models.Model):
    _name = 'hr.training'

    training_applicant_id = fields.Many2one('hr.applicant', 'Applicant')
    training_employee_id = fields.Many2one('hr.employee', 'Employee')
    training_name = fields.Char('Name')
    training_organizer = fields.Char('Organizer')
    training_year = fields.Char('Year') 
    instansi = fields.Char('Instansi')
    training_cerificate = fields.Boolean('Certification')
    training_expired_date = fields.Date('Expired Date')
    file_name = fields.Char('File Name')
    tanggal_pelatihan = fields.Date('Tanggal Pelatihan')
    # masa_ikatan_dinas = fields.Float('Masa Ikatan Dinas')
    # kesimpulan = fields.Char('Kesimpulan')


class HRReference(models.Model):
    _name = 'hr.reference'

    reference_applicant_id = fields.Many2one('hr.applicant', 'Applicant')
    reference_employee_id = fields.Many2one('hr.employee', 'Employee')
    reference_name = fields.Char('Name')
    reference_mobile = fields.Char('Mobile')
    reference_relation = fields.Char('Relation') 
    reference_position = fields.Char('Position')


class HrApplicantFamily(models.Model):
    _inherit = 'applicant.family'

    # relation_id = fields.Many2one(
    #     'applicant.relation',
    #     string='Relation',
    #     required=False,
    # )

    relation_id = fields.Selection([
            ('suami', 'Husband'),
            ('istri', 'Wife'),
            ('ayah', 'Father'),
            ('ibu', 'Mother'),
            ('saudara', 'Sibling'),
            ('anak', 'Child'), ], 'Relation')

    # app_relation_id = fields.Selection([
    #         ('suami','Suami'),
    #         ('istri','Istri'),
    #         ('ayah','Ayah'),
    #         ('lbu','Lbu'),
    #         ('saudara','Saudara'),
    #         ('anak','Anak'),],'Relation')

    name = fields.Char('Name')
    gender = fields.Selection([
            ('laki', 'Laki'),
            ('perempuan', 'Perempuan')], 'Gender')
    place_of_birth = fields.Char('Place Of Birth')
    date_of_birth = fields.Date('Date Of Birth')
    mobile = fields.Char('Mobile')
    passed_away = fields.Boolean('Passed Away')


class HrEmployeeLevel(models.Model):
    _name = "hr.employee.level"

    name = fields.Char('Name')

    allowance = fields.Float('Allowance')
    # wage = fields.Monetary('Wage', digits=(16, 2), required=True, track_visibility="onchange", help="Employee's monthly gross wage.")    

    currency_id = fields.Many2one('res.currency', string='Account Currency')
    meal = fields.Monetary('Meal', digits=(16, 2))


class HrCity(models.Model):
    _name = "hr.city"

    name = fields.Char('City')


class HrBpjs(models.Model):
    _name = "hr.bpjs"

    name = fields.Char('Complete Name')
    no_bpjs = fields.Char('BPJS Number')

    faskes = fields.Char('Faskes')
    status = fields.Selection([
        ('suami', 'Suami'),
        ('istri', 'Istri'),
        ('anak', 'Anak')
    ], string='Status')
    
    bpjs_id = fields.Many2one('hr.employee')
    kelas = fields.Selection([
        (1, 1),
        (2, 2),
        (3, 3)
    ])

# class HrInventaris(models.Model):
#     _name = "hr.inventaris"
# 
#     name = fields.Char('Name')
#     size = fields.Char('Description')
#     inventaris_id = fields.Many2one('hr.employee')

# class HartaKekayaan(models.Model):
#     _name = "harta"


class HartaKategori(models.Model):
    _name = "harta.kategori"

    name = fields.Char('Nama Kategori', required=True)


class HartaKodeNama(models.Model):
    _name = "harta.kode_nama"

    harta_kategori_id = fields.Many2one(
        comodel_name='harta.kategori',
        required=True

    )
    name = fields.Char('Nama', required=True)
    kode = fields.Char('Kode', required=True)


class HartaNama(models.Model):
    _name = "harta.nama"

    name = fields.Char('Nama')   


class HartaKode(models.Model):
    _name = "harta.kode"

    name = fields.Char('Nama Kode')      


class HrHarta(models.Model):
    _name = "hr.harta"

    # name = fields.Char('Name')
    kategori_ids = fields.Many2one('harta.kategori', required=True)

    kode_nama_ids = fields.Many2one('harta.kode_nama', required=True)

    @api.onchange('kategori_ids')
    def _kategori_onchange(self):
        res = {}
        res['domain'] = {'kode_nama_ids':[('harta_kategori_id', '=', self.kategori_ids.id)]}
        return res

    # kode_ids = fields.Many2one('harta.kode',required=True)
    # nama_ids = fields.Many2one('harta.nama',required=True)
    tahun_perolehan = fields.Char('Tahun Perolehan', required=True)

    nilai = fields.Monetary(string="Nilai", required=True)
    currency_id = fields.Many2one('res.currency', string="Currency", default=lambda self : self.env.user.company_id.currency_id.id, readonly=True)    
    # nilai = fields.Integer('Nilai')

    harta_id = fields.Many2one('hr.employee')


class HrBpaEmployee(models.Model):
    _inherit = "hr.employee"
    
    @api.one
    def _compute_wage(self):
        first_date = date(date.today().year, 1, 1)
        last_date = date(date.today().year, 12, 31)
        domain = [
            ('employee_id', '=', self.id),
            ('date_from', '>=', str(first_date)),
            ('date_from', '<=', str(last_date)),
        ]
        payslips = self.env['hr.payslip'].search(domain)
        if payslips:
            self.cummulative_wage_year = sum(payslips.mapped('line_ids.total')) + sum(payslips.mapped('input_line_ids.amount'))

    npwp_status = fields.Boolean('Npwp Status', default=False)
    cummulative_wage_year = fields.Float('Total Penghasilan Tidak Teratur Tahun Ini', compute='_compute_wage')

    # work_location = fields.Many2one('res.country.city')
    work_location = fields.Many2one('project.project')

    # barcode = fields.Char(string="NIK", default='New')

    ptkp = fields.Selection([
        ('TK', 'TK'),
        ('K0', 'K0'),
        ('K1', 'K1'),
        ('K2', 'K2'),
        ('K3', 'K3')    
    ])
    
    ptkp_id = fields.Many2one('hr.ptkp', 'PTKP Status')
    ptkp_amount = fields.Float(string='PTKP Amount')
    
    @api.onchange('ptkp_id')
    def change_ptkp_id(self):
        if not self.ptkp_id:
            return {}
        self.ptkp_amount = self.ptkp_id.amount
        
    def _get_pengali_gaji(self):
        for line in self:
            if line.joining_date:
                from datetime import datetime
                python_tanggal = datetime.strptime(line.joining_date, '%Y-%m-%d')
                tahun = python_tanggal.year
                tahun_sekarang = datetime.today().year
                if tahun != tahun_sekarang:
                    line.pengali_gaji = 12
                else:
                    bulan_masuk = python_tanggal.month
                    line.pengali_gaji = 13 - bulan_masuk

    pengali_gaji = fields.Integer(compute='_get_pengali_gaji')

    harta_kekayaan_ids = fields.One2many('hr.harta', 'harta_id', string="Harta Kekayaan")

    # history mutation
    mutation_ids = fields.One2many('employee.transfer', 'employee_id', string="Mutation")

    # inventaris_karyawan
#     inventaris_ids = fields.One2many('hr.inventaris', 'inventaris_id', string="Inventaris")

    # menghitung umur sekarang
    @api.multi
    def _get_age(self):
        for lines in self:
            if lines.birthday:
                from datetime import datetime
                birthday = datetime.strptime(lines.birthday, '%Y-%m-%d')
                tanggal_now = datetime.today()
                from dateutil.relativedelta import relativedelta
                jumlah_tahun = relativedelta(tanggal_now, birthday).years
                lines.umur_sekarang = jumlah_tahun

    umur_sekarang = fields.Integer('Age', compute="_get_age")

    # list bpjs
    bpjs_ids = fields.One2many('hr.bpjs', 'bpjs_id', string="BPJS")

    # @api.multi
    def _get_masa_kerja(self):
        # qq=0
        for lines in self:
            if lines.joining_date == False:
                lines.masa_kerja = 'Join date belum di set'
            else:
                from datetime import datetime
                # datetime.strptime(strDate, '%m/%d/%y')
                join_date = datetime.strptime(lines.joining_date, '%Y-%m-%d')
                # from datetime import date
                tanggal_now = datetime.today()
                from dateutil.relativedelta import relativedelta
                jumlah_tahun = relativedelta(tanggal_now, join_date).years
                jumlah_bulan = relativedelta(tanggal_now, join_date).months
                str_masa_kerja = ''
                if jumlah_tahun > 0:
                    str_masa_kerja = str_masa_kerja + str(jumlah_tahun) + ' Tahun '
                    # jumlah_bulan = jumlah_bulan - jumlah_tahun * 12
                if jumlah_bulan > 0:
                    str_masa_kerja = str_masa_kerja + str(jumlah_bulan) + ' Bulan'
                lines.masa_kerja = str_masa_kerja

    masa_kerja = fields.Char(
        string="Masa Kerja",
        compute="_get_masa_kerja"
        # store = True
    )

    # @api.depends('masa_pensiun')
    def _get_masa_pensiun(self):
        for lines in self:
            if lines.birthday:            
                tanggal_lahir = lines.birthday
                from datetime import datetime
                tahun_sekarang = datetime.today().year
                pensiun = self.env['usia.pensiun'].search([('tahun', '=', tahun_sekarang)])
                from dateutil.relativedelta import relativedelta
                if type(tanggal_lahir) is str:
                    tanggal_lahir = datetime.strptime(tanggal_lahir, '%Y-%m-%d')
                tanggal_lahir = tanggal_lahir + relativedelta(years=pensiun.usia_pensiun)
                tanggal_lahir = tanggal_lahir + relativedelta(months=1)
                tahun = tanggal_lahir.year
                bulan = tanggal_lahir.month
                qq = 0

                tanggal_lahir = datetime.strptime(str(tahun) + '-' + str(bulan) + '-' + '01', '%Y-%m-%d')

                lines.masa_pensiun = tanggal_lahir
                query = "update hr_employee set internal_masa_pensiun ='" + lines.masa_pensiun + "' where id = " + str(lines.id)
                # lines.id
                self.env.cr.execute(query)

    masa_pensiun = fields.Date('Masa Pensiun', compute="_get_masa_pensiun")

    internal_masa_pensiun = fields.Date('Masa Pensiun')

    no_kk = fields.Char('No. KK')
    no_bpjs_kesehatan = fields.Char('No. BPJS Kesehatan')
    no_bpjs_ketenagakerjaan = fields.Char('No. BPJS Ketenagakerjaan')

    country_of_birth = fields.Many2one('res.country', string="Country of Birth", groups="hr.group_hr_user")
    spouse_complete_name = fields.Char(string="Spouse Complete Name", groups="hr.group_hr_user")
    spouse_birthdate = fields.Date(string="Spouse Birthdate", groups="hr.group_hr_user")
    job_title = fields.Char('Job Title')

    city = fields.Many2one('res.country.city')
    
    @api.multi
    def _cek_ketersediaan_contract(self):
        for i in self:
            if i.contract_id.id == False:
                i.idnya_contract = 'Kosong'
            else:
                i.idnya_contract = 'Ada'

    idnya_contract = fields.Char(compute='_cek_ketersediaan_contract')

    # _rec_name= 'barcode'    

    year_of_service_start = fields.Char('Year Of Service Start')
    year_of_service_end = fields.Char('Year Of Service End')   

    npwp = fields.Char(string="NPWP")

    @api.onchange('npwp')
    def onchange_npwp(self):
        if self.npwp != '':
            self.npwp_status = True
        else:
            self.npwp_status = False

    @api.multi
    def name_get(self):
        result = []
        for employee in self:
            if type(employee.barcode) is bool:
                name = employee.name
            else:
                name = '[' + employee.barcode + '] ' + employee.name
            result.append((employee.id, name))
        return result

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        args = args or []
        recs = self.browse()
        if not recs:
            recs = self.search(['|', ('barcode', operator, name), ('name', operator, name)] + args, limit=limit)
        return recs.name_get()
      
    status_id = fields.Char(string='Status')        

    # Change field value
    mobile_phone = fields.Char('Work Mobile')
    identification_id = fields.Char(string='Identification No', groups="hr.group_hr_user")
    bank_account_number = fields.Char(string='Bank Account Number', groups="hr.group_hr_user",
        help='Employee bank salary account')
    gender = fields.Selection([
        ('male', 'Male'),
        ('female', 'Female'),
    ], string='Gender', groups="hr.group_hr_user", default='')
    # Change String 
    department_id = fields.Many2one('hr.department', 'Organization')
    job_level_id = fields.Many2one('hr.employee.level', 'Job Level')
    coach_id = fields.Many2one('hr.employee', 'Superior')
    home_based_id = fields.Many2one('res.company', 'Home Based')
    # Private Information
    bank_name = fields.Char('Bank Name')
    bank_id = fields.Many2one(
        'res.bank',
        string='Bank Name'
    )
    other_bank_id = fields.Many2one(
        'res.bank',
        string='Bank Name'
    )    

    other_bank_account_number = fields.Char('Other Bank Account Name')
    other_bank_name = fields.Char('Other Bank Name')

    mother = fields.Char('Mother')

    bank_branch = fields.Char('Branch')
    account_name = fields.Char('Account Name')

    # Change String
    address_home_id = fields.Many2one(
        'res.partner', 'Address', help='Enter here the private address of the employee, not the one linked to your company.',
        groups="hr.group_hr_user")
    address_home = fields.Text('Address', groups="hr.group_hr_user")
    id_card_address = fields.Text('ID Card Address')
    home_ownership = fields.Selection([
                                        ('milik_sendiri', 'Milik Sendiri'),
                                        ('milik_orang_tua', 'Milik Orang Tua'),
                                        ('sewa', 'Sewa/Kontrak/Kos'),
                                        ('lain', 'Lain')], string='Home Ownership', default='')
    hr_email = fields.Char('Email')
    hr_phone = fields.Char('Phone')
    hr_mobile = fields.Char('Mobile')
    hr_facebook = fields.Char('Facebook')
    hr_instagram = fields.Char('Instagram')
    religion_id = fields.Selection([
                                    ('islam', 'Islam'),
                                    ('kristen', 'Kristen'),
                                    ('khatolik', 'Khatolik'),
                                    ('budha', 'Budha'),
                                    ('hindu', 'Hindu'),
                                    ('konghucu', 'Konghucu'),
                                    ('lain', 'Lain lain')], string='Religion', default='')

    hr_height = fields.Float('Height')
    hr_weight = fields.Float('Weight')
    hr_blood_type = fields.Selection([('a', 'A'),
                                      ('b', 'B'),
                                      ('ab', 'AB'),
                                      ('o', 'O')], 'Blood Type', default='a')
    marital_status = fields.Selection([
                ('belum_menikah', 'Belum Menikah'),
                ('menikah', 'Menikah'),
                ('janda', 'Janda'),
                ('duda', 'Duda'),
    ], string='Marital Status', groups="hr.group_hr_user", default='')
    applicant_experience_ids = fields.One2many('hr.applicant.experience', 'employee_id', 'Applicant Experience')
    hr_skill_ids = fields.One2many('hr.skills', 'skill_employee_id', 'Employee Skill')
    hr_language_ids = fields.One2many('hr.language', 'lan_employee_id', 'Employee Language')
    hr_training_ids = fields.One2many('hr.training', 'training_employee_id', 'Employee Training')

    employee_training_ids = fields.One2many('training.request', 'employee_id', 'Employee')

    hr_reference_ids = fields.One2many('hr.reference', 'reference_employee_id', 'Employee Reference')
    applicant_ids = fields.One2many('applicant.id', 'employee_id', 'Id')
    emergency_contact_ids = fields.One2many('hr.emergency.contact', 'employee_id', 'Emergency Contact')


class UsiaPensiun(models.Model):
    _name = 'usia.pensiun'

    # name = fields.Char('Name')

    tahun = fields.Char('Tahun', required=True)

    usia_pensiun = fields.Integer('Usia Pensiun', required=True)
