import logging
from datetime import datetime
from odoo.exceptions import Warning, ValidationError
from odoo import models, fields, exceptions, api, _

class PermissionRequest(models.Model):
    _inherit = 'project.project'

    meal = fields.Monetary()
    working_hours = fields.Many2one('resource.calendar')
    cuti_per_bulan = fields.Boolean('Cuti Per Bulan')
    ho_checklist = fields.Boolean('Is HO?', default=True)

    sequence_emp_transfer = fields.Integer('Sequence Employee Transfer',
        default=0
    )



