# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

import logging
from datetime import datetime
from odoo.exceptions import Warning, ValidationError
from odoo import models, fields, exceptions, api, _

_logger = logging.getLogger(__name__)

class HrAppraisal(models.Model):
    _inherit = 'hr.appraisal'

    @api.multi
    def _attachment_count(self):
        attachment_obj = self.env['ir.attachment']
        for record in self:
            record.attachment_count = attachment_obj.search_count([('res_model', '=', 'hr.appraisal'),
                                                                    ('res_id', '=', record.id)])

    attachment_count = fields.Integer(compute='_attachment_count', string='Attachments')

    @api.multi
    def open_attachment(self):
        xml_id = 'mail.view_document_file_kanban'
        kanban_view_id = self.env.ref(xml_id).id
        xml_id = 'base.view_attachment_tree'
        tree_view_id = self.env.ref(xml_id).id
        xml_id = 'base.view_attachment_form'
        form_view_id = self.env.ref(xml_id).id
        return {
            'name': _('Attachments'),
            'view_type': 'form',
            'view_mode': 'kanban,tree,form',
            'views': [(kanban_view_id, 'kanban'),(tree_view_id, 'tree'), (form_view_id, 'form')],
            'res_model': 'ir.attachment',
            'domain': [('res_id', 'in', self._ids),
                       ('res_model', '=', 'hr.appraisal')],
            'type': 'ir.actions.act_window',
        }