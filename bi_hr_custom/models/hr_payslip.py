from odoo import models, fields, api, _
import odoo.addons.decimal_precision as dp
from odoo.exceptions import except_orm, Warning, RedirectWarning, UserError, ValidationError


class InheritHrPayslip(models.Model):
    _inherit = 'hr.payslip'

    @api.model
    def create(self, vals):
        obj_hr_payslip = super(InheritHrPayslip, self).create(vals)
        """
        ambil employeenya
        cek apakah ada pengganti cuti, limit 1
        kalo ada masukin ke bawah
        """
        employee_id = obj_hr_payslip.employee_id.id
        pengganti_cuti = self.env['pengganti.cuti'].search([('employee_id', '=', 1)], limit=1)
        if pengganti_cuti.id:
            # insert ke bawah
            payment_amount = pengganti_cuti.payment_amount
            # payslip id 
            payslip_id = obj_hr_payslip.id
            contract_id = self.env['hr.contract'].search([('employee_id', '=', 1)], limit=1).id
            if (contract_id == False):
                raise ValidationError(_("Contract untuk account ini belom ada!"))

            # insert ke hr.payslip.input
            self.env['hr.payslip.input'].create({
                'payslip_id': payslip_id,
                'name': 'Pengganti Cuti',
                'code': 'PG',
                'amount': payment_amount,
                'contract_id': contract_id
            })

        return obj_hr_payslip
    
    @api.multi
    def compute_sheet(self):
        super(InheritHrPayslip, self).compute_sheet()
        for payslip in self:
#             payslip.line_ids.unlink()
#             attendance_days_lines = self.env['hr.attendance'].search([
#                 ('create_date', '>=', self.date_from),
#                 ('create_date' , '<=', self.date_to),
#                 ('employee_id', '=', self.employee_id.id)
#             ])
#             attendances = {
#                 'payslip_id': payslip.id,
#                 'name': 'Attendance',
#                 'code': 'ATT',
#                 'number_of_days': len(attendance_days_lines) if attendance_days_lines else 0,
#                 'contract_id': self.contract_id.id
#             }
#             self.env['hr.payslip.worked_days'].create(attendances)
#             meal = {
#                 'payslip_id': payslip.id,
#                 'name': 'Meal',
#                 'code': 'MEAL',
#                 'number_of_days': len(attendance_days_lines.filtered(lambda att: att.meal)) if attendance_days_lines.filtered(lambda att: att.meal) else 0,
#                 'contract_id': self.contract_id.id
#             }
#             self.env['hr.payslip.worked_days'].create(meal)
            pengganti_cuti_lines = self.env['pengganti.cuti'].search([('payslip_reference', '=', self.id)])
            if self.input_line_ids.filtered(lambda l: l.code == 'PC'):
                self.input_line_ids.filtered(lambda l: l.code == 'PC')[0].write({'amount': sum(pengganti_cuti_lines.mapped('jumlah'))})
            else:
                pengganti_cuti = {
                    'name': 'Pengganti Cuti',
                    'code': 'PC',
                    'amount': sum(pengganti_cuti_lines.mapped('jumlah')) if pengganti_cuti_lines else 0,
                    'contract_id': self.contract_id.id
                }
                self.env['hr.payslip.input'].create(meal)
        return True
    
