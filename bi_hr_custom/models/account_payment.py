from odoo import models, fields, api, _


class AccountPayment(models.Model):
    _inherit = 'account.payment'
    
    payment_id = fields.Many2one('employee.assignment', 'Assignment Reference')
    training_request_id = fields.Many2one('training.request')
    
    @api.multi
    def post(self):
        for rec in self:
            rec.payment_id.action_advance_paid()
        return super(AccountPayment, self).post()


class AccountVoucher(models.Model):
    _inherit = 'account.voucher'
    
    voucher_id = fields.Many2one('employee.assignment', 'Assignment Reference')
    emp_transfer_id = fields.Many2one('employee.transfer', 'Employee Transfer Reference')
    training_request_id = fields.Many2one('training.request')
    
    @api.multi
    def action_validate(self):
        res = super(AccountVoucher, self).action_validate()
        for rec in self:
            rec.voucher_id.action_voucher_paid()
            rec.emp_transfer_id.write({'state': 'done'})
        return res
    
