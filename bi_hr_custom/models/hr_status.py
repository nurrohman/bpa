from odoo import models, fields, exceptions, api, _

class HrStatus(models.Model):
    _name = "hr.status"
    name = fields.Char('Status')