import logging
from datetime import datetime
from odoo.exceptions import Warning, ValidationError
from odoo import models, fields, exceptions, api, _

class InheritCity(models.Model):
    _inherit = 'res.country.city'

    sequence_emp_transfer = fields.Integer('Sequence Employee Transfer',
        default=0
    )