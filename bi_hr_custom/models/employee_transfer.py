# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo.exceptions import Warning
from odoo import models, fields, exceptions, api, _
from odoo.exceptions import UserError, ValidationError

class EmployeeTransfer(models.Model):
    _inherit = 'employee.transfer' 

    emp_transfer_urutan = fields.Integer(default=0)
    
    # ANIF
    @api.depends('state', 'voucher_ids.state')
    def _get_voucher(self):
        for rec in self:
            rec.voucher_count = len(rec.voucher_ids)
    ####

    # attachment 
    @api.multi
    def _attachment_count(self):
        attachment_obj = self.env['ir.attachment']
        for record in self:
            record.attachment_count = attachment_obj.search_count([('res_model', '=', 'employee.transfer'),
                                                                    ('res_id', '=', record.id)])    

    attachment_count = fields.Integer(compute='_attachment_count', string='Attachments')

    @api.multi
    def open_attachment(self):
        xml_id = 'mail.view_document_file_kanban'
        kanban_view_id = self.env.ref(xml_id).id
        xml_id = 'base.view_attachment_tree'
        tree_view_id = self.env.ref(xml_id).id
        xml_id = 'base.view_attachment_form'
        form_view_id = self.env.ref(xml_id).id
        return {
            'name': _('Attachments'),
            'view_type': 'form',
            'view_mode': 'kanban,tree,form',
            'views': [(kanban_view_id, 'kanban'),(tree_view_id, 'tree'), (form_view_id, 'form')],
            'res_model': 'ir.attachment',
            'domain': [('res_id', 'in', self._ids),
                       ('res_model', '=', 'employee.transfer')],
            'type': 'ir.actions.act_window',
        }

    # name auto increment buat sk mutation no
    name = fields.Char(string="Name", readonly=True, required=True, copy=False, default='Employee Transfer')
    
    sk_mutation_no = fields.Char(string="Sk Mutation No", readonly=True, required=True, copy=False, default='New SK')

    @api.model
    def create(self, vals):
        # vals['sk_mutation_no'] = self.env['ir.sequence'].next_by_code('seq.mutation.employee.transfer')
        rec = super(EmployeeTransfer, self).create(vals)
        # cr = self.env.cr
        # query_urutan_terakhir = 'select max(emp_transfer_urutan) from employee_transfer'
        # cr.execute(query_urutan_terakhir)
        urutan_terakhir = rec.employee_id.project_id.sequence_emp_transfer
        urutan_selanjutnya = urutan_terakhir+1
        # cek apakah sudah ada locationnnya 
        if len(rec.employee_id.project_id.ids) < 1:
            raise ValidationError(_("Work location di employee belum di set."))
        # qq=0
        rec.employee_id.project_id.sequence_emp_transfer = urutan_selanjutnya
        tiga_digit_urutan_plus_satu = "{0:0=3d}".format(urutan_selanjutnya)
        location_asal = rec.employee_id.project_id.name
        from datetime import datetime
        tahun_sekarang = datetime.today().year
        bulan_sekarang = datetime.today().month
        qq=0
        rec.sk_mutation_no = tiga_digit_urutan_plus_satu + '.SM.DIR/BPA/' + location_asal + '/' + str(bulan_sekarang) + '/' + str(tahun_sekarang)
        # Sequence: XXX.SM.DIR/BPA/Location(Asal)/Bulan/Tahun        
        return rec


    reason = fields.Many2one(
        comodel_name = 'hr.reason',
        string = 'Reason'
    )

    employee_status = fields.Char('Status', readonly=True)


    joint_date = fields.Date('Joint Date')
    year_of_service = fields.Char('Year Of Service')

    job_id = fields.Many2one('hr.job', string='Position')
    job_level_id = fields.Many2one('hr.employee.level', 'Level')
    department_id = fields.Many2one('hr.department', string='Department')
    main_job = fields.Text('Main Job')
    # status = fields.Selection([('probation','Probation'),('kontrack','Kontrack'),('tetap','Tetap')],'Status')
    new_position = fields.Char('Position')
    new_job_id = fields.Many2one('hr.job', 'Position', required=True)
    new_level_id = fields.Many2one('hr.employee.level', 'Level', required=True)
    new_department_id = fields.Many2one('hr.department','Department', required=True)
    new_main_job = fields.Text('Main Job')

    new_work_location = fields.Many2one('project.project',string='Location / Branch')


    # home_based = fields.Many2one('hr.city',compute='_onchange_employee')
    new_home_based = fields.Many2one('hr.city',string='Home Based Location')

    new_coach_id = fields.Many2one('hr.employee',string='Superior')
    effective_date = fields.Date('Effective Date')
    remark = fields.Text('Remark')

    proposed_manager_id = fields.Many2one('res.partner','Manager')
    proposed_date = fields.Date('Date')
    proposed_note = fields.Text('Note')

    new_branch_manager_id = fields.Many2one('res.partner','New Branch Manager')
    branch_manager_date = fields.Date('Date')
    branch_manager_note = fields.Text('Note')

    recomended_manager_id = fields.Many2one('res.partner','AOH/AR Manager')
    recomended_manager_date = fields.Date('Date')
    recomended_manager_note = fields.Text('Note')

    approved_manager_id = fields.Many2one('res.partner','Sr.Mgr/Gm')
    approved_manager_date = fields.Date('Date')
    approved_manager_note = fields.Text('Note')

    recruitment_manager_id = fields.Many2one('res.partner','Recruitment Manager')
    recruitment_manager_date = fields.Date('Date')
    recruitment_manager_note = fields.Text('Note')

    hrd_manager_id = fields.Many2one('res.partner','HRD Manager')
    hrd_manager_date = fields.Date('Date')
    hrd_manager_note = fields.Text('Note')

    # branch = fields.Char('Location / Branch', compute='_onchange_employee')
    branch = fields.Many2one('project.project', readonly=True)


    home_based = fields.Many2one('res.country.city')

    coach = fields.Char('Coach')

    @api.onchange('employee_id')
    def _onchange_employee(self):
        for emp in self:
            qq=0
            emp.name = 'Transfer of %s' % (emp.employee_id.name) if emp.employee_id else 'Employee Transfer'
            emp.joint_date = emp.employee_id.joining_date
            emp.coach = emp.employee_id.coach_id.name
            # qq=0
            emp.home_based = emp.employee_id.city

            emp.branch = emp.employee_id.project_id
            emp.department_id = emp.employee_id.department_id.id
            emp.job_id = emp.employee_id.job_id
            emp.job_level_id = emp.employee_id.job_level_id
            emp.nik = emp.employee_id.barcode
            emp.employee_status = emp.employee_id.status_id

            return

    nik = fields.Char(string='NIK', compute='_onchange_employee')

    tipe = fields.Selection([
        ('acting','Acting'),
        ('promotion','Promotion'),
    ], string="Contract Type")

    from datetime import datetime
    date_start = fields.Date('Date Start', default=datetime.today())
    date_end = fields.Date('Date End')
    
    # ANIF
    expense_line = fields.One2many('employee.transfer.expense', 'transfer_id', 'Expense Lines')
    
    voucher_count = fields.Integer(compute='_get_voucher', string='Voucher', default=0)
    voucher_ids = fields.One2many('account.voucher', 'emp_transfer_id', string='Voucher', copy=False)

    @api.multi
    def button_expense_create(self):
        self.ensure_one()
        if not self.expense_line:            
            raise UserError(_('Please input your expense !'))
        if any(amount <= 0 for amount in self.expense_line.mapped('amount')):
            raise UserError(_("Amount for expense must greather than 0"))
        lines = []
        advances = []
        part = self.env.user.partner_id
        fpos = part.property_account_position_id
        journal_id = self.env['account.journal'].search([('company_id', '=', self.company_id.id), ('type', '=', 'purchase')], limit=1)
        for line in self.expense_line:
            accounts = line.product_id.product_tmpl_id.get_product_accounts(fpos)
            account = accounts['expense']
            vals = (0, 0, {'product_id': line.product_id.id,
                           'name': line.product_id.display_name,
                           'account_id': account.id,
                           'quantity': 1,
                           'price_unit': line.amount})
            lines.append(vals)
        data = {
            'partner_id': part.id,
            'emp_transfer_id': self.id,
            'payment_type': 'cash',
            'voucher_type': 'purchase',
            'journal_id': journal_id[0] and journal_id[0].id or False,
            'date': fields.Date.today(),
            'name': 'Expenses for ' + self.name, 'line_ids': lines
        }
        self.env['account.voucher'].create(data)
        return self.action_view_settlement()
    
    @api.multi
    def button_done(self):
        self.ensure_one()
        self.write({'state':'done'})

    @api.multi
    def action_view_settlement(self):
        action = self.env.ref('account_payment_request.view_voucher_tree')
        result = action.read()[0]
        imd = self.env['ir.model.data']
        list_view_id = imd.xmlid_to_res_id('account_payment_request.view_voucher_tree')
        form_view_id = imd.xmlid_to_res_id('account_payment_request.view_purchase_receipt_form')
        result['domain'] = "[('id', 'in', " + str(self.voucher_ids.ids) + ")]"
        return {
            'name': 'Expenses',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'views': [[list_view_id, 'tree'], [form_view_id, 'form']],
            'domain' : result.get('domain'),
            'res_model': 'account.voucher',
            'type': 'ir.actions.act_window',
        }

    
class EmployeeTransferExpense(models.Model):
    _name = 'employee.transfer.expense' 
    _description = 'Employee Transfer Expense'
    
    transfer_id = fields.Many2one('employee.transfer', string="Transfer Reference", index=True, required=True, ondelete='cascade')
    product_id = fields.Many2one('product.product', 'Expense')
    name = fields.Char('Description')
    amount = fields.Float('Amount')
    
    @api.onchange('product_id')
    def change_product_id(self):
        if not self.product_id:
            return
        self.name = self.product_id.display_name





