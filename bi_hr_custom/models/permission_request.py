import logging
from datetime import datetime
from odoo.exceptions import Warning, ValidationError
from odoo import models, fields, exceptions, api, _

class PermissionRequest(models.Model):
    _name = 'permission.request'

    name = fields.Char('Name')
    arrival_time = fields.Datetime(string='Arrival Time')
    return_time = fields.Datetime('Return Time')

    # <field name="time" widget="float_time"/>
    employee_id = fields.Many2one('hr.employee',string="Employee" ,required=True)

    department_id = fields.Many2one('hr.department',string="Department", compute='onchange_employee', readonly=True)
    job_id = fields.Many2one('hr.job',string="Job Position", compute='onchange_employee', readonly=True )
    # work_location = fields.Many2one('res.country.city', compute='onchange_employee', readonly=True)
    work_location = fields.Many2one('project.project')

    reason = fields.Char('Reason')
    parent_id = fields.Many2one('hr.employee', string='Manager',compute='onchange_employee', readonly=True)
    approve_date = fields.Date('Approve Date')
    approve_by = fields.Char('Approve By')
    number_of_hours = fields.Float('Number Of Hours')
    meal = fields.Boolean('Meal')
    department_approved_date = fields.Date('Department Approved Date')
    department_manager = fields.Char('Department Manager')

    state = fields.Selection([
        ('draft', 'Draft'),
        ('waiting_approval_hrd', 'Waiting Approval HRD'),
        ('waiting_approval_manager', 'Waiting Approval Manager'),
        ('cancelled', 'Cancelled'),
        ('approved', 'Approved')
    ], string="Status", index=True, copy=False, default='draft', track_visibility='onchange')

    @api.multi
    def submit(self):
        self.ensure_one()
        arrival_time = self.arrival_time
        arrival_time_date = arrival_time[0:10]
        employee_id = self.employee_id.id
        
        sql = "select count(*) as jumlah from hr_attendance where check_in::date = '" + arrival_time_date + "' and employee_id = " + str(employee_id);
        self.env.cr.execute(sql)
        jumlah_attendance = self.env.cr.fetchall()[0][0]

        if jumlah_attendance > 1: # sudah ada data attendance dengan employee itu dan hari yang sama
            raise ValidationError(_("Sudah ada di attendance dengan tanggal yang sama."))
        else:
            self.write({'state':'waiting_approval_hrd'})

    @api.multi
    def approve_hrd(self):
        self.ensure_one()
        self.write({'state':'waiting_approval_manager'})

    @api.multi
    def approve_manager(self):
        self.ensure_one()

        self.env['hr.attendance'].create({
            'employee' : self.employee_id.id,
            'check_in' : self.arrival_time,
            'check_out' : self.return_time,
            'meal' : self.meal
        })

        self.write({'state':'approved'})

    @api.multi
    def cancel(self):
        self.ensure_one()
        self.write({'state':'cancelled'})

    @api.multi
    def set_to_draft(self):
        self.ensure_one()
        self.write({'state':'draft'})
        
        

    @api.onchange('employee_id')
    def onchange_employee(self):
        for line in self:
            line.department_id = line.employee_id.department_id
            line.job_id = line.employee_id.job_id
            line.work_location = line.employee_id.work_location
            line.parent_id = line.employee_id.parent_id

