from odoo.exceptions import Warning
from odoo import models, fields, exceptions, api, _

class HrLeaveExpenses(models.Model):

    _name = 'hr.leave.expenses'


    leave_id = fields.Many2one('hr.holidays', string="Employee Leave", index=True, required=True, ondelete='cascade')
    product_id = fields.Many2one('product.product', 'Expense')
    name = fields.Char('Description')
    amount = fields.Float('Amount')



    @api.onchange('product_id')
    def change_product_id(self):
        if not self.product_id:
            return
        self.name = self.product_id.display_name
