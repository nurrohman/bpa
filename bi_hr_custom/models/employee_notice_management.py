from odoo.exceptions import Warning
from odoo import models, fields, exceptions, api, _

class InheritEmpNotice(models.Model):
	_inherit = 'employee.notice.management'

	status = fields.Char('Status',compute="_get_status")
    
    @api.depends('employee_id')
    def _get_status(self):
        for i in self:
            qq=0
