# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from . import hr
from . import hr_applicant
from . import hr_evaluation
from . import advance_expense
from . import hr_resignation
from . import hr_leave
from . import employee_transfer
from . import hr_promotion
from . import hr_demotion
from . import medical_request
from . import hr_appraisal
from . import hr_status
from . import hr_reason
from . import hr_place_of_birth
from . import hr_contract
from . import employee_assignment
from . import account_payment
from . import res_country_city
from . import permission_request
from . import training_request
from . import hr_attendance
from . import daily_wage
from . import hr_leave_expenses
from . import project_inherit
from . import pengganti_cuti
from . import hr_payslip



# from . import employee_notice_management

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
