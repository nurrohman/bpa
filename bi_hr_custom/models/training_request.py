import logging
from datetime import datetime
from odoo.exceptions import Warning, ValidationError, UserError

from odoo import models, fields, exceptions, api, _

class trainingRequest(models.Model):
    _name = 'training.request'

    @api.depends('state', 'voucher_ids.state')
    def _get_voucher(self):
        for rec in self:
            rec.voucher_count = len(rec.voucher_ids)
            
    gaji_pokok = fields.Monetary(string="Gaji Pokok", compute='onchange_employee')
    total_expense = fields.Monetary(string="Total Expense", compute='onchange_employee')
    total_pengeluaran = fields.Monetary(string="Total Pengeluaran", compute='onchange_employee')
    masa_ikatan_dinas = fields.Float(string="Masa Ikatan Dinas", compute='onchange_employee')
    instansi = fields.Char('Instansi')

    tr_multiple_training_id = fields.Integer() # di isi bila di create data dari menu multiple training request

    total_nilai_recapitulation = fields.Float('Total Nilai',compute='_get_total_nilai_recapitulation')

    @api.depends('evaluation_line_manager','evaluation_line_hr')
    def _get_total_nilai_recapitulation(self):
        total_evaluation_manager = sum(self.evaluation_line_manager.mapped('nilai_akhir'))
        total_evaluation_hr = sum(self.evaluation_line_hr.mapped('nilai_akhir'))
        self.total_nilai_recapitulation = total_evaluation_manager + total_evaluation_hr

        if self.total_nilai_recapitulation >= 75:
        # kesimpulan_recapitulation = fields.Char('Kesimpulan')
            self.kesimpulan_recapitulation = 'PELATIHAN EFEKTIF'
        else:
            self.kesimpulan_recapitulation = 'PELATIHAN TIDAK EFEKTIF'


    kesimpulan_recapitulation = fields.Char('Kesimpulan',compute='_get_total_nilai_recapitulation')



    company_id = fields.Many2one('res.company', 'Company', default=lambda self:self.env.user.company_id, required=True)
    currency_id = fields.Many2one(related='company_id.currency_id', readonly="1")

    name = fields.Char(string="Training Request Number", readonly=True, required=True, copy=False, default='New')
    
    voucher_count = fields.Integer('Voucher Count', compute='_get_voucher')
    voucher_ids = fields.One2many('account.voucher', 'training_request_id', string='Voucher/Expenses')

    @api.model
    def create(self, vals):
        if vals.get('name', 'New') == 'New':
            vals['name'] = self.env['ir.sequence'].next_by_code(
                'training.request') or 'New'
        result = super(trainingRequest, self).create(vals)
        return result

    expense_line = fields.One2many('employee.training.expense', 'training_id', 'Expense Lines')

    evaluation_line_manager = fields.One2many('hr.evaluation', 'training_id', 'Evaluation Lines',domain=[('pengecek','=','manager')])

    evaluation_line_hr = fields.One2many('hr.evaluation', 'training_id', 'Evaluation Lines',domain=[('pengecek','=','hr')])


    @api.onchange('evaluation_line_hr')
    def onchange_evaluation_line_hr(self):        
        for line in self.evaluation_line_hr:
            nilai = line.nilai
            list_nilai = line.training_id.evaluation_line_hr.mapped('nilai')
            # del list_nilai[-1]
            total_nilai = sum(list_nilai)
            
            # hitung bobot
            line.bobot = line.nilai / total_nilai * 100
            line.nilai_x_bobot = line.nilai * line.bobot / 100
            line.nilai_akhir = line.nilai_x_bobot * 30 / 100 

    @api.onchange('evaluation_line_manager')
    def onchange_evaluation_line_manager(self):    
        
        for line in self.evaluation_line_manager:
            nilai = line.nilai
            list_nilai = line.training_id.evaluation_line_manager.mapped('nilai')
            # del list_nilai[-1]
            total_nilai = sum(list_nilai)
            
            # hitung bobot
            line.bobot = line.nilai / total_nilai * 100
            line.nilai_x_bobot = line.nilai * line.bobot / 100
            line.nilai_akhir = line.nilai_x_bobot * 70 / 100

    voucher_count = fields.Integer(compute='_get_voucher', string='Voucher', default=0)

    @api.depends('state', 'voucher_ids.state')
    def _get_voucher(self):
        for rec in self:
            rec.voucher_count = len(rec.voucher_ids)

    employee_id = fields.Many2one('hr.employee',string="Employee" ,required=True)

    def create_default_data_evaluation(self, obj_training):
        training_id = obj_training.id

        hr_evaluation = obj_training.env['hr.evaluation']
        if len(hr_evaluation.search([('training_id','=',training_id)]).ids) < 1: # kalo sebelumnya belom ada maka buat baru
            param_evaluation = [
                {
                    'training_id' : training_id,
                    'kriteria_penilaian' : 'Peningkatan kompetensi (dibidang terkait)',
                    'pengecek' : 'hr'
                },
                {
                    'training_id' : training_id,
                    'kriteria_penilaian' : 'Peningkatan kedisiplinan',
                    'pengecek' : 'hr'
                    
                },   
                {
                    'training_id' : training_id,
                    'kriteria_penilaian' : 'Peningkatan kepemimpinan',
                    'pengecek' : 'hr'

                },    
                {
                    'training_id' : training_id,
                    'kriteria_penilaian' : 'Kerjasama / Koordinasi (dibidang terkait)',
                    'pengecek' : 'hr'

                },    
                {
                    'training_id' : training_id,
                    'kriteria_penilaian' : 'Peningkatan kompetensi (dibidang terkait)',
                    'pengecek' : 'manager'
                },
                {
                    'training_id' : training_id,
                    'kriteria_penilaian' : 'Peningkatan kedisiplinan',
                    'pengecek' : 'manager'
                    
                },   
                {
                    'training_id' : training_id,
                    'kriteria_penilaian' : 'Peningkatan kepemimpinan',
                    'pengecek' : 'manager'

                },    
                {
                    'training_id' : training_id,
                    'kriteria_penilaian' : 'Kerjasama / Koordinasi (dibidang terkait)',
                    'pengecek' : 'manager'

                },                                              
            ]

            for line in param_evaluation:
                obj_training.env['hr.evaluation'].create(line)        
                # self.write({'state':'manager_evaluation'})

    @api.multi
    def button_finish_training(self):
        self.create_default_data_evaluation(self)
        self.write({'state':'finish'})

    @api.multi
    def button_refuse(self):
        self.write({'state':'refused'})

    @api.multi
    def set_to_draft(self):
        self.write({'state':'new'})        

    @api.multi
    def submit_manager_evaluation(self):
        self.write({'state':'hr_evaluation'})

    @api.multi
    def submit_hr_evaluation(self):
        self.write({'state':'done'})        


    @api.multi
    def button_expense_create(self):
        self.ensure_one()
        self.write({'state':'submitted_expenses'})
        if not self.expense_line:            
            raise UserError(_('Please input your expense !'))
        if any(amount <= 0 for amount in self.expense_line.mapped('amount')):
            raise UserError(_("Amount for expense must greather than 0"))
        lines = []
        advances = []
        part = self.employee_id.user_id.partner_id
        fpos = part.property_account_position_id
        qq=0
        journal_id = self.env['account.journal'].search([('company_id', '=', self.env.user.company_id.id), ('type', '=', 'purchase')], limit=1)
        for line in self.expense_line:
            accounts = line.product_id.product_tmpl_id.get_product_accounts(fpos)
            account = accounts['expense']
            vals = (0, 0, {'product_id': line.product_id.id,
                           'name': line.product_id.display_name,
                           'account_id': account.id,
                           'quantity': line.quantity,
                           'price_unit': line.price_unit})
            lines.append(vals)
        data = {
            'partner_id': part.id,
            'training_request_id': self.id,
            'payment_type': 'cash',
            'voucher_type': 'purchase',
            'journal_id': journal_id[0] and journal_id[0].id or False,
            'date': fields.Date.today(),
            'name': 'Expenses for ' + self.employee_id.name, 'line_ids': lines
        }
        self.env['account.voucher'].create(data)

        return self.action_view_settlement()

    department_id = fields.Many2one('hr.department',string="Department", compute='onchange_employee', readonly=True)
    job_id = fields.Many2one('hr.job',string="Job Position", compute='onchange_employee', readonly=True )
    work_location = fields.Many2one('project.project', compute='onchange_employee', readonly=True)
    training_name = fields.Char('Training Name')
    parent_id = fields.Many2one('hr.employee', string='Manager',compute='onchange_employee', readonly=True)
    approve_date = fields.Date('Approve Date', readonly=True)
    approve_by = fields.Char('Approve By' , readonly=True)

    @api.multi
    def action_view_settlement(self):
        action = self.env.ref('account_payment_request.view_voucher_tree')
        result = action.read()[0]
        imd = self.env['ir.model.data']
        list_view_id = imd.xmlid_to_res_id('account_payment_request.view_voucher_tree')
        form_view_id = imd.xmlid_to_res_id('account_payment_request.view_purchase_receipt_form')
        result['domain'] = "[('id', 'in', " + str(self.voucher_ids.ids) + ")]"
        return {
            'name': 'Expenses',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'views': [[list_view_id, 'tree'], [form_view_id, 'form']],
            'domain' : result.get('domain'),
            'res_model': 'account.voucher',
            'type': 'ir.actions.act_window',
        }

    @api.onchange('employee_id', 'lama_pelatihan', 'expense_line')
    def onchange_employee(self):
        self.department_id = self.employee_id.department_id
        self.job_id = self.employee_id.job_id
        self.work_location = self.employee_id.project_id
        self.parent_id = self.employee_id.parent_id
        
        employee_id = self.employee_id.id
        wage = self.env['hr.contract'].search([('employee_id','=',employee_id)], order="id desc", limit=1).wage

        
        if wage > 1: # jalankan jika ada gajinya                
            self.gaji_pokok = wage / 30 * self.lama_pelatihan
            self.total_expense = sum(self.expense_line.mapped('amount'))
            self.total_pengeluaran = self.gaji_pokok + self.total_expense
            self.masa_ikatan_dinas = self.total_pengeluaran / wage * 2

    tanggal_training = fields.Date('Tanggal Training')
    direksi_approved_date = fields.Date('Direksi Approved Date', readonly=True)

    training_place = fields.Char('Training Place')

    state = fields.Selection([
        ('new', 'Draft'),
        ('waiting_approval_hrd', 'Waiting Approval HRD'),
        ('waiting_approval_direksi', 'Waiting Approval Direksi'),
        ('approved', 'Approved'),
        ('submit_advance', 'Submitted Advance'),
        ('submitted_expenses', 'Submitted Expenses'),
        ('hr_evaluation', 'HR Evaluation'),
        ('refused', 'Refused'),
        ('done', 'Done'),
        ('finish', 'Training Finished'),
        ('manager_evaluation', 'Manager Evaluation'),
        ('cancel', 'Cancel')
    ],string="Status",readonly="True", default="new")


    # training_multiple_request = fields.Char('Training Multiple Request')
    training_multiple_request = fields.Many2one('multiple.training', 'Training Multiple Request', compute="_get_training_multiple_request")

    # account_id = fields.Many2one('account.account', domain=[('deprecated', '=', False)], string='Tax Account', ondelete='restrict',


    @api.onchange('training_multiple_request')
    def _get_training_multiple_request(self):        

        self.training_multiple_request = self.tr_multiple_training_id



    urgensi = fields.Selection([
        ('a','A'),
        ('b','B'),
        ('c','C')
    ])
    advance_ids = fields.One2many('account.payment', 'training_request_id', string='Advances', copy=False)

    @api.depends('state', 'advance_ids.state', 'voucher_ids.state')
    def _get_advanced(self):
        cc=0
        for rec in self:
            rec.advance_count = len(rec.advance_ids)
            rec.voucher_count = len(rec.voucher_ids)

    lama_pelatihan = fields.Integer('Lama Pelatihan')
    advance_count = fields.Integer(compute='_get_advanced', string='Advance', default=0)
    advance_amount = fields.Float('Cash Advance')

    @api.one
    def get_default_journal(self):
        if self._context.get('settlement'):
            journal_id = self.env['account.journal'].search([('company_id', '=', self.env.user.company_id.id), ('type', '=', 'purchase')], limit=1)
        else:
            journal_id = self.env['account.journal'].search([('company_id', '=', self.env.user.company_id.id), ('type', '=', 'cash')], limit=1)
        return journal_id

    @api.multi
    def action_view_advance(self):
        action = self.env.ref('account.view_account_supplier_payment_tree')
        result = action.read()[0]
        imd = self.env['ir.model.data']
        list_view_id = imd.xmlid_to_res_id('account.view_account_supplier_payment_tree')
        form_view_id = imd.xmlid_to_res_id('account.view_account_payment_form')
        result['domain'] = "[('id', 'in', " + str(self.advance_ids.ids) + ")]"
        return {
            'name': 'Advances',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'views': [[list_view_id, 'tree'], [form_view_id, 'form']],
            'domain' : result.get('domain'),
            'res_model': 'account.payment',
            'type': 'ir.actions.act_window',
        }

    @api.multi
    def submit(self):

        employee_id = self.employee_id.id
        if len(self.env['hr.contract'].search([('employee_id','=',employee_id)]).ids) < 1:
            raise ValidationError(_("This employee doesn't have contract!"))
        else:
            self.write({'state':'waiting_approval_hrd'})

    @api.multi
    def approve_hrd(self):
        from datetime import datetime
        self.approve_date = datetime.today()
        self.approve_by = self.env.user.name
        self.write({'state':'waiting_approval_direksi'})    

    @api.multi
    def approve_direksi(self):
        self.direksi_approved_date = datetime.today()        
        self.write({'state':'approved'})    


    @api.multi
    def cancel_training(self):
        self.write({'state':'cancel'})                               

    @api.multi
    def button_advance_create(self):
        self.ensure_one()
        if self.advance_amount <= 1:
            raise UserError(_("Amount cash advance must greater than 0 ! \n Cancel & set cash amount or continue without advance !"))
        self.write({'state':'submit_advance'})
        journal_id = self.get_default_journal()
        payment_method_id = journal_id[0].outbound_payment_method_ids and journal_id[0].outbound_payment_method_ids[0].id
        partner = self.employee_id.user_id.partner_id or self.employee_id.address_id
        data = {
            'payment_type': 'outbound',
            'partner_type': 'employee',
            'partner_id': partner and partner.id or self.user_id.partner_id.id,
            'advance_type': 'advance',
            'training_request_id': self.id,
            'journal_id': journal_id[0] and journal_id[0].id or False,
            'payment_date': fields.Date.today(),
            'payment_method_id': payment_method_id,
            'communication': 'Advance for ' + self.name,
            'amount': self.advance_amount}
        self.env['account.payment'].create(data)

class EmployeeTrainingExpense(models.Model):
    _name = 'employee.training.expense' 
    _description = 'Employee Training Expense'
    
    training_id = fields.Many2one('training.request', string="Training Reference", index=True, required=True, ondelete='cascade')
    product_id = fields.Many2one('product.product', 'Expense')
    name = fields.Char('Description')
    amount = fields.Float('Amount',compute='onchange_quantity_price_unit')
    quantity = fields.Float('Quantity', required=True)
    price_unit = fields.Float('Price Unit' , required=True )



    @api.onchange('quantity','price_unit')
    def onchange_quantity_price_unit(self):
        for line in self:
            line.amount = line.quantity * line.price_unit

    @api.onchange('product_id')
    def change_product_id(self):
        if not self.product_id:
            return
        self.name = self.product_id.display_name    


class MultipleTraining(models.Model):
    _name = 'multiple.training'

    name = fields.Char(string="Multiple Training Number", readonly=True, required=True, copy=False, default='New')

    @api.model
    def create(self, vals):
        if vals.get('name', 'New') == 'New':
            vals['name'] = self.env['ir.sequence'].next_by_code(
                'multiple.training') or 'New'
        result = super(MultipleTraining, self).create(vals)
        return result


    department_id = fields.Many2one('hr.department',string="Department")
    parent_id = fields.Many2one('hr.employee', string='Manager', compute="onchange_employee")
    multiple_training_line = fields.One2many('multiple.training.line', 'multiple_training_id')
    state = fields.Selection([
        ('draft','Draft'),
        ('waiting_approval','Waiting Approval'),
        ('approved','Approved')
    ],default='draft')

    @api.multi
    def confirm(self):
        self.write({'state':'waiting_approval'})

    @api.multi
    def approve(self):
        for training in self.multiple_training_line:
            qq=0
            obj_training = self.env['training.request'].create({
                'employee_id': training.employee_id.id,
                'training_name': training.training_name,
                'urgensi': training.urgensi,
                'tr_multiple_training_id' : self.id
            })
        self.write({'state':'approved'})

    @api.onchange('department_id')
    def onchange_employee(self):
        for line in self:            
            line.parent_id =  line.department_id.manager_id 



class MultipleTrainingLine(models.Model):
    _name = 'multiple.training.line'

    multiple_training_id = fields.Many2one('multiple.training', 'Multiple Training')
    employee_id = fields.Many2one('hr.employee',string="Employee", required=True)
    job_id = fields.Many2one('hr.job',string="Job Position", compute='onchange_employee',store=True)
    training_name = fields.Char('Training Name')

    urgensi = fields.Selection([
        ('a','A'),
        ('b','B'),
        ('c','C')
    ])    


    @api.onchange('employee_id')
    def onchange_employee(self):
        for line in self:
            line.job_id = line.employee_id.job_id.id
            return {'domain': {'employee_id': [('id', 'in', line.multiple_training_id.department_id.member_ids.ids)]}}
