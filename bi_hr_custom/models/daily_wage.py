from odoo.exceptions import Warning
from odoo import models, fields, exceptions, api, _


class DailyWage(models.Model):
    _name = 'daily.wage'

    name = fields.Char(string="Name", readonly=True, required=True, copy=False, default='New')

    # period_start = fields.Date('Period Start', compute='periode_berubah')
    period_start = fields.Date('Period Start')
    
    
    # period_end = fields.Date('Period End', compute='periode_berubah')
    period_end = fields.Date('Period End')

    @api.onchange('period_start','period_end')
    def periode_berubah(self):
        if len(self.ids) < 2: 
            self.daily_wage_salary = False
  
            # for i in self.daily_wage_salary:
            #     i.id_daily_wage = False
            #     q=0


    reference = fields.Char('Reference')
    daily_wage_salary = fields.One2many('daily.salary', 'id_daily_wage')

    @api.model
    def create(self, vals):
        if vals.get('name', 'New') == 'New':
            vals['name'] = self.env['ir.sequence'].next_by_code('daily.wage') or 'New'
        result = super(DailyWage, self).create(vals)
        return result

    state = fields.Selection([
        ('draft', 'Draft'),
        ('waiting_approval', 'Waiting Approval'),
        ('approved', 'Approved'),
        ('canceled', 'Canceled'),
        ('refused', 'Refused'),
        ('paid', 'Paid'),
    ], string="Status", index=True, copy=False, default='draft', track_visibility='onchange')    

    @api.multi
    def button_confirm(self):
        self.ensure_one()
        self.write({'state':'waiting_approval'})

    @api.multi
    def button_approve(self):
        self.ensure_one()
        self.write({'state':'approved'})      

    @api.multi
    def button_refuse(self):
        self.ensure_one()
        self.write({'state':'refused'})     

    @api.multi
    def button_cancel(self):
        self.ensure_one()
        self.write({'state':'canceled'})       

    @api.multi
    def button_pay(self):
        self.ensure_one()
        self.write({'state':'paid'})           

    
class DailyWageSalaryComputation(models.Model):
    _name = 'daily.salary'

    @api.multi
    def write(self, vals):
        res = super(DailyWageSalaryComputation, self).write(vals)
        qq = 1
        return res

    @api.model
    def create(self, vals):
        qq = 1

        result = super(DailyWageSalaryComputation, self).create(vals)
        return result        

    id_daily_wage = fields.Many2one('daily.wage', 'Daily Wage')
    name = fields.Char('Name')

    employee_id = fields.Many2one('hr.employee', string="Employee" , required=True)

# customer_of_division_id = fields.Many2one(compute='_get_division_id', comodel_name='res.partner', string='Customer of the division', store=True)
 
# @api.depends('sale_id','partner_id')    
# def _get_division_id(self):
#     if self.sale_id:
#         self.customer_of_division_id = self.sale_id.customer_of_division_id.id        
#     elif self.partner_id:
#         self.customer_of_division_id = self.partner_id.customer_of_division_id.id



    job_id = fields.Many2one('hr.job', 'Job Position', store=True, compute='onchange_employee')
    joining_date = fields.Date(store=True, compute='onchange_employee')
    number_of_days = fields.Integer('Number of Days', store=True, compute='onchange_employee')
    daily_wage = fields.Float('Daily Wage', store=True, compute='onchange_employee')
    amount = fields.Float('Amount', store=True, compute='onchange_employee')

    overtime_hours = fields.Float('Overtime Hours', store=True, compute='onchange_employee')

    overtime_wages_hours = fields.Float('Overtime Wages/Hours', store=True, compute='onchange_employee')
    overtime_amount = fields.Float('Overtime Amount', store=True, compute='onchange_employee')
    sub_total = fields.Float('Sub Total', store=True, compute='onchange_employee')

    @api.onchange('employee_id')
    def onchange_employee(self):
        for emp in self:
            id_employee = emp.employee_id.id
            if id_employee:
                emp.job_id = emp.employee_id.job_id
                emp.joining_date = emp.employee_id.joining_date
                attendance = self.env['hr.attendance'].search([('check_in', '>=', emp.id_daily_wage.period_start), ('check_in', '<=', emp.id_daily_wage.period_end), ('employee_id', '=', id_employee)])
                is_ho = emp.employee_id.project_id.cuti_per_bulan
                if attendance:
                    if is_ho:
                        emp.number_of_days = len(attendance) / 8
                    else:
                        emp.number_of_days = len(attendance)
                emp.daily_wage = emp.employee_id.contract_id.wage
                emp.amount = emp.number_of_days * emp.daily_wage

                obj_overtime = self.env['hr.overtime'].search([('date_from', '>=', emp.id_daily_wage.period_start), ('date_from', '<=', emp.id_daily_wage.period_start), ('employee_id', '=', id_employee)])

                emp.overtime_hours = sum(obj_overtime.mapped('number_of_hours'))
                if is_ho:
                    emp.overtime_wages_hours = 1 / 173 * emp.daily_wage * 21
                else:
                    emp.overtime_wages_hours = 1 / 173 * emp.daily_wage * 25
                emp.overtime_amount = emp.overtime_hours * emp.overtime_wages_hours
                emp.sub_total = emp.amount + emp.overtime_amount

