# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo.exceptions import Warning
from odoo import models, fields, exceptions, api, _

class AdvanceExpense(models.Model):
    _name = 'advance.expense'
    _inherit = ['advance.expense', 'mail.thread', 'mail.activity.mixin']


    @api.multi
    def _attachment_count(self):
        attachment_obj = self.env['ir.attachment']
        for record in self:
            record.attachment_count = attachment_obj.search_count([('res_model', '=', 'advance.expense'),
                                                                    ('res_id', '=', record.id)])

    attachment_count = fields.Integer(compute='_attachment_count', string='Attachments')
    employee_id = fields.Many2one('hr.employee',string="Employee Name" ,required=True)
    departure_date = fields.Date(string="Departure Date")
    return_date = fields.Date(string="Return Date")
    accompanied_by_id = fields.Many2one('res.users','Accompanied By')
    destination = fields.Text('Destination')

    @api.multi
    def open_attachment(self):
        xml_id = 'mail.view_document_file_kanban'
        kanban_view_id = self.env.ref(xml_id).id
        xml_id = 'base.view_attachment_tree'
        tree_view_id = self.env.ref(xml_id).id
        xml_id = 'base.view_attachment_form'
        form_view_id = self.env.ref(xml_id).id
        return {
            'name': _('Attachments'),
            'view_type': 'form',
            'view_mode': 'kanban,tree,form',
            'views': [(kanban_view_id, 'kanban'),(tree_view_id, 'tree'), (form_view_id, 'form')],
            'res_model': 'ir.attachment',
            'domain': [('res_id', 'in', self._ids),
                       ('res_model', '=', 'advance.expense')],
            'type': 'ir.actions.act_window',
        }