from odoo.exceptions import Warning
from odoo import models, fields, exceptions, api, _

class HrEvaluation(models.Model):
    _name = 'hr.evaluation'

    training_id = fields.Many2one('training.request', string="Training Reference", index=True, required=True, ondelete='cascade')
    kriteria_penilaian = fields.Char(string='Kriteria Penilaian',readonly="True")
    nilai = fields.Integer('Nilai')
    bobot = fields.Float('Bobot',store="True")
    nilai_x_bobot = fields.Float('Nilai x bobot',store="True")
    nilai_akhir = fields.Float('Nilai Akhir',store="True")
    pengecek = fields.Char()
