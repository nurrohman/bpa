from odoo import models, fields, api, _


class RequestWizard(models.Model):
    _name = 'request.wizard'
    
    partner_id = fields.Many2one('res.partner', string='Vendor')
    date_planned = fields.Datetime('Schedule Date')
    line_ids = fields.One2many('request.wizard.line', 'request_wizard_id', string='Lines')
    picking_type_id = fields.Many2one('stock.picking.type', string='Picking Type')
    location_src_id = fields.Many2one('stock.location', string='Source Location')
    location_dest_id = fields.Many2one('stock.location', string='Destination Location')
    type = fields.Selection([
        ('purchase', 'Purchase Order'),
        ('picking', 'Internal Transfer'),
    ], string='Action', default='purchase')
    maintenance_request_id = fields.Many2one('maintenance.request', string='Maintenance Request')
    
    @api.onchange('type')
    def type_change(self):
        if not self.type:
            return {}

        new_lines = self.env['request.wizard.line']
        for line in self.maintenance_request_id.mapped('line_ids'):
            data = {
                'request_line_id': line.id,
                'qty': line.qty
            }
            new_line = new_lines.new(data)
            new_lines += new_line

        self.line_ids += new_lines
        return {}
    
    @api.onchange('picking_type_id')
    def change_piking_type(self):
        if not self.picking_type_id:
            return {}
        self.location_src_id = self.picking_type_id.default_location_src_id.id
        self.location_dest_id = self.picking_type_id.default_location_dest_id.id
        
    @api.multi
    def action_confirm(self):
        for wizard in self:
            if wizard.type == 'purchase':
                wizard.action_purchase_create()
            else:
                wizard.action_picking_create()
    
    @api.multi
    def action_purchase_create(self):
        purchase = self.env['purchase.order'].create({
                'partner_id': self.partner_id.id,
                'date_planned': self.date_planned,
                'notes': self.maintenance_request_id.name
            })
        for line in self.line_ids:
            self.env['purchase.order.line'].create({
                    'order_id': purchase.id,
                    'name': line.product_id.display_name + ' : %s' % (self.maintenance_request_id.name),
                    'product_id': line.product_id.id,
                    'date_planned': self.date_planned,
                    'product_qty': line.qty,
                    'product_uom': line.uom_id.id,
                    'price_unit': 0,
                })
        self.maintenance_request_id.purchase_id = purchase.id
        
    @api.multi
    def action_picking_create(self):
        picking = self.env['stock.picking'].create({
                'partner_id': self.env.user.partner_id.id,
                'picking_type_id': self.picking_type_id.id,
                'location_id': self.location_src_id.id,
                'location_dest_id': self.location_dest_id.id,
            })
        for line in self.line_ids:
            self.env['stock.move'].create({
                    'picking_id': picking.id,
                    'product_id': line.product_id.id,
                    'product_uom_qty': line.qty,
                    'name': line.product_id.display_name + ' : %s' % (self.maintenance_request_id.name),
                    'product_uom': line.uom_id.id,
                    'location_id': self.location_src_id.id,
                    'location_dest_id': self.location_dest_id.id
                })
        picking.action_confirm()
        picking.action_assign()
        self.maintenance_request_id.picking_id = picking.id


class RequestWizardLine(models.Model):
    _name = 'request.wizard.line'
    
    request_wizard_id = fields.Many2one('request.wizard', string='Request WIzard', ondelete='cascade')
    request_line_id = fields.Many2one('maintenance.request.line', string='Request Line')
    product_id = fields.Many2one(related='request_line_id.product_id', string='Product')
    qty = fields.Float('Quantity', default=1)
    uom_id = fields.Many2one(related='request_line_id.uom_id', string='Unit of Measure')
    hours = fields.Float(related='request_line_id.hours', string='Hours')

