# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError


class MaintenanceEquipment(models.Model):
    _inherit = 'maintenance.request'
    
    custom_request_job_id = fields.Many2one(
        'project.task',
        string='Job Order',
        readonly=True,
        copy=False,
    )
    
    state = fields.Selection([
        ('new', 'New Request'),
        ('in_progress', 'In Progress'),
        ('repaired', 'Repaired'),
        ('scrap', 'Scrap'),
        ('cancel', 'Cancelled'),
    ], string='Status', copy=False, default='new', group_expand='_expand_states', track_visibility='onchange')
    
    def _expand_states(self, states, domain, order):
        return [key for key, val in type(self).state.selection]    
    
    @api.multi
    def approve_maintenance_request(self):
        self.write({'state': 'in_progress'})
        
    @api.multi
    def repaired_maintenance_request(self):
        self.write({'state': 'repaired', 'close_date': fields.Date.today()})
        
    @api.multi
    def scrap_maintenance_request(self):
        self.write({'state': 'scrap'})
        
    @api.multi
    def reset_equipment_request(self):
        super(MaintenanceEquipment, self).reset_equipment_request()
        self.write({'state': 'new'})
        
    @api.multi
    def archive_equipment_request(self):
        super(MaintenanceEquipment, self).archive_equipment_request()
        self.write({'state': 'cancel'})
        
    line_ids = fields.One2many('maintenance.request.line', 'request_id', string='Lines')
    purchase_id = fields.Many2one('purchase.order', string='Purchase Order')
    picking_id = fields.Many2one('stock.picking', string='Picking')
    
    @api.multi
    def action_create_purchase(self):
        self.ensure_one()
        ctx = self._context.copy()
        if self.purchase_id:
            raise UserError(_('Purchase Request Already Created !'))
        ctx.update({
            'default_maintenance_request_id': self.id,
            'default_type': 'purchase'
        })
        return {
            'name': _('Purchase Orders'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'request.wizard',
            'target': 'new',
            'context': ctx,
        }
    
    @api.multi
    def action_create_picking(self):
        self.ensure_one()
        ctx = self._context.copy()
        if self.picking_id:
            raise UserError(_('Internal Transfer Already Created !'))
        ctx.update({
            'default_maintenance_request_id': self.id,
            'default_type': 'picking'
        })
        return {
            'name': _('Internal Transfers'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'request.wizard',
            'target': 'new',
            'context': ctx,
        }

    
class MaintenanceEquipmentLine(models.Model):
    _name = 'maintenance.request.line'
    _description = 'Maintenance Request Line'
    
    request_id = fields.Many2one('maintenance.request', string='Maintenance Request', ondelete='cascade')
    name = fields.Char('Description')
    action = fields.Selection([
        ('purchase', 'Purchase Order'),
        ('stock', 'Internal Transfer'),
    ], string='Action', default='purchase')
    
    product_id = fields.Many2one('product.product', string='Product')
    qty = fields.Float('Quantity', default=1)
    uom_id = fields.Many2one('product.uom', string='Unit of Measure')
    hours = fields.Float('Hours')
    
    @api.onchange('product_id')
    def change_product_id(self):
        if not self.product_id:
            return {}
        self.name = self.product_id.display_name
        self.uom_id = self.product_id.uom_id.id
        
