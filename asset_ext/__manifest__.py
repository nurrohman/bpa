# -*- coding: utf-8 -*-
{
    'name': 'Custom Module Aset for BPA',
    'version': "1.0",
    'price': 9999.0,
    'currency': 'IDR',
    'summary': 'This module Aset for BPA',
    'description': """This module Aset for BPA""",
    'category': 'Custom',
    'author': 'PT. VISI',
    'website': 'www.visi.co.id',
    'images': [],
    'depends': [
        'account_invoicing',
        'account_asset',
    ],
    'data': [
        'wizard/asset_wizard_view.xml',
        'views/account_asset_view.xml',
    ],
    "installable": True,
    "application": False,
}
