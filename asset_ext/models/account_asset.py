# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError
from odoo.tools.float_utils import float_compare
from datetime import datetime

class ParentAssetCategory(models.Model):
    _name = 'parent.asset.category'
    
    name = fields.Char('Name', required=True)
    
class AccountAssetCategory(models.Model):
    _inherit = 'account.asset.category'
    
    categ_id = fields.Many2one('parent.asset.category', 'Category')
    account_id = fields.Many2one('account.account', string="Asset Account")
    suspend_account_id = fields.Many2one('account.account', string="Suspend Account")
    gain_account_id = fields.Many2one('account.account', string="Disposal Gain Account")
    loss_account_id = fields.Many2one('account.account', string="Disposal Loss Account")
    sequence_id = fields.Many2one('ir.sequence', 'Asset Sequence')
    
    @api.model
    def create(self, vals):
        if not vals.get('sequence_id'):
            vals.update({'sequence_id': self.sudo()._create_sequence(vals).id})
        categ = super(AccountAssetCategory, self).create(vals)
        return categ
            
    @api.model
    def _create_sequence(self, vals):
        prefix = vals['name'].upper()
        seq = {
            'name': vals['name'],
            'implementation': 'no_gap',
            'prefix': prefix,
            'padding': 4,
            'number_increment': 1,
            'use_date_range': True,
        }
        if 'company_id' in vals:
            seq['company_id'] = vals['company_id']
        return self.env['ir.sequence'].create(seq)
    
class AccountAsset(models.Model):
    _inherit = 'account.asset.asset'
    
    @api.one
    @api.depends('move_line_ids')
    def _get_invoices(self):
        self.invoice_ids = self.move_line_ids.mapped('invoice_id')
        self.invoices_count = len(self.invoice_ids)
        return
    
#     @api.one
#     @api.depends('account_move_ids')
#     def _can_draft(self):
#         self.can_draft = bool(self.account_move_ids)
#         return
    
    @api.one
    @api.depends('value', 'salvage_value', 'depreciation_line_ids.move_check', 'depreciation_line_ids.amount')
    def _get_acc_depreciation(self):
        total_amount_this_year = total_amount_before_year = 0.0
        this_year = datetime.now().year
        for line in self.depreciation_line_ids.filtered(lambda d:datetime.strptime(d.depreciation_date, '%Y-%m-%d').year == this_year):
            if line.move_check:
                total_amount_this_year += line.amount
        for line in self.depreciation_line_ids.filtered(lambda d:datetime.strptime(d.depreciation_date, '%Y-%m-%d').year != this_year):
            if line.move_check:
                total_amount_before_year += line.amount
        self.acc_depreciation_this_year = total_amount_this_year
        self.acc_depreciation_before_year = total_amount_before_year
        
    invoice_ids = fields.Many2many('account.invoice', string="Invoices", compute="_get_invoices", store=False)
    invoices_count = fields.Integer('#Invoices', compute="_get_invoices")
    move_line_ids = fields.One2many('account.move.line', 'asset_id', string="Account Move Line")
    invoice_disposal_id = fields.Many2one('account.invoice', 'Sale Invoice')
    value = fields.Float(string='Gross Value', required=False, readonly=True, digits=0, states={'draft': [('readonly', False)]}, oldname='purchase_value')
#     can_draft = fields.Boolean('Technical Fields to show draft button', compute="_can_draft")
    seq_number = fields.Char('Sequence Number')
    acc_depreciation_this_year = fields.Float(compute=_get_acc_depreciation, string='Acc Depreciation this year')
    acc_depreciation_before_year = fields.Float(compute=_get_acc_depreciation, string='Acc Depreciation before year')
    
    @api.model
    def create(self, vals):
        if not vals.get('seq_number'):
            categ_id = vals.get('category_id', False)
            categ = self.env['account.asset.category'].browse(categ_id)
            vals['seq_number'] = categ.sequence_id.with_context(ir_sequence_date=vals.get('date')).next_by_id()
        return super(AccountAsset, self.with_context(mail_create_nolog=True)).create(vals)
    
    @api.multi
    def add_account_move_line(self):
        return {
             'name': _('Add Invoices/Related Cost'),
             'type': 'ir.actions.act_window',
             'view_type': 'form',
             'view_mode': 'form',
             'res_model': 'asset.wizard',
             'target': 'new',
             'context' : {'default_asset_id' : self.id,
                         'form_view_ref' : 'asset_ext.asset_wizard_cost_form'}
         }
            
    @api.multi
    def set_to_close(self):
        return {
             'name': _('Sell Asset'),
             'type': 'ir.actions.act_window',
             'view_type': 'form',
             'view_mode': 'form',
             'res_model': 'asset.wizard',
             'target': 'new',
             'context': {'default_asset_id' : self.id,
                         'form_view_ref' : 'asset_ext.asset_wizard_sell_form'}
         }
        
    @api.multi
    def action_confirm(self):
        if self.move_line_ids.filtered(lambda ml: not ml.reconciled):
            return {
                 'name': _('Validate'),
                 'type': 'ir.actions.act_window',
                 'view_type': 'form',
                 'view_mode': 'form',
                 'res_model': 'asset.wizard',
                 'target': 'new',
                 'context': {'default_asset_id' : self.id,
                             'form_view_ref' : 'cam.asset_wizard_confirm_form'}
             }

    @api.multi
    def view_invoices(self):
        return {
             'name': _('Vendor Bills'),
             'type': 'ir.actions.act_window',
             'view_type': 'form',
             'view_mode': 'tree,form',
             'res_model': 'account.invoice',
             'domain' : "[('id', 'in', %s)]" % self.invoice_ids.ids,
         }
        
    @api.multi
    def reclass(self, date=fields.Date.today()):
        self.ensure_one()
        for line in self.move_line_ids.filtered(lambda ml: not ml.reconciled):
            move = self.env['account.move'].create({
                'ref': 'Reclass of Suspended (%s) %s' % (self.seq_number, self.name,),
                'date': date or False,
                'journal_id': self.category_id.journal_id.id,
                'asset_id' : False
                })
            move_line = self.env['account.move.line'].with_context({'check_move_validity':False}).create({
                'name': 'Reclass ' + line.name,
                'account_id': self.category_id.suspend_account_id.id,
                'debit': 0.0,
                'credit': line.balance,
                'currency_id': line.currency_id.id,
                'amount_currency': line.amount_currency and -line.amount_currency or 0.0,
                'move_id':move.id
            })
            (line + move_line).reconcile()
            self.env['account.move.line'].with_context({'check_move_validity':False}).create({
                'name': 'Reclass ' + line.name,
                'account_id': self.category_id.account_id.id,
                'debit': line.balance,
                'credit': 0.0,
                'currency_id': line.currency_id.id,
                'amount_currency': line.amount_currency and -line.amount_currency or 0.0,
                'move_id':move.id
            })
            move.post()
        return
    
    @api.model
    def _validate_asset_all(self):
        assets = self.env['account.asset.asset'].search([('state', '=', 'draft')])
        for asset in assets:
            asset.validate()
        return
    
class AccountAssetDepreciationLine(models.Model):
    _inherit = 'account.asset.depreciation.line'
    
    @api.multi
    def create_move(self, post_move=True):
        created_moves = self.env['account.move']
        for line in self:
            depreciation_date = self.env.context.get('depreciation_date') or line.depreciation_date or fields.Date.context_today(self)
            company_currency = line.asset_id.company_id.currency_id
            current_currency = line.asset_id.currency_id
            amount = current_currency.compute(line.amount, company_currency)
            sign = (line.asset_id.category_id.journal_id.type == 'purchase' or line.asset_id.category_id.journal_id.type == 'sale' and 1) or -1
            asset_name = line.asset_id.name + ' (%s/%s)' % (line.sequence, len(line.asset_id.depreciation_line_ids))
            reference = line.asset_id.code
            journal_id = line.asset_id.category_id.journal_id.id
            partner_id = line.asset_id.partner_id.id
            categ_type = line.asset_id.category_id.type
            credit_account = line.asset_id.category_id.account_asset_id.id
            debit_account = line.asset_id.category_id.account_depreciation_id.id
            prec = self.env['decimal.precision'].precision_get('Account')
            move_line_1 = {
                'name': asset_name,
                'account_id': credit_account,
                'debit': 0.0 if float_compare(amount, 0.0, precision_digits=prec) > 0 else -amount,
                'credit': amount if float_compare(amount, 0.0, precision_digits=prec) > 0 else 0.0,
                'journal_id': journal_id,
                'partner_id': partner_id,
                'currency_id': company_currency != current_currency and current_currency.id or False,
                'amount_currency': company_currency != current_currency and -sign * line.amount or 0.0,
                'analytic_account_id': line.asset_id.category_id.account_analytic_id.id if categ_type == 'sale' else False,
                'date': depreciation_date,
            }
            move_line_2 = {
                'name': asset_name,
                'account_id': debit_account,
                'credit': 0.0 if float_compare(amount, 0.0, precision_digits=prec) > 0 else -amount,
                'debit': amount if float_compare(amount, 0.0, precision_digits=prec) > 0 else 0.0,
                'journal_id': journal_id,
                'partner_id': partner_id,
                'currency_id': company_currency != current_currency and current_currency.id or False,
                'amount_currency': company_currency != current_currency and sign * line.amount or 0.0,
                'analytic_account_id': line.asset_id.category_id.account_analytic_id.id if categ_type == 'purchase' else False,
                'date': depreciation_date,
            }
            move_vals = {
                'ref': reference,
                'date': depreciation_date or False,
                'journal_id': line.asset_id.category_id.journal_id.id,
                'line_ids': [(0, 0, move_line_1), (0, 0, move_line_2)],
                'asset_id': line.asset_id.id,
                }
            move = self.env['account.move'].create(move_vals)
            line.write({'move_id': move.id, 'move_check': True})
            created_moves |= move

        if post_move and created_moves:
            created_moves.filtered(lambda r: r.asset_id and r.asset_id.category_id and r.asset_id.category_id.open_asset).post()
        return [x.id for x in created_moves]

class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'
    
    asset_id = fields.Many2one('account.asset.asset', string='Asset')
    
