# -*- coding: utf-8 -*-

from odoo import models, fields, api, _


class PurchaseRequisitionLine(models.Model):
    _inherit = 'material.purchase.requisition.line'

    @api.multi
    @api.constrains('custom_job_costing_line_id', 'qty')
    def check_quantity(self):
        for line in self:
            if line.custom_job_costing_line_id.actual_requisition_qty > line.custom_job_costing_line_id.product_qty:
                raise UserWarning(_('Unable to request more than planned quantity!'))

    @api.multi
    @api.constrains('custom_job_costing_line_id', 'product_id')
    def check_product(self):
        for line in self:
            if line.custom_job_costing_line_id and line.custom_job_costing_line_id.product_id.id != line.product_id.id:
                raise UserWarning(_('Unable to request different product!'))
    
    custom_job_costing_id = fields.Many2one(
        'job.costing',
        string='Job Cost Center',
    )
    custom_job_costing_line_id = fields.Many2one(
        'job.cost.line',
        string='Job Cost Line',
    )
    
    @api.onchange('custom_job_costing_id', 'custom_job_costing_line_id')
    def onchange_job_costing(self):
        if not self.custom_job_costing_id and not self.custom_job_costing_line_id:
            return {}
        elif self.custom_job_costing_id and not self.custom_job_costing_line_id:
            return {'domain' : {'product_id' : [('id', 'in', self.custom_job_costing_id.mapped('job_cost_line_ids.product_id').ids)]}}
        elif self.custom_job_costing_line_id:
            self.product_id = self.custom_job_costing_line_id.product_id.id
            return {'domain' : {'product_id' : [('id', '=', self.custom_job_costing_line_id.product_id.id)]}}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
