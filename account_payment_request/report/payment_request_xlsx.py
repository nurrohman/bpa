from odoo import models, _
import operator
import xlwt
from odoo.exceptions import UserError


class PaymentRequest(models.AbstractModel):
    _name = 'report.account_payment_request.payment_request_xlsx'
    _inherit = 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, data, objects):
        columns = [
              ['No.', 5],
              ['Vendor', 18],
              ['Invoice Number', 15],
              ['Internal Number', 15],
              ['Invoice Date', 13],
              ['Amount', 19],
              ['To Pay', 19],
              ['Company', 18],
              ['Due Date', 10],
        ]
        style_hdr_blue = workbook.add_format({'align': 'center', 'border': 1, 'bg_color': 'cyan'})
        style_yellow_center = workbook.add_format({'align': 'center', 'border': 1, 'bg_color': 'yellow'})
        style_normal_center = workbook.add_format({'align': 'center', 'border': 1})
        style_bold_center = workbook.add_format({'align': 'center', 'bold': True})
        style_normal = workbook.add_format({'border': 1, 'num_format': '#,##0'})
        style_normal_bold = workbook.add_format({'border': 1, 'bold': True, 'num_format': '#,##0'})
        
        for obj in objects:
            data = obj.action_print_datas()
            report_name = 'Payment Request'
            ws = workbook.add_worksheet(report_name[:31])
            ws.freeze_panes(4, 0)
            
            ws.merge_range('A1:I1', 'PAYMENT REQUEST %s - %s' % (data['reference'], data['company']), style_normal_bold)
            ws.merge_range('A3:B3', 'Date', style_hdr_blue)
            ws.merge_range('C3:D3', 'Requested By', style_hdr_blue)
            ws.merge_range('E3:F3', 'Approved By', style_hdr_blue)
            ws.merge_range('G3:H3', 'State', style_hdr_blue)
#             ws.row(3).height_mismatch = True
#             ws.row(3).height = 20 * 28
            ws.merge_range('A4:B4', data['date'], style_normal_center)
            ws.merge_range('C4:D4', data['requestor'], style_normal_center)
            ws.merge_range('E4:F4', data['approval'], style_normal_center)
            ws.merge_range('G4:H4', data['state'], style_normal_center)
            
            row_count = 5
            col_count = 0
            for column in columns:
                ws.set_column(col_count, col_count, column[1]) 
                ws.write(row_count, col_count, column[0], style_hdr_blue)
                col_count += 1
            row_count += 1
            number = 1
            for vendor in data['line_request']:
                for invoice_date in data['line_request'][vendor]:
                    for company in data['line_request'][vendor][invoice_date]:
                        ws.write(row_count, 0, number, style_normal) 
                        ws.write(row_count, 1, vendor, style_normal) 
                        amount_start = xlwt.Utils.rowcol_to_cell(row_count, 5)
                        residual_start = xlwt.Utils.rowcol_to_cell(row_count, 6)
                        for value in  data['line_request'][vendor][invoice_date][company]:
                            ws.write(row_count, 2, value[0], style_normal)
                            ws.write(row_count, 3, value[4] or '', style_normal)
                            ws.write(row_count, 4, invoice_date, style_normal)
                            ws.write(row_count, 5, value[1], style_normal)
                            ws.write(row_count, 6, value[2], style_normal)
                            ws.write(row_count, 8, value[3], style_normal)
                            row_count += 1
                        amount_stop = xlwt.Utils.rowcol_to_cell(row_count - 1, 5)
                        residual_stop = xlwt.Utils.rowcol_to_cell(row_count - 1, 6)
                        if amount_start == amount_stop:
                            ws.write(row_count - 1, 7, company, style_normal)
                        else:
                            ws.write(row_count, 5, '=sum(' + amount_start + ':' + amount_stop + ')', style_normal_bold)
                            ws.write(row_count, 6, '=sum(' + residual_start + ':' + residual_stop + ')', style_normal_bold)
                            ws.write(row_count, 7, company, style_normal)
                            row_count += 1
                        row_count += 1
                        number += 1
            row_count += 1
            
            if data['line_budget']:
                ws.merge_range('A%s:I%s' % (row_count, row_count), 'Mohon untuk dianggarkan terlebih dahulu', style_yellow_center)
                col_count = 0
                for column in columns:
                    ws.set_column(col_count, col_count, column[1]) 
                    ws.write(row_count, col_count, column[0], style_hdr_blue)
                    col_count += 1
                row_count += 1
                number = 1
                
            for vendor in data['line_budget']:
                for invoice_date in data['line_budget'][vendor]:
                    for company in data['line_budget'][vendor][invoice_date]:
                        ws.write(row_count, 0, number, style_normal) 
                        ws.write(row_count, 1, vendor, style_normal) 
                        amount_start = xlwt.Utils.rowcol_to_cell(row_count, 5)
                        residual_start = xlwt.Utils.rowcol_to_cell(row_count, 6)
                        for value in  data['line_budget'][vendor][invoice_date][company]:
                            ws.write(row_count, 2, value[0], style_normal)
                            ws.write(row_count, 3, value[4] or '', style_normal)
                            ws.write(row_count, 4, invoice_date, style_normal)
                            ws.write(row_count, 5, value[1], style_normal)
                            ws.write(row_count, 6, value[2], style_normal)
                            ws.write(row_count, 8, value[3], style_normal)
                            row_count += 1
                        amount_stop = xlwt.Utils.rowcol_to_cell(row_count - 1, 5)
                        residual_stop = xlwt.Utils.rowcol_to_cell(row_count - 1, 6)
                        if amount_start == amount_stop:
                            ws.write(row_count - 1, 7, company, style_normal)
                        else:
                            ws.write(row_count, 5, '=sum(' + amount_start + ':' + amount_stop + ')', style_normal_bold)
                            ws.write(row_count, 6, '=sum(' + residual_start + ':' + residual_stop + ')', style_normal_bold)
                            ws.write(row_count, 7, company, style_normal)
                            row_count += 1
                        row_count += 1
                        number += 1
            
            






















