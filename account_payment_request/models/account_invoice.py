# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'
    
    request_id = fields.Many2one('account.payment.request', "Payment Request", copy=False)
    request_status = fields.Selection(related='request_id.state', string="Request Status", store=True)
    date_payment_plan = fields.Date('Payment Plan Date')
    tax_validity = fields.Selection([
            (0, 'Not Valid'),
            (1, 'Valid'),
        ], index=True, default=0, copy=False, string='Tax Status', track_visibility='onchange')
    
    @api.multi
    def button_tax_valid(self):
        self.write({'tax_validity' : 1})
        
    @api.multi
    def action_payment_plan_date(self):
        return {
            'name': 'Assign Payment Date Plan',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'payment.request.wizard',
            'type': 'ir.actions.act_window',
            'target' : 'new',
            'context' : {'active_ids':self.ids}
        }
    
    @api.multi
    def action_payment(self):
        return {
            'name': 'Register Payment',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'account.payment',
            'type': 'ir.actions.act_window',
            'domain' : "[('partner_type', '=', 'supplier'), ('payment_type', '!=', 'transfer'), ('method_type','=','liquid')]",
            'context' : {'default_payment_type': 'outbound',
                         'default_partner_id':self.partner_id.id,
                         'default_payment_date' : self.date_payment_plan}
        }