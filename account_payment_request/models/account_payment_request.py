from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class AccountPaymentRequest(models.Model):
    _name = 'account.payment.request'
    _inherit = ['mail.thread']

    READONLY_STATES = {
        'draft': [('readonly', False)],
    }

    name = fields.Char('Name', default='/')
    date_request = fields.Date(string='Date Requested', readonly=True, states=READONLY_STATES, default=fields.Date.today(),)
    date_planned = fields.Date(string='Date Planned', readonly=True, states={'waiting':[('readonly', False)]}, copy=False)
    date_payment = fields.Date(string='Date Payment', copy=False)
    user_id = fields.Many2one('res.users', string='Requested By', readonly=True, states=READONLY_STATES, default=lambda self: self.env.user, copy=False)
    user_approve_id = fields.Many2one('res.users', 'Approved By', readonly=True, copy=False)
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.user.company_id, copy=False)
    partner_id = fields.Many2one('res.partner', 'Supplier', readonly=True, states=READONLY_STATES)
    journal_id = fields.Many2one('account.journal', 'Bank plan to pay', readonly=True, states=READONLY_STATES)
    
    state = fields.Selection([
            ('draft', 'Draft'),
            ('waiting', 'Waiting Approval'),
            ('approve', 'Approved'),
            ('paid', 'Paid'),
            ('close', 'Close'),
            ('cancel', 'Cancelled'),
        ], string='Status', default='draft',
        help="""
            Draft - ketika Staf Accounting membuat dokumen pertama kali
            Waiting - ketika Staf Accounting menunggu approval dari Manager Finance
            Approved - ketika Manager Finance sudah melakukan approval
            Paid - ketika Staf Accounting sudah melakukan transaksi pembayaran
        """,
        track_visibility='onchange', copy=False)

    invoice_ids = fields.Many2many(
        comodel_name='account.invoice',
        relation='payment_request_invoice_rel',
        column1='request_id',
        column2='invoice_id',
        string='Invoice',
        copy=False,
        readonly=True,
        states=READONLY_STATES,
        )

    @api.multi
    def action_cancel(self):
        for doc in self:
            doc.state = 'cancel'

            # Lepas Flag Semua
            for inv_id in doc.invoice_ids:
                inv_id.request_id = False

    @api.multi
    def action_to_draft(self):
        for doc in self:
            doc.state = 'draft'

    @api.multi
    def action_to_waiting(self):
        self.ensure_one()
        for doc in self:
            if doc.name == '/':
                doc.name = self.env['ir.sequence'].next_by_code('payment.request')
            doc.state = 'waiting'
            # Cek dulu ada yang udah nyangkut
            inv_bermasalah = doc.invoice_ids.filtered(lambda x: x.request_id)
            if inv_bermasalah:
                invoice_and_request = ""
                for inv_doc in inv_bermasalah:
                    invoice_and_request += "Vendor Bill : %s - Payment Request : %s (%s) \n" % (inv_doc.number, inv_doc.request_id.name, inv_doc.request_id.state)
                message = '''%s Vendor Bill(s) sudah memiliki Payment Request
                    %s
                ''' % (len(inv_bermasalah), invoice_and_request)
                raise UserError(message)

            # Kasih Flag Semua
            for inv_id in doc.invoice_ids:
                inv_id.request_id = self.id

    @api.multi
    def action_approve(self):
        for doc in self:
            if not doc.date_planned:
                raise UserError("Dokumen ini membutuhkan kolom Date Planned dalam keadaan terisi")
            if doc.date_request > doc.date_planned:
                raise UserError("Tanggal pada kolom Date Planned harus lebih dibanding kolom tanggal pada Date Requst")
            if not doc.journal_id:
                raise UserError("Dokumen ini membutuhkan data Bank Plan to Pay untuk keperluan forecast !")
            doc.user_approve_id = self.env.user
            doc.state = 'approve'

    @api.multi
    def action_pay(self):
        for doc in self:
            form_view_id = self.env.ref('account.view_account_payment_from_invoices')
            invoice_ids_amount = sum(self.invoice_ids.mapped('residual_signed'))
            invoice_ids_to_pay = [invoice_id.id for invoice_id in self.invoice_ids]
            return {
                'name': 'Pembayaran AP - (Multiple)',
                'view_type': 'form',
                'view_mode': 'form',
                'views': [[form_view_id.id, 'form']],
                'context': {
                    'default_payment_type': 'outbound',
                    'active_ids': invoice_ids_to_pay,
                    'request_id': self.id,
                    'default_communication': self.name,
                },
                'res_model': 'account.register.payments',
                'type': 'ir.actions.act_window',
                'target': 'new',
            }
            
    @api.multi
    def action_paygiro(self):
        for doc in self:
            form_view_id = self.env.ref('account_payment_request.account_giropayment_payable_form_request')
            invoice_ids_amount = sum(self.invoice_ids.mapped('residual_signed'))
            return {
                'name': 'Pembayaran Giro - (Multiple)',
                'view_type': 'form',
                'view_mode': 'form',
                'views': [[form_view_id.id, 'form']],
                'context': {
                    'default_partner_id': self.partner_id.id,
                    'default_giro_journal_id': self.journal_id.id,
                    'default_date': fields.Date.today(),
                    'default_request_id': self.id,
                    'default_invoice_ids': [(6, 0, self.invoice_ids.filtered(lambda i: i.state == 'open').ids)],
                    'default_amount': invoice_ids_amount,
                    'default_communication': self.name,
                    'default_dir_type': 'out'
                },
                'res_model': 'account.giropayment',
                'type': 'ir.actions.act_window',
                'target': 'new',
            }
    
    @api.multi
    def unlink(self):
        for doc in self:
            raise UserError("Silakan di set to draft untuk mengedit data payment request !")
        return super(AccountPaymentRequest, self).unlink()

    @api.multi
    def after_payment_callback(self):
        for doc in self:
            doc.write({'state': 'paid', 'date_payment': fields.Date.today()})
            for inv_id in doc.invoice_ids:
                inv_id.request_id = False

    def payment_request_get_journal(self):
        domain = ['|', ('name', 'ilike', self.name), ('ref', 'ilike', self.name)]
        return {
            'name': 'Related Journal Entry',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'domain' : domain,
            'res_model': 'account.move',
            'type': 'ir.actions.act_window',
            'context': {'create': False, 'edit': False }
        }
    
    def action_view_giropayment(self):
        domain = [('id', 'in', self.invoice_ids.mapped('giro_ids').filtered(lambda g: g.dir_type == 'out').ids)]
        view_id = self.env.ref('account_giropayment.account_giropayment_payables_tree').id
        form_view_id = self.env.ref('account_giropayment.account_giropayment_payable_form').id
        return {
            'name': 'Giro payments ' + self.name,
            'view_type': 'form',
            'view_mode': 'tree,form',
            'views': [[view_id, 'list'], [form_view_id, 'form']],
            'domain' : domain,
            'context': {
                'context': {
                    'default_request_id': self.id,
                    'default_communication': self.name,
                },
            },
            'res_model': 'account.giropayment',
            'type': 'ir.actions.act_window',
        }
        
    @api.multi
    def print_payment_request(self):
        value = {
            'sum_amount_total' : sum(self.invoice_ids.mapped('amount_total')),
            'sum_residual' : sum(self.invoice_ids.mapped('residual')),
        }
        return value
