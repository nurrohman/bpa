from odoo import models, fields, api, _
from odoo.exceptions import UserError


class AccountGiroPayemnt(models.Model):
    _inherit = 'account.giropayment'
    
    request_id = fields.Many2one('account.payment.request', 'Payment Request')
    
    @api.multi
    def action_giro_validate(self):
        res = super(AccountGiroPayemnt, self).action_giro_validate()
        if self.request_id:
            self.request_id.state = 'paid'
        return res
