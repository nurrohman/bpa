# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import json
from odoo import fields, models, api, _
import odoo.addons.decimal_precision as dp
from odoo.exceptions import UserError
from odoo.tools.float_utils import float_is_zero


class AccountVoucher(models.Model):
    _name = 'account.voucher'
    _description = 'Accounting Voucher'
    _inherit = ['mail.thread']
    _order = "date desc, id desc"
    
    @api.one
    @api.depends('move_id.line_ids.reconciled', 'move_id.line_ids.account_id.internal_type')
    def _check_paid(self):
        self.paid = any([((line.account_id.internal_type, 'in', ('receivable', 'payable')) and line.reconciled) for line in self.move_id.line_ids])

    @api.one
    @api.depends('journal_id', 'company_id')
    def _get_journal_currency(self):
        self.currency_id = self.journal_id.currency_id.id or self.company_id.currency_id.id

    @api.multi
    @api.depends('tax_correction', 'line_ids.price_subtotal', 'advance_ids.amount', 'line_ids.discount')
    def _compute_total(self):
        for voucher in self:
            total = 0
            tax_amount = 0
            for line in voucher.line_ids:
                tax_info = line.tax_ids.compute_all(line.price_unit, voucher.currency_id, line.quantity, line.product_id, voucher.partner_id)
                total += tax_info.get('total_included', 0.0) - line.quantity * (line.price_unit - line.price_unit * (1 - (line.discount or 0.0) / 100.0))
                tax_amount += sum([t.get('amount', 0.0) for t in tax_info.get('taxes', False)])
            voucher.advance_amount = sum(voucher.advance_ids.mapped('amount'))
            voucher.amount = total + voucher.tax_correction - voucher.advance_amount
            voucher.tax_amount = tax_amount
            voucher.untaxed_amount = total - tax_amount

    @api.one
    @api.depends('partner_id')
    def _get_outstanding(self):
        self.has_outstanding_advance = bool(self.env['account.payment'].search([('advance_type', '=', 'advance'),
                                                                                ('partner_id', '=', self.partner_id.id),
                                                                                ('reconciled', '=', False)]))
              
    @api.model
    def _default_journal(self):
        return self.env['account.journal'].search([('type', '=', 'purchase'),
                                                   ('company_id', '=', self.env.user.company_id.id)], limit=1)          
                
    voucher_type = fields.Selection([('sale', 'Sale'), ('purchase', 'Purchase')], string='Type', readonly=True, states={'draft': [('readonly', False)]}, oldname="type")
    name = fields.Char('Payment Reference', readonly=True, states={'draft': [('readonly', False)]})
    date = fields.Date(readonly=True, index=True, states={'open': [('readonly', False)]},
                           help="Effective date for accounting entries", copy=False, default=fields.Date.context_today)
    date_request = fields.Date(readonly=True, index=True, states={'draft': [('readonly', False)]},
                           help="Effective date for accounting entries", copy=False, default=fields.Date.context_today)
    journal_id = fields.Many2one('account.journal', 'Journal', readonly=True, states={'draft': [('readonly', False)], 'open': [('readonly', False)]}, default=_default_journal)
    account_id = fields.Many2one('account.account', 'Account', readonly=True, states={'draft': [('readonly', False)], 'open': [('readonly', False)]},
                                 domain="[('deprecated', '=', False), ('internal_type','=', 'liquidity')]")
    line_ids = fields.One2many('account.voucher.line', 'voucher_id', 'Voucher Lines',
                                   readonly=True, copy=True,
                                   states={'draft': [('readonly', False)]})
    narration = fields.Text('Notes', readonly=True, states={'draft': [('readonly', False)]})
    currency_id = fields.Many2one('res.currency', compute='_get_journal_currency', string='Currency', readonly=True)
    company_id = fields.Many2one('res.company', 'Company', required=True, readonly=True, default=lambda s:s.env.user.company_id)
    state = fields.Selection(
            [('draft', 'Draft'),
             ('cancel', 'Cancelled'),
             ('open', 'Open'),
             ('posted', 'Paid')
            ], 'Status', readonly=True, track_visibility='onchange', copy=False, default='draft',
            help=" * The 'Draft' status is used when a user is encoding a new and unconfirmed Voucher.\n"
                 " * The 'Pro-forma' status is used when the voucher does not have a voucher number.\n"
                 " * The 'Posted' status is used when user create voucher,a voucher number is generated and voucher entries are created in account.\n"
                 " * The 'Cancelled' status is used when user cancel voucher.")
    reference = fields.Char('Bill Reference', readonly=True, states={'draft': [('readonly', False)]},
                                 help="The partner reference of this document.", copy=False)
    amount = fields.Monetary(string='Total', store=True, readonly=True, compute='_compute_total')
    tax_amount = fields.Monetary(readonly=True, store=True, compute='_compute_total')
    untaxed_amount = fields.Monetary(readonly=True, store=True, compute='_compute_total')
    advance_amount = fields.Monetary(readonly=True, store=True, compute='_compute_total')
    tax_correction = fields.Monetary(readonly=True, states={'draft': [('readonly', False)]}, help='In case we have a rounding problem in the tax, use this field to correct it')
    number = fields.Char(readonly=True, copy=False, default='New')
    move_id = fields.Many2one('account.move', 'Journal Entry', copy=False)
    move_line_ids = fields.One2many('account.move.line', 'Journal Item', related='move_id.line_ids')
    partner_id = fields.Many2one('res.partner', 'Partner', change_default=1, readonly=True, states={'draft': [('readonly', False)]})
    paid = fields.Boolean(compute='_check_paid', help="The Voucher has been totally paid.")
    advance_ids = fields.One2many('account.voucher.advance', 'voucher_id', 'Allocated Advance')
    
    purchase_id = fields.Many2one('purchase.order', string='Add Purchase Order',
        help='Encoding help. When selected, the associated purchase order lines are added to the vendor bill. Several PO can be selected.')
    request_id = fields.Many2one('account.payment.request', "Payment Request")
    payment_type = fields.Selection([('cash', 'Cashier'), ('bank', 'Payment Request')], string="Type")
    request_status = fields.Selection(related='request_id.state', string="Request Status", store=True)
    has_outstanding_advance = fields.Boolean(compute='_get_outstanding')
    user_id = fields.Many2one('res.users', 'Responsible', default=lambda self: self.env.user)
    tax_line_ids = fields.One2many('account.voucher.tax', 'voucher_id', string='Tax Lines', oldname='tax_line',
        readonly=True, states={'draft': [('readonly', False)]}, copy=True)
    tax_no = fields.Char('Tax Number')
    
    def _prepare_tax_line_vals(self, line, tax):
        """ Prepare values to create an account.invoice.tax line

        The line parameter is an account.invoice.line, and the
        tax parameter is the output of account.tax.compute_all().
        """
        vals = {
            'purchase_id': self.id,
            'name': tax['name'],
            'tax_id': tax['id'],
            'amount': tax['amount'],
            'base': tax['base'],
            'manual': False,
            'sequence': tax['sequence'],
            'account_analytic_id': tax['analytic'] and line.account_analytic_id.id or False,
            'account_id': tax['account_id']
        }

        # If the taxes generate moves on the same financial account as the invoice line,
        # propagate the analytic account from the invoice line to the tax line.
        # This is necessary in situations were (part of) the taxes cannot be reclaimed,
        # to ensure the tax move is allocated to the proper analytic account.

        return vals
    
    @api.multi
    def get_taxes_values(self):
        tax_grouped = {}
        for line in self.line_ids:
            price_unit = line.price_unit
            taxes = line.tax_ids.compute_all(price_unit, self.journal_id.company_id.currency_id, line.quantity, line.product_id, self.partner_id)['taxes']
            for tax in taxes:
                val = self._prepare_tax_line_vals(line, tax)
                key = self.env['account.tax'].browse(tax['id']).get_grouping_key(val)

                if key not in tax_grouped:
                    tax_grouped[key] = val
                else:
                    tax_grouped[key]['amount'] += val['amount']
                    tax_grouped[key]['base'] += val['base']
        return tax_grouped
    
    @api.onchange('line_ids')
    def _onchange_line_ids(self):
        taxes_grouped = self.get_taxes_values()
        tax_lines = self.tax_line_ids.filtered('manual')
        for tax in taxes_grouped.values():
            tax_lines += tax_lines.new(tax)
        self.tax_line_ids = tax_lines
        return
    
    @api.model
    def create(self, vals):
        if vals.get('number', 'New') == 'New':
            vals['number'] = self.env['ir.sequence'].next_by_code('account.voucher') or '/'
        return super(AccountVoucher, self).create(vals)
    
    @api.onchange('state', 'partner_id', 'line_ids', 'purchase_id')
    def _onchange_allowed_purchase_id(self):
        '''
        The purpose of the method is to define a domain for the available
        purchase orders.
        '''
        result = {}

        # A PO can be selected only if at least one PO line is not already in the invoice
        purchase_line_ids = self.line_ids.mapped('purchase_line_id')
        purchase_ids = self.line_ids.mapped('purchase_id').filtered(lambda r: r.order_line <= purchase_line_ids)

        result['domain'] = {'purchase_id': [
            ('invoice_status', '=', 'to invoice'),
            ('partner_id', 'child_of', self.partner_id.id),
            ('id', 'not in', purchase_ids.ids),
            ]}
        return result
    
    def _prepare_voucher_line_from_po_line(self, line):
        if line.product_id.purchase_method == 'purchase':
            qty = line.product_qty - line.qty_invoiced
        else:
            qty = line.qty_received - line.qty_invoiced
        voucher_line_tax_ids = line.order_id.fiscal_position_id.map_tax(line.taxes_id)
        voucher_line = self.env['account.voucher.line']
        data = {
            'purchase_line_id': line.id,
            'name': line.name,
            'origin': self.purchase_id.origin,
            'product_id': line.product_id.id,
            'account_id': voucher_line.with_context({'journal_id': self.journal_id.id, 'type': 'in_invoice'})._default_account(),
            'price_unit': line.order_id.currency_id.compute(line.price_unit, self.currency_id, round=False),
            'quantity': qty,
#             'discount': line.discount,
            'account_analytic_id': line.account_analytic_id.id,
            'tax_ids': voucher_line_tax_ids.ids
        }
        account = voucher_line.get_voucher_line_account('in_invoice', line.product_id, self.purchase_id.fiscal_position_id, self.env.user.company_id)
        if account:
            data['account_id'] = account.id
        return data

    # Load all unsold PO lines
    @api.onchange('purchase_id')
    def purchase_order_change(self):
        if not self.purchase_id:
            return {}
        if not self.journal_id:
            raise UserError(_('Please fill the journal first.'))
        if not self.partner_id:
            self.partner_id = self.purchase_id.partner_id.id
        new_lines = self.env['account.voucher.line']
        for line in self.purchase_id.order_line:
            # Load a PO line only once
            if line in self.line_ids.mapped('purchase_line_id'):
                continue
            data = self._prepare_voucher_line_from_po_line(line)
            new_line = new_lines.new(data)
            new_lines += new_line

        self.line_ids += new_lines
        self.purchase_id = False
        return {}

    @api.multi
    @api.depends('name', 'number')
    def name_get(self):
        return [(r.id, (r.number or _('Draft Voucher'))) for r in self]
    
    @api.multi
    def action_confirm(self):
        self.state = 'open'
        
    @api.multi
    def action_validate(self):
        if not self.mapped('account_id'):
            raise UserError(_('Please input Bank or Cash Account!'))
        self.action_move_line_create()

    @api.multi
    def action_cancel_draft(self):
        self.write({'state': 'draft'})

    @api.multi
    def cancel_voucher(self):
        for voucher in self:
            if voucher.request_status in ('request', 'paid'):
                raise UserError('Cannot cancel a requested voucher!')
            voucher.move_id.button_cancel()
            voucher.move_line_ids.remove_move_reconcile()
            voucher.move_id.unlink()
        self.write({'state': 'cancel', 'move_id': False})

    @api.multi
    def unlink(self):
        for voucher in self:
            if voucher.state not in ('draft', 'cancel'):
                raise UserError(_('Cannot delete voucher(s) which are already opened or paid.'))
        return super(AccountVoucher, self).unlink()

    @api.multi
    def first_move_line_get(self, move_id, company_currency, current_currency, amount, account_id):
        amount = self._convert_amount(amount)
        debit = 0.0
        credit = amount
        if amount < 0.0: 
            debit = -amount
            credit = 0.0
        sign = debit - credit < 0 and -1 or 1
        # set the first line of the voucher
        move_line = {
                'name': self.name or '/',
                'debit': debit,
                'credit': credit,
                'account_id': account_id,
                'move_id': move_id,
                'journal_id': self.journal_id.id,
                'partner_id': self.partner_id.id,
                'currency_id': company_currency != current_currency and current_currency or False,
                'amount_currency': (sign * abs(self.amount)
                    if company_currency != current_currency else 0.0),
                'date': self.date,
            }
        return move_line

    @api.multi
    def account_move_get(self):
        if self.number:
            name = self.number
        elif self.journal_id.sequence_id:
            if not self.journal_id.sequence_id.active:
                raise UserError(_('Please activate the sequence of selected journal !'))
            name = self.journal_id.sequence_id.with_context(ir_sequence_date=self.date).next_by_id()
        else:
            raise UserError(_('Please define a sequence on the journal.'))

        move = {
            'name': name,
            'journal_id': self.journal_id.id,
            'narration': self.narration,
            'date': self.date,
            'ref': self.reference,
        }
        return move

    @api.multi
    def _convert_amount(self, amount):
        for voucher in self:
            return voucher.currency_id.compute(amount, voucher.company_id.currency_id)

    @api.multi
    def voucher_move_line_create(self, move_id, company_currency, current_currency):
        for line in self.line_ids:
            # create one move line per voucher line where amount is not 0.0
            if not line.price_subtotal:
                continue
            # convert the amount set on the voucher line into the currency of the voucher's company
            # this calls res_curreny.compute() with the right context, so that it will take either the rate on the voucher if it is relevant or will use the default behaviour
            amount = self._convert_amount(line.price_subtotal)
            move_line = {
                'journal_id': self.journal_id.id,
                'name': line.name or '/',
                'account_id': line.account_id.id,
                'move_id': move_id,
                'partner_id': self.partner_id.id,
                'analytic_account_id': line.account_analytic_id and line.account_analytic_id.id or False,
                'quantity': 1,
                'credit': abs(amount) if self.voucher_type == 'sale' else 0.0,
                'debit': abs(amount) if self.voucher_type == 'purchase' else 0.0,
                'date': self.date,
                'tax_ids': [(4, t.id) for t in line.tax_ids],
                'amount_currency': line.price_subtotal if current_currency != company_currency else 0.0,
            }

            self.env['account.move.line'].create(move_line)
    
    @api.model
    def tax_line_move_line_get(self):
        res = []
        # keep track of taxes already processed
        done_taxes = []
        # loop the invoice.tax.line in reversal sequence
        for tax_line in sorted(self.tax_line_ids, key=lambda x:-x.sequence):
            if tax_line.amount_total:
                tax = tax_line.tax_id
                if tax.amount_type == "group":
                    for child_tax in tax.children_tax_ids:
                        done_taxes.append(child_tax.id)
                res.append({
                    'invoice_tax_line_id': tax_line.id,
                    'tax_line_id': tax_line.tax_id.id,
                    'type': 'tax',
                    'name': tax_line.name,
                    'price_unit': tax_line.amount_total,
                    'quantity': 1,
                    'price': tax_line.amount_total,
                    'account_id': tax_line.account_id.id,
                    'account_analytic_id': tax_line.account_analytic_id.id,
                    'invoice_id': self.id,
                    'tax_ids': [(6, 0, list(done_taxes))] if tax_line.tax_id.include_base_amount else []
                })
                done_taxes.append(tax.id)
        return res
    
    @api.multi
    def action_move_line_create(self):
        '''
        Confirm the vouchers given in ids and create the journal entries for each of them
        ''' 
        company_currency = self[0].journal_id.company_id.currency_id.id
        current_currency = self[0].currency_id.id or company_currency
        local_context = dict(self._context, force_company=self[0].journal_id.company_id.id)
        ctx = local_context.copy()
        ctx['date'] = self[0].date
        ctx['check_move_validity'] = False
        move = self.env['account.move'].create(self[0].account_move_get())
        move_line = self.env['account.move.line']
        move_line |= self.env['account.move.line'].with_context(ctx).create(self[0].with_context(ctx).first_move_line_get(move.id, company_currency, current_currency, \
                                                                                                    sum(self.mapped('amount')), self[0].account_id.id))
        for voucher in self:
            for adv_line in voucher.advance_ids:
                if adv_line.amount_total > abs(adv_line.name.amount_residual):
                    raise UserError(_('Wrong advance amount'))
                ml = self.env['account.move.line'].with_context(ctx).create(voucher.first_move_line_get(move.id, company_currency, \
                                                                                                                 current_currency, adv_line.amount, \
                                                                                                                 adv_line.name.destination_account_id.id))
                (ml + adv_line.name.move_line_ids.filtered(lambda ml: ml.account_id == adv_line.name.destination_account_id and not ml.reconciled)).reconcile()
                move_line |= ml
            for tax in voucher.tax_line_ids:
                tax_line = {
                    'name': tax.name or '/',
                    'debit': tax.amount,
                    'credit': 0,
                    'account_id': tax.account_id.id,
                    'move_id': move.id,
                    'journal_id': self.journal_id.id,
                    'partner_id': self.partner_id.id,
                    'currency_id': company_currency != current_currency and current_currency or False,
                    'amount_currency': (sign * abs(self.amount)
                        if company_currency != current_currency else 0.0),
                    'date': self.date,
                }
                self.env['account.move.line'].with_context(ctx).create(tax_line)
                
            # Create one move line per voucher line where amount is not 0.0
            voucher.with_context(ctx).voucher_move_line_create(move.id, company_currency, current_currency)
            
            # Add tax correction to move line if any tax correction specified
            if voucher.tax_correction != 0.0:
                tax_move_line = self.env['account.move.line'].search([('move_id', '=', move.id), ('tax_line_id', '!=', False)], limit=1)
                if len(tax_move_line):
                    tax_move_line.write({'debit': tax_move_line.debit + voucher.tax_correction if tax_move_line.debit > 0 else 0,
                        'credit': tax_move_line.credit + voucher.tax_correction if tax_move_line.credit > 0 else 0})
        
        move.post()
        self.write({
            'move_id': move.id,
            'state': 'posted',
            'number': move.name
        })
#         self.mapped('trip_id').test_done()
        return True
    

class account_voucher_line(models.Model):
    _name = 'account.voucher.line'
    _description = 'Voucher Lines'

    @api.one
    @api.depends('price_unit', 'tax_ids', 'quantity', 'product_id', 'voucher_id.currency_id', 'discount')
    def _compute_subtotal(self):
        price = self.price_unit * (1 - (self.discount or 0.0) / 100.0)
        self.price_subtotal = self.quantity * price
        if self.tax_ids:
            taxes = self.tax_ids.compute_all(price, self.voucher_id.currency_id, self.quantity, product=self.product_id, partner=self.voucher_id.partner_id)
            self.price_subtotal = taxes['total_excluded']

    name = fields.Text(string='Description', required=True)
    sequence = fields.Integer(default=10,
        help="Gives the sequence of this line when displaying the voucher.")
    voucher_id = fields.Many2one('account.voucher', 'Voucher', required=1, ondelete='cascade')
    product_id = fields.Many2one('product.product', string='Product',
        ondelete='set null', index=True)
    account_id = fields.Many2one('account.account', string='Account',
        required=True, domain=[('deprecated', '=', False)],
        help="The income or expense account related to the selected product.")
    price_unit = fields.Float(string='Unit Price', required=True, digits=dp.get_precision('Product Price'), oldname='amount')
    price_subtotal = fields.Monetary(string='Amount',
        store=True, readonly=True, compute='_compute_subtotal')
    quantity = fields.Float(digits=dp.get_precision('Product Unit of Measure'),
        required=True, default=1)
    account_analytic_id = fields.Many2one('account.analytic.account', 'Analytic Account')
    company_id = fields.Many2one('res.company', related='voucher_id.company_id', string='Company', store=True, readonly=True)
    tax_ids = fields.Many2many('account.tax', string='Tax', help="Only for tax excluded from price")
    currency_id = fields.Many2one('res.currency', related='voucher_id.currency_id')
    
    purchase_line_id = fields.Many2one('purchase.order.line', 'Purchase Order Line', ondelete='set null', index=True, readonly=True)
    purchase_id = fields.Many2one('purchase.order', related='purchase_line_id.order_id', string='Purchase Order', store=False, readonly=True,
        help='Associated Purchase Order. Filled in automatically when a PO is chosen on the vendor bill.')
    discount = fields.Float(string='Discount (%)', digits=dp.get_precision('Discount'), default=0.0)
    
    @api.model
    def _default_account(self):
        if self._context.get('journal_id'):
            journal = self.env['account.journal'].browse(self._context.get('journal_id'))
            if self._context.get('type') in ('out_invoice', 'in_refund'):
                return journal.default_credit_account_id.id
            return journal.default_debit_account_id.id
        
    @api.v8
    def get_voucher_line_account(self, type, product, fpos, company):
        accounts = product.product_tmpl_id.get_product_accounts(fpos)
        if type in ('out_invoice', 'out_refund'):
            return accounts['income']
        return accounts['expense']

    def _get_account(self, product, fpos):
        accounts = product.product_tmpl_id.get_product_accounts(fpos)
        return accounts['expense']

    @api.multi
    @api.onchange('product_id')
    def product_id_change(self):
        if self.voucher_id.partner_id and self.voucher_id.partner_id.lang:
            self = self.with_context(lang=self.voucher_id.partner_id.lang)

        self.name = self.product_id.partner_ref
        self.account_id = self._get_account(self.product_id, self.voucher_id.partner_id.property_account_position_id)
        
        if self.product_id.description_purchase:
            self.name += '\n' + self.product_id.description_purchase
        self.price_unit = self.product_id.standard_price
        self.tax_ids = self.product_id.supplier_taxes_id.ids


class AccountVoucherAdvance(models.Model):
    _name = 'account.voucher.advance'
    _description = 'Deduct voucher amount with Advance'
    
    @api.multi
    @api.constrains('name') 
    def _check_duplicate(self):
        for voucher in self.mapped('voucher_id'):
            if len(voucher.advance_ids.ids) != len(set(voucher.advance_ids.mapped('name').ids)):
                return False
        return True
        
    voucher_id = fields.Many2one('account.voucher', 'Voucher')
    tax_id = fields.Many2one('account.tax', 'Tax', readonly=False, related='name.tax_id')
    name = fields.Many2one('account.payment', 'Advance number')
    amount = fields.Float('Amount advance', readonly=False)
    amount_tax = fields.Monetary(string='Tax Amount', readonly=False)
    amount_total = fields.Monetary(string='Total Amount', readonly=False)
    currency_id = fields.Many2one('res.currency', 'Currency', readonly=False, related='name.currency_id')
    
    _constraints = [
        (_check_duplicate, 'Duplicate Advance Reference!', ['name']),
    ]
    
    @api.onchange('name')
    def advance_change(self):
        self.amount_tax = 0.0
        if self.tax_id:
            assert self.tax_id.amount_type == 'percent', 'Unsupported type of tax!'
            self.amount_tax = abs(self.name.amount_residual) * self.tax_id.amount / (100 + self.tax_id.amount)
        self.amount = abs(self.name.amount_residual) - self.amount_tax
        self.amount_total = self.amount_tax + self.amount
        return {
            'domain' : {
                'name' : [('id', 'not in', self.voucher_id.advance_ids.mapped('name').ids),
                          ('advance_type', '=', 'advance'),
                          ('partner_id', '=', self.voucher_id.partner_id.id),
                          ('reconciled', '=', False)]
            }
        }

    
class AccountVoucherTax(models.Model):
    _name = "account.voucher.tax"
    _description = "Voucher Tax"
    _order = 'sequence'

    @api.depends('voucher_id.line_ids')
    def _compute_base_amount(self):
        tax_grouped = {}
        for voucher in self.mapped('voucher_id'):
            tax_grouped[voucher.id] = voucher.get_taxes_values()
        for tax in self:
            tax.base = 0.0
            if tax.tax_id:
                key = tax.tax_id.get_grouping_key({
                    'tax_id': tax.tax_id.id,
                    'account_id': tax.account_id.id,
                    'account_analytic_id': tax.account_analytic_id.id,
                })
                if tax.voucher_id and key in tax_grouped[tax.voucher_id.id]:
                    tax.base = tax_grouped[tax.voucher_id.id][key]['base']
                else:
                    _logger.warning('Tax Base Amount not computable probably due to a change in an underlying tax (%s).', tax.tax_id.name)

    voucher_id = fields.Many2one('account.voucher', string='Voucher', ondelete='cascade', index=True)
    name = fields.Char(string='Tax Description', required=True)
    tax_id = fields.Many2one('account.tax', string='Tax', ondelete='restrict')
    account_id = fields.Many2one('account.account', string='Tax Account', required=True, domain=[('deprecated', '=', False)])
    account_analytic_id = fields.Many2one('account.analytic.account', string='Analytic account')
    amount = fields.Monetary()
    amount_rounding = fields.Monetary()
    amount_total = fields.Monetary(string="Amount", compute='_compute_amount_total')
    manual = fields.Boolean(default=True)
    sequence = fields.Integer(help="Gives the sequence order when displaying a list of invoice tax.")
    company_id = fields.Many2one('res.company', string='Company', related='account_id.company_id', store=True, readonly=True)
    currency_id = fields.Many2one('res.currency', related='company_id.currency_id', store=True, readonly=True)
    base = fields.Monetary(string='Base', compute='_compute_base_amount', store=True)

    @api.depends('amount', 'amount_rounding')
    def _compute_amount_total(self):
        for tax_line in self:
            tax_line.amount_total = tax_line.amount + tax_line.amount_rounding
