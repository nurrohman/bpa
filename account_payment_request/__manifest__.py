# -*- coding: utf-8 -*-
{
    'name': 'Custom Module Payment Request for CARFIX',
    'version': "1.0",
    'price': 9999.0,
    'currency': 'IDR',
    'summary': 'This module Payment Request for CARFIX',
    'description': """This module Payment Request for CARFIX""",
    'category': 'Custom',
    'author': 'PT. VISI',
    'website': 'www.visi.co.id',
    'images': [],
    'depends': [
        'account_invoicing',
        'purchase',
        'account_giropayment',
        'report_xlsx'
    ],
    'data': [
        'security/ir.model.access.csv',
        'wizard/payment_request_wizard_view.xml',
        'views/account_payment_request_view.xml',
        'views/ir_sequence.xml',
        'views/account_invoice_view.xml',
        'views/account_payment_view.xml',
        'views/partner_view.xml',
        'views/account_voucher_view.xml',
        'views/print_payment_request.xml',
    ],
    "installable": True,
    "application": False,
}
