# -*- coding: utf-8 -*-

###################################################################################
# 
#    Copyright (C) 2017 MuK IT GmbH
#
#    Odoo Proprietary License v1.0
#    
#    This software and associated files (the "Software") may only be used 
#    (executed, modified, executed after modifications) if you have
#    purchased a valid license from the authors, typically via Odoo Apps,
#    or if you have received a written agreement from the authors of the
#    Software (see the COPYRIGHT file).
#    
#    You may develop Odoo modules that use the Software as a library 
#    (typically by depending on it, importing it and using its resources),
#    but without copying any source code or material from the Software.
#    You may distribute those modules under the license of your choice,
#    provided that this license is compatible with the terms of the Odoo
#    Proprietary License (For example: LGPL, MIT, or proprietary licenses
#    similar to this one).
#    
#    It is forbidden to publish, distribute, sublicense, or sell copies of
#    the Software or modified copies of the Software.
#    
#    The above copyright notice and this permission notice must be included
#    in all copies or substantial portions of the Software.
#    
#    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
#    OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
#    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
#    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
#    DEALINGS IN THE SOFTWARE.
#
###################################################################################

import json
import inspect
import logging
import base64

from odoo import _
from odoo import http
from odoo.http import request
from odoo.http import Response

from odoo.addons.muk_dms_finder.services import elfinder

_logger = logging.getLogger(__name__)

class Finder(http.Controller):
	
	def __init__(self):
		super(Finder, self).__init__()
		self._connect()
	
	def _connect(self):
		config_parameter = request.env['ir.config_parameter'].sudo()
		max_size = config_parameter.get_param('muk_dms.max_upload_size', default=25)
		max_upload = config_parameter.get_param('muk_dms.max_upload_files', default=25)
		try:
			max_size = int(max_size)
		except ValueError:
			max_size = 25
		try:
			max_upload = int(max_upload)
		except ValueError:
			max_upload = 25	
		self.connection = elfinder.Connector({
			'uplMaxSize': "%sM" % int((max_size * (max_upload / 2))),
			'uplMaxFile': max_upload}, {
			'uploadMaxSize': (max_size * 1024 * 1024)})
		
	def _process_request(self):
		return self.connection.cmd(request)
		
	@http.route('/dms/connector/elfinder', auth='user', type="http")
	def connector_elfinder(self, **kw):
		if 'cmd' in request.params:
			result = self._process_request()
			if "error" in result:
				return Response(json.dumps(self._process_request()),
							content_type='application/json;charset=utf-8', status=500) 
			data = None
			headers = []
			content_type = 'application/json;charset=utf-8'
			if request.params['cmd'] == 'file':
				content_type = "%s;charset=utf-8" % result.mimetype
				content = base64.b64decode(result.content)
				if 'download' in request.params:
					headers.append(('Content-disposition', 'attachment; filename={}'.format(result.name)))
				else:
					headers.append(('Content-disposition', 'inline; filename={}'.format(result.name)))
				headers.append(('Content-Length', len(content)))
				data = content
			elif request.params['cmd'] == 'muk_tmb':
				content = result["muk_tmb"]
				headers.append(('Content-disposition', 'inline;'))
				headers.append(('Content-Length', len(content)))
				data = content
			elif request.params['cmd'] == 'url':
				data = result
			elif request.params['cmd'] == 'zipdl' and 'download' in request.params:
				content_type = 'application/zip;charset=utf-8'
				headers.append(('Content-disposition', 'attachment; filename={}'.format(request.params['targets[2]'])))
				headers.append(('Content-Length', len(result)))
				data = result
			else:
				data = json.dumps(result)
			return Response(data, headers=headers, content_type=content_type, status=200) 
		return Response(json.dumps({"error" : ["errCmdParams", _("cmd couldn't be found in request params!")]}),
					content_type='application/json;charset=utf-8', status=400) 
	
	@http.route('/dms/connector/elfinder/theme', auth='user', type="json")
	def theme(self, **kw):
		config_parameter = request.env['ir.config_parameter'].sudo()
		return config_parameter.get_param('muk_dms_finder.elfinder_themes', default="default")
	
	@http.route('/dms/connector/elfinder/csrf', auth='user', type="json")
	def csrf(self, **kw):
		return request.csrf_token(None)
	
	@http.route('/dms/connector/elfinder/sync', auth='user', type="json")
	def sync(self, **kw):
		config_parameter = request.env['ir.config_parameter'].sudo()
		sync_time = config_parameter.get_param('muk_dms.sync_time', default=100000)
		try:
			sync_time = int(sync_time)
		except ValueError:
			sync_time = 100000
		return sync_time