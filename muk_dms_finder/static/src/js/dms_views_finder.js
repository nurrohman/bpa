/**********************************************************************************
* 
*    Copyright (C) 2017 MuK IT GmbH
*
*    Odoo Proprietary License v1.0
*    
*    This software and associated files (the "Software") may only be used 
*    (executed, modified, executed after modifications) if you have
*    purchased a valid license from the authors, typically via Odoo Apps,
*    or if you have received a written agreement from the authors of the
*    Software (see the COPYRIGHT file).
*    
*    You may develop Odoo modules that use the Software as a library 
*    (typically by depending on it, importing it and using its resources),
*    but without copying any source code or material from the Software.
*    You may distribute those modules under the license of your choice,
*    provided that this license is compatible with the terms of the Odoo
*    Proprietary License (For example: LGPL, MIT, or proprietary licenses
*    similar to this one).
*    
*    It is forbidden to publish, distribute, sublicense, or sell copies of
*    the Software or modified copies of the Software.
*    
*    The above copyright notice and this permission notice must be included
*    in all copies or substantial portions of the Software.
*    
*    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
*    OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
*    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
*    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
*    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
*    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
*    DEALINGS IN THE SOFTWARE.
*
**********************************************************************************/

odoo.define('muk_dms_finder_views.finder', function(require) {
"use strict";

var ajax = require('web.ajax');
var core = require('web.core');
var config = require('web.config');
var session = require('web.session');
var web_client = require('web.web_client');
var framework = require('web.framework');
var crash_manager = require('web.crash_manager');

var dms_utils = require('muk_dms.utils');

var Widget = require('web.Widget');
var Dialog = require('web.Dialog');

var _t = core._t;
var QWeb = core.qweb;

var FinderView = Widget.extend({
	cssLibs: [
		'/muk_dms_finder/static/lib/elFinder/css/elfinder.min.css',
    ],
    jsLibs: [
        '/muk_dms_finder/static/lib/elFinder/js/elfinder.full.js',
    ],
    languages: {
    	ar: "/muk_dms_finder/static/lib/elFinder/js/i18n/elfinder.ar.js",
    	bg: "/muk_dms_finder/static/lib/elFinder/js/i18n/elfinder.bg.js",
    	ca: "/muk_dms_finder/static/lib/elFinder/js/i18n/elfinder.ca.js",
    	cs: "/muk_dms_finder/static/lib/elFinder/js/i18n/elfinder.cs.js",
    	da: "/muk_dms_finder/static/lib/elFinder/js/i18n/elfinder.da.js",
    	de: "/muk_dms_finder/static/lib/elFinder/js/i18n/elfinder.de.js",
    	el: "/muk_dms_finder/static/lib/elFinder/js/i18n/elfinder.el.js",
    	es: "/muk_dms_finder/static/lib/elFinder/js/i18n/elfinder.es.js",
    	fa: "/muk_dms_finder/static/lib/elFinder/js/i18n/elfinder.fa.js",
    	fo: "/muk_dms_finder/static/lib/elFinder/js/i18n/elfinder.fo.js",
    	fr: "/muk_dms_finder/static/lib/elFinder/js/i18n/elfinder.fr.js",
    	he: "/muk_dms_finder/static/lib/elFinder/js/i18n/elfinder.he.js",
    	hr: "/muk_dms_finder/static/lib/elFinder/js/i18n/elfinder.hr.js",
    	hu: "/muk_dms_finder/static/lib/elFinder/js/i18n/elfinder.hu.js",
    	id: "/muk_dms_finder/static/lib/elFinder/js/i18n/elfinder.id.js",
    	it: "/muk_dms_finder/static/lib/elFinder/js/i18n/elfinder.it.js",
    	jp: "/muk_dms_finder/static/lib/elFinder/js/i18n/elfinder.jp.js",
    	ko: "/muk_dms_finder/static/lib/elFinder/js/i18n/elfinder.ko.js",
    	nl: "/muk_dms_finder/static/lib/elFinder/js/i18n/elfinder.nl.js",
    	no: "/muk_dms_finder/static/lib/elFinder/js/i18n/elfinder.no.js",
    	pl: "/muk_dms_finder/static/lib/elFinder/js/i18n/elfinder.pl.js",
    	pt: "/muk_dms_finder/static/lib/elFinder/js/i18n/elfinder.pt.js",
    	ro: "/muk_dms_finder/static/lib/elFinder/js/i18n/elfinder.ro.js",
    	ru: "/muk_dms_finder/static/lib/elFinder/js/i18n/elfinder.ru.js",
    	sk: "/muk_dms_finder/static/lib/elFinder/js/i18n/elfinder.sk.js",
    	sl: "/muk_dms_finder/static/lib/elFinder/js/i18n/elfinder.sl.js",
    	sr: "/muk_dms_finder/static/lib/elFinder/js/i18n/elfinder.sr.js",
    	sv: "/muk_dms_finder/static/lib/elFinder/js/i18n/elfinder.sv.js",
    	tr: "/muk_dms_finder/static/lib/elFinder/js/i18n/elfinder.tr.js",
    	ug: "/muk_dms_finder/static/lib/elFinder/js/i18n/elfinder.ug.js",
    	uk: "/muk_dms_finder/static/lib/elFinder/js/i18n/elfinder.uk.js",
    	vi: "/muk_dms_finder/static/lib/elFinder/js/i18n/elfinder.vi.js",
    	zh: "/muk_dms_finder/static/lib/elFinder/js/i18n/elfinder.zh.js",
    },
    themes: {
    	"default": "/muk_dms_finder/static/lib/elFinder/css/theme.css",
    	"material": "/muk_dms_finder/static/lib/elFinderThemes/elFinderMaterialTheme/css/theme.min.css",
    	"material_gray": "/muk_dms_finder/static/lib/elFinderThemes/elFinderMaterialTheme/css/theme-gray.min.css",
    	"bootstrap": "/muk_dms_finder/static/lib/elFinderThemes/elFinderBootstrapTheme/css/theme-bootstrap-libreicons-svg.css",
    	"windows": "/muk_dms_finder/static/lib/elFinderThemes/elFinderWindowsTheme/css/theme.css",
    },
	template : "FinderTemplate",
	init: function(parent) {
        this._super(parent);
    },
    willStart: function() {
    	var self = this;
    	return $.when(this.load_lang(), this.load_theme(), this._super())
    	  .then(function(lang) {
    		self.lang = lang;
            return $.when(self.load_csrf(), self.load_sync(),
            		ajax.loadLibs(self)).then(function(csrf, sync) {
            	self.csrf = csrf;
            	self.sync = sync;
            });
    	});
   },
    start: function() {
    	var self = this;
    	this._super(parent);
    	this.finder = this.$el.elfinder({
    		sync: self.sync,
    		width: '100%',
     	    height: $('.o_main_content').height(),
            url: '/dms/connector/elfinder',
            lang: self.lang,
            cssAutoLoad : false,
            useBrowserHistory: false,
            uiOptions: {
                toolbar : [
                    ['open'],
                    ['back', 'forward'],
                    ['reload'],
                    ['home', 'up'],
                    ['mkdir', 'mkfile', 'upload'],
                    ['info'],
                    ['copy', 'cut', 'paste'],
                    ['rm'],
                    ['duplicate', 'rename', 'edit'],
                    ['search'],
                    ['view'],
                    ['help']
                ]
            },
            contextmenu : {
                files  : [
                    'getfile', '|','open', '|', 'copy', 'cut', 'paste', 'duplicate', '|',
                    'rm', '|', 'edit', 'rename', '|', 'info'
                ]
            },
            debug: session.debug ? [
            	'error',
            	'warning',
            	'event-destroy'
            ] : [
	        	'error',
	        	'warning',
            ],
            customData: {
            	csrf_token: self.csrf,
            }
        }).elfinder('instance');
    	core.bus.on('resize', this, function() {
    		dms_utils.delay(function() {
    			var h = parseInt($('.o_main_content').height());
    	        if (h != parseInt(self.$el.height())) {
    	            self.finder.resize('100%', h);
    	        }
		    }, 200);
        });
    	return $.when();
    },
    load_theme: function() {
    	var self = this;
    	var theme = $.Deferred();
    	var default_theme = [self.themes["default"]];
    	session.rpc('/dms/connector/elfinder/theme', {})
    	  .done(function(result) {
  	  		if(result) {
  	  	  		var css = [self.themes[result]] || default_theme;
  	  	  		self.cssLibs.push(css);
  	  	  		theme.resolve();
  	  		} else {
  	  	  		self.cssLibs.push(default_theme);
  	  	  		theme.resolve();
  	  		}
		  })
		  .fail(function(xhr, status, text) {
	  	  		self.cssLibs.push(default_theme);
  	  	  		theme.resolve();
		  });
    	return theme;
    },
    load_lang: function() {
    	var lang = $.Deferred();	
		var user_lang = session.user_context.lang.substr(0, 2);
		var script = this.languages[user_lang];
		if(script) {
			this.jsLibs.push(script);
			lang.resolve(user_lang);
		} else {
		  	lang.resolve("en");
		}
    	return lang;
    },
    load_csrf: function() {
    	var self = this;
    	var csrf = $.Deferred();
    	session.rpc('/dms/connector/elfinder/csrf', {})
    	  .done(function(result) {
  	  		if(result) {
  	  			csrf.resolve(result);
  	  		} else {
  	  			csrf.resolve(core.csrf_token);
  	  		}
		  })
		  .fail(function(xhr, status, text) {
	  			csrf.resolve(core.csrf_token);
		  });
    	return csrf;
    },
    load_sync: function() {
    	var self = this;
    	var sync = $.Deferred();
    	session.rpc('/dms/connector/elfinder/sync', {})
    	  .done(function(result) {
  	  		if(result) {
  	  			sync.resolve(result);
  	  		} else {
  	  			sync.resolve(0);
  	  		}
		  })
		  .fail(function(xhr, status, text) {
			  sync.resolve(0);
		  });
    	return sync;
    },
});

core.action_registry.add('muk_dms_finder_views.finder', FinderView);

return FinderView

});