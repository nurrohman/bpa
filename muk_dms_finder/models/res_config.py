# -*- coding: utf-8 -*-

###################################################################################
# 
#    Copyright (C) 2017 MuK IT GmbH
#
#    Odoo Proprietary License v1.0
#    
#    This software and associated files (the "Software") may only be used 
#    (executed, modified, executed after modifications) if you have
#    purchased a valid license from the authors, typically via Odoo Apps,
#    or if you have received a written agreement from the authors of the
#    Software (see the COPYRIGHT file).
#    
#    You may develop Odoo modules that use the Software as a library 
#    (typically by depending on it, importing it and using its resources),
#    but without copying any source code or material from the Software.
#    You may distribute those modules under the license of your choice,
#    provided that this license is compatible with the terms of the Odoo
#    Proprietary License (For example: LGPL, MIT, or proprietary licenses
#    similar to this one).
#    
#    It is forbidden to publish, distribute, sublicense, or sell copies of
#    the Software or modified copies of the Software.
#    
#    The above copyright notice and this permission notice must be included
#    in all copies or substantial portions of the Software.
#    
#    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
#    OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
#    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
#    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
#    DEALINGS IN THE SOFTWARE.
#
###################################################################################

from odoo import api, fields, models
from odoo.exceptions import AccessDenied

class DocumentFinderSettings(models.TransientModel):
    
    _inherit = 'res.config.settings'
    
    elfinder_themes = fields.Selection(
        [
            ('default', 'Default Theme'),
            ('material', 'Material Theme'),
            ('material_gray', 'Material Gray Theme'),
            ('bootstrap', 'Bootstrap Theme'),
            ('windows', 'Windows Theme'),
        ],
        string='Theme')
    
    max_upload_files = fields.Char(
        string="Max Files",
        help="Defines the maximum number of files which can be uploaded simultaneously.")
    
    sync_time = fields.Char(
        string="Sync Time",
        help="Sync content by refreshing every N milliseconds. Value must be bigger than 1000. (0 - do not sync)")
    
    @api.model
    def get_values(self):
        res = super(DocumentFinderSettings, self).get_values()
        get_param = self.env['ir.config_parameter'].sudo().get_param
        res.update(
            elfinder_themes=get_param('muk_dms_finder.elfinder_themes', default="default"),
            max_upload_files=get_param('muk_dms_finder.max_upload_files', default="25"),
            sync_time=get_param('muk_dms_finder.sync_time', default="100000")
        )
        return res
        
    def set_values(self):
        config = self.env['ir.config_parameter']
        get_param = config.sudo().get_param
        set_param = config.sudo().set_param
        elfinder_themes = get_param('muk_dms_finder.elfinder_themes', default="default")
        max_upload_files = get_param('muk_dms_finder.max_upload_files', default="25")
        sync_time = get_param('muk_dms_finder.sync_time', default="100000")
        if self.elfinder_themes and self.elfinder_themes != elfinder_themes:
            if not self.user_has_groups('muk_dms.group_dms_admin'):
                raise AccessDenied()
            set_param('muk_dms_finder.elfinder_themes', self.elfinder_themes or "default")
        if self.max_upload_files and self.max_upload_files != max_upload_files:
            if not self.user_has_groups('muk_dms.group_dms_admin'):
                raise AccessDenied()
            set_param('muk_dms_finder.max_upload_files', self.max_upload_files or "25")   
        if self.sync_time and self.sync_time != sync_time:
            if not self.user_has_groups('muk_dms.group_dms_admin'):
                raise AccessDenied()
            set_param('muk_dms_finder.sync_time', self.sync_time or "100000")
        super(DocumentFinderSettings, self).set_values()