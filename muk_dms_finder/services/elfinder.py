# -*- coding: utf-8 -*-

###################################################################################
# 
#    Copyright (C) 2017 MuK IT GmbH
#
#    Odoo Proprietary License v1.0
#    
#    This software and associated files (the "Software") may only be used 
#    (executed, modified, executed after modifications) if you have
#    purchased a valid license from the authors, typically via Odoo Apps,
#    or if you have received a written agreement from the authors of the
#    Software (see the COPYRIGHT file).
#    
#    You may develop Odoo modules that use the Software as a library 
#    (typically by depending on it, importing it and using its resources),
#    but without copying any source code or material from the Software.
#    You may distribute those modules under the license of your choice,
#    provided that this license is compatible with the terms of the Odoo
#    Proprietary License (For example: LGPL, MIT, or proprietary licenses
#    similar to this one).
#    
#    It is forbidden to publish, distribute, sublicense, or sell copies of
#    the Software or modified copies of the Software.
#    
#    The above copyright notice and this permission notice must be included
#    in all copies or substantial portions of the Software.
#    
#    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
#    OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
#    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
#    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
#    DEALINGS IN THE SOFTWARE.
#
###################################################################################

import io
import re
import json
import time
import PIL
import base64
import logging
import urllib
import tempfile
import zipfile
import datetime

from odoo.addons.muk_dms_connector.services import connector
from odoo.addons.muk_dms_connector.tools import utils

from odoo.exceptions import AccessError, ValidationError

_logger = logging.getLogger(__name__)

class Connector(connector.DocumentConnector):
    
    _api = '2.1'
    _init_params =  {
        'uplMaxSize': '50M',
        "uplMaxFile": "25",
        "netDrivers": [
        ],
    }
    _options = {
        'separator': '/',
        'disabled': [],
        'copyOverwrite': 1,
        'uploadOverwrite': 1,
        'uploadMaxSize': 1073741824,
        'uploadMaxConn': 10,
        'uploadMime': {
            'allow': [],
            'deny': [],
            'firstOrder': "deny"
        },
        'dispInlineRegex': "^(?:image|text/plain$)",
        'jpgQuality': 100,
        'syncChkAsTs': 1,
        'syncMinMs': 1000,
        'uiCmdMap': { "chmod" : "perm" },
        'i18nFolderName': 1,
        'archivers': {
            'create'  : [
               "application/zip",
            ],
            'extract' : [
               "application/zip",
            ],
            'createext': {
               "application/zip": "zip",
               "application/x-tar": "tar", 
               "application/x-gzip": "tgz"
            }                                          
        }
    }

    def __init__(self, params={}, opts={}):
        for param in params:
            self._init_params[param] = params.get(param)
        for opt in opts:
            self._options[opt] = opts.get(opt)
        self._init_args = {
            'api': self._api,
        }
        self._init_args.update(self._init_params)
        self._init_args.update({'options':self._options})
    
    def __info_directory(self, env, directory):
        root = directory.is_root_directory or not directory.parent_directory.check_access("read")
        info = {
            "name": directory.name,
            "hash": 'directory_' + str(directory.id),
            "mime": "directory",
            "ts": time.mktime(datetime.datetime.strptime(directory.write_date, "%Y-%m-%d %H:%M:%S").timetuple()),
            "size": directory.size,
            "dirs": len(directory.child_directories) > 0 and 1 or 0,
            "read": directory.perm_read and 1 or 0,
            "create": directory.perm_create and 1 or 0,
            "write": directory.perm_write and 1 or 0,
            "unlink": directory.perm_unlink and 1 or 0,
            "locked": directory.is_locked() and 1 or 0,
            "isowner": directory.create_uid == env.user,
            "volumeid": 'root_' + str(directory.settings.id),
        }
        if not root:
            info.update({"phash": 'directory_{}'.format(directory.parent_directory.id)})
        return info
    
    def __info_file(self, env, file, csrf_token = None):
        root = file.directory.check_access("read")
        info = {
            'name': file.name,
            'hash': 'file_' + str(file.id),
            'phash': root and 'directory_' + str(file.directory.id) or "",
            'mime': file.mimetype,
            'ts': time.mktime(datetime.datetime.strptime(file.write_date, "%Y-%m-%d %H:%M:%S").timetuple()),
            'read': file.perm_read and 1 or 0,
            'write': file.perm_write and 1 or 0,
            'rm': file.perm_unlink and 1 or 0,
            'locked': file.is_locked() and 1 or 0,
            'size': file.size,
            'root': 'root_' + str(file.settings.id),
            'isowner': file.create_uid == env.user,
        }
        
        if csrf_token and file.mimetype in ['image/jpeg', 'image/png']: 
            info.update({"tmb": self.__get_thumbnail_url(file, csrf_token)})
            
        return info
            
   
    def _open(self, env, args):
        response = {}
        init = args.get('init')
        target = args.get('target')
        tree = args.get('tree')
        if init and init == u'1':
            response.update(self._init_args)
        else:
            response.update({'options':self._options})
        if target and target != u'':
            directory = env['muk_dms.directory'].search([('id', '=', target.split('_')[1])], limit=1)
            if len(directory) > 0:
                current = self.__info_directory(env, directory)
                response['cwd'] = current
                response['files'] =  []
                for child in directory.child_directories:
                    response['files'].append(self.__info_directory(env, child))
                for file in  directory.files:
                    response['files'].append(self.__info_file(env, file, args["csrf_token"]))
                if tree and tree == u'1':
                    roots = env['muk_dms.directory']
                    files = env['muk_dms.file']
                    settings = env['muk_dms.settings'].search([('show_tree', '=', True)])
                    for setting in settings:
                        roots |= setting.top_directories
                        files |= setting.top_files
                    for root in roots[0].id == directory.id and roots[1:] or roots:
                        response['files'].append(self.__info_directory(env, root))
                    for file in files:
                        response['files'].append(self.__info_directory(env, file.sudo().directory))
                        response['files'].append(self.__info_file(env, file, args["csrf_token"]))
            else:
                response = {"error" : ["errOpen", directory.name]} 
        else:
            roots = env['muk_dms.directory']
            files = env['muk_dms.file']
            settings = env['muk_dms.settings'].search([('show_tree', '=', True)])
            for setting in settings:
                roots |= setting.top_directories
                files |= setting.top_files
            if len(roots) > 0:
                current = self.__info_directory(env, roots[0])
                response['cwd'] = current
                response['files'] =  [current]
                for child in  roots[0].child_directories:
                    response['files'].append(self.__info_directory(env, child))
                for file in  roots[0].files:
                    response['files'].append(self.__info_file(env, file, args["csrf_token"]))
                if tree and tree == u'1':
                    for root in roots[1:]:
                        response['files'].append(self.__info_directory(env, root))
                    for file in files:
                        response['files'].append(self.__info_file(env, file, args["csrf_token"]))
            else:
                response = {"error" : ["errConf", "errJSON"]}

            
        return response
    
    def _file(self, env, args):
        target = env['muk_dms.file'].search([('id', '=', args['target'].split('_')[1])], limit = 1)
        return target
    
    def __tree(self, env, directory):
        tree = []
        for child in directory.child_directories:
            tree.append(self.__info_directory(env, child))
        return tree
    
    def _tree(self, env, args):
        if 'target' in args:
            return {'tree': self.__tree(env, env['muk_dms.directory'].search([('id', '=', args.get('target').split('_')[1])], limit=1))}
        if 'target[]' in args:
            tree = []
            targets = args.get('target[]')
            for target in targets:
                tree.extend(self.__tree(env, env['muk_dms.directory'].search([('id', '=', target.split('_')[1])], limit=1)))
            return {'tree': tree}
    
    def _parents(self, env, args):
        tree = []
        directory = env['muk_dms.directory'].search([('id', '=', args.get('target').split('_')[1])], limit=1)
        while directory.parent_directory:
            directory = directory.parent_directory
            tree.extend(self.__tree(env, directory))
        tree.append(self.__info_directory(env, directory))
        
        return {'tree': tree}
    
    def _ls(self, env, args):
        directories = env['muk_dms.file'].search([('directory', '=', args.get('target').split('_')[1])])
        return {'list': directories.mapped('name')}
    
    def _size(self, env, args):
        size = 0
        if 'targets[]' in args:
            targets = args['targets[]']
            for value in targets:
                type, id =  value.split('_')
                if type == 'file':
                    size += env['muk_dms.file'].search([('id', '=', id)], limit=1).size
                if type == 'directory':
                    size += env['muk_dms.directory'].search([('id', '=', id)], limit=1).size
        for key, value in args.items():
            if key.startswith("targets[") and key != "targets[]":
                type, id =  value.split('_')
                if type == 'file':
                    size += env['muk_dms.file'].search([('id', '=', id)], limit=1).size
                if type == 'directory':
                    size += env['muk_dms.directory'].search([('id', '=', id)], limit=1).size
        return {"size": size}
    
    def _dim(self, env, args):
        file = env['muk_dms.file'].search([('id', '=', args.get('target').split('_')[1])], limit=1)
        dimensions = PIL.Image.open(io.BytesIO(base64.b64decode(file.content))).size
        return {"dim" : "%sx%s" % (dimensions[0], dimensions[1])}
    
    def _mkdir(self, env, args):
        target_directory = env['muk_dms.directory'].search([('id', '=', args.get('target').split('_')[1])], limit=1)
        
        if not target_directory.perm_create:
            return {"error" : ["errWrite", target_directory.name]}
        
        directory_store = {'/': target_directory}
        new_directories = []
        if 'name' in args:
            name = args.get('name')
            if name in target_directory.child_directories.mapped("name"):
                return {"error" : ["errExists", name]}
            
            new_directory = env['muk_dms.directory'].create({'name': name, 'parent_directory': target_directory.id})
            new_directories.append(self.__info_directory(env, new_directory))
        if 'dirs[]' in args:
            dirs = args.get('dirs[]')
            
            already_exists = []
            for value in dirs:
                foldername = value.split('/')[-1:][0]
                if not value[:value.rfind('/')]:
                    if foldername in target_directory.child_directories.mapped("name"):
                        already_exists.append(foldername)
                        
            if already_exists:
                return {"error" : ["errExists",  ",".join(already_exists)]}
            
            for value in dirs:
                foldername = value.split('/')[-1:][0]
                parentfoldername = value[:value.rfind('/')]
                _logger.info(directory_store)
                _logger.info(parentfoldername)
                ### TODO Prüfen ob parent erzeugt werden darf
                if not parentfoldername:
                    parentfoldername = '/'
                if directory_store[parentfoldername]:
                    new_directory = env['muk_dms.directory'].create({'name': foldername, 'parent_directory': directory_store[parentfoldername].id})
                    directory_store.update({value: new_directory})
                    new_directories.append(self.__info_directory(env, new_directory))
        return {
            'added': new_directories, 
            'changed': [self.__info_directory(env, target_directory)],
            'removed': []
        }
    
    def _mkfile(self, env, args):
        target_directory = env['muk_dms.directory'].search([('id', '=', args.get('target').split('_')[1])], limit=1)
        name = args.get('name')
        
        if not target_directory.perm_create:
            return {"error" : ["errWrite", target_directory.name]}
        if name in target_directory.files.mapped("name"):
            return {"error" : ["errExists", name]}
        
        empty_file = io.BytesIO(b'# New File')
        file = base64.b64encode(empty_file.getvalue())
        empty_file.close()
        new_file = env['muk_dms.file'].create({
            'name': name,
            'content': file,
            'directory': target_directory.id
        })
        return {
            'added': [self.__info_file(env, new_file)], 
            'changed': [self.__info_directory(env, target_directory)],
            'removed': []
        }
    
    def _rm(self, env, args):
        targets = args['targets[]']
        
        missing_rights = []
        for value in targets:
            type, id =  value.split('_')
            if type == 'file':
                file = env['muk_dms.file'].search([('id', '=', id)], limit=1)
                if not file.perm_unlink:
                    missing_rights.append(file.name)
            if type == 'directory':
                directory = env['muk_dms.directory'].search([('id', '=', id)], limit=1)
                if not directory.perm_unlink:
                    missing_rights.append(directory.name)
        if missing_rights:
            return {"error" : ["errRm", ",".join(missing_rights)]}
                  
        changed = []
        removed = [] 
        
        for value in targets:
            removed.append(value)
            type, id =  value.split('_')
            if type == 'file':
                file = env['muk_dms.file'].search([('id', '=', id)], limit=1)
                changed.append(self.__info_directory(env, file.directory))
                file.unlink()
            if type == 'directory':
                directory = env['muk_dms.directory'].search([('id', '=', id)], limit=1)
                changed.append(self.__info_directory(env, directory.parent_directory))
                directory.unlink()
        return {
            'added': [], 
            'changed': changed,
            'removed': removed
        }

    def _rename(self, env, args):
        type, id =  args.get('target').split('_')
        if type == 'file':
            file = env['muk_dms.file'].search([('id', '=', id)], limit=1)
            
            if not file.perm_write:
                return {"error" : ["errRename", file.name]}
            
            file.write({'name': args.get('name')})
            info = self.__info_file(env, file, args["csrf_token"])
        if type == 'directory':
            directory = env['muk_dms.directory'].search([('id', '=', id)], limit=1)
            
            if not directory.perm_write:
                return {"error" : ["errRename", directory.name]}
            
            directory.write({'name': args.get('name')})
            info = self.__info_directory(env, directory)
        return {
            'added': [info], 
            'changed': [],
            'removed': [args.get('target')]
        }

    def _duplicate(self, env, args):
        targets = args["targets[]"]
        
        missing_rights = []
        for value in targets:
            if type == 'file':
                file = env['muk_dms.file'].search([('id', '=', id)], limit=1)
                if not file.directory.perm_create:
                    missing_rights.append(file.directory.name)
            if type == 'directory':
                directory = env['muk_dms.directory'].search([('id', '=', id)], limit=1)
                if not directory.parent_directory.perm_create:
                    missing_rights.append(directory.parent_directory.name)
                    
        if missing_rights:
            return {"error" : ["errWrite", ",".join(missing_rights)]}
            
        added = []
        changed = []
                
        for value in targets:
            type, id =  value.split('_')
            if type == 'file':
                file = env['muk_dms.file'].search([('id', '=', id)], limit=1)
                new_file = file.copy()
                info = self.__info_file(env, new_file)
                info_parent = self.__info_directory(env, new_file.directory)
            if type == 'directory':
                directory = env['muk_dms.directory'].search([('id', '=', id)], limit=1)
                new_directory = directory.copy()
                info = self.__info_directory(env, new_directory)
                info_parent = self.__info_directory(env,new_directory.parent_directory)
            changed.append(info_parent)
            added.append(info)
        return {
            'added': added, 
            'changed': changed,
            'removed': []
        }

    def _paste(self, env, args):
        type, id =  args.get('dst').split('_')
        dst = env['muk_dms.directory'].search([('id', '=', id)], limit=1)
        
        if not dst.perm_create:
            return {"error" : ["errWrite", dst.name]}
        
        added = []
        changed = []
        removed = []
        cut = 'cut' in args and int(args.get('cut')) == 1
        targets = args['targets[]']
        for value in targets:
            if cut:
                removed.append(value)
            type, id =  value.split('_')
            if type == 'file':
                file = env['muk_dms.file'].search([('id', '=', id)], limit=1)
                if len(dst.files.filtered(lambda r: r.name == file.name)) > 0:
                    dst.files.filtered(lambda r: r.name == file.name).unlink()
                if cut:
                    file.directory = dst
                else:
                    new_file = file.copy()
                    new_file.write({'name': file.name, 'directory': dst.id})
                    file = new_file
                info = self.__info_file(env, file, args["csrf_token"])
            if type == 'directory':
                directory = env['muk_dms.directory'].search([('id', '=', id)], limit=1)
                if len(dst.child_directories.filtered(lambda r: r.name == directory.name)) > 0:
                    dst.child_directories.filtered(lambda r: r.name == directory.name).unlink()
                if cut:
                    directory.parent_directory = dst
                else:
                    new_dir = directory.copy()
                    new_dir.write({'name': directory.name, 'parent_directory': dst.id})
                    directory = new_dir
                info = self.__info_directory(env, directory)  
            added.append(info)
                
        changed.append(self.__info_directory(env,dst))
        
        if 'src' in args:
            type, id =  args.get('src').split('_')
            src = env['muk_dms.directory'].search([('id', '=', id)], limit=1)
            changed.append(self.__info_directory(env,src))
        
        return {
            'added': added, 
            'changed': changed,
            'removed': removed
        }
        

    def _upload(self, env, args):
        target_directory = env['muk_dms.directory'].search([('id', '=', args.get('target').split('_')[1])], limit=1)
        upload = args['upload[]']
        upload_paths = args['upload_path[]']
        upload_index = 0
        
        if not target_directory.perm_create:
            return {"error": ["errWrite", target_directory.name]}
        
        already_exists = []
        for value in upload:
            if not upload_paths[upload_index].split('/')[1:-1]:
                if value.filename.split("/")[-1:][0] in target_directory.files.mapped("name"):
                    already_exists.append(value.filename.split("/")[-1:][0])
            upload_index = upload_index + 1
                    
        if already_exists:
            return {"error" : ["errExists", ",".join(already_exists)]}
        
        upload_index = 0
        new_files = []
        for value in upload:
            folders = upload_paths[upload_index].split('/')[1:-1]
            dir = target_directory
            for folder in folders:
                dir = env['muk_dms.directory'].search(['&',('name','=',folder),('parent_directory','=',dir.id)])
            new_file = env['muk_dms.file'].create({
                'name': value.filename.split("/")[-1:][0],
                'content': base64.b64encode(value.stream.read()).decode("UTF-8"),
                'directory': dir.id
            })
            new_files.append(self.__info_file(env, new_file))
            upload_index = upload_index + 1
            
        return {
            'added': new_files, 
            'changed': [self.__info_directory(env, target_directory)],
            'removed': []
        }
        
    
    def _get(self, env, args):
        target = env['muk_dms.file'].search([('id', '=', args['target'].split('_')[1])], limit = 1)
        
        if not target.perm_write:
            return {"error": ["errOpen", target.name]}
        
        if 'conv'  in args:
            conv = args['conv']
        result = {"content": base64.b64decode(target.content).decode('UTF-8')}
        return result
        
    def _put(self, env, args):
        target = env['muk_dms.file'].search([('id', '=', args['target'].split('_')[1])], limit = 1)
        
        if not target.perm_write:
            return {"error": ["errWrite", target.name]}
        
        content = args['content']
        encoding = args['encoding']
        target.update({'content': base64.b64encode(content.encode(encoding))})
        return {
            'added': [],
            'changed': [self.__info_file(env, target)],
            'removed': []
        }
        
    def __get_all_files_including_subdirectories(self,directory):
        result = directory.files
        for directory in directory.child_directories:
            result = result + self.__get_all_files_including_subdirectories(directory)
        return result
    
    def _archive(self, env, args):
        target = env['muk_dms.directory'].search([('id', '=', args['target'].split('_')[1])], limit=1)
        name = args['name']
        
        if not target.perm_create:
            return {"error" : ["errWrite", target.name]}
        if name in target.files.mapped("name"):
            return {"error" : ["errExists", name]}
        
        type = args['type']
        added_archives = []
        if 'targets[]' in args:
            o = io.BytesIO()
            zf = zipfile.ZipFile(o, mode='w', compression=zipfile.ZIP_DEFLATED)
            for object in args['targets[]']:
                type, id =  object.split('_')
                if type == 'file':
                    file = env['muk_dms.file'].search([('id', '=', id)], limit=1)
                    zf.writestr(file.name, base64.b64decode(file.content))
                if type == 'directory':
                    directory = env['muk_dms.directory'].search([('id', '=', id)], limit=1)
                    path_prefix = directory.name + "/"
                    all_files_including_subdirectories = self.__get_all_files_including_subdirectories(directory)
                    for file in all_files_including_subdirectories:
                        # FIXME 
                        filepath = path_prefix + file.directory.path[len(directory.path):] + file.name
                        zf.writestr(filepath, base64.b64decode(file.content))
                pass
            zf.close()
            archive = env['muk_dms.file'].create({
                    'name': name,
                    'content': base64.b64encode(o.getvalue()), # TODO Archive Inhalt
                    'directory': target.id
                })
            o.close()
            added_archives.append(self.__info_file(env, archive))
        
        return {
            'added': added_archives,
            'changed': [],
            'removed': []
        }
        
    def _info(self, env, args):
        fileinfo = []
        if 'targets[]' in args:
            targets = args['targets[]']
            for target in targets:
                type, id =  target.split('_')
                if type == 'file':
                    file = env['muk_dms.file'].search([('id', '=', target.split('_')[1])], limit = 1)
                    fileinfo.append(self.__info_file(env, file, args["csrf_token"]))
                if type == 'directory':
                    file = env['muk_dms.directory'].search([('id', '=', target.split('_')[1])], limit = 1)
                    fileinfo.append(self.__info_directory(env, file))
        return {"files" : fileinfo}
    
    def _url(self, env, args):
        target = env['muk_dms.file'].search([('id', '=', args['target'].split('_')[1])], limit = 1)
        url = "/web/content?model=muk_dms.file&filename_field=name&field=content&id={}&download=true".format(target.id)
        return url
    
    def _zipdl(self, env, args):
        if 'download' in args:
            objects = json.load(args['targets[1]'])
            o = io.BytesIO()
            zf = zipfile.ZipFile(o, mode='w', compression=zipfile.ZIP_DEFLATED)
            for object in objects:
                type, id =  object.split('_')
                if type == 'file':
                    file = env['muk_dms.file'].search([('id', '=', id)], limit=1)
                    zf.writestr(file.name, base64.b64decode(file.content))
                if type == 'directory':
                    directory = env['muk_dms.directory'].search([('id', '=', id)], limit=1)
                    path_prefix = directory.name + "/"
                    all_files_including_subdirectories = self.__get_all_files_including_subdirectories(directory)
                    for file in all_files_including_subdirectories:
                        filepath = path_prefix + file.path[len(directory.path):]
                        zf.writestr(filepath, base64.b64decode(file.content))
                pass
            zf.close()
            content = o.getvalue()
            o.close()
            return content
        else:
            return {
                'file': json.dumps(args['targets[]']),
                'name': 'Download.zip',
                'mime': 'application/zip'
                }
    
    def _tmb(self, env, args):
        images = {}
        if 'targets[]' in args:
            targets = args['targets[]']
            for target in targets:
                type, id =  target.split('_')
                directory = env['muk_dms.directory'].search([('id', '=', id)], limit=1)
                for file in directory.files:
                    images['file_{}'.format(file.id)] = __get_thumbnail_url(file, args['csrf_token'])
        return {"images" : images}
    
    def __get_thumbnail_url(self,file,csrf_token):
        url = "/dms/connector/elfinder?csrf_token={}&cmd=muk_tmb&file=file_{}".format(csrf_token,file.id)
        return url
    
    def _muk_tmb(self, env, args):
        type, id = args.get("file").split("_")
        file = env["muk_dms.file"].search([('id', '=', id)], limit=1)
        if file.mimetype in ['image/jpeg', 'image/png']:
            image = PIL.Image.open(io.BytesIO(base64.b64decode(file.content)))
            dim = image.size
            image.thumbnail((128, 128 * dim[0] / dim[1]),PIL.Image.ANTIALIAS)
            tmb = io.BytesIO()
            image.save(tmb,"JPEG")
            return {"muk_tmb": tmb.getvalue()}
        else:
            return {"error" : "Thumbnails are not supported for this mime type!"}
    
    def _netmount(self, env, args):
        return {"error" : "errUnknownCmd"}
    
    def _extract(self, env, args):
        return {"error" : "errUnknownCmd"}
    
    def _search(self, env, args):
        return {"error" : "errUnknownCmd"}
    
    def _resize(self, env, args):
        return {"error" : "errUnknownCmd"}
    
    def _callback(self, env, args):
        return {"error" : "errUnknownCmd"}
    
    def _chmod(self, env, args):
        return {"error" : "errUnknownCmd"}
   
    _dispatch  = {
        'open': _open,
        'file': _file,
        'tree': _tree,
        'parents': _parents,
        'ls': _ls,
        'tmb': _tmb,
        'muk_tmb': _muk_tmb,
        'size': _size,
        'dim': _dim,
        'mkdir': _mkdir,
        'mkfile': _mkfile,
        'rm': _rm,
        'rename': _rename,
        'duplicate': _duplicate,
        'paste': _paste,
        'upload': _upload,
        'get': _get,
        'put': _put,
        'archive': _archive,
        'extract': _extract,
        'search': _search,
        'info': _info,
        'resize': _resize,
        'url': _url,
        'netmount': _netmount,
        'zipdl': _zipdl,
        'callback': _callback,
        'chmod': _chmod,
    }
    
    def cmd(self, request):
        try:
            params = request.params
            for key, value in params.items():
                if key.endswith("[]"):
                    if key in request.httprequest.args:
                        params.update({key: request.httprequest.args.getlist(key)})
                    if key in request.httprequest.form:
                        params.update({key: request.httprequest.form.getlist(key)})
                    if key in request.httprequest.files:
                        params.update({key: request.httprequest.files.getlist(key)})
            return self._dispatch[params['cmd']](self, request.env, params)
        except KeyError as e:
            return {"error" : "This command is not supported yet! {}".format(e)}
  #      except AccessError as e:
  #          return {"error" : "Operation is not allowed! {}".format(e.name)}
  #      except ValidationError as e:
  #          return {"error" : "Operation couldn't be executed! {}".format(e.name)}

    