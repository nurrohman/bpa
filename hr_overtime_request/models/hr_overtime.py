# -*- coding: utf-8 -*-

import time
from datetime import datetime

from odoo import models, fields, api, _
import odoo.addons.decimal_precision as dp
from odoo.exceptions import except_orm, Warning, RedirectWarning, UserError, ValidationError



Datetime_FORMAT = '%Y-%m-%d'


class hr_overtime(models.Model):
    _name = 'hr.overtime'
    _description = 'Employee Overtime'

    _rec_name = 'employee_id'


    def get_total_minutes(self, rec):
        from datetime import datetime
        dt_start_lembur = datetime.strptime(self.date_from, '%Y-%m-%d %H:%M:%S')

        if 'date_to' in rec:
            date_to = rec['date_to']
        else:
            date_to = self.date_to

        dt_end_lembur = datetime.strptime(date_to, '%Y-%m-%d %H:%M:%S')

        lembur_dalam_menit = round(
            (dt_end_lembur - dt_start_lembur).total_seconds() / 60)

        """
        # jika senin sampai jumat
        if dt_end_lembur.weekday() not in (5, 6):
            if (lembur_dalam_menit > 60):
                # minutes 1 jam pertama
                minutes_jam_pertama = 60 * 1.5

                # sisa minutes dikurangi 1 jam
                minutes_selanjutnya = (lembur_dalam_menit - 60) * 2

                total_minutes = minutes_jam_pertama + minutes_selanjutnya
            else:
                total_minutes = lembur_dalam_menit * 1.5
        else:
            # jika sabtu dan minggu
            total_minutes = 0

            # kurang dari 8 jam
            if lembur_dalam_menit <= (8*60):
                total_minutes = lembur_dalam_menit * 2

            # lebih dari 8 jam kurang tapi kurang dari 10 jam
            elif lembur_dalam_menit > (8*60) and lembur_dalam_menit <= (9*60):
                konversi_8_jam_pertama = (8*60*2)
                minutes_sisa = lembur_dalam_menit - (8*60)
                konversi_sisa = minutes_sisa * 3
                total_minutes = konversi_8_jam_pertama + konversi_sisa
            else:
                konversi_8_jam_pertama = (8*60*2)
                konversi_jam_8_9 = (60*3)                
                minutes_sisa = lembur_dalam_menit - (8*60) - 60
                konversi_sisa = minutes_sisa * 4
                total_minutes = konversi_8_jam_pertama + konversi_jam_8_9 + konversi_sisa
        """

        return lembur_dalam_menit

    def validasi_end_date(self, rec, tanggal_akhir):
        qq=0
        tanggal_awal = rec.date_from
        from datetime import datetime, timedelta
        # datetime.strptime('2018-03-04 12:03:03', '%Y-%m-%d %H:%M:%S')
        datetime_tanggal_awal = datetime.strptime(tanggal_awal, '%Y-%m-%d %H:%M:%S')
        datetime_tanggal_akhir = datetime.strptime(tanggal_akhir, '%Y-%m-%d %H:%M:%S')

        # jika tanggal awal lebih besar dari tanggal akhir
        if datetime_tanggal_awal > datetime_tanggal_akhir:
            raise ValidationError(_("Tanggal awal tidak boleh lebih besar dari tanggal akhir !"))


    @api.model
    def create(self, vals):
        rec = super(hr_overtime, self).create(vals)
        return rec

    @api.multi
    def write(self,vals):
        # import ipdb
        # ipdb.set_trace(context=35)
        if (self.state == 'approved_manager' and 'date_to' in vals) or 'istirahat' in vals:
            if 'date_to' in vals:
                date_to = vals['date_to']
            else:
                date_to = self.date_to
            tanggal_akhir = date_to
            # self.date_to = tanggal_akhir
            self.validasi_end_date(self,tanggal_akhir)


            if 'istirahat' in vals:
                istirahat = vals['istirahat']
            else:
                istirahat = self.istirahat

            vals['leave_rights_convertion'] = self.get_total_minutes(vals) - istirahat
        res = super(hr_overtime,self).write(vals)
        return res

    @api.multi
    def unlink(self):
        for request in self:
            if request.state not in ('draft', 'cancel'):
                raise Warning(
                    _('You cannot delete an overtime request which is not draft or cancelled.'))
        return super(hr_overtime, self).unlink()

    @api.model
    def _employee_get(self):
        ids = self.env['hr.employee'].search(
            [('user_id', '=', self._uid)], limit=1)
        if ids:
            return ids

    @api.onchange('employee_id')
    def onchange_employee_id(self):
        self.job_id = self.employee_id.job_id
        # self.work_location = self.employee_id.work_location
        self.overtime_work_location = self.employee_id.project_id

        if self.employee_id.category_ids:
            self.category_id = self.employee_id.category_ids[0].id
        self.company_id = self.employee_id.company_id.id
        self.manager_id = self.employee_id.parent_id.id
        self.department_manager_id = self.employee_id.department_id and self.employee_id.department_id.manager_id and self.employee_id.department_id.manager_id.id or False

    #_inherit = ['mail.thread', 'ir.needaction_mixin']
    _inherit = ['mail.thread', 'mail.activity.mixin', 'portal.mixin']

    istirahat = fields.Integer('Break')


    name = fields.Char(string='Description', required=False, states={
                       'draft': [('readonly', False)]}, readonly=True)
    number_of_hours = fields.Float(string='Overtime Minutes', states={
                                   'draft': [('readonly', False)]}, readonly=True, store=True)
    include_payroll = fields.Boolean(
        string='Include In Payroll', help='Tick if you want to include this overtime in employee payroll', default=True)

    state = fields.Selection(selection=[
        ('draft', 'New'),
        ('approved_manager', 'Approved Manager'),
        ('approved_hrd', 'Approved HRD'),
        ('approved_direksi', 'Approved Direksi'),
        ('done', 'Done'),
        ('cancel', 'Cancelled')
    ],string='Status', readonly=True, default='draft', track_visibility='onchange')

    user_id = fields.Many2one(related='employee_id.user_id', string='User', store=True,
                              default=lambda self: self.env.user, states={'draft': [('readonly', False)]}, readonly=True)
    date_from = fields.Datetime(string='Start Date', states={'draft': [(
        'readonly', False)]}, readonly=True, default=fields.datetime.now())
    date_to = fields.Datetime(string='End Date')
    approve_date = fields.Date(
        string='Department Approved Date', readonly=True, copy=False)
    hr_approve_date = fields.Date(
        string='Approved Date', readonly=True, copy=False)
    employee_id = fields.Many2one('hr.employee', string="Employee", select=True, required=True,
                                  default=_employee_get, states={'draft': [('readonly', False)]}, readonly=True)
    manager_id = fields.Many2one('hr.employee', 'Manager', states={'draft': [(
        'readonly', False)]}, readonly=True, help='This area is automatically filled by the user who will approve the request', copy=False)
    notes = fields.Text(string='Overtime Summary')
    department_id = fields.Many2one(related='employee_id.department_id', string='Department',
                                    type='many2one', relation='hr.department', readonly=True, store=True)
    category_id = fields.Many2one('hr.employee.category', string="Category", readonly=False, states={
                                  'validate': [('readonly', True)]}, help='Category of Employee')
    company_id = fields.Many2one('res.company', string='Company', required=True, readonly=False, states={
                                 'validate': [('readonly', True)]})
    approve_hr_manager_id = fields.Many2one(
        'res.users', string='Approved By', readonly=True, copy=False)
    approve_dept_manager_id = fields.Many2one(
        'res.users', string='Department Manager', readonly=True, copy=False)
    multiple_overtime_id = fields.Many2one(
        'hr.overtime.multiple', string="Overtime Multiple Request")
    department_manager_id = fields.Many2one(
        'hr.employee', string='Department Manager(to hide)')

    job_id = fields.Many2one('hr.job', 'Job Position')
    work_location = fields.Many2one('res.country.city')
    overtime_work_location = fields.Many2one('project.project')

    keperluan = fields.Char('Keperluan')
    overtime_meal = fields.Boolean('Overtime Meal')
    leave_rights_convertion = fields.Integer('Overtime Duration')

    @api.onchange('date_from')
    def onchange_start_date(self):
        if self.date_to and self.date_from:
            date_start = datetime.strptime(self.date_from, "%Y-%m-%d %H:%M:%S")
            date_end = datetime.strptime(self.date_to, "%Y-%m-%d %H:%M:%S")
            diff_hours = date_end - date_start
            self.number_of_hours = (
                diff_hours.seconds / 3600.00) + (diff_hours.days * 24.00)
        else:
            self.number_of_hours = 0.0

    @api.onchange('date_to')
    def onchange_end_date(self):
        if self.date_to and self.date_from:
            date_start = datetime.strptime(self.date_from, "%Y-%m-%d %H:%M:%S")
            date_end = datetime.strptime(self.date_to, "%Y-%m-%d %H:%M:%S")
            diff_hours = date_end - date_start
            self.number_of_hours = (
                diff_hours.seconds / 3600.00) + (diff_hours.days * 24.00)
        else:
            self.number_of_hours = 0.0

    @api.multi
    def set_to_draft(self):
        self.write({
            'state': 'draft',
            'approve_date': False,
            'hr_approve_date': False,
            'approve_hr_manager_id': False,
            'approve_dept_manager_id': False
        })
        return True

    @api.multi
    def ot_cancel(self):
        self.write({'state': 'cancel'})
        return True

    @api.multi
    def approve_manager(self):
        self.write({'state': 'approved_manager'})
        return True

    @api.multi
    def approve_hrd(self):
        self.write({'state': 'approved_hrd'})
        return True

    @api.multi
    def approve_direksi(self):
        self.write({'state': 'approved_direksi'})

        leave_days = self.leave_rights_convertion / 8 /60


        leave = self.env['hr.holidays'].create({
            'name': 'Tambah cuti dari leave rights convertion',
            'employee_id': self.employee_id.id,
            'holiday_status_id': 1,
            'type': 'add',
            'state': 'validate',
            'limit': False,
            'holiday_type': 'employee',
            'number_of_days_temp': leave_days
        })

        return True



    @api.multi
    def ot_refuse(self):
        obj_emp = self.env['hr.employee']
        ids2 = obj_emp.search([('user_id', '=', self._uid)], limit=1)
        manager = ids2 or False
        self.write(
            {'state': 'refuse', 'manager_id':  manager and manager.id or False})
        return True

    @api.multi
    def ot_validate(self):
        obj_emp = self.env['hr.employee']
        ids2 = obj_emp.search([('user_id', '=', self._uid)], limit=1)
        manager = ids2 or False
        return self.write({'state': 'validate', 'manager_id':  manager and manager.id or False,  'approve_date': time.strftime('%Y-%m-%d'), 'approve_dept_manager_id': self.env.user.id})

    @api.multi
    def ot_confirm(self):
        return self.write({'state': 'confirm'})

    @api.multi
    def hr_approval(self):
        return self.write({'state': 'approve_by_hr', 'approve_hr_manager_id': self.env.user.id, 'hr_approve_date': time.strftime('%Y-%m-%d')})


class Employee(models.Model):
    _inherit = 'hr.employee'

    overtime_ids = fields.One2many(
        'hr.overtime', 'employee_id', string='Overtimes')
