from odoo import fields, models, api, _
from datetime import datetime, timedelta
from odoo.exceptions import UserError


class bpjs_wizard(models.TransientModel):
    _name = 'bpjs.wizard'
    _description = 'BPJS Wizard'
    
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.user.company_id)
    date_from = fields.Date('Start Date', default=fields.Date.today())
    date_to = fields.Date('End Date', default=fields.date.today())
    type = fields.Selection([
        ('health', 'BPJS Kesehatan'),
        ('work', 'BPJS Ketenagakerjaan'),
    ], string='BPJS Type', default='health')
    
    employee_ids = fields.Many2many('hr.employee', string='Employees')
    
    @api.multi
    def view_bpjs_report(self):
        if self.type == 'work':
            return self.env.ref('bpa_report_xls.bpjs_ketenagakerjaan_xlsx').report_action(self.ids, config=False)
        else:
            return self.env.ref('bpa_report_xls.bpjs_kesehatan_xlsx').report_action(self.ids, config=False)
    
    @api.multi
    def _get_bpjs_kesehatan_datas(self):
        date_cast = datetime.strptime(self.date_from, '%Y-%m-%d')
        last_day_month = date_cast.replace(day=28) + timedelta(days=4)
        last_date_month = last_day_month - timedelta(days=last_day_month.day)
        domain = [
            ('state', '=', 'done'),
            ('date_from', '>=', str(datetime.strptime(self.date_from, '%Y-%m-%d').replace(day=1))[:10]),
            ('date_from', '<=', last_date_month)
        ]
        print(last_date_month)
        if self.employee_ids:
            domain.append(('employee_id', 'in', self.employee_ids.ids))

        payslips = self.env['hr.payslip'].search(domain)
        datas = []
        for payslip in payslips.sorted(key=lambda p: p.date_from):
            if payslip.employee_id.contract_ids:
                kescom_line = payslip.line_ids.filtered(lambda l: l.salary_rule_id.code == 'BPJSKESCOM')
                kes_line = payslip.line_ids.filtered(lambda l: l.salary_rule_id.code == 'BPJSKES')
                data = [
                    payslip.employee_id.name,
                    '',
                    payslip.employee_id.contract_ids[0].wage or 'No Contract',
                    payslip.employee_id.contract_ids[0].allowance / 100 * (payslip.employee_id.contract_ids[0].wage),
                    0,
                    payslip.employee_id.contract_ids[0].wage + payslip.employee_id.contract_ids[0].allowance / 100 * (payslip.employee_id.contract_ids[0].wage),
                    0.04 * sum(line.total for line in kescom_line) if kescom_line else 0,
                    0.01 * sum(line.total for line in kes_line) if kes_line else 0,
                    (0.04 * sum(line.total for line in kescom_line)) + 0.01 * sum(line.total for line in kes_line),
                    '',
                    payslip.employee_id.project_id.name
                ]
                datas.append(data)
        return datas
    
    @api.multi
    def _get_bpjs_ketenagakerjaan_datas(self):        
        date_cast = datetime.strptime(self.date_from, '%Y-%m-%d')
        last_day_month = date_cast.replace(day=28) + timedelta(days=4)
        last_date_month = last_day_month - timedelta(days=last_day_month.day)
        domain = [
            ('state', '=', 'done'),
            ('date_from', '>=', str(datetime.strptime(self.date_from, '%Y-%m-%d').replace(day=1))[:10]),
            ('date_from', '<=', last_date_month)
        ]
        print(last_date_month)
        if self.employee_ids:
            domain.append(('employee_id', 'in', self.employee_ids.ids))

        payslips = self.env['hr.payslip'].search(domain)
        datas = []
        for payslip in payslips.sorted(key=lambda p: p.date_from):
            if payslip.employee_id.contract_ids:
                data = [
                    payslip.employee_id.no_bpjs_ketenagakerjaan,
                    payslip.employee_id.name,
                    payslip.employee_id.project_id.name,
                    payslip.employee_id.contract_ids[0].wage or 'No Contract',
                    0.024 * payslip.employee_id.contract_ids[0].wage or 0,
                    0.02 * payslip.employee_id.contract_ids[0].wage or 0,
                    0.037 * payslip.employee_id.contract_ids[0].wage or 0,
                    0.003 * payslip.employee_id.contract_ids[0].wage or 0,
                    0.01 * payslip.employee_id.contract_ids[0].wage or 0,
                    0.02 * payslip.employee_id.contract_ids[0].wage or 0,
                ]
                datas.append(data)
        return datas
    
