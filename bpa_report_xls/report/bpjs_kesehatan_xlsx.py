from odoo import models, _, tools
import operator, time
from datetime import datetime
import babel
from odoo.exceptions import UserError


class bpjs_kesehatanxlsx(models.AbstractModel):
    _name = 'report.bpa_report_xls.bpjs.kesehatan.xlsx'
    _inherit = 'report.report_xlsx.abstract'
    
    def generate_xlsx_report(self, workbook, data, objects):
        columns = [
                      ['NO', 4],
                      ['Nama', 14],
                      ['Jumlah Jiwa', 10],
                      ['Gaji Pokok', 14],
                      ['Tunjangan Tetap', 14],
                      ['Total Riil', 10],
                      ['Total', 10],
                      ['Pemberi Kerja (4%)', 16],
                      ['Pekerja (1%)', 10],
                      ['Total', 10],
                      ['Kelas Rawat', 10],
                      ['Keterangan', 13],
                ]
    
        style_hdr_blue = workbook.add_format({'align': 'center', 'border': 1, 'bg_color': 'cyan'})
        style_normal_center = workbook.add_format({'align': 'center', 'border': 1})
        style_bold_center = workbook.add_format({'border': 1, 'align': 'center', 'bold': True})
        style_normal = workbook.add_format({'border': 1, 'num_format': '#,##0'})
        style_normal_bold = workbook.add_format({'border': 1, 'bold': True, 'num_format': '#,##0'})
        
        for obj in objects:
            datas = obj._get_bpjs_kesehatan_datas()
            report_name = 'BPJS Kesehatan'
            sheet = workbook.add_worksheet(report_name[:31])
            sheet.freeze_panes(5, 0)
            
            
            ttyme = datetime.fromtimestamp(time.mktime(time.strptime(obj.date_from, "%Y-%m-%d")))
            sheet.merge_range('A1:L1', 'Rekapitulasi Iuran BPJS Kesehatan - ' + obj.company_id.name, style_bold_center)
            sheet.merge_range('A2:L2', tools.ustr(babel.dates.format_date(date=ttyme, format='MMMM y', locale='en_US')), style_bold_center)

            row_count = 3
            col_count = 0
            sheet.merge_range('H4:J4', 'Iuran', style_hdr_blue)
            row_count += 1
            for column in columns:
                sheet.set_column(col_count, col_count, column[1]) 
                sheet.write(row_count, col_count, column[0], style_hdr_blue)
                col_count += 1
            
            col_count = 0
            row_count += 1
            row_start = row_count
            no = 1
            for payslip in datas:
                row_start = row_count + 1
                sheet.write(row_count, col_count, no, style_normal)
                col_count += 1  
                for line in payslip:
                    sheet.write(row_count, col_count, line, style_normal)
                    col_count += 1  
                no += 1
                col_count = 0
                row_count += 1
                
