{
    "name": "XLS Reports- BPA",
    "version": "1.0",
    "depends": ['bpa'],
    "author":"PT Visi",
    "category":"Reports",
    "description" : """Modul Print XLS Report for BPA""",
    'depends': [
        'hr_payroll',
        'report_xlsx'
    ],
    'data': [   
        'wizard/bpjs_wizard_view.xml',
    ],
    'demo':[
            # files containing demo data            
    ],
    'test':[
            # files containing tests
    ],
    'installable' : True,
    'auto_install' : False,
    'application': False,
}
