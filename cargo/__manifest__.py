# -*- coding: utf-8 -*-
{
    'name': 'Cargo Manifest',
    'version': "1.0",
    'price': 9999.0,
    'currency': 'IDR',
    'summary': 'Cargo Manifest',
    'description': """Cargo Manifest""",
    'category': 'Custom',
    'author': 'PT. VISI',
    'website': 'www.visi.co.id',
    'images': [],
    'depends': [
        'equipment_allocations'
    ],
    'data': [
      'security/ir.model.access.csv',
      'views/cargo_manifest.xml',
      'views/allocation_view.xml',
    ],
    'sequence': 1,
    'auto_install': False,
    'installable': True,
    'application': True,
}
