from odoo import models, fields, api
    

class CargoManifestLine(models.Model):
    _name = 'cargo.manifest.line'

    cargo_manifest_id = fields.Many2one('cargo.manifest',
        string='Cargo Manifest'
    )
    deskripsi = fields.Many2one(
        comodel_name='maintenance.equipment',
        string='Deskripsi'
    )

    @api.onchange('deskripsi')
    def onchange_deskripsi(self):
        self.nomor_seri = self.deskripsi.serial_no
    
    equipment_id = fields.Many2one('maintenance.equipment', string='Equipment')
    qty = fields.Integer(string='QTY')
    nomor_seri = fields.Char(string='Kode', readonly=True)    
    amount = fields.Float(string='Jumlah(RP)')        
    dimensi = fields.Integer(string='Dimension')        
    volume = fields.Integer(string='Volume')        
    weight = fields.Float(string='Berat(TON)')        
    remarks = fields.Char(string='Keterangan') 

    @api.multi
    @api.depends('qty', 'amount')
    def _compute_price(self):
        for rec in self:
            rec.subtotal = rec.qty * rec.amount    

    subtotal = fields.Float('Subtotal', compute='_compute_price')       
    
    allocation_id = fields.Many2one('allocation.request', string='SPB')
    equipment_child_id = fields.Many2one('equipment.lines', string='Equipment Child')
    
    @api.model
    def create(self, vals):
        res = super(CargoManifestLine, self).create(vals)
        hasil = self.allocation_id.write({'cargo_id': self.cargo_manifest_id.id})
        return res

