from odoo import models, fields, api
from odoo.exceptions import UserError


class Allocation(models.Model):
    _inherit = 'allocation.request'
     
    cargo_id = fields.Many2one('cargo.manifest', string='Cargo Reference')


class CargoManifest(models.Model):
    _name = 'cargo.manifest'
    
    @api.one
    @api.depends('line_ids.allocation_id')
    def _compute_allocations(self):
        self.allocation_ids = self.line_ids.mapped('allocation_id')
        
    customer = fields.Many2one(
        comodel_name='res.partner',
        string='Customer'
    )

    name = fields.Char(string='No.', required=True, copy=False, readonly=True, index=True, default='New')    
	# name = fields.Char(string='Order Reference', required=True, copy=False, readonly=True, index=True, default=lambda self: _('New'))

    tanggal = fields.Date(string='Tanggal')

    pelabuhan_keberangkatan = fields.Char(string='Pelabuhan Keberangkatan')
    pelabuhan_tujuan = fields.Char(string='Pelabuhan Tujuan')  

    cp_pelabuhan_keberangkatan = fields.Many2one(
        comodel_name='res.partner',
        string='Contact Person',
        required=True
    )

    cp_pelabuhan_tujuan = fields.Many2one(
        comodel_name='res.partner',
        string='Contact Person',
        required=True
    )

    kapal = fields.Char(string='Kapal')
    estimasi_tanggal_keberangkatan = fields.Date(string='Estimasi Tanggal Keberangkatan')
    estimasi_tanggal_kedatangan = fields.Date(string='Estimasi Tanggal Kedatangan')
    penerima_barang = fields.Char(string='Penerima Barang')
    agen_pelayaran_keberangkatan = fields.Char(string='Agen Pelayaran untuk Keberangkatan')
    agen_pelayaran_kedatangan = fields.Char(string='Agen Pelayaran untuk Kedatangan')
    asuransi_pelayaran = fields.Char(string='Asuransi Pelayaran')
    line_ids = fields.One2many('cargo.manifest.line', 'cargo_manifest_id',
        string='Cargo Manifest Line', copy=False
    )

    state = fields.Selection([('draft', 'Draft'),
                              ('to_approve_ka_peralatan', 'To Approve KA.Peralatan/Site Manager'),
                              ('to_approve_m_peralatan', 'To Approve Manager Perlatan'),
                              ('approved', 'Approved'),
                              ('cancel', 'Cancel')], string='Status', required=True,
                              track_visibility='onchange', default='draft', copy=False)    

    tanggal_konfirmasi_logistik = fields.Datetime(string='Tanggal dan jam dikonfirmasi logistik.')
    tanggal_konfirmasi_ka_peralatan = fields.Datetime(string='Tanggal dan jam dikonfirmasi ka peralatan.')
    tanggal_konfirmasi_m_peralatan = fields.Datetime(string='Tanggal dan jam dikonfirmasi Manager peralatan.')
    tanggal_cancel = fields.Datetime(string='Tanggal Cancel')
    tanggal_to_draft = fields.Datetime(string='Tanggal To Draft')
    # amount_total = fields.Float('Estimated Amount', compute='_compute_amount', digits=dp.get_precision('Product Price'), store=True)
    amount_total = fields.Float('Total', default=0, readonly=True, compute='_compute_all_total')
    allocation_id = fields.Many2one('allocation.request', string='Choose SPB')
    allocation_ids = fields.Many2many('allocation.request', string='SPB Reference', compute='_compute_allocations', store=True)
    
    # Load all unsold PO lines
    @api.onchange('allocation_id')
    def allocation_id_change(self):
        if not self.allocation_id:
            return {}
 
        new_lines = self.env['cargo.manifest.line']
        equipments = self.allocation_id.equipment_id + self.allocation_id.equipment_child_ids
        for line in equipments - self.line_ids.mapped('equipment_child_id'):
            data = {
                'allocation_id': self.allocation_id.id,
                'equipment_child_id': line.id,
                'equipment_id': line.equipment_child
            }
            new_line = new_lines.new(data)
            new_lines += new_line
 
        self.line_ids += new_lines
        self.allocation_id = False
        return {}
    
    @api.multi
    def button_confirm(self):
        for confirm in self:
            confirm.write({'state': 'to_approve_ka_peralatan', 'tanggal_konfirmasi_logistik': fields.Datetime.now()})
            for allocation in confirm.line_ids.mapped('allocation_id'):
                if allocation.cargo_id:
                    raise UserError(_('SPB %s sudah terdapat dalam Manifest %s') % (allocation.name, allocation.cargo_id.name))
            confirm.line_ids.mapped('allocation_id').write({'cargo_id': self.id})

    @api.multi
    def button_approve_ka_peralatan(self):
        for approve_kp in self:
            approve_kp.write({'state': 'to_approve_m_peralatan', 'tanggal_konfirmasi_ka_peralatan': fields.Datetime.now()})            
    
    @api.multi
    def button_approve_m_peralatan(self):
        for approve_m in self:
            approve_m.write({'state': 'approved', 'tanggal_konfirmasi_m_peralatan': fields.Datetime.now()})                

    @api.multi
    def button_cancel(self):
        for cargo in self:
            cargo.write({'state': 'cancel', 'tanggal_cancel': fields.Datetime.now()})
            cargo.line_ids.mapped('allocation_id').write({'cargo_id': False})
            cargo.line_ids.unlink()

    @api.multi
    def to_draft(self):
        for draft in self:
            draft.write({'state': 'draft', 'tanggal_to_draft': fields.Datetime.now()})                            

    @api.model
    def create(self, vals):
        from datetime import datetime
        vals['name'] = self.env['ir.sequence'].next_by_code('code') + '-' + datetime.today().strftime('%d/%m/%Y')
        res = super(CargoManifest, self).create(vals)
        return res

    @api.one
    @api.depends('line_ids')
    def _compute_all_total(self):
        total_amount = 0
        for rec in self.line_ids:
            total_amount = total_amount + rec.subtotal
        self.amount_total = total_amount

