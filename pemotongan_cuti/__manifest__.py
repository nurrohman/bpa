{
    'name': 'Pemotongan Cuti', 
    'version': '1.0', 
    'description': """ 
    Modul Pemotongan Cuti
    """, 
    'author': 'Bowo', 
    'depends': ['hr_attendance','hr_allowance','hr_holidays'], 
    'data': [
      'views/attendance_inherit.xml',
      'data/cron_pemotongan_cuti.xml'
    ], 
    'demo': [], 
    'installable': True, 
    'auto_install': False, 
}
