from odoo import models, fields, api
from odoo.exceptions import Warning, UserError, ValidationError


class InheritEmployee(models.Model):
    _inherit = 'hr.employee'

    pengurang_cuti = fields.Float('Leaves Deduction')

class LeavesDeduction(models.Model):
    _inherit = 'hr.attendance'

    leaves_deduction = fields.Float('Leave Deduction', default=0)

    leaves_deduction_done = fields.Boolean(default=False)

    @api.model
    def pemotongan_cuti_cron(self):

        from datetime import datetime
        str_year = datetime.today().strftime('%Y')
        str_month = datetime.today().strftime('%m')
        tgl_awal_bulan = str_year+'-'+str_month+'-01'                                                                                                                                    


        for i in self.env['hr.attendance'].search([
            ('leaves_deduction','>',0),
            ('leaves_deduction_done','=',False),
            ('check_in','<',tgl_awal_bulan)
            ]):

            # import ipdb
            # ipdb.set_trace(context=35)
            # i.leaves_deduction
            
            # tambah ke employee pemotongan cuti
            
            i.employee_id.pengurang_cuti = i.employee_id.pengurang_cuti + i.leaves_deduction

            # jadikan true leaves deduction supaya tidak mengurangi lagi
            i.leaves_deduction_done = True

    @api.multi
    def unlink(self):
        import datetime
        check_in = datetime.datetime.strptime(self.check_in, "%Y-%m-%d %H:%M:%S") 
        from datetime import datetime
        str_year = datetime.today().strftime('%Y')
        str_month = datetime.today().strftime('%m')
        tgl_awal_bulan = str_year+'-'+str_month+'-01'
        tgl_awal_bulan = datetime.strptime(tgl_awal_bulan, '%Y-%m-%d')
        if check_in < tgl_awal_bulan:
            raise ValidationError('Tidak dapat merubah/menghapus data bulan sebelum nya.')

    @api.multi
    def write(self, vals):

        import datetime
        check_in = datetime.datetime.strptime(self.check_in, "%Y-%m-%d %H:%M:%S")   
        check_out = datetime.datetime.strptime(self.check_out, "%Y-%m-%d %H:%M:%S")  
        difference = check_out - check_in 
        difference_second = difference.total_seconds()

        # check bila checkin sudah lewat bulan maka dibuat tidak bisa ngedit
        from datetime import datetime
        str_year = datetime.today().strftime('%Y')
        str_month = datetime.today().strftime('%m')
        tgl_awal_bulan = str_year+'-'+str_month+'-01'
        tgl_awal_bulan = datetime.strptime(tgl_awal_bulan, '%Y-%m-%d')

        if check_in < tgl_awal_bulan:
            raise ValidationError('Tidak dapat merubah/menghapus data bulan sebelum nya.')


        import ipdb
        ipdb.set_trace(context=35)

        check_in_weekday = check_in.weekday()  

        employee_obj = self.employee_id

        for val in employee_obj.resource_calendar_id.attendance_ids:

            if int(val['dayofweek']) == check_in_weekday:
                detik_hour_from = val['hour_from'] * 3600
                detik_hour_to = val['hour_to'] * 3600
                selisih = detik_hour_to - detik_hour_from
                # setengah_jam_kerja = selisih / 2

                # import ipdb
                # ipdb.set_trace(context=35)

                if difference_second > 0:
                    leaves_deduction = round( (selisih - difference_second) /60/60/8,2  )
                    vals['leaves_deduction'] = leaves_deduction
                else:
                    vals['leaves_deduction'] = 0.0


        # import ipdb
        # ipdb.set_trace(context=35)

        # self.leaves_deduction = leaves_deduction
        res = super(LeavesDeduction, self).write(vals)

        return res

    @api.model
    def create(self, values):
        import datetime
        check_in = datetime.datetime.strptime(values['check_in'], "%Y-%m-%d %H:%M:%S")   
        check_out = datetime.datetime.strptime(values['check_out'], "%Y-%m-%d %H:%M:%S")  
        difference = check_out - check_in 
        difference_second = difference.total_seconds()

        # import ipdb
        # ipdb.set_trace(context=35)

        # ipdb> check_in.weekday()                                                                                                                                                  


        # ambil days of week python 
        check_in_weekday = check_in.weekday()  
        # employee object
        employee_obj = self.env['hr.employee'].search([('id','=',values['employee_id'])])

        for val in employee_obj.resource_calendar_id.attendance_ids:

            if int(val['dayofweek']) == check_in_weekday:
                detik_hour_from = val['hour_from'] * 3600
                detik_hour_to = val['hour_to'] * 3600
                selisih = detik_hour_to - detik_hour_from
                # setengah_jam_kerja = selisih / 2

                if difference_second > 0:
                    leaves_deduction = round( (selisih - difference_second) /60/60/8,2  )
                    values['leaves_deduction'] = leaves_deduction
                else:
                    values['leaves_deduction'] = 0.0

                # import ipdb
                # ipdb.set_trace(context=35)

        return super(LeavesDeduction, self).create(values)

