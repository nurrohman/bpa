from odoo import models, fields, api

class ScheduleDateInvoice(models.TransientModel):
    _name = 'schedule.date.invoice'
    
    invoice_ids = fields.Many2many('account.invoice', 'Invoices')
    payment_method_id = fields.Many2one('account.journal', 'Payment')
    date_scheduled = fields.Date('Maturity Date')
    
    @api.multi
    def act_change_date(self):
        for invoice in self.invoice_ids:
            continue