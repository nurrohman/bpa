from odoo import models, fields, api, _


class TenderWizard(models.TransientModel):
    _name = "tender.wizard"
    
    purchase_ids = fields.Many2many('purchase.order', string='Purchases')
    tender_id = fields.Many2one('tender.management', 'Tender')
    project_location_id = fields.Many2one(related='tender_id.project_location_id', string='Project Location')
    reason = fields.Text('Comments')
    
    @api.multi
    def action_add_po(self):
        self.purchase_ids.write({'tender_id': self.tender_id.id})
        return True
    
    @api.multi
    def action_add_reason(self):
        if self._context.get('pending'):
            self.tender_id.action_pending()
            self.tender_id.pending_note = self.reason
        elif self._context.get('won'):
            self.tender_id.state = 'waiting_spk'
            self.tender_id.won_note = self.reason
        elif self._context.get('lost'):          
            self.tender_id.action_lost()
            self.tender_id.lost_note = self.reason
        elif self._context.get('withdraw'):          
            self.tender_id.action_withdraw()
            self.tender_id.withdraw_note = self.reason
        elif self._context.get('cancel'):
            self.tender_id.action_cancel()
            self.tender_id.cancel_note = self.reason
        elif self._context.get('submit') or self._context.get('resubmit'):
            self.tender_id.action_submit()
            self.tender_id.submit_note = 'SUBMIT -> ' + self.reason or '' if self._context.get('submit') else 'RESUBMIT \n' + self.reason or ''
        else:            
            self.tender_id.action_revision()
            self.tender_id.revision_note = self.reason
        return True


class TenderDocumentWizard(models.TransientModel):
    _name = "tender.document.wizard"
    
    tender_id = fields.Many2one('tender.management', 'Tender')
    line_ids = fields.One2many('tender.document.line.wizard', 'tender_document_id', 'Reference')

    @api.multi
    def confirm(self):
        for line in self.line_ids:
            self.env['tender.document'].create({'tender_id': line.tender_id.id,
                                                'description': line.description,
                                                'attach_filename': line.attach_filename,
                                                'attach': line.attach,
                                                'done': True,
                                                'type': 'external'
                                            })

            
class TenderDocumenLinetWizard(models.TransientModel):
    _name = "tender.document.line.wizard"
    
    tender_document_id = fields.Many2one('tender.document.wizard', 'Tender')
    tender_id = fields.Many2one('tender.management', 'Tender')
    description = fields.Char('Description')
    date = fields.Date('Date')
    attach_filename = fields.Char(string="Attachement Filename")
    attach = fields.Binary(string='File')
    
