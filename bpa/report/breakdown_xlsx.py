from urllib import request
from odoo import models

class BreakdownPackage(models.AbstractModel):
    _name = 'report.bpa.breakdown_package_xlsx'
    _inherit = 'report.report_xlsx.abstract'
    
    def generate_xlsx_report(self, workbook, data, models):
        cr = self._cr
        ids = '(' + str(models.ids)[1:-1] + ')'
        cr.execute("select sej.id, sej.number from sale_estimate_job sej where sej.id in %s" % ids)
        cr.dictfetchall()
        for obj in models:
            report_name = obj.number
            # One sheet by partner
            sheet = workbook.add_worksheet(report_name[:31])
            bold = workbook.add_format({'bold': True})
            col_head = 3
            row = 0
#             sheet.insert_image('H3', '%s/web/binary/company_logo' % self.env['ir.config_parameter'].search_read([('key', '=', 'web.base.url')], ['value'])[0]['value'])
            sheet.merge_range(row, row, 0, col_head+3, 'Break Down Price', bold)
            row += 1
            col_item_rom = 0
            col_item_arb = col_item_rom + 1 
            col_description_point = col_item_arb + 1
            col_description_name = col_description_point + 1
            col_blank = col_description_name + 1
            col_unit = col_blank + 1
            col_qty = col_unit + 1
            col_material = col_qty + 1
            col_upah = col_material + 1
            col_total = col_upah + 1
            col_material2 = col_total + 1
            col_upah2 = col_material2 + 1
            col_total2 = col_upah2 + 1
            sheet.merge_range(col_item_rom, col_item_arb, row, row+1, 'Item No.', bold)
            sheet.merge_range(col_description_point, col_description_name, row, row+1, 'Description')
            sheet.merge_range(col_blank, col_blank, row, row+1, '')
            sheet.merge_range(col_unit, col_unit, row, row+1, 'Unit')
            sheet.merge_range(col_qty, col_qty, row+1, row+1, 'Qty')
            sheet.merge_range(col_material, col_total, row, row, 'Unit Price')
            sheet.merge_range(col_material, col_material, row+1, row+1, 'Material')
            sheet.merge_range(col_upah, col_upah, row+1, row+1, 'Upah')
            sheet.merge_range(col_total, col_total, row+1, row+1, 'Total')
            sheet.merge_range(col_material2, col_total2, row, row, 'Amount')
            sheet.merge_range(col_material2, col_material2, row+1, row+1, 'Material')
            sheet.merge_range(col_upah2, col_upah2, row+1, row+1, 'Upah')
            sheet.merge_range(col_total2, col_total2, row+1, row+1, 'Total')
            # row += 1
            # for estimate_id in obj.estimate_ids:
            #     sheet.write(row, row, col_description_point, col_description_name, estimate_id.product_id.name)
            #     estimate_id.product_description