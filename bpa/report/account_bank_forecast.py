# -*- coding: utf-8 -*-
from odoo import tools
from odoo import models, fields, api, _


class AccountBankForecast(models.Model):
    _name = "account.bank.forecast"
    _description = "Bank Forecast"
    _auto = False
    _rec_name = 'journal_id'
    
    @api.one
    def _get_forecast(self):
        Forecast = self.env['account.bank.forecast.line']
        for line in self.line_ids:
            invoice = self.env['account.invoice'].browse(line.id)
            if invoice.type in ['out_invoice', 'out_refund']:
                Forecast += line
            if invoice.type in ['in_invoice', 'in_refund'] and invoice.request_id:
                Forecast += line
        self.display_line_ids = Forecast.ids
        self.ending_balance = sum(self.display_line_ids.mapped('amount'))
        self.display_balance_forecast = sum(self.display_line_ids.mapped('amount'))
            
    journal_id = fields.Many2one(
        'account.journal', 'Bank Account', readonly=True)
    balance = fields.Float('Starting Balance', readonly=True)
    balance_forecast = fields.Float('Forecast Balance', readonly=True)
    display_balance_forecast = fields.Float(string='Forecast Balance', readonly=True, compute='_get_forecast')
    line_ids = fields.One2many(
        'account.bank.forecast.line', 'journal_id', 'Forecast')
    ending_balance = fields.Float(string='Ending Balance', readonly=True, compute='_get_forecast')
    display_line_ids = fields.One2many('account.bank.forecast.line', string='Display Forecast', compute='_get_forecast')
    
    @api.model_cr
    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""CREATE or REPLACE VIEW %s as (
            SELECT
                id,
                id AS journal_id,
                SUM(balance) AS balance,
                SUM(balance) + SUM(balance_forecast) AS balance_forecast
            FROM ((
                SELECT
                    aj.id,
                    SUM(COALESCE(aml.debit,0)-COALESCE(aml.credit,0)) AS balance,
                    0 AS balance_forecast
                FROM
                    account_journal aj
                    LEFT JOIN account_move_line aml ON aml.journal_id = aj.id
                WHERE
                    aj.type = 'bank'
                GROUP BY
                    aj.id
                ) UNION ALL (
                SELECT
                    aj.id,
                    0 AS balance,
                    SUM(ai.residual_company_signed * (
                        CASE WHEN ai.type IN ('in_invoice','in_refund')
                        THEN -1 ELSE 1 END)) AS balance_forecast
                FROM
                    account_journal aj
                    LEFT JOIN account_invoice ai ON ai.payment_journal_id = aj.id AND ai.state = 'open'
                WHERE
                    aj.type = 'bank'
                GROUP BY
                    aj.id)) sub
            GROUP BY
                sub.id
        )""" % self._table)

    @api.multi
    def action_view_invoices(self):
        return {
            'name': _('View Invoices'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.invoice',
            'domain': [('payment_journal_id', 'in', self.ids), ('state', '=', 'open'), ('type', 'in', ('out_invoice', 'out_refund'))],
            'context': {
                'type': 'out_invoice',
                'journal_type': 'sale',
                'form_view_ref': 'account.invoice_form',
                'tree_view_ref': 'account.invoice_tree'}
        }

    @api.multi
    def action_view_supplier_invoices(self):
        return {
            'name': _('View Payment Requests'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.payment.request',
            'domain': [('journal_id', 'in', self.journal_id.ids), ('state', 'in', ('draft', 'waiting', 'approve'))],
#             'context': {
#                 'form_view_ref': 'account_payment_request.payment_request_form',
#                 'tree_view_ref': 'account_payment_request.payment_request_tree'}
        }


class AccountBankForecastLine(models.Model):
    _name = "account.bank.forecast.line"
    _description = "Bank Forecast Transaction"
    _auto = False
    _order = 'date, id'

    journal_id = fields.Many2one(
        'account.journal', 'Bank Account', readonly=True)
    date = fields.Date('Date Payment', readonly=True)
    date_invoice = fields.Date('Date Invoice', readonly=True)
    number = fields.Char('Invoice Number', readonly=True)
    name = fields.Char('Description', readonly=True)
    amount = fields.Monetary('Amount', readonly=True)
    balance = fields.Monetary('Balance', readonly=True)
    currency_id = fields.Many2one('res.currency', 'Currency', readonly=True)
    payment_status = fields.Char('Payment Status')

    @api.model_cr
    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""CREATE or REPLACE VIEW %s as (
            SELECT
                ai.id,
                ai.payment_journal_id AS journal_id,
                CASE WHEN (apr.state = 'paid' AND ai.type IN ('in_invoice','in_refund')) OR (ai.state = 'paid' AND ai.type IN ('out_invoice','out_refund')) THEN 'PAID' 
                    WHEN (apr.state IN ('draft','waiting','approve') AND ai.type IN ('in_invoice','in_refund')) OR (ai.state IN ('draft','open') AND ai.type IN ('out_invoice','out_refund')) THEN 'NOT PAID' ELSE '' 
                END AS payment_status,
                CASE WHEN ai.type IN ('in_invoice','in_refund') THEN COALESCE(apr.date_payment, apr.date_planned) ELSE COALESCE(ai.date_due, ai.date_invoice) 
                END AS date,
                ai.date_invoice,
                ai.number,
                COALESCE(ai.reference, ai.name) AS name,
                ai.residual_company_signed * (
                    CASE WHEN ai.type IN ('in_invoice','in_refund') THEN -1 ELSE 1 END) AS amount,
                    (SELECT
                        SUM(COALESCE(balance,0))
                        FROM ((
                            SELECT
                                SUM(COALESCE(aml.debit,0)-COALESCE(aml.credit,0)) AS balance
                            FROM
                                account_move_line aml
                            WHERE
                                aml.journal_id = ai.payment_journal_id
                    ) UNION ALL (
                    SELECT
                        SUM(aii.residual_company_signed * (
                            CASE WHEN aii.type IN ('in_invoice','in_refund')
                            THEN -1 ELSE 1 END)) AS balance
                        FROM
                            account_invoice aii
                        WHERE
                            aii.state = 'open' AND
                            aii.payment_journal_id = ai.payment_journal_id AND
                            COALESCE(aii.date_due, aii.date_invoice) < COALESCE(ai.date_due, ai.date_invoice) OR
                            (COALESCE(aii.date_due, aii.date_invoice) = COALESCE(ai.date_due, ai.date_invoice) AND
                            aii.id <= ai.id))) sub
                    ) AS balance,
                rc.currency_id AS currency_id
            FROM
                account_invoice ai
                LEFT JOIN account_payment_request apr on apr.id = ai.request_id
                JOIN res_company rc ON rc.id = ai.company_id
            WHERE
                ai.state = 'open'
        )""" % self._table)
