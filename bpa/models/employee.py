# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import datetime


class ResEmployee(models.Model):
    _inherit = 'hr.employee'
    
    @api.multi
    @api.constrains('name') 
    def _check_duplicate(self):
        for employee in self:
            if employee.nik in self.search([('id', '!=', self.id),('nik', '!=', False)]).mapped('nik'):
                return False
        return True
    
    nik = fields.Char('NIK')
    finger_print = fields.Char('Finger Print')
    inventaris = fields.Char('Inventaris')
    ptkp_id = fields.Many2one('hr.ptkp', 'PTKP')
    employee_status = fields.Selection([('permanent', 'Permanent'), ('contract', 'Contract'),
                                        ('probation', 'Probation'), ('other', 'Other')], string='Employee Status', default='permanent')
    date_from = fields.Date('Date From')
    date_to = fields.Date('Date To')
    
    npwp = fields.Char('NPWP No')
    join_date = fields.Date('Join Date')
    religion = fields.Selection([('islam', 'Islam'), ('christian', 'Christian'), ('hindu', 'Hindu'),
                                 ('budha', 'Budha'), ('other', 'Other')], string='Religion', default='islam')
    place_of_birth = fields.Char('Place of Birth')
    
    bank_salary_name = fields.Char('Bank Salary Name')
    bank_salary_number = fields.Char('Bank Salary Number')
    bank_salary_account_id = fields.Many2one('res.bank', 'Bank Salary Number')
    bank_nonsalary_name = fields.Char('Bank Non Salary Name')
    bank_nonsalary_number = fields.Char('Bank Non Salary Number')
    bank_nonsalary_account_id = fields.Many2one('res.bank', 'Bank Non Salary Number')
    
    bpjs_employee = fields.Char('BPJS Employee')
    bpjs_health = fields.Char('BPJS Health')
    bpjs_address = fields.Text('BPJS Address')
    
    kk_number = fields.Char('Family Register')
    ktp_address = fields.Text('KTP Address')
    family_phone = fields.Char('Phone Family')
    blood_type = fields.Selection([('A', 'A'), ('B', 'B'), ('AB', 'AB'), ('O', 'O')], string='Blood Type', default='O')
    last_education = fields.Selection([('Prof', 'Professor'), ('doctor', 'Doctor'), ('s2', 'S2'), ('s1', 'S1'),
                                       ('d3', 'D3'), ('sma', 'SMA'), ('smp', 'SMP'), ('sd', 'SD'), ('other', 'Other')],
                                       string='Last Education', default='s1')
    graduation_year = fields.Char('Graduation Year')
    major_education = fields.Char('Major')
    university = fields.Char('University')
    
    pdl_date = fields.Date('Receive PDL Date')
    pdl_uniform = fields.Selection([('special', 'Special'), ('xxxl', 'XXXl'), ('xxl', 'XXl'), ('xl', 'XL'),
                                    ('L', 'L'), ('m', 'M'), ('s', 'S')], string='PDL Uniform')
    pdk_date = fields.Date('Receive PDK Date')
    pdk_uniform = fields.Selection([('special', 'Special'), ('xxxl', 'XXXl'), ('xxl', 'XXl'), ('xl', 'XL'),
                                    ('L', 'L'), ('m', 'M'), ('s', 'S')], string='PDK Uniform')
    
    shoes_date = fields.Date('Request Shoes Date')
    position_request = fields.Char('Position Request')
    
    family_ids = fields.One2many('hr.employee.family', 'employee_id', string='Families')
    certificate_ids = fields.One2many('hr.employee.certificate', 'employee_id', string='Certificate')
    training_ids = fields.One2many('hr.employee.training', 'employee_id', string='Training')
    warning_ids = fields.One2many('hr.employee.warning', 'employee_id', string='Warning')
    skors_ids = fields.One2many('hr.employee.skors', 'employee_id', string='Skors')
    compensation_ids = fields.One2many('hr.employee.vacation_compensation', 'employee_id', string='Compensation')
    mother = fields.Char('Mother')
    family_type = fields.Selection([('father', 'Father'), ('mother', 'Mother'), ('husband', 'Husband'), ('wife', 'Wife'),
                                    ('son', 'Son'), ('daughter', 'Daughter')], string='Family Status')

    _constraints = [
        (_check_duplicate, 'Duplicate NIK Reference!', ['nik'])
    ]
    
class ResEmployeeFamily(models.Model):
    _name = 'hr.employee.family'
    
    employee_id = fields.Many2one('hr.employee', 'Employee Ref')
    name = fields.Char('Name')
    family_type = fields.Selection([('father', 'Father'), ('mother', 'Mother'), ('husband', 'Husband'), ('wife', 'Wife'),
                                    ('son', 'Son'), ('daughter', 'Daughter')], string='Status')
    birthday = fields.Date('Date of Birth')
    education = fields.Selection([('Prof', 'Professor'), ('doctor', 'Doctor'), ('s2', 'S2'), ('s1', 'S1'),
                                  ('d3', 'D3'), ('sma', 'SMA'), ('smp', 'SMP'), ('sd', 'SD'), ('other', 'Other')],
                                 string='Education')
    bpjs = fields.Char('BPJS')
    faskes_address = fields.Char('Faskes Address')
    note = fields.Char('Note')


class ResEmployeeCertificate(models.Model):
    _name = 'hr.employee.certificate'
    _order = 'date_to'
    
    employee_id = fields.Many2one('hr.employee', 'Employee Ref')
    name = fields.Char('Description')
    date_from = fields.Date('Date From')
    date_to = fields.Date('Date To')
    note = fields.Char('Note')
    attach_filename = fields.Char(string="Attachement Filename")
    attach = fields.Binary(string='File')

     
class ResEmployeeTraining(models.Model):
    _name = 'hr.employee.training'
    _order = 'date_from'
    
    employee_id = fields.Many2one('hr.employee', 'Employee Ref')
    name = fields.Char('Description')
    date_from = fields.Date('Date From')
    date_to = fields.Date('Date To')
    note = fields.Char('Note')
    attach_filename = fields.Char(string="Attachement Filename")
    attach = fields.Binary(string='File')


class ResEmployeeWarning(models.Model):
    _name = 'hr.employee.warning'
    _order = 'date_from'
    
    employee_id = fields.Many2one('hr.employee', 'Employee Ref')
    name = fields.Char('Description')
    date_from = fields.Date('Date From')
    date_to = fields.Date('Date To')
    note = fields.Char('Note')
    attach_filename = fields.Char(string="Attachement Filename")
    attach = fields.Binary(string='File')


class ResEmployeeSkors(models.Model):
    _name = 'hr.employee.skors'
    _order = 'date_from'
    
    @api.multi
    @api.depends('date_from', 'date_to')
    def _get_duration(self):
        date_format = "%Y-%m-%d"
        for rec in self:
            if rec.date_from and rec.date_to:
                a = datetime.strptime(rec.date_from, date_format)
                b = datetime.strptime(rec.date_to, date_format)
                rec.duration = b - a
        return True
            
    employee_id = fields.Many2one('hr.employee', 'Employee Ref')
    project_position = fields.Char('Project Position')
    name = fields.Char('Description')
    date_from = fields.Date('Date From')
    date_to = fields.Date('Date To')
    duration = fields.Char('Duration', compute=_get_duration, store=True)
    note = fields.Char('Note')
    state = fields.Selection([('new', 'In Progress'), ('done', 'Done')], string='Status', default='new')
    attach_filename = fields.Char(string="Attachement Filename")
    attach = fields.Binary(string='File')


class ResEmployeeVacationCompensation(models.Model):
    _name = 'hr.employee.vacation_compensation'
    _order = 'date'
    
    employee_id = fields.Many2one('hr.employee', 'Employee Ref')
    name = fields.Char('Description')
    date = fields.Date('Date')
    day = fields.Integer('Days')
    amount = fields.Float('Amount')
    state = fields.Selection([('new', 'In Progress'), ('done', 'Done')], string='Status', default='new')


class HrPTKP(models.Model):
    _name = "hr.ptkp"
    _description = "HR PTKP"
    
    name = fields.Char('Code')
    description = fields.Char('Description')
    amount = fields.Float(related='line_ids.amount', string='Amount', store=True)
    line_ids = fields.One2many('hr.ptkp.line', 'ptkp_id', string='Lines')


class HrPTKPLine(models.Model):
    _name = "hr.ptkp.line"
    _order = 'date_start desc'
    
    ptkp_id = fields.Many2one('hr.ptkp', string="Reference", ondelete='cascade')
    date_start = fields.Date('Start Date')
    date_end = fields.Date('End Date')
    amount = fields.Float('Amount')

# class HREmployeeProject(models.Model):
#     _name = 'hr.employee.project'
#     
#     project_id = fields.
    
