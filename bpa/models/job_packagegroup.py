from odoo import fields, models, api


class JobPackagegroup(models.Model):
    _name = "job.packagegroup"
    _description = "Work Package Group"
    
    @api.multi
    @api.depends('line_ids.price_subtotal')
    def _price_total(self):
        for package in self:
            package.price_total = sum(x.price_subtotal for x in package.line_ids)

    line_ids = fields.One2many('job.packagegroup.line', 'packagegroup_id')
    name = fields.Char('Task Name', required=True)
    price_total = fields.Float('Total Price', compute='_price_total')

    
class JobPackagegroupLine(models.Model):
    _name = "job.packagegroup.line"
    _description = "Work Package Group Lines"

    @api.multi
    @api.depends('package_id.price_total', 'quantity')
    def _price(self):
        for line in self:
            line.price_unit = line.package_id.price_total
            line.price_subtotal = line.price_unit * line.quantity
    
    packagegroup_id = fields.Many2one('job.packagegroup', 'Work Package Group', required=True, ondelete='cascade')
    package_id = fields.Many2one('job.package', 'Package', required=True, ondelete='restrict')
    quantity = fields.Float('Quantity', default=0, required=True)
    uom_id = fields.Many2one(related='package_id.uom_id', readonly=True)
    price_unit = fields.Float('Total', compute='_price', store=True)
    price_subtotal = fields.Float('Total', compute='_price', store=True)
