from odoo import models, fields, api, _
from datetime import datetime


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    @api.multi
    @api.depends('order_line.move_ids.state', 'order_line.move_ids', 'order_line')
    def _delivery_state(self):
        for purchase in self:
            delivery_state = 'Ready to Receive'
            done = True
            all_cancel = True
            for line in purchase.order_line:
                for move in line.move_ids:
                    if move.state == 'done' and delivery_state != 'Partially Cancelled':
                        delivery_state = 'Partially Received'
                    if move.state == 'cancel':
                        delivery_state = 'Partially Cancelled'
                    else:
                        all_cancel = False
                    if move.state != 'done':
                        done = False
            if all_cancel:
                delivery_state = 'Cancelled'
            purchase.delivery_state = delivery_state if not done else 'Received'

    tender_id = fields.Many2one(
        'tender.management', string='Tender', copy=False)
    project_location_id = fields.Many2one('project.location', 'Location')
    type = fields.Selection([('purchase', 'Purchase Order'),
                             ('tender', 'Tender')], default='purchase', string='Purchase Type')
    delivery_state = fields.Char('Delivery State', compute='_delivery_state', store=True, readonly=True)
    tax_line_ids = fields.One2many('purchase.order.tax', 'purchase_id', string='Tax Lines', oldname='tax_line',
        readonly=True, states={'draft': [('readonly', False)]}, copy=True)
    
    def _prepare_tax_line_vals(self, line, tax):
        """ Prepare values to create an account.invoice.tax line

        The line parameter is an account.invoice.line, and the
        tax parameter is the output of account.tax.compute_all().
        """
        vals = {
            'purchase_id': self.id,
            'name': tax['name'],
            'tax_id': tax['id'],
            'amount': tax['amount'],
            'base': tax['base'],
            'manual': False,
            'sequence': tax['sequence'],
            'account_analytic_id': tax['analytic'] and line.account_analytic_id.id or False,
            'account_id': tax['account_id']
        }

        # If the taxes generate moves on the same financial account as the invoice line,
        # propagate the analytic account from the invoice line to the tax line.
        # This is necessary in situations were (part of) the taxes cannot be reclaimed,
        # to ensure the tax move is allocated to the proper analytic account.

        return vals
    
    @api.multi
    def get_taxes_values(self):
        tax_grouped = {}
        for line in self.order_line:
            price_unit = line.price_unit
            taxes = line.taxes_id.compute_all(price_unit, self.currency_id, line.product_qty, line.product_id, self.partner_id)['taxes']
            for tax in taxes:
                val = self._prepare_tax_line_vals(line, tax)
                key = self.env['account.tax'].browse(tax['id']).get_grouping_key(val)

                if key not in tax_grouped:
                    tax_grouped[key] = val
                else:
                    tax_grouped[key]['amount'] += val['amount']
                    tax_grouped[key]['base'] += val['base']
        return tax_grouped
    
    @api.onchange('order_line')
    def _onchange_order_line(self):
        taxes_grouped = self.get_taxes_values()
        tax_lines = self.tax_line_ids.filtered('manual')
        for tax in taxes_grouped.values():
            tax_lines += tax_lines.new(tax)
        self.tax_line_ids = tax_lines
        return

    @api.multi
    def action_remove_tender(self):
        for purchase in self:
            purchase.tender_id = False
        return True

    @api.model
    def create(self, vals):
        if vals.get('type') == 'tender':
            vals['name'] = self.env['ir.sequence'].next_by_code(
                'tender.purchase.order') or _('TENDER')
        else:
            if vals.get('project_location_id'):
                project_location_id = self.env['project.location'].browse(
                    vals.get('project_location_id'))
                get_next_char = project_location_id.sequence_id.next_by_id().split('-')
                name = '%s/PROC.PO/BPA/%s/%s' % (get_next_char[1] if len(
                    get_next_char) > 1 else get_next_char[0], project_location_id.code, datetime.now().year)
                vals.update({'name': name})
        return super(PurchaseOrder, self).create(vals)

    @api.multi
    def button_revision(self):
        count_po = len(self.search([('name', 'like', self.name)]))
        for purchase in self:
            purchase_rev_id = purchase.copy({
                'name': purchase.name + '-%s' % (count_po),
            })
            purchase.message_post(body=_(
                "Purchase Revision <em>%s</em> <b>created</b>.") % (purchase_rev_id.name))

            return {
                'name': 'Purchase Order',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'purchase.order',
                'res_id': purchase_rev_id.id,
                'type': 'ir.actions.act_window',
            }
            
    @api.model
    def _prepare_picking(self):
        res = super(PurchaseOrder, self)._prepare_picking()
        res.update(
            project_id=self.custom_requisition_id.project_id.id,
            requisition_id=self.custom_requisition_id.id)
        return res


class PurchaseOrderLine(models.Model):
    _inherit = 'purchase.order.line'

    tender_id = fields.Many2one(
        'purchase.tender', 'tender', ondelete='restrict')


class PurchaseOrderTax(models.Model):
    _name = "purchase.order.tax"
    _description = "Purchase Tax"
    _order = 'sequence'

    @api.depends('purchase_id.order_line')
    def _compute_base_amount(self):
        tax_grouped = {}
        for purchase in self.mapped('purchase_id'):
            tax_grouped[purchase.id] = purchase.get_taxes_values()
        for tax in self:
            tax.base = 0.0
            if tax.tax_id:
                key = tax.tax_id.get_grouping_key({
                    'tax_id': tax.tax_id.id,
                    'account_id': tax.account_id.id,
                    'account_analytic_id': tax.account_analytic_id.id,
                })
                if tax.purchase_id and key in tax_grouped[tax.purchase_id.id]:
                    tax.base = tax_grouped[tax.purchase_id.id][key]['base']
                else:
                    _logger.warning('Tax Base Amount not computable probably due to a change in an underlying tax (%s).', tax.tax_id.name)

    purchase_id = fields.Many2one('purchase.order', string='Purchase', ondelete='cascade', index=True)
    name = fields.Char(string='Tax Description', required=True)
    tax_id = fields.Many2one('account.tax', string='Tax', ondelete='restrict')
    account_id = fields.Many2one('account.account', string='Tax Account', required=True, domain=[('deprecated', '=', False)])
    account_analytic_id = fields.Many2one('account.analytic.account', string='Analytic account')
    amount = fields.Monetary()
    amount_rounding = fields.Monetary()
    amount_total = fields.Monetary(string="Amount", compute='_compute_amount_total')
    manual = fields.Boolean(default=True)
    sequence = fields.Integer(help="Gives the sequence order when displaying a list of invoice tax.")
    company_id = fields.Many2one('res.company', string='Company', related='account_id.company_id', store=True, readonly=True)
    currency_id = fields.Many2one('res.currency', related='company_id.currency_id', store=True, readonly=True)
    base = fields.Monetary(string='Base', compute='_compute_base_amount', store=True)

    @api.depends('amount', 'amount_rounding')
    def _compute_amount_total(self):
        for tax_line in self:
            tax_line.amount_total = tax_line.amount + tax_line.amount_rounding
