from odoo import models, fields, api, _
from odoo.exceptions import UserError


class TenderManagement(models.Model):
    _name = 'tender.management'
    _order = 'create_date desc'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'Tender Management'
    
    @api.multi
    @api.depends('purchase_ids')
    def _compute_orders_number(self):
        for tender in self:
            tender.order_count = len(tender.purchase_ids)
    
    @api.multi
    @api.depends('general_line_ids.done', 'technic_line_ids.done', 'comercial_line_ids.done')
    def _get_precondition(self):
        for tender in self:
            if all(done for done in tender.general_line_ids.mapped('done')) and tender.general_line_ids:
                tender.general_precondition = True
            if all(done for done in tender.technic_line_ids.mapped('done')) and tender.technic_line_ids:
                tender.technic_precondition = True
            if all(done for done in tender.comercial_line_ids.mapped('done')) and tender.comercial_line_ids:
                tender.comercial_precondition = True
            
    READONLY_STATES = {
        'submit': [('readonly', True)],
        'waiting_spk': [('readonly', True)],
        'won': [('readonly', True)],
        'lost': [('readonly', True)],
        'withdraw': [('readonly', True)],
        'cancel': [('readonly', True)]
    }
    
    number = fields.Char('Number', default='New', states=READONLY_STATES, copy=False)
    name = fields.Char('Code', states=READONLY_STATES)
    title = fields.Char('Title', states=READONLY_STATES)
    description = fields.Text('Description', states=READONLY_STATES)
    date_expired = fields.Datetime('Expired Date', states=READONLY_STATES)
    user_id = fields.Many2one('res.users', 'Responsible', default=lambda self: self.env.user, states=READONLY_STATES)
    amount = fields.Float('Purchase Fee', states=READONLY_STATES, track_visibility='onchange')
    type = fields.Selection([('goverment', 'Goverment'),
                             ('private', 'Private')], default='private', string='Tender Type', states=READONLY_STATES)
    date_sent = fields.Datetime('Submit Date', states=READONLY_STATES, track_visibility='onchange', copy=False)
    
    state = fields.Selection([('draft', 'Info'), ('withdraw', 'Withdraw'),
                              ('waiting_office', 'Aanwitjing Office'), ('waiting_site', 'Aanwitjing Area'),
                              ('waiting_document', 'Waiting Document'),
                              ('waiting_rfq', 'Waiting RfQ'), ('submit', 'Submit RfQ'),
                              ('negotiation', 'Negotation'), ('pending', 'Pending'),
                              ('revision', 'Revision'), ('won', 'Won'),
                              ('waiting_spk', 'Won : Waiting SPK'), ('lost', 'Lost'),
                              ('cancel', 'Cancel')], default='draft', string='Status', track_visibility='onchange')
    
    project_location_id = fields.Many2one('project.location', 'Location', states=READONLY_STATES)
    partner_id = fields.Many2one('res.partner', 'Customer', states=READONLY_STATES)
    line_ids = fields.One2many('tender.document', 'tender_id', 'Documents', states=READONLY_STATES, domain=[('type', '=', 'internal')])
    ext_line_ids = fields.One2many('tender.document', 'tender_id', 'Ext Documents', states=READONLY_STATES, domain=[('type', '=', 'external')])
    purchase_ids = fields.One2many('purchase.order', 'tender_id', 'RfQ', copy=False)
    project_id = fields.Many2one('project.project', 'Project', copy=False)
    order_count = fields.Integer(compute='_compute_orders_number', string='Number of Orders')
    pending_note = fields.Text('PENDING NOTE', track_visibility='onchange', copy=False)
    won_note = fields.Text('WON NOTE', track_visibility='onchange', copy=False)
    lost_note = fields.Text('LOST NOTE', track_visibility='onchange', copy=False)
    revision_note = fields.Text('REVISION NOTE', track_visibility='onchange', copy=False)
    withdraw_note = fields.Text('WITHDRAW NOTE', track_visibility='onchange', copy=False)
    cancel_note = fields.Text('CANCEL NOTE', track_visibility='onchange', copy=False)
    submit_note = fields.Text('SUBMIT NOTE', track_visibility='onchange', copy=False)
    general_precondition = fields.Boolean('General', compute=_get_precondition, store=False)
    technic_precondition = fields.Boolean('Technic', compute=_get_precondition, store=False)
    comercial_precondition = fields.Boolean('Comercial', compute=_get_precondition, store=False)
    general_line_ids = fields.One2many('tender.condition', 'tender_id', 'General', copy=True, states=READONLY_STATES, domain=[('type', '=', 'general')])
    technic_line_ids = fields.One2many('tender.condition', 'tender_id', 'Technic', copy=True, states=READONLY_STATES, domain=[('type', '=', 'technic')])
    comercial_line_ids = fields.One2many('tender.condition', 'tender_id', 'Comercial', copy=True, states=READONLY_STATES, domain=[('type', '=', 'comercial')])
    
    @api.model
    def create(self, vals):
        if vals.get('number', 'New') == 'New':
            vals['number'] = self.env['ir.sequence'].next_by_code('tender.management') or '/'
        return super(TenderManagement, self).create(vals)
    
    @api.multi
    @api.depends('number')
    def name_get(self):
        result = []
        for tender in self:
            name = '%s' % (tender.number or '')
            result.append((tender.id, name))
        return result
    
    @api.multi
    def button_wizard(self):
        data_obj = self.env['ir.model.data']
        view = data_obj.xmlid_to_res_id('bpa.tender_wizard_form')
        ctx = self._context.copy()
        ctx.update({
            'default_tender_id': self.id,
            'default_domain_doc_ids': str(self.mapped('line_ids').ids + self.mapped('ext_line_ids').ids)
        })
        return {
            'name': _('Tender'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'tender.wizard',
            'target': 'new',
            'views': [(view, 'form')],
            'view_id': view,
            'context' : ctx
        }
    
    @api.multi
    def action_add_rfq(self):
        data_obj = self.env['ir.model.data']
        view = data_obj.xmlid_to_res_id('bpa.tender_wizard_form')
        ctx = self._context.copy()
        ctx.update({
            'default_tender_id': self.id,
        })
        return {
            'name': _('Add Quotation'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'tender.wizard',
            'target': 'new',
            'views': [(view, 'form')],
            'view_id': view,
            'context' : ctx
        }
    
    @api.multi
    def action_create_rfq(self):
        data_obj = self.env['ir.model.data']
        view = data_obj.xmlid_to_res_id('purchase.purchase_order_form')
        return {
            'name': _('Create RfQ'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'purchase.order',
            'target': 'new',
            'views': [(view, 'form')],
            'view_id': view,
            'context' : {'default_tender_id' : self.id}
        }
    
    @api.multi
    def button_confirm(self):
        for tender in self:
            tender.state = 'waiting_office'
        return True
    
    @api.multi
    def button_confirm_office(self):
        for tender in self:
            tender.state = 'waiting_site'
        return True
    
    @api.multi
    def button_confirm_site(self):
        for tender in self:
            tender.state = 'waiting_document'
        return True
    
    @api.multi
    def action_submit(self):        
        for tender in self:
            if tender.state == 'revision':
                revision = str(tender.number).split('-')
                if len(revision) == 1:
                    tender.number = "%s-%s" % (tender.number, '1')
                else:
                    count_rev = revision[1]
                    tender.number = "%s-%s" % (revision[0], int(count_rev) + 1)
            tender.state = 'negotiation'
            tender.date_sent = fields.Datetime.now()
        return True
    
    @api.multi
    def action_pending(self):
        for tender in self:
            tender.state = 'pending'
        return True
    
    @api.multi
    def action_revision(self):
        for tender in self:
            tender.state = 'revision'
        return True
    
    @api.multi
    def button_confirm_spk(self):
        for tender in self:
            datas = {
                'name': tender.title,
                'project_location_id': tender.project_location_id.id,
                'partner_id': tender.partner_id.id,
                'tender_number': tender.number,
                'amount_contract_show': tender.amount,
                'date_start': fields.Date.today()
            }
            if not tender.project_id:
                project_id = self.env['project.project'].create(datas)
                tender.write({'project_id': project_id.id})
            tender.state = 'won'
        return True
    
    @api.multi
    def button_add_spk(self):
        view = self.env.ref('bpa.tender_document_wizard_view_form')
        wiz_id = self.env['tender.document.wizard'].create({'tender_id' : self.id})
        return {
            'name': _('SPK Documents'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'tender.document.wizard',
            'views': [(view.id, 'form')],
            'view_id': view.id,
            'target': 'new',
            'res_id': wiz_id.id,
            'context': self.env.context,
        }
        
    @api.multi
    def action_lost(self):
        for tender in self:
            tender.state = 'lost'
        return True
    
    @api.multi
    def action_withdraw(self):
        for tender in self:
            tender.state = 'withdraw'
        return True
    
    @api.multi
    def action_cancel(self):
        for tender in self:
            tender.state = 'cancel'
            tender.purchase_ids.action_remove_tender()
        return True
    
    @api.multi
    def button_draft(self):
        for tender in self:
            tender.state = 'draft'
            tender.tender_state = 'draft'
        return True
    
    @api.multi
    def button_retender(self):
        for tender in self:
            retender_id = tender.copy()
        return {
            'name': 'Tender Management',
            'view_type': 'form',
            'view_mode': 'form',
            'res_id' : retender_id.id,
            'res_model': 'tender.management',
            'type': 'ir.actions.act_window'
        } 
    
    @api.multi
    def action_view_project(self):
        action = self.env.ref('project.view_project')
        result = action.read()[0]
        imd = self.env['ir.model.data']
        result['context'] = self._context
        list_view_id = imd.xmlid_to_res_id('project.view_project')
        form_view_id = imd.xmlid_to_res_id('project.edit_project')
        result['domain'] = "[('id', '=', " + str(self.project_id.id) + ")]"
        return {
            'name': 'Projects',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'views': [[list_view_id, 'tree'], [form_view_id, 'form']],
            'domain' : result.get('domain'),
            'res_model': 'project.project',
            'type': 'ir.actions.act_window',
            'context' : result.get('context')
        }
        
    @api.multi
    def unlink(self):
        for tender in self:
            if tender.state != 'cancel':
                raise UserError(_('In order to delete a Tender, you must cancel it first.')) 
        return super(TenderManagement, self).unlink()


class TenderDocument(models.Model):
    _name = 'tender.document'
    _description = 'Tender Documents'
    
    @api.multi
    def _get_attach_name(self):
        for doc in self:
            doc.name = doc.dms_file_id.display_name or doc.attach_filename
            
    tender_id = fields.Many2one('tender.management', 'Tender Ref')
    description = fields.Char('Description')
    state = fields.Char('Status')
    date = fields.Date('Date')
    dms_file_id = fields.Many2one('muk_dms.file', 'Dms File')
    
    attach_filename = fields.Char(string="Attachement Filename")
    attach = fields.Binary(string='File')
    type = fields.Selection([('internal', 'Internal'), ('external', 'External')], string='Type')
    name = fields.Char('Name', compute=_get_attach_name)
    done = fields.Boolean('To Project', default=False)
    project_id = fields.Many2one('project.project', related='tender_id.project_id', string="Project", store=True)
    
    @api.model
    def create(self, vals):
        if 'attach' or 'dms_file_id' in vals:
            vals['date'] = fields.Date.today()     
        return super(TenderDocument, self).create(vals)
    
    @api.multi
    def write(self, vals):
        for rec in self:
            if 'attach' in vals:
                rec.date = fields.Date.today()  
            if 'dms_file_id' in vals:
                rec.date = fields.Date.today()
        return super(TenderDocument, self).write(vals)

    
class TenderCondition(models.Model):
    _name = 'tender.condition'
    _description = 'Tender Condition'
    
    tender_id = fields.Many2one('tender.management', 'Tender Ref')
    name = fields.Char('Description')
    done = fields.Boolean('Done')
    type = fields.Selection([('general', 'General'), ('technic', 'Technic'), ('comercial', 'Comercial')], string='Type')
    attach_filename = fields.Char(string="Attachement Filename")
    attach = fields.Binary(string='File')
    