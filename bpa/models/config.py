from odoo import api, fields, models


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    project_sale_account_id = fields.Many2one(
        related='company_id.project_sale_account_id')
    project_advance_account_id = fields.Many2one(
        related='company_id.project_advance_account_id')
    days_warning_project_end_date = fields.Integer(
        related='company_id.days_warning_project_end_date')
    tender_todo_approval_id = fields.Many2one(
        related='company_id.tender_todo_approval_id')
    tender_todo_approval_ids = fields.One2many(
        related='company_id.tender_todo_approval_ids')


class ResCompany(models.Model):
    _inherit = 'res.company'

    project_sale_account_id = fields.Many2one(
        'account.account', 'Project Sale Account')
    project_advance_account_id = fields.Many2one(
        'account.account', 'Project Advance Account')
    days_warning_project_end_date = fields.Integer(
        'Day(s) warning for Project End Date', default=7)
    tender_todo_approval_id = fields.Many2one(
        'res.users', 'Create ToDo for Tender Approval to')
    tender_todo_approval_ids = fields.One2many(
        'res.company.tenderapproval', 'company_id', 'Tender Approval based on Amount')


class ResCompanyTenderapproval(models.Model):
    _name = 'res.company.tenderapproval'
    _description = 'Tender Approval'
    _order = 'amount_limit asc'

    company_id = fields.Many2one('res.company', 'company', ondelete='cascade')
    tender_todo_approval_id = fields.Many2one(
        'res.users', 'Create ToDo for Tender Approval to')
    currency_id = fields.Many2one(
        related='company_id.currency_id', readonly=True)
    amount_limit = fields.Monetary('Limit Amount')
