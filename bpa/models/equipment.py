from odoo import models, fields, api


class MaintenanceEquipment(models.Model):
    _inherit = 'maintenance.equipment'
    
    state = fields.Selection([('ready', 'Ready'), ('on_dutty', 'On Dutty'), ('maintenance', 'Maintenance'), ('broken', 'Broken')],
                             default='ready', string='Status', track_visibility='onchange', copy=False)
    
    @api.multi
    def button_ready(self):
        self.state = 'ready'
        return True
    
    @api.multi
    def button_maintenance(self):
        self.state = 'maintenance'
        return True
