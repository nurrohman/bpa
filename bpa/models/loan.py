from odoo import models, fields
from datetime import datetime


class Loan(models.Model):
    _inherit = 'hr.loan'
    
    def dayNameFromWeekday(self, weekday):
        self.ensure_one()
        if weekday == 0:
            return "Senin"
        if weekday == 1:
            return "Selasa"
        if weekday == 2:
            return "Rabu"
        if weekday == 3:
            return "Kamis"
        if weekday == 4:
            return "Jum'at"
        if weekday == 5:
            return "Sabtu"
        if weekday == 6:
            return "Minggu"
        
    def get_day_name(self):
        self.ensure_one()
        weekday = datetime.strptime(self.date, '%Y-%m-%d').weekday()
        day = self.dayNameFromWeekday(weekday)
        return day
    
    
