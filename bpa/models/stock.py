from odoo import models, fields, api


class StockPicking(models.Model):
    _inherit = 'stock.picking'
    
    project_id = fields.Many2one('project.project', 'Project')
    employee_id = fields.Many2one('hr.employee', 'Pemesan')


class StockMove(models.Model):
    _inherit = 'stock.move'
    
    remark = fields.Char('Remark')
    
    def _prepare_account_move_line(self, qty, cost, credit_account_id, debit_account_id):
        res = super(StockMove, self)._prepare_account_move_line(qty, cost, credit_account_id, debit_account_id)
        for index in range(len(res)):
            res[index][2].update(analytic_account_id=self.picking_id.project_id.analytic_account_id.id)
        return res
    
    @api.multi
    def _get_accounting_data_for_valuation(self):
        """
        Return the accounts and journal to use to post Journal Entries for the real-time
        valuation of the quant.

        :param context: context dictionary that can explicitly mention the company to consider via the 'force_company' key
        :returns: journal_id, source account, destination account, valuation account
        :raise: openerp.exceptions.UserError if any mandatory account or journal is not defined.
        """
        journal_id, acc_src, acc_dest, acc_valuation = super(StockMove, self)._get_accounting_data_for_valuation()
        if self.picking_id.move_line_ids.filtered(lambda m: m.move_id == self).account_id:
            acc_dest = self.picking_id.move_line_ids.filtered(lambda m: m.move_id == self).account_id.id
        if self.picking_id.move_line_ids.filtered(lambda m: m.move_id == self).remark:
            self.write({'name': self.picking_id.move_line_ids.filtered(lambda m: m.move_id == self).remark})
        return journal_id, acc_src, acc_dest, acc_valuation

    
class StockMoveLine(models.Model):
    _inherit = 'stock.move.line'
    
    account_id = fields.Many2one('account.account', 'Force Expense Account')
    remark = fields.Char(string='Remark')
    
