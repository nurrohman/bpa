from odoo import models, fields, api, tools, _
from odoo.tools.float_utils import float_is_zero
from odoo.exceptions import UserError
from odoo.tools.misc import DEFAULT_SERVER_DATE_FORMAT
from dateutil.relativedelta import relativedelta
from datetime import datetime


class ProjectLocation(models.Model):
    _name = "project.location"
    _description = "Project Location"

    name = fields.Char('Name', required=True)
    code = fields.Char('Code', required=True)
    sequence_id = fields.Many2one('ir.sequence', 'Sequence')

    @api.model
    def create(self, vals):
        if not vals.get('sequence_id'):
            vals.update({'sequence_id': self.sudo()._create_sequence(vals).id})
        location = super(ProjectLocation, self).create(vals)
        return location

    @api.model
    def _create_sequence(self, vals):
        prefix = vals['code'].upper() + '-'
        seq = {
            'name': vals['name'],
            'implementation': 'no_gap',
            'prefix': prefix,
            'padding': 3,
            'number_increment': 1,
            'use_date_range': True,
        }
        if 'company_id' in vals:
            seq['company_id'] = vals['company_id']
        return self.env['ir.sequence'].create(seq)


class ProjectTask(models.Model):
    _inherit = 'project.task'

    material_plan_ids = fields.One2many(
        'job.cost.line', related='job_cost_ids.job_cost_line_ids')
    consumed_material_ids = fields.One2many(
        'job.cost.line', related='job_cost_ids.job_cost_line_ids')
    line_ids = fields.One2many(
        'project.task.line', 'task_id', 'Work Package Items', copy=True)
    equipment_ids = fields.One2many(
        'project.task.equipment', 'task_id', 'Work Package Equipments', copy=True)
    task_package_ids = fields.One2many(
        'project.task.package', 'task_id', 'Work Packages', copy=True)
    task_packagegroup_ids = fields.One2many(
        'project.task.packagegroup', 'task_id', 'Task Packages', copy=True)

    task_type = fields.Selection([
        ('job_order', 'Job Order'),
        ('header', 'Header'),
        ('task', 'Task')], string='Type', default='job_order', required=True, index=True)

    @api.multi
    def action_compute_task_package(self):
        self.mapped('task_package_ids').action_compute_job_package()

    @api.multi
    def action_compute_job_package(self):
        self.action_compute_task_package()
        for task in self:
            self.env['project.task.line'].search(
                [('task_id', '=', task.id), ('auto_generate', '=', True)]).unlink()
            self.env['project.task.equipment'].search(
                [('task_id', '=', task.id), ('auto_generate', '=', True)]).unlink()
            product_data = {}
            equipment_data = {}
            for line in task.task_package_ids.mapped('line_ids'):
                if not product_data.get(line.product_id.id):
                    product_data[line.product_id.id] = {
                        'product_id': line.product_id.id,
                        'type': line.type,
                        'product_qty': line.product_qty * line.package_qty *
                        (1 + line.coefisient) *
                        (1 + line.task_package_id.coefisient),
                        'price_unit': line.price_unit,
                        'uom_id': line.uom_id.id,
                        'auto_generate': True
                    }
                else:
                    product_data[line.product_id.id]['product_qty'] += line.product_qty * line.package_qty * \
                        (1 + line.coefisient) * \
                        (1 + line.task_package_id.coefisient)
            for line in task.task_packagegroup_ids:
                for packagegroup_line in line.packagegroup_id.line_ids:
                    for package_line in packagegroup_line.package_id.line_ids:
                        if not product_data.get(package_line.product_id.id):
                            product_data[package_line.product_id.id] = {
                                'product_id': package_line.product_id.id,
                                'type': package_line.type,
                                'product_qty': package_line.product_qty * packagegroup_line.quantity * line.quantity,
                                'price_unit': package_line.price_unit,
                                'uom_id': package_line.uom_id.id,
                                'auto_generate': True
                            }
                        else:
                            product_data[package_line.product_id.id]['product_qty'] += package_line.product_qty * \
                                packagegroup_line.quantity * line.quantity

            for line in task.task_package_ids.mapped('equipment_ids'):
                if not equipment_data.get(line.equipment_id.id):
                    equipment_data[line.equipment_id.id] = {
                        'equipment_id': line.equipment_id.id,
                        'qty': line.qty * line.package_qty *
                        (1 + line.coefisient) *
                        (1 + line.task_package_id.coefisient),
                        'price_unit': line.price_unit,
                        'auto_generate': True
                    }
                else:
                    equipment_data[line.equipment_id.id]['qty'] += line.qty * line.package_qty * \
                        (1 + line.coefisient) * \
                        (1 + line.task_package_id.coefisient)
            for line in task.task_packagegroup_ids:
                for packagegroup_line in line.packagegroup_id.line_ids:
                    for package_line in packagegroup_line.package_id.equipment_line_ids:
                        if not equipment_data.get(package_line.equipment_id.id):
                            equipment_data[package_line.equipment_id.id] = {
                                'equipment_id': package_line.equipment_id.id,
                                'qty': package_line.qty * packagegroup_line.quantity * line.quantity,
                                'price_unit': package_line.price_unit,
                                'auto_generate': True
                            }
                        else:
                            equipment_data[package_line.equipment_id.id]['qty'] += package_line.qty * \
                                packagegroup_line.quantity * line.quantity,

            task.line_ids = [(0, 0, x) for x in product_data.values()]
            task.equipment_ids = [(0, 0, x) for x in equipment_data.values()]


class ProjectTaskPackage(models.Model):
    _name = "project.task.package"
    _description = 'Work Packages on Task'

    @api.multi
    @api.depends('quantity', 'coefisient', 'line_ids.price_subtotal', 'equipment_ids.price_subtotal')
    def _price(self):
        for rec in self:
            material = 0
            labour = 0
            for line in rec.line_ids:
                if line.type in ('material', 'overhead', 'consumable'):
                    material += line.price_subtotal
                else:
                    labour += line.price_subtotal
            labour += sum(rec.equipment_ids.mapped('price_subtotal'))

            total_material = rec.quantity * (1 + rec.coefisient) * material
            total_labour = rec.quantity * (1 + rec.coefisient) * labour
            rec.update({
                'price_material': material,
                'price_labour': labour,
                'total': total_labour + total_material,
                'total_labour': total_labour,
                'total_material': total_material,
            })

    task_id = fields.Many2one('project.task', 'Task',
                              required=True, ondelete='cascade')
    package_id = fields.Many2one(
        'job.package', 'Package', required=True, ondelete='restrict')
    quantity = fields.Float('Quantity')
    uom_id = fields.Many2one(related='package_id.uom_id', readonly=True)
    line_ids = fields.One2many(
        'project.task.package.line', 'task_package_id', 'Items')
    equipment_ids = fields.One2many(
        'project.task.package.equipment', 'task_package_id', 'Equipment Items')
    coefisient = fields.Float('Coefisient', default=0.0)
    price_material = fields.Float('Material', compute='_price')
    price_labour = fields.Float('Upah', compute='_price')
    total_material = fields.Float('Total Material', compute='_price')
    total_labour = fields.Float('Total Upah', compute='_price')
    total = fields.Float('Total', compute='_price')

    @api.multi
    def action_compute_job_package(self):
        for rec in self:
            rec.line_ids = [(0, 0, {
                'package_line_id': line.id,
                'product_id': line.product_id.id,
                'product_qty': line.product_qty,
                'price_unit': line.price_unit,
                'type': line.type,
                'uom_id': line.uom_id.id,
                'coefisient': line.coefisient,
            }) for line in rec.package_id.line_ids if line.id not in rec.line_ids.mapped('package_line_id').ids]
            rec.line_ids.filtered(
                lambda l: l.package_line_id.package_id.id != rec.package_id.id).unlink()
            rec.equipment_ids = [(0, 0, {
                'package_equipment_id': line.id,
                'equipment_id': line.equipment_id.id,
                'qty': line.qty,
                'price_unit': line.price_unit,
                'coefisient': line.coefisient,
            }) for line in rec.package_id.equipment_line_ids if line.id not in rec.equipment_ids.mapped('package_equipment_id').ids]
            rec.equipment_ids.filtered(
                lambda l: l.package_equipment_id.package_id.id != rec.package_id.id).unlink()

    @api.multi
    def action_open_package(self):
        return {
            'name': _('Packages'),
            'res_model': 'project.task.package',
            'type': 'ir.actions.act_window',
            'target': 'new',
            'view_mode': 'form',
            'view_type': 'form',
            'res_id': self.ids[0]
        }


class ProjectTaskPackageLine(models.Model):
    _name = "project.task.package.line"
    _description = 'Work Packages on Task'

    @api.multi
    @api.depends('price_unit', 'product_qty', 'package_qty')
    def _price_subtotal(self):
        for line in self:
            line.price_subtotal = line.package_qty * line.product_qty * \
                line.price_unit * (1 + line.coefisient)

    @api.multi
    @api.depends('product_id')
    def _product_type(self):
        for line in self:
            if line.product_id.type == 'product':
                line.product_type = 'material'
            elif line.product_id.type == 'consu':
                line.product_type = 'consumable'
            elif line.product_id.type == 'service':
                line.product_type = 'service'
            else:
                line.product_type = 'equipment'

    task_package_id = fields.Many2one(
        'project.task.package', 'Task Package', ondelete='cascade')
    task_id = fields.Many2one(related='task_package_id.task_id', readonly=True)
    package_line_id = fields.Many2one(
        'job.package.line', 'Related Package Item', ondelete='restrict')
    package_id = fields.Many2one(
        related='package_line_id.package_id', readonly=True, store=True, ondelete='restrict')
    product_id = fields.Many2one('product.product', 'Product', required=True)
    product_name = fields.Char(
        related='product_id.name', string='Description', readonly=True)
    package_qty = fields.Float(
        'Package Qty', related='task_package_id.quantity', readonly=True)
    type = fields.Selection([
        ('material', 'Material'),
        ('consumable', 'Consumable'),
        ('labour', 'Labour'),
        ('overhead', 'Overhead')], string='Type', default='material', required=True)
    product_type = fields.Selection([
        ('material', 'Bahan'),
        ('consumable', 'Consumable'),
        ('service', 'Upah'),
        ('equipment', 'Alat')], string='Product Type', compute='_product_type')
    uom_id = fields.Many2one('product.uom', 'UoM', required=True)
    product_qty = fields.Float('Quantity', required=True)
    price_unit = fields.Float('Price Unit', required=True)
    coefisient = fields.Float('Coefisient', default=0.0)
    price_subtotal = fields.Float('Total Price', compute='_price_subtotal')


class ProjectTaskPackageEquipment(models.Model):
    _name = "project.task.package.equipment"
    _description = 'Work Packages Equipment on Task'

    @api.multi
    @api.depends('price_unit', 'qty', 'package_qty')
    def _price_subtotal(self):
        for line in self:
            line.price_subtotal = line.package_qty * line.qty * \
                line.price_unit * (1 + line.coefisient)

    task_package_id = fields.Many2one(
        'project.task.package', 'Task Package', ondelete='cascade')
    task_id = fields.Many2one(related='task_package_id.task_id', readonly=True)
    package_equipment_id = fields.Many2one(
        'job.package.equipment', 'Related Package Item', ondelete='restrict')
    package_id = fields.Many2one(
        related='package_equipment_id.package_id', readonly=True, store=True, ondelete='restrict')
    equipment_id = fields.Many2one(
        'crane.equipment', 'Equipment', required=True)
    package_qty = fields.Float(
        'Package Qty', related='task_package_id.quantity', readonly=True)
    qty = fields.Float('Quantity', required=True)
    price_unit = fields.Float('Price Unit', required=True)
    coefisient = fields.Float('Coefisient', default=0.0)
    price_subtotal = fields.Float('Total Price', compute='_price_subtotal')


class ProjectTaskLine(models.Model):
    _name = "project.task.line"
    _description = 'Work Packages Item on Task'

    @api.multi
    @api.depends('price_unit', 'product_qty')
    def _price_subtotal(self):
        for line in self:
            line.price_subtotal = line.product_qty * \
                line.price_unit * (1 + line.coefisient)

    @api.multi
    @api.depends('product_id')
    def _product_type(self):
        for line in self:
            if line.product_id.type == 'product':
                line.product_type = 'material'
            elif line.product_id.type == 'consu':
                line.product_type = 'consumable'
            elif line.product_id.type == 'service':
                line.product_type = 'service'
            else:
                line.product_type = 'equipment'

    auto_generate = fields.Boolean(
        help='technical fields', copy=False, default=False)
    task_id = fields.Many2one('project.task', 'Task', ondelete='cascade')
    product_id = fields.Many2one('product.product', 'Product', required=True)
    product_name = fields.Char(
        related='product_id.name', string='Description', readonly=True)
    type = fields.Selection([
        ('material', 'Material'),
        ('consumable', 'Consumable'),
        ('labour', 'Labour'),
        ('equipment', 'Equipment'),
        ('overhead', 'Overhead')], string='Type', default='material', required=True)
    product_type = fields.Selection([
        ('material', 'Bahan'),
        ('consumable', 'Consumable'),
        ('service', 'Upah'),
        ('equipment', 'Alat')], string='Product Type', compute='_product_type')
    uom_id = fields.Many2one('product.uom', 'UoM', required=True)
    product_qty = fields.Float('Quantity', required=True)
    price_unit = fields.Float('Price Unit', required=True)
    coefisient = fields.Float('Coefisient', default=0)
    price_subtotal = fields.Float('Total Price', compute='_price_subtotal')


class ProjectTaskEquipment(models.Model):
    _name = "project.task.equipment"
    _description = 'Work Packages Equipment on Task'

    @api.multi
    @api.depends('price_unit', 'qty')
    def _price_subtotal(self):
        for line in self:
            line.price_subtotal = line.qty * \
                line.price_unit * (1 + line.coefisient)

    @api.multi
    @api.depends('product_id')
    def _product_type(self):
        for line in self:
            if line.product_id.type == 'product':
                line.product_type = 'material'
            elif line.product_id.type == 'consu':
                line.product_type = 'consumable'
            elif line.product_id.type == 'service':
                line.product_type = 'service'
            else:
                line.product_type = 'equipment'

    auto_generate = fields.Boolean(
        help='technical fields', copy=False, default=False)
    task_id = fields.Many2one('project.task', 'Task', ondelete='cascade')
    equipment_id = fields.Many2one(
        'crane.equipment', 'Equipment', required=True)
    qty = fields.Float('Quantity', required=True)
    price_unit = fields.Float('Price Unit', required=True)
    coefisient = fields.Float('Coefisient', default=0)
    price_subtotal = fields.Float('Total Price', compute='_price_subtotal')


class ProjectTaskPackagegroup(models.Model):
    _name = "project.task.packagegroup"
    _description = 'Task Packages on Task'

    @api.multi
    @api.depends('quantity', 'packagegroup_id.price_total')
    def _price(self):
        for rec in self:
            rec.price_unit = rec.packagegroup_id.price_total
            rec.price_subtotal = rec.quantity * rec.price_unit

    task_id = fields.Many2one('project.task', 'Task',
                              required=True, ondelete='cascade')
    packagegroup_id = fields.Many2one(
        'job.packagegroup', 'Task Package', required=True, ondelete='restrict')
    quantity = fields.Float('Quantity')
    price_unit = fields.Float(
        'Price per package', compute='_price', store=True)
    price_subtotal = fields.Float('Total', compute='_price', store=True)


class ProjectProject(models.Model):
    _name = 'project.project'
    _inherit = ['project.project', 'mail.alias.mixin',
                'mail.thread', 'portal.mixin', 'mail.activity.mixin']

    def _compute_attached_docs_count(self):
        for project in self:
            document_ids = self.env['tender.document'].search(
                [('project_id', '=', self.id), ('done', '=', True)])
            project.doc_count = len(document_ids)

    @api.multi
    def _progress(self):
        for project in self:
            project.progress = max(
                project.progress_ids.mapped('percentage') or [0])

    @api.multi
    @api.depends('amount_advance', 'amount_contract')
    def _percent_advance(self):
        for project in self:
            project.percent_advance = project.amount_advance / \
                project.amount_contract * 100 if project.amount_contract else 0

    @api.multi
    @api.depends('amount_contract_show', 'vat_include')
    def _amount_contract(self):
        for project in self:
            project.amount_contract = project.amount_contract_show if not project.vat_include\
                else (project.amount_contract_show / 1.1)

    @api.multi
    @api.depends('duration_work', 'date_start')
    def _date_end(self):
        for project in self:
            if project.date_start:
                project.date_end = (datetime.strptime(
                    project.date_start, DEFAULT_SERVER_DATE_FORMAT) + relativedelta(days=project.duration_work - 1)).strftime(
                        DEFAULT_SERVER_DATE_FORMAT)

    project_location_id = fields.Many2one('project.location', 'Location')
    type_id = fields.Many2one('project.type', 'Type of Construction')
    general_manager_id = fields.Many2one('res.partner', 'General Manager')

    tender_number = fields.Char('Nomor Tender')
    contract_number = fields.Char('Nomor Kontrak')
    date_contract = fields.Date('Tanggal Kontrak')
    date_end_contract = fields.Date('Tanggal Kontrak Berakhir')
    scope_of_work = fields.Text('Lingkup Pekerjaan')
    retention = fields.Float('Retensi (%)')
    amount_contract_show = fields.Monetary('Nilai Kontrak')
    amount_contract = fields.Monetary(
        'Nilai DPP Kontrak', compute='_amount_contract', store=True)
    amount_advance = fields.Monetary('Uang Muka')
    vat_include = fields.Boolean('VAT Included in Price?', default=False)
    percent_advance = fields.Monetary(
        'Uang Muka (%)', digits=0, compute='_percent_advance', store=True)
    date_start = fields.Date('Start Date')
    date_end = fields.Date('End Date', compute='_date_end',
                           store=True, readonly=True)
    duration_work = fields.Integer('Durasi')
    date_due_dp = fields.Char('Tanggal Jatuh Tempo Uang Muka')

    no_bond = fields.Char('No Jaminan')
    publisher = fields.Char('Penerbit')
    date_start_jaminan = fields.Date('Start Date')
    date_end_jaminan = fields.Date('End Date')
    amount_bond = fields.Float('Nilai Jaminan')
    line_ids = fields.One2many(
        'project.bond', 'project_id', 'Jaminan', domain=[('type', '=', 'dp')])

    performance_no_bond = fields.Char('No Jaminan')
    performance_publisher = fields.Char('Penerbit')
    performance_date_start = fields.Date('Start Date')
    performance_date_end = fields.Date('End Date')
    performance_amount_bond = fields.Float('Nilai Jaminan')
    performance_line_ids = fields.One2many(
        'project.bond', 'project_id', 'Jaminan', domain=[('type', '=', 'performance')])

    maintenance_no_bond = fields.Char('No Jaminan')
    maintenance_publisher = fields.Char('Penerbit')
    maintenance_date_start = fields.Date('Start Date')
    maintenance_date_end = fields.Date('End Date')
    maintenance_amount_bond = fields.Float('Nilai Jaminan')
    maintenance_line_ids = fields.One2many(
        'project.bond', 'project_id', 'Jaminan', domain=[('type', '=', 'maintenance')])

    payment_term = fields.Char('Tempo Pembayaran')
    maintenance_period = fields.Integer('Masa Pemeliharaan (Hari)')
    progress_ids = fields.One2many(
        'project.progress', 'project_id', 'Progress')
    payment_ids = fields.One2many('project.payment', 'project_id', 'Payments')
    payment_type = fields.Char('Cara Pembayaran')
    attach_filename = fields.Char(string="Attachement Filename")
    attach = fields.Binary(string='Attachment')

    advance_invoice_id = fields.Many2one('account.invoice', 'Advance Invoice')
    retention_invoice_id = fields.Many2one(
        'account.invoice', 'Retention Invoice')
    progress = fields.Float('Progress', compute='_progress')

    @api.multi
    def write(self, vals):
        if any((x in vals) for x in ('date_end_jaminan', 'performance_date_end', 'maintenance_date_end')):
            self.env['mail.activity'].search([
                ('res_model_id', '=', self.env.ref(
                    'project.model_project_project').id),
                ('res_id', 'in', self.ids)
            ]).unlink()
        return super(ProjectProject, self).write(vals)

    @api.multi
    def attachment_tree_view(self):
        self.ensure_one()
        document_ids = self.env['tender.document'].search(
            [('project_id', '=', self.id), ('done', '=', True)])
        tender = self.env['tender.management'].search(
            [('number', '=', self.tender_number)], limit=1)
        domain = [('id', 'in', document_ids.ids)]
        ctx = self._context.copy()
        ctx.update({
            'default_tender_id': tender.id,
            'default_done': True,
        })
        return {
            'name': _('Documents'),
            'domain': domain,
            'res_model': 'tender.document',
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,form',
            'view_type': 'form',
            'context': ctx,
            'limit': 80,
        }

    @api.multi
    def project_to_jobcost_action(self):
        res = super(ProjectProject, self).project_to_jobcost_action()
        if not isinstance(res['context'], dict):
            res['context'] = {}
        if self:
            res['context'].update(default_partner_id=self[0].partner_id.id)
        return res

    @api.model
    def _warn_end_date(self):
        try:
            activity_type_id = self.env.ref('mail.mail_activity_data_todo').id
        except Exception:
            activity_type_id = False
        date = fields.Date.from_string(fields.Date.context_today(
            self)) + relativedelta(days=self.env.user.company_id.days_warning_project_end_date)
        model_id = self.env.ref('project.model_project_project').id
        late_warrant_advance = self.search([('date_end_jaminan', '=', date)])
        for project in late_warrant_advance:
            user_to_warn = [1]
            for user_id in user_to_warn:
                self.env['mail.activity'].create({
                    'activity_type_id': activity_type_id,
                    'note': 'Jaminan uang muka will be ended soon!',
                    'user_id': user_id,
                    'res_id': project.id,
                    'res_model_id': model_id,
                    'date_deadline': date
                })
        late_warrant_execution = self.search(
            [('performance_date_end', '=', date)])
        for project in late_warrant_execution:
            user_to_warn = [1]
            for user_id in user_to_warn:
                self.env['mail.activity'].create({
                    'activity_type_id': activity_type_id,
                    'note': 'Jaminan pelaksanaan will be ended soon!',
                    'user_id': user_id,
                    'res_id': project.id,
                    'res_model_id': model_id,
                    'date_deadline': date
                })
        late_warrant_maintenance = self.search(
            [('maintenance_date_end', '=', date)])
        for project in late_warrant_maintenance:
            user_to_warn = [1]
            for user_id in user_to_warn:
                self.env['mail.activity'].create({
                    'activity_type_id': activity_type_id,
                    'note': 'Jaminan pemeliharaan will be ended soon!',
                    'user_id': user_id,
                    'res_id': project.id,
                    'res_model_id': model_id,
                    'date_deadline': date
                })


class ProjectBond(models.Model):
    _name = 'project.bond'
    _description = 'Project Bond'

    project_id = fields.Many2one('project.project', 'Project')
    name = fields.Char('Description')
    date = fields.Date('Date')
    attach_filename = fields.Char(string="Attachement Filename")
    attach = fields.Binary(string='File')
    type = fields.Selection([('dp', 'Down Payment'), ('performance', 'Performance'), (
        'maintenance', 'Maintenance')], default='dp', string='Bond Type')


class ProjectProgress(models.Model):
    _name = 'project.progress'
    _order = 'sequence asc'
    _description = 'Project Progress Reconcilliation'
    _rec_name = 'project_id'

    @api.multi
    @api.depends('amount', 'project_id.amount_contract')
    def _percentage(self):
        for progress in self:
            progress.percentage = (progress.amount / progress.project_id.amount_contract * 100) \
                if progress.project_id.amount_contract else 0

    @api.multi
    def _percentage_set(self):
        for progress in self:
            progress.amount = progress.percentage / \
                100 * progress.project_id.amount_contract

    @api.constrains('sequence')
    def check_advance_percentage(self):
        for progress in self:
            if progress.sequence == 0:
                raise UserError(_("Unable to set sequence 0!"))

    project_id = fields.Many2one(
        'project.project', 'Project', ondelete='restrict', required=True)
    currency_id = fields.Many2one(
        related='project_id.company_id.currency_id', readonly=False)
    sequence = fields.Integer('Sequence', required=True)
    date = fields.Date('Date')
    percentage = fields.Float(
        'Progress (%)', digits=0, compute='_percentage', inverse='_percentage_set', store=True)
    amount = fields.Monetary('Progress (Rp)', required=True)
    attachment = fields.Binary('Attachment', attachment=True)
    attachment_name = fields.Char('Attachment Name')
    invoice_id = fields.Many2one('account.invoice', 'Invoice')

    @api.model
    def default_get(self, default_fields):
        res = super(ProjectProgress, self).default_get(default_fields)
        sequence = 0
        for line_dict in self.env['project.project'].resolve_2many_commands('progress_ids', self._context.get('progress_ids')):
            sequence = max([sequence, line_dict['sequence']])
        res['sequence'] = sequence + 1
        return res

    @api.multi
    def _set_comment(self):
        for progress in self:
            progress.write({'amount': progress.percentage /
                            100 * progress.project_id.amount_contract})

    @api.onchange('percentage')
    def percentage_change(self):
        self.amount = self.percentage / 100 * self.project_id.amount_contract


class ProjectPayments(models.Model):
    _name = 'project.payment'
    _description = 'Project Payments Progress'
    _order = 'sequence asc'
    _auto = False

    project_id = fields.Many2one('project.project', 'Project')
    project_progress_id = fields.Many2one('project.progress', 'Progress Ref')
    sequence = fields.Integer('Sequence')
    date = fields.Date('Date')
    invoice_sequence = fields.Char('Tagihan')
    currency_id = fields.Many2one('res.currency', 'Currency')
    amount_invoice = fields.Monetary('Nilai Pekerjaan')
    percentage = fields.Float('Percentage (%)', digits=(16, 2))
    amount_accumulation = fields.Monetary('Akumulasi Pekerjaan')
    amount_advance = fields.Monetary('Advance Amount')
    retention = fields.Monetary('Retensi')
    amount_subtotal = fields.Monetary('Phisik Incl. PPh')
    amount_vat = fields.Monetary('PPN 10%')
    amount_total = fields.Monetary('Total Incl. PPn Incl. PPh')
    invoice_id = fields.Many2one('account.invoice', 'Invoice')

    @api.model_cr
    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""CREATE or REPLACE VIEW project_payment as (
            SELECT
                1000000000000 + p.id AS id,
                p.id AS project_id, 
                NULL AS project_progress_id,
                1 AS Sequence,
                p.date_start AS date,
                'UM' AS invoice_sequence,
                c.currency_id,
                0 amount_invoice,
                0 AS percentage,
                0 AS amount_accumulation,
                -1 * p.amount_advance amount_advance,
                0 AS retention,
                p.amount_advance AS amount_subtotal,
                p.amount_advance/10 AS amount_vat,
                p.amount_advance*1.1 AS amount_total,
                p.advance_invoice_id AS invoice_id
            FROM project_project p
                JOIN account_analytic_account a on a.id = p.analytic_account_id
                JOIN res_company c ON c.id = a.company_id
            WHERE amount_advance > 0
        UNION ALL
            SELECT
                pp.id,
                pp.project_id, 
                pp.id AS project_progress_id,
                pp.sequence + (CASE WHEN p.amount_advance > 0 THEN 1 ELSE 0 END) AS Sequence,
                pp.date,
                to_char(pp.sequence,'RN') AS invoice_sequence,
                c.currency_id,
                pp.percentage/100*p.amount_contract AS amount_invoice,
                pp.percentage,
                ppa.percentage_accumulation/100*p.amount_contract AS amount_accumulation,
                pp.percentage/100*p.amount_advance AS amount_advance,
                pp.percentage/100*p.retention*p.amount_contract/100 AS retention,
                pp.percentage/100*(p.amount_contract-p.amount_advance-p.retention*p.amount_contract/100) AS amount_subtotal,
                pp.percentage/100*(p.amount_contract-p.amount_advance-p.retention*p.amount_contract/100)/10 AS amount_vat,
                pp.percentage/100*(p.amount_contract-p.amount_advance-p.retention*p.amount_contract/100)*1.1 AS amount_total,
                pp.invoice_id
            FROM project_progress pp
                LEFT JOIN (
                    SELECT 
                        id,
                        (SELECT SUM(percentage) 
                        FROM project_progress pp2 
                        WHERE 
                            pp2.project_id = pp1.project_id AND 
                            pp2.sequence <= pp1.sequence) percentage_accumulation  
                    FROM project_progress pp1
                ) ppa ON ppa.id = pp.id
                JOIN project_project p on p.id = pp.project_id
                JOIN account_analytic_account a on a.id = p.analytic_account_id
                JOIN res_company c ON c.id = a.company_id
        UNION ALL
            SELECT
                2000000000000 + p.id AS id,
                p.id AS project_id, 
                NULL AS project_progress_id,
                (CASE WHEN p.amount_advance > 0 THEN 2 ELSE 1 END) + 
                    (SELECT MAX(sequence) FROM project_progress WHERE project_id = p.id) AS Sequence,
                p.date_start AS date,
                'RETENSI' AS invoice_sequence,
                c.currency_id,
                0 amount_invoice,
                0 AS percentage,
                0 AS amount_accumulation,
                0 AS amount_advance,
                p.retention*p.amount_contract/-100 AS retention,
                p.retention*p.amount_contract/100 AS amount_subtotal,
                p.retention*p.amount_contract/1000 AS amount_vat,
                p.retention*p.amount_contract/100*1.1 AS amount_total,
                p.retention_invoice_id AS invoice_id
            FROM project_project p
                JOIN account_analytic_account a on a.id = p.analytic_account_id
                JOIN res_company c ON c.id = a.company_id
            WHERE (SELECT ROUND(SUM(percentage)) FROM project_progress WHERE project_id = p.id) = 100
        )""")

    @api.multi
    def action_view_invoice(self):
        view_id = self.env.ref('account.invoice_tree').id
        view_form_id = self.env.ref('account.invoice_form').id
        return {
            'name': _('Invoices'),
            'domain': [('id', 'in', self.mapped('invoice_id').ids)],
            'res_model': 'account.invoice',
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,form',
            'view_type': 'form',
            'views': [[view_id, 'list'], [view_form_id, 'form']],
        }

    @api.multi
    def action_create_invoice(self):
        for line in self:
            lines = []
            if line.amount_invoice:
                sale_account = line.project_id.company_id.project_sale_account_id
                lines.append((0, 0, {
                    'name': 'Tagihan %s' % line.invoice_sequence,
                    'account_analytic_id': line.project_id.analytic_account_id.id,
                    'price_unit': line.amount_invoice - line.retention,
                    'account_id': sale_account.id,
                    'invoice_line_tax_ids': [(6, 0, sale_account.tax_ids.ids)]
                }))
            if line.amount_advance:
                advance_account = line.project_id.company_id.project_advance_account_id
                lines.append((0, 0, {
                    'name': 'Uang Muka',
                    'account_analytic_id': line.project_id.analytic_account_id.id,
                    'price_unit': -line.amount_advance,
                    'account_id': advance_account.id,
                    'invoice_line_tax_ids': [(6, 0, advance_account.tax_ids.ids)]
                }))
            invoice = self.env['account.invoice'].create({
                'partner_id': line.project_id.partner_id.id,
                'date_invoice': line.project_progress_id.date,
                'invoice_line_ids': lines
            })
            if line.project_progress_id:
                line.project_progress_id.invoice_id = invoice.id
            elif line.invoice_sequence == 'UM':
                line.project_id.advance_invoice_id = invoice.id
            else:
                line.project_id.retention_invoice_id = invoice.id

        view_id = self.env.ref('account.invoice_tree').id
        view_form_id = self.env.ref('account.invoice_form').id
        return {
            'name': _('Invoice'),
            'res_id': invoice.id,
            'res_model': 'account.invoice',
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,form',
            'view_type': 'form',
            'domain': [('id', '=', invoice.id)],
            'views': [[view_id, 'list'], [view_form_id, 'form']],
        }


class ProjectType(models.Model):
    _name = 'project.type'
    _description = 'Project Type of Construction'

    name = fields.Char('Name')
