# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.osv import expression


class CraneEquipment(models.Model):
    _inherit = 'crane.equipment'
    
    code = fields.Char('Code', required=True)
    
    _sql_constraints = [
        ('crane_equipment_code_uniq', 'unique (code)', 'Code must be unique')
    ]
    
    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        args = args or []
        domain = []
        if name:
            domain = ['|', ('code', '=ilike', name + '%'), ('name', operator, name)]
            if operator in expression.NEGATIVE_TERM_OPERATORS:
                domain = ['&', '!'] + domain[1:]
        accounts = self.search(domain + args, limit=limit)
        return accounts.name_get()

    @api.multi
    def name_get(self):
        if self._context.get('product_only_display_code'):
            return [(r.id, r.code) for r in self]
        return [(r.id, '[%s] %s' % (r.code, r.name)) for r in self]
