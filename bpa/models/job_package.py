from odoo import api, fields, models


class JobPackage(models.Model):
    _name = "job.package"
    _description = "Work Package"

    @api.multi
    @api.depends('line_ids.price_subtotal', 'equipment_line_ids.price_subtotal')
    def _price_total(self):
        for package in self:
            package.price_material = sum(x.price_subtotal for x in package.material_line_ids)
            package.price_labour = sum(x.price_subtotal for x in package.labour_line_ids)
            package.price_equipment = sum(x.price_subtotal for x in package.equipment_line_ids)
            package.price_consumable = sum(x.price_subtotal for x in package.consumable_line_ids)
            package.price_overhead = sum(x.price_subtotal for x in package.overhead_line_ids)
            package.price_total = sum(x.price_subtotal for x in package.line_ids) + package.price_equipment
            
    name = fields.Char('Work Type', required=True)
    cost_type = fields.Selection([
        ('direct', 'Direct Cost'),
        ('indirect', 'Indirect Cost')], string='Cost Type', required=True)
    date = fields.Date('Date')
    analytic_account_id = fields.Many2one('account.analytic.account', 'Analytic Account')
    job_type = fields.Char('Job Type')
    uom_id = fields.Many2one('product.uom', 'UoM', required=True)
    sale_price = fields.Float('Sale Price', related='price_total', readonly=True)
    validation = fields.Char('Validation')
    price_total = fields.Float('Total Price', compute='_price_total', store=True)
    price_material = fields.Float('Total Material Price', compute='_price_total', store=True)
    price_labour = fields.Float('Total Labour Price', compute='_price_total', store=True)
    price_equipment = fields.Float('Total Equipment Price', compute='_price_total', store=True)
    price_consumable = fields.Float('Total Consumable Price', compute='_price_total', store=True)
    price_overhead = fields.Float('Total Overhead Price', compute='_price_total', store=True)
    line_ids = fields.One2many('job.package.line', 'package_id', 'Items', required=True)
    material_line_ids = fields.One2many('job.package.line', 'package_id', 'Materials', domain=[('type', '=', 'material')])
    labour_line_ids = fields.One2many('job.package.line', 'package_id', 'Labours', domain=[('type', '=', 'labour')])
    equipment_line_ids = fields.One2many('job.package.equipment', 'package_id', 'Equipments')
    consumable_line_ids = fields.One2many('job.package.line', 'package_id', 'Consumables', domain=[('type', '=', 'consumable')])
    overhead_line_ids = fields.One2many('job.package.line', 'package_id', 'Equipments', domain=[('type', '=', 'overhead')])
    
    
class JobPackageEquipment(models.Model):
    _name = "job.package.equipment"
    _description = "Work Package Equipments"

    @api.multi
    @api.depends('price_unit', 'qty', 'coefisient')
    def _price_subtotal(self):
        for line in self:
            line.price_subtotal = line.qty * line.price_unit * (1 + line.coefisient)
            
    package_id = fields.Many2one('job.package', 'Package', ondelete='cascade', required=True)
    equipment_id = fields.Many2one('crane.equipment', 'Equipment', required=True)
    qty = fields.Float('Quantity', required=True)
    price_unit = fields.Float('Price Unit')
    coefisient = fields.Float('Coefisient', default=0)
    price_subtotal = fields.Float('Total Price', compute='_price_subtotal', store=True)

    
class JobPackageLine(models.Model):
    _name = "job.package.line"
    _description = "Work Package Items"
    
    @api.multi
    @api.depends('price_unit', 'product_qty', 'coefisient')
    def _price_subtotal(self):
        for line in self:
            line.price_subtotal = line.product_qty * line.price_unit * (1 + line.coefisient)

    @api.multi
    @api.depends('product_id')
    def _product_type(self):
        for line in self:
            if line.product_id.type == 'product':
                line.product_type = 'material'
            elif line.product_id.type == 'consu':
                line.product_type = 'consumable'
            elif line.product_id.type == 'service':
                line.product_type = 'service'
            else:
                line.product_type = 'equipment'
    
    package_id = fields.Many2one('job.package', 'Package', ondelete='cascade', required=True)
    product_id = fields.Many2one('product.product', 'Product', required=True)
    product_name = fields.Char(related='product_id.name', string='Description', readonly=True)
    type = fields.Selection([
        ('material', 'Material'),
        ('consumable', 'Consumable'),
        ('labour', 'Labour'),
        ('overhead', 'Overhead')], string='Type', default='material', required=True)
    product_type = fields.Selection([
        ('material', 'Bahan'),
        ('consumable', 'Consumable'),
        ('service', 'Upah'),
        ('equipment', 'Alat')], string='Product Type', compute='_product_type')
    uom_id = fields.Many2one('product.uom', 'UoM', required=True)
    product_qty = fields.Float('Quantity', required=True)
    price_unit = fields.Float('Price Unit', related='product_id.standard_price', store=True, readonly=True)
    coefisient = fields.Float('Coefisient', default=0)
    price_subtotal = fields.Float('Total Price', compute='_price_subtotal', store=True)

    @api.onchange('product_id')
    def product_change(self):
        self.uom_id = self.product_id.uom_id.id
