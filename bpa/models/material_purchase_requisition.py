# -*- coding: utf-8 -*-
from odoo import models, api, fields, _
from odoo.exceptions import UserError
from odoo.addons.material_purchase_requisitions.models.purchase_requisition import MaterialPurchaseRequisition as MaterialPurchaseRequisitionOriginal
from dateutil.relativedelta import relativedelta


class PurchaseTender(models.Model):
    _name = 'purchase.tender'
    _description = 'Purchase Vendor Selection'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    @api.multi
    @api.depends('product_qty', 'price_unit')
    def _price_subtotal(self):
        for tender in self:
            tender.price_subtotal = tender.price_unit * tender.product_qty

    @api.multi
    def _delivery_state(self):
        for tender in self:
            if tender.purchase_line_ids:
                tender.delivery_state = tender.purchase_line_ids[0].delivery_state
            else:
                tender.delivery_state = ''

    requisition_line_id = fields.Many2one(
        'material.purchase.requisition.line', 'Requisition Line', required=True, ondelete='cascade')
    requisition_id = fields.Many2one(
        related='requisition_line_id.requisition_id', store=True, readonly=True)
    project_id = fields.Many2one(
        related='requisition_id.project_id', store=True, readonly=True)
    partner_id = fields.Many2one('res.partner', 'Vendor', required=True)
    product_id = fields.Many2one(
        'product.product', 'Product', required=True, ondelete='restrict')
    product_qty = fields.Float('Quantity', default=0, required=True)
    product_uom = fields.Many2one('product.uom', 'UoM', required=True)
    price_unit = fields.Monetary('Price Unit')
    price_subtotal = fields.Monetary(
        'Price Subtotal', compute='_price_subtotal')
    currency_id = fields.Many2one(
        'res.currency', 'Currency', default=lambda s: s.env.user.company_id.currency_id)
    purchase_line_ids = fields.One2many(
        'purchase.order.line', 'tender_id', 'Purchase Order Lines')
    delivery_state = fields.Char('Delivery State', compute='_delivery_state')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('confirmed', 'Waiting Approval'),
        ('approved', 'Approved'),
        ('purchase', 'Purchase Order Created'),
        ('cancel', 'Rejected')],
        default='draft', required=True, copy=False
    )
    
    @api.multi
    @api.depends('requisition_line_id')
    def name_get(self):
        result = []
        for tender in self:
            name = tender.id
            if tender.requisition_line_id:
                name = '%s' % (tender.requisition_line_id.requisition_id.name)
            result.append((tender.id, name))
        return result

    @api.multi
    def action_confirmed(self):
        if any(t.state != 'draft' for t in self):
            raise UserError(_("You can only confirm draft tender!"))
        self.write({'state': 'confirmed'})
        try:
            activity_type_id = self.env.ref('mail.mail_activity_data_todo').id
        except Exception:
            activity_type_id = False
        ctx = self._context.copy()
        for tender in self:
            approval_user_id = tender.requisition_id.company_id.tender_todo_approval_id
            for l in tender.requisition_id.company_id.tender_todo_approval_ids:
                if tender.price_subtotal < l.amount_limit:
                    approval_user_id = l.tender_todo_approval_id
                    break
            ctx.update({'recipient_ids': approval_user_id.ids})
            activity = self.env['mail.activity'].with_context(ctx).create({
                'activity_type_id': activity_type_id,
                'note': 'Tender need approval!',
                'user_id': approval_user_id.id,
                'res_id': tender.id,
                'res_model_id': self.env.ref('bpa.model_purchase_tender').id,
                'date_deadline': fields.Date.from_string(fields.Date.context_today(
                    self)) + relativedelta(days=5)
            })
        return {}

    @api.multi
    def action_approved(self):
        if any(t.state != 'confirmed' for t in self):
            raise UserError(
                _("You can only approve a waiting approval tender!"))
        self.write({'state': 'approved'})
        self.env['mail.activity'].search([
            ('res_id', 'in', self.ids),
            ('res_model_id', '=', self.env.ref('bpa.model_purchase_tender').id)
        ]).action_feedback('Tender Approved!')

    @api.multi
    def action_draft(self):
        if any(t.state != 'cancel' for t in self):
            raise UserError(_("You can only set a cancelled tender!"))
        self.write({'state': 'draft'})

    @api.multi
    def action_cancel(self):
        self.write({'state': 'cancel'})
        self.env['mail.activity'].search([
            ('res_id', 'in', self.ids),
            ('res_model_id', '=', self.env.ref('bpa.model_purchase_tender').id)
        ]).action_feedback('Tender Cancelled!')

    @api.multi
    def create_purchase(self):
        if any(t.state != 'approved' for t in self):
            raise UserError(_("You can only process an approved tender!"))
        order_data = {}
        for tender in self.filtered(lambda t: t.state == 'approved'):
            if not order_data.get(tender.partner_id.id):
                order_data[tender.partner_id.id] = {
                    'partner_id': tender.partner_id.id,
                    'currency_id': tender.currency_id.id,
                    'date_order': fields.Date.today(),
                    'company_id': self.env.user.company_id.id,
                    'custom_requisition_id': tender.requisition_id.id,
                    'origin': tender.requisition_id.name,
                    'order_line': []
                }
            order_data[tender.partner_id.id]['order_line'].append((0, 0, {
                'product_id': tender.product_id.id,
                'name': tender.product_id.name,
                'tender_id': tender.id,
                'product_qty': tender.product_qty,
                'product_uom': tender.product_uom.id,
                'date_planned': fields.Date.today(),
                'price_unit': tender.price_unit,
                'job_cost_id': tender.requisition_line_id.custom_job_costing_id.id,
                'job_cost_line_id': tender.requisition_line_id.custom_job_costing_line_id.id
            }))
        self.write({'state': 'purchase'})
        purchases = self.env['purchase.order']
        for po_vals in order_data.values():
            purchase = self.env['purchase.order'].create(po_vals)
            purchase.button_confirm()
            purchases |= purchase

        return {
            'name': _('Tender'),
            'res_model': 'purchase.order',
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,form',
            'view_type': 'form',
            'domain': "[('id','in',%s)]" % (purchases.ids,)
        }


class MaterialPurchaseRequisition(models.Model):
    _inherit = 'material.purchase.requisition'

    date_approved_by_wh = fields.Date('Approved by WH Date')

    @api.model
    def create(self, vals):
        code = self.env['project.project'].browse(
            vals['analytic_account_id']).code or ''
        vals['name'] = self.env['ir.sequence'].next_by_code(
            'purchase.requisition.seq')
        vals['name'] += code and ('/' + code) or ''
        return super(MaterialPurchaseRequisitionOriginal, self).create(vals)

    @api.multi
    @api.onchange('task_id',
                  'project_id')
    def onchange_project_task(self):
        for rec in self:
            rec.project_id = rec.task_id.project_id.id

    @api.multi
    def action_approve_wh(self):
        self.write({'date_approved_by_wh': fields.Date.context_today(self)})

    @api.multi
    def requisition_reject(self):
        self.write({'date_approved_by_wh': False})
        return super(MaterialPurchaseRequisition, self).requisition_reject()

    @api.multi
    def request_stock(self):
        stock_obj = self.env['stock.picking']
        move_obj = self.env['stock.move']
        for rec in self:
            if not rec.requisition_line_ids:
                raise UserError(_('Please create some requisition lines.'))
            if any(line.requisition_type == 'internal' for line in rec.requisition_line_ids):
                if not rec.location_id.id:
                    raise UserError(
                        _('Select Source location under the picking details.'))
                if not rec.custom_picking_type_id.id:
                    raise UserError(
                        _('Select Picking Type under the picking details.'))
                if not rec.dest_location_id:
                    raise UserError(
                        _('Select Destination location under the picking details.'))
                picking_vals = {
                    'partner_id': rec.employee_id.address_home_id.id,
                    'min_date': fields.Date.today(),
                    'location_id': rec.location_id.id,
                    'location_dest_id': rec.dest_location_id and rec.dest_location_id.id or rec.employee_id.dest_location_id.id or rec.employee_id.department_id.dest_location_id.id,
                    'picking_type_id': rec.custom_picking_type_id.id,  # internal_obj.id,
                    'note': rec.reason,
                    'requisition_id': rec.id,
                    'origin': rec.name,
                }
                stock_id = stock_obj.sudo().create(picking_vals)
                rec.write({'delivery_picking_id': stock_id.id})

            for line in rec.requisition_line_ids:
                if line.requisition_type == 'internal':
                    pick_vals = {
                        'product_id': line.product_id.id,
                        'product_uom_qty': line.qty,
                        'product_uom': line.uom.id,
                        'location_id': rec.location_id.id,
                        'location_dest_id': rec.dest_location_id.id,
                        'name': line.product_id.name,
                        'picking_id': stock_id.id,
                    }
                    move_obj.sudo().create(pick_vals)
                else:
                    if not line.partner_id:
                        raise UserError(
                            _('PLease Enter Atleast One Vendor on Requisition Lines'))
                    for partner in line.partner_id:
                        self.env['purchase.tender'].create({
                            'product_id': line.product_id.id,
                            'product_qty': line.qty,
                            'product_uom': line.uom.id,
                            'price_unit': line.product_id.lst_price,
                            'job_cost_id': line.custom_job_costing_id.id,
                            'job_cost_line_id': line.custom_job_costing_line_id.id,
                            'partner_id': partner.id,
                            'requisition_line_id': line.id,
                        })
                rec.state = 'stock'

    @api.multi
    def action_view_tender(self):
        return {
            'name': _('Tender'),
            'res_model': 'purchase.tender',
            'type': 'ir.actions.act_window',
            'view_mode': 'tree',
            'view_type': 'form',
            'context': {'search_default_waitings': True},
            'domain': "[('id','in',%s)]" % (
                self.mapped('requisition_line_ids.tender_ids').
                filtered(lambda t: t.state in ('draft', 'confirmed', 'approved')).ids,)
        }


class PurchaseRequisitionLine(models.Model):
    _inherit = 'material.purchase.requisition.line'

    @api.multi
    @api.constrains('custom_job_costing_line_id', 'qty')
    def check_quantity(self):
        for line in self:
            if line.custom_job_costing_line_id.actual_requisition_qty > line.custom_job_costing_line_id.product_qty:
                raise UserWarning(
                    _('Unable to request more than planned quantity!'))

    @api.multi
    @api.constrains('custom_job_costing_line_id', 'product_id')
    def check_product(self):
        for line in self:
            if line.custom_job_costing_line_id and line.custom_job_costing_line_id.product_id.id != line.product_id.id:
                raise UserWarning(_('Unable to request different product!'))

    @api.constrains('product_id', 'custom_job_costing_line_id', 'requisition_id', 'requisition_state')
    def check_product_duplicate(self):
        for line in self:
            if line.requisition_state in ('approve', 'stock', 'receive', 'cancel', 'reject'):
                continue
            for other_line in line.requisition_id.requisition_line_ids:
                if line.product_id != other_line.product_id or line.id == other_line.id:
                    continue
                raise UserWarning(
                    _('Unable to request same product! (Duplicate product : %s)' % line.product_id.display_name))

    @api.multi
    @api.depends('qty')
    def _qty_actual(self):
        for line in self:
            # TODO: qty actual terisi qty bila belum ada PO atau picking,
            # jika sudah ada confirmed PO maka qty actual terisi qty to receive PO + qty received PO - qty returned PO
            # jika po di receive, maka qty actual terisi receiving - retur
            line.qty_actual = line.qty

    date_approved_by_wh = fields.Date(
        related='requisition_id.date_approved_by_wh', readonly=True)
    qty_actual = fields.Float('Actual Qty', compute='_qty_actual', store=True)
    tender_ids = fields.One2many(
        'purchase.tender', 'requisition_line_id', 'Tender')
    requisition_state = fields.Selection(
        related='requisition_id.state', readonly=True)

    @api.onchange('custom_job_costing_id', 'custom_job_costing_line_id')
    def onchange_job_costing(self):
        if not self.custom_job_costing_id and not self.custom_job_costing_line_id:
            return {}
        elif self.custom_job_costing_id and not self.custom_job_costing_line_id:
            return {'domain': {'product_id': [('id', 'in', self.custom_job_costing_id.mapped('job_cost_line_ids.product_id').ids)]}}
        elif self.custom_job_costing_line_id:
            self.product_id = self.custom_job_costing_line_id.product_id.id
            return {'domain': {'product_id': [('id', '=', self.custom_job_costing_line_id.product_id.id)]}}

    @api.multi
    def action_request_stock(self):
        for line in self:
            if line.requisition_type == 'internal':
                raise UserError(_('Unable to recreate picking!'))
            else:
                if not line.partner_id:
                    raise UserError(
                        _('PLease Enter Atleast One Vendor on Requisition Lines'))
                for partner in line.partner_id.filtered(lambda p: p.id not in line.tender_ids.mapped('partner_id.ids')):
                    self.env['purchase.tender'] = {
                        'product_id': line.product_id.id,
                        'product_qty': line.qty,
                        'product_uom': line.uom.id,
                        'price_unit': line.product_id.lst_price,
                        'job_cost_id': line.custom_job_costing_id.id,
                        'job_cost_line_id': line.custom_job_costing_line_id.id,
                        'partner_id': partner.id,
                        'requisition_line_id': line.id,
                    }

# class MailTemplate(models.Model):
#     "Templates for sending email"
#     _inherit = "mail.template"
# 
#     @api.multi
#     def send_mail(self, res_id, force_send=False, raise_exception=False, email_values=None):
#         res = super(MailTemplate, self).send_mail(res_id, force_send, raise_exception, email_values)
# #         import ipdb;ipdb.set_trace()
#         context = self._context
#         values = self.generate_email(res_id)
#         if context.get('recipient_ids'):
#             values['recipient_ids'] = [(4, pid) for pid in context.get('recipient_ids')]
#         return res

