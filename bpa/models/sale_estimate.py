from odoo import models, fields, api, _
from odoo.exceptions import UserError

    
class SaleEstimateLineJob(models.Model):
    _inherit = 'sale.estimate.line.job'
    
    job_type = fields.Selection([
        ('material', 'Material'),
        ('labour', 'Labour'),
        ('consumable', 'Consumable'),
        ('overhead', 'Overhead')], string="Type", required=True, default='material')
        
    @api.multi
    @api.onchange('product_id')
    def product_id_change(self):
        if not self.product_id:
            return {'domain': {'product_uom': []}}
        vals = {}
        domain = {'product_uom': [('category_id', '=', self.product_id.uom_id.category_id.id)]}
        if not self.product_uom or (self.product_id.uom_id.id != self.product_uom.id):
            vals['product_uom'] = self.product_id.uom_id
            vals['product_uom_qty'] = 1.0

        product = self.product_id.with_context(
            lang=self.estimate_id.partner_id.lang,
            partner=self.estimate_id.partner_id.id,
            quantity=vals.get('product_uom_qty') or self.product_uom_qty,
            date=self.estimate_id.estimate_date,
            pricelist=self.estimate_id.pricelist_id.id,
            uom=self.product_uom.id
        )

        name = product.name_get()[0][1]
        if product.description_sale:
            name += '\n' + product.description_sale
        vals['product_description'] = name

        self._compute_tax_id()
        self.update(vals)

        title = False
        message = False
        warning = {}
        if product.sale_line_warn != 'no-message':
            title = _("Warning for %s") % product.name
            message = product.sale_line_warn_msg
            warning['title'] = title
            warning['message'] = message
            if product.sale_line_warn == 'block':
                self.product_id = False
            return {'warning': warning}
        return {'domain': domain}

    
class SaleEstimateJob(models.Model):
    _inherit = "sale.estimate.job"

    @api.depends('consumable_estimate_line_ids.price_subtotal')
    def _compute_consumable_total(self):
        for rec in self:
            for line in rec.consumable_estimate_line_ids:
                rec.consumable_total += line.price_subtotal

    @api.depends('equipment_estimate_line_ids.price_subtotal')
    def _compute_equipment_total(self):
        for rec in self:
            for line in rec.equipment_estimate_line_ids:
                rec.equipment_total += line.price_subtotal
    
    consumable_total = fields.Monetary(
        compute='_compute_consumable_total',
        string='Total Consumable Estimate',
        store=True
    )
    equipment_total = fields.Monetary(
        compute='_compute_equipment_total',
        string='Total Equipment Estimate',
        store=True
    )
    consumable_estimate_line_ids = fields.One2many(
        'sale.estimate.line.job',
        'estimate_id',
        copy=False,
        domain=[('job_type', '=', 'consumable')],
    )
    equipment_estimate_line_ids = fields.One2many(
        'sale.estimate.equipment.job',
        'estimate_id',
        copy=False,
    )
    
    @api.multi
    def compute_line_from_project(self):
        for rec in self:
            if not rec.project_id:
                raise UserError(_('Please Select project!'))
            self.env['sale.estimate.line.job'].search([('estimate_id', '=', rec.id)]).unlink()
            self.env['sale.estimate.equipment.job'].search([('estimate_id', '=', rec.id)]).unlink()
            
            product_data = {}
            equipment_data = {}
            tasks = self.env['project.task'].search([('parent_task_id', 'child_of', rec.project_id.task_ids.ids)])
            for line in tasks.mapped('line_ids'):
                try:
                    if not product_data.get(line.product_id.id):
                        product_data[line.product_id.id] = {
                            'product_id': line.product_id.id,
                            'product_uom_qty': line.product_qty,
                            'price_unit': line.price_unit,
                            'job_type' : line.type,
                            'product_uom': line.uom_id.id,
                            'auto_generate' : True
                        }
                    else:
                        product_data[line.product_id.id]['product_qty'] += line.product_qty
                except:
                    #TODO: raise
                    pass
            for line in tasks.mapped('equipment_ids'):
                if not equipment_data.get(line.equipment_id.id):
                    equipment_data[line.equipment_id.id] = {
                        'equipment_id': line.equipment_id.id,
                        'qty': line.qty,
                        'price_unit': line.price_unit,
                        'auto_generate' : True
                    }
                else:
                    equipment_data[line.equipment_id.id]['qty'] += line.qty 
            
            rec.consumable_estimate_line_ids = [(0, 0, x) for x in product_data.values()]
            rec.equipment_estimate_line_ids = [(0, 0, x) for x in equipment_data.values()]
                    
    @api.multi
    def create_job_costing(self):
        for rec in self:
            rec.jobcost_id = self.env['job.costing'].create({
                'name' : rec.project_id.name,
                'project_id' : rec.project_id.id,
                'analytic_id' : rec.analytic_id.id,
                'partner_id' : rec.partner_id.id,
                'start_date' : fields.Date.context_today(rec),
                'so_number' : rec.number,
                'cost_estimate_ids' : [(6, 0, rec.ids)],
                'job_cost_line_ids' : [(0, 0, {
                    'date' : fields.Date.context_today(rec),
                    'job_type' : l.job_type,
                    'product_id' : l.product_id.id,
                    'description' : l.product_description,
                    'reference' : rec.number,
                    'uom_id' : l.product_uom.id,
                    'product_qty' : l.product_uom_qty,
                    'cost_price' : l.price_unit
                }) for l in self.env['sale.estimate.line.job'].search([('estimate_id', '=', rec.id)])],
                'equipment_ids' : [(0, 0, {
                    'equipment_id' : l.equipment_id.id,
                    'volume': l.qty,
                    'price_rab': l.price_unit,
                }) for l in self.equipment_estimate_line_ids]
                
            }).id
            return {
                'name': _('Job Cost Sheet'),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'job.costing',
                'res_id' : rec.jobcost_id.id
            }
            
            
class SaleEstimateEquipmentJob(models.Model):
    _name = "sale.estimate.equipment.job"
    _description = 'Equipments Estimation'
    
    @api.multi
    @api.depends('price_unit', 'qty')
    def _price_subtotal(self):
        for line in self:
            line.price_subtotal = line.qty * line.price_unit

    estimate_id = fields.Many2one('sale.estimate.job', 'Sale Estimation', ondelete='cascade')
    equipment_id = fields.Many2one('crane.equipment', 'Equipment', required=True)
    qty = fields.Float('Quantity', required=True)
    price_unit = fields.Float('Price Unit', required=True)
    price_subtotal = fields.Float('Total Price', compute='_price_subtotal')
