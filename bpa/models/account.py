# -*- coding: utf-8 -*-

from odoo import models, fields, api


class AcccountInvoice(models.Model):
    _inherit = 'account.invoice'

    @api.onchange('payment_term_id', 'date_invoice', 'date')
    def _onchange_payment_term_date_invoice(self):
        date_invoice = self.date or self.date_invoice
        if not date_invoice:
            date_invoice = fields.Date.context_today(self)
        if self.payment_term_id:
            pterm = self.payment_term_id
            pterm_list = pterm.with_context(currency_id=self.company_id.currency_id.id).compute(
                value=1, date_ref=date_invoice)[0]
            self.date_due = max(line[0] for line in pterm_list)
        elif self.date_due and (date_invoice > self.date_due):
            self.date_due = date_invoice

    def _prepare_invoice_line_from_po_line(self, line):
        res = super(AcccountInvoice,
                    self)._prepare_invoice_line_from_po_line(line)
        res.update({
            'job_cost_id': line.job_cost_id.id,
            'job_cost_line_id': line.job_cost_line_id.id
        })
        return res

    payment_journal_id = fields.Many2one(
        'account.journal', 'Planned Payment Method')
