# -*- coding: utf-8 -*-
from odoo import models, fields, api, tools
from odoo.exceptions import UserError
import odoo.addons.decimal_precision as dp


class JobCosting(models.Model):
    _inherit = 'job.costing'
    
    @api.depends(
        'equipment_ids.amount_rap',
    )
    def _compute_equipment_total(self):
        for rec in self:
            rec.equipment_total = sum([p.amount_rap for p in rec.equipment_ids])
    
    @api.depends(
        'material_total',
        'labor_total',
        'overhead_total',
        'equipment_total'
    )
    def _compute_jobcost_total(self):
        for rec in self:
            rec.jobcost_total = rec.material_total + rec.labor_total + rec.overhead_total + rec.equipment_total + rec.equipment_total
            
    @api.depends(
        'job_labour_line_ids',
        'job_labour_line_ids.product_qty',
        'job_labour_line_ids.cost_price'
    )
    def _compute_labor_total(self):
        for rec in self:
            rec.labor_total = sum([(p.product_qty * p.cost_price) for p in rec.job_labour_line_ids])

    @api.depends(
        'job_consumable_line_ids',
        'job_consumable_line_ids.product_qty',
        'job_consumable_line_ids.cost_price',
    )
    def _compute_consumable_total(self):
        for rec in self:
            rec.consumable_total = sum([(p.product_qty * p.cost_price) for p in rec.job_consumable_line_ids])
            
    @api.multi
    def _profit(self):
        for rec in self:
            rec.amount_actual = sum(rec.profit_ids.mapped('actual'))
            rec.profit = (rec.amount_contract - rec.amount_actual) / rec.amount_contract * 100 if rec.amount_contract else 0
             
    equipment_ids = fields.One2many('job.equipment', 'job_cost_id', 'Equipment List')
    equipment_total = fields.Float(
        string='Total Equipment Cost',
        compute='_compute_equipment_total',
        store=True,
    )
    job_consumable_line_ids = fields.One2many(
        'job.cost.line',
        'direct_id',
        string='Consumable Items',
        copy=False,
        domain=[('job_type', '=', 'consumable')],
    )
    consumable_total = fields.Float(
        string='Total Consumable Cost',
        compute='_compute_consumable_total',
        store=True,
    )
    rab_material = fields.Monetary('RAB Material')
    rab_labour = fields.Monetary('RAB Labour')
    rab_overhead = fields.Monetary('RAB Overhead')
    rab_equipment = fields.Monetary('RAB Equipment')
    rab_consumable = fields.Monetary('RAB Consumable')
    profit_ids = fields.One2many('job.costing.profit', 'job_cost_id', 'Profits')
    percent_progress_labour = fields.Float('Labour Progress (%)')
    percent_progress_overhead = fields.Float('Overhead Progress (%)')
    percent_progress_equipment = fields.Float('Equipment Progress (%)')
    percent_progress_consumable = fields.Float('Consumable Progress (%)')
    amount_contract = fields.Monetary(related='project_id.amount_contract', string='RAB', readonly=True)
    amount_rap = fields.Float(related='jobcost_total', string='RAP', readonly=True)
    amount_actual = fields.Monetary('Actual', compute='_profit')
    profit = fields.Float('Profit (%)', compute='_profit')

    
class JobCostType(models.Model):
    _name = 'job.cost.type'
    _parent_name = "parent_id"
    _parent_store = True
    _parent_order = 'code'
    _description = 'Job Cost Type'
    
    code = fields.Char('Code', index=True)
    name = fields.Char('Name', index=True)
    parent_id = fields.Many2one('job.cost.type', 'Parent Cost Type', index=True)
    parent_left = fields.Integer('Left Parent', index=True)
    parent_right = fields.Integer('Right Parent', index=True)


class JobCostProfit(models.Model):
    _name = 'job.costing.profit'
    _description = 'Job Cost Profit Plan'
    _auto = False
    _order = 'id desc'
    
    job_cost_id = fields.Many2one('job.costing', 'Job Costing')
    currency_id = fields.Many2one(related='job_cost_id.currency_id')
    profit_type = fields.Char('Profit Type')
    rab = fields.Monetary('RAB')
    rap = fields.Monetary('RAP')
    actual = fields.Monetary('Aktual')
    profit = fields.Monetary('Profit Value')
    profit_percentage = fields.Float('Profit (%)')
    
    @api.model_cr
    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""CREATE or REPLACE VIEW job_costing_profit as (
            SELECT
                c.id * 5 AS id,
                c.id AS job_cost_id,
                'Material' AS profit_type,
                c.rab_material AS rab,
                c.material_total AS rap,
                SUM(am.amount) AS actual,
                c.rab_material - c.material_total AS profit,
                (c.rab_material - c.material_total) / c.rab_material * 100 AS profit_percentage
            FROM
                job_costing c
                LEFT JOIN (
                    SELECT p.id, p.project_id FROM stock_picking p
                    JOIN stock_picking_type t ON p.picking_type_id = t.id AND t.code = 'outgoing'
                ) sp ON sp.project_id = c.project_id 
                LEFT JOIN stock_move sm ON sm.picking_id = sp.id
                LEFT JOIN account_move am ON am.stock_move_id = sm.id
            GROUP BY c.id, c.rab_material, c.material_total
        UNION ALL
            SELECT
                (c.id * 5) - 1 AS id,
                c.id AS job_cost_id,
                'Labour' AS profit_type,
                c.rab_labour AS rab,
                c.labor_total AS rap,
                c.percent_progress_labour * c.labor_total AS actual,
                c.rab_labour - c.labor_total AS profit,
                (c.rab_labour - c.labor_total) / c.rab_labour * 100 AS profit_percentage
            FROM
                job_costing c
            GROUP BY c.id, c.rab_labour, c.labor_total
        UNION ALL
            SELECT
                (c.id * 5) - 2 AS id,
                c.id AS job_cost_id,
                'Overhead' AS profit_type,
                c.rab_overhead AS rab,
                c.overhead_total AS rap,
                c.percent_progress_overhead * c.overhead_total AS actual,
                c.rab_overhead - c.overhead_total AS profit,
                (c.rab_overhead - c.overhead_total) / c.rab_overhead * 100 AS profit_percentage
            FROM
                job_costing c
            GROUP BY c.id, c.rab_overhead, c.overhead_total
        UNION ALL
            SELECT
                (c.id * 5) - 3 AS id,
                c.id AS job_cost_id,
                'Consumable' AS profit_type,
                c.rab_consumable AS rab,
                c.consumable_total AS rap,
                c.percent_progress_consumable * c.consumable_total AS actual,
                c.rab_overhead - c.consumable_total AS profit,
                (c.rab_overhead - c.consumable_total) / c.rab_consumable * 100 AS profit_percentage
            FROM
                job_costing c
            GROUP BY c.id, c.rab_consumable, c.consumable_total
        UNION ALL
            SELECT
                (c.id * 5) - 4 AS id,
                c.id AS job_cost_id,
                'Equipment' AS profit_type,
                c.rab_equipment AS rab,
                c.equipment_total AS rap,
                c.percent_progress_equipment * c.equipment_total AS actual,
                c.rab_equipment - c.equipment_total AS profit,
                (c.rab_equipment - c.equipment_total) / c.rab_equipment * 100 AS profit_percentage
            FROM
                job_costing c
            GROUP BY c.id, c.rab_equipment, c.equipment_total
        )""")
        
    @api.multi
    def write(self, vals):
        if vals.get('rab'):
            for rec in self:
                if rec.profit_type == 'Material':
                    rec.job_cost_id.rab_material = vals.get('rab')
                elif rec.profit_type == 'Labour':
                    rec.job_cost_id.rab_labour = vals.get('rab')
                elif rec.profit_type == 'Overhead':
                    rec.job_cost_id.rab_overhead = vals.get('rab')
                elif rec.profit_type == 'Equipment':
                    rec.job_cost_id.rab_equipment = vals.get('rab')
                elif rec.profit_type == 'Consumable':
                    rec.job_cost_id.rab_consumable = vals.get('rab')


class JobCostLine(models.Model):
    _inherit = 'job.cost.line'
    
    @api.multi
    @api.depends('actual_requisition_qty', 'cost_price', 'purchase_order_line_ids')
    def _actual_value(self):
        for line in self:
            if line.job_type == 'material':
                moves = self.env['stock.move'].search([('picking_id.project_id', '=', line.direct_id.project_id.id),
                                               ('state', '=', 'done'),
                                               ('product_id', '=', line.product_id.id),
                                               ('picking_id.picking_type_id.code', '=', 'outgoing')])
                actual_used_qty = sum(moves.mapped('product_qty'))
                actual_used_value = sum(moves.mapped('account_move_ids.amount'))
            else:
                actual_used_qty = line.product_qty * line.direct_id['percent_progress_%s' % line.job_type] / 100
                actual_used_value = line.total_cost * line.direct_id['percent_progress_%s' % line.job_type] / 100
            line.update({
                'actual_requisition_value' : line.actual_requisition_qty * line.cost_price,
                'actual_value' : sum([p.price_subtotal for p in line.purchase_order_line_ids if p.order_id.state in ['purchase', 'done']]),
                'actual_used_qty' : actual_used_qty,
                'actual_used_value' : actual_used_value,
                'ratio_usage' : (100 * actual_used_value / line.total_cost) if line.total_cost else 0
            })
            
    @api.depends('product_qty', 'cost_price')
    def _compute_total_cost(self):
        for rec in self:
            rec.total_cost = rec.product_qty * rec.cost_price
            
    @api.multi
    @api.depends('custom_mpr_line_ids', 'custom_mpr_line_ids.qty_actual', 'custom_mpr_line_ids.requisition_id.state')
    def _compute_actual_requisition_quantity(self):
        for rec in self:
            rec.actual_requisition_qty = sum([p.requisition_id.state not in ['cancel'] and p.qty_actual for p in rec.custom_mpr_line_ids])
    
    job_cost_type_id = fields.Many2one('job.cost.type', 'Cost Code')
    actual_requisition_value = fields.Monetary("Actual Requisition Value", compute="_actual_value", digits=dp.get_precision('Product Price'))
    actual_value = fields.Monetary("Actual Purchase Value", compute="_actual_value", digits=dp.get_precision('Product Price'))
    actual_used_qty = fields.Float('Actual Used Qty', compute="_actual_value", digits=dp.get_precision('Product Price'))
    actual_used_value = fields.Monetary('Actual Used Value', compute="_actual_value", digits=dp.get_precision('Product Price'))
    ratio_usage = fields.Float('% Qty Planned VS Used', compute="_actual_value", digits=dp.get_precision('Product Price'))
    job_type = fields.Selection(
        selection=[('material', 'Material'),
                    ('labour', 'Labour'),
                    ('consumable', 'Consumable'),
                    ('overhead', 'Overhead')
                ],
        string="Type",
        required=True,
    )

    
class JobRAB(models.Model):
    _name = 'job.rab'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'Job Costing RAB'
    
    READONLY_STATES = {
        'done': [('readonly', True)],
        'cancel': [('readonly', True)]
    }
    
    project_id = fields.Many2one('project.project', 'Project', states=READONLY_STATES)
    name = fields.Char('Description', states=READONLY_STATES)
    user_id = fields.Many2one('res.users', 'Responsible', default=lambda self: self.env.user)
#     line_ids = fields.One2many('job.equipment', 'rab_id', 'Equipment List', states=READONLY_STATES)
    state = fields.Selection([('draft', 'New'), ('done', 'Submit'), ('cancel', 'Cancel')], default='draft', string='Status', track_visibility='onchange')
    
    @api.multi
    @api.depends('project_id', 'Name')
    def name_get(self):
        result = []
        for job in self:
            name = '%s - %s' % (job.project_id.name or '', job.name or '')
            result.append((job.id, name))
        return result
    
    @api.multi
    def action_submit(self):
        for job in self:
            job.state = 'done'
        return True
    
    @api.multi
    def action_cancel(self):
        for job in self:
            job.state = 'cancel'
        return True
    
    @api.multi
    def action_draft(self):
        for job in self:
            job.state = 'draft'
        return True
    
    @api.multi
    def unlink(self):
        for job in self:
            if job.state != 'cancel':
                raise UserError(_('In order to delete a RAB, you must cancel it first.')) 
        return super(JobRAB, self).unlink()

    
class JobEquipment(models.Model):
    _name = 'job.equipment'
    _description = 'Job Equipment for Costing RAB'
    
    @api.multi
    @api.depends('price_rab', 'price_rap', 'volume', 'price_owning_market')
    def _compute_amount(self):
        for job in self:
            job.amount_rab = job.price_rab * job.volume
            job.amount_rap = job.price_rap * job.volume
            job.amount_owning = job.price_owning_market * job.volume
            
#     rab_id = fields.Many2one('job.rab', 'RAB', on_delete='CASCADE')
    job_cost_id = fields.Many2one('job.costing', 'Job Cost', on_delete='CASCADE')
    job_cost_type_id = fields.Many2one('job.cost.type', 'Cost Code')
    equipment_id = fields.Many2one('maintenance.equipment', 'Equipment')
    equipment_name = fields.Char(related='equipment_id.name')
    uom_id = fields.Many2one('product.uom', 'Unit')
    price_rab = fields.Float('Price (RAB)')
    margin = fields.Float('Margin')
    price_rap = fields.Float('Price Unit (RAP)')
    volume = fields.Float('Volume')
    amount_rab = fields.Float('Amount (RAB)', compute=_compute_amount, store=True)
    amount_rap = fields.Float('Amount (RAP)', compute=_compute_amount, store=True)
    
    amount_owning = fields.Float('Amount Owning', compute=_compute_amount, store=True)
    price_owning = fields.Float('Owning Price (BPA)')
    price_owning_market = fields.Float('Owning Price Market')
    cost_operation = fields.Float('Operating Cost')
    
