from odoo import models, fields, api, tools, _


class ProductProduct(models.Model):
    _inherit = 'product.product'
    
    @api.multi
    def name_get(self):
        if self._context.get('product_only_display_code'):
            return [(product.id, product.default_code or product.name) for product in self.sudo()]
        return super(ProductProduct, self).name_get()
