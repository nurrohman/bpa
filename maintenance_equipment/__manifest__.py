# -*- coding: utf-8 -*-
{
    'name': 'Custom Equipment',
    'version': "1.0",
    'price': 9999.0,
    'currency': 'IDR',
    'summary': 'Custom Equipment for BPA',
    'description': """Custom Equipment for BPA""",
    'category': 'Custom',
    'author': 'PT. VISI',
    'website': 'www.visi.co.id',
    'images': [],
    'depends': [
        'hr_maintenance'
    ],
    'data': [
    'views/product_information.xml',
#     'views/equipment_form.xml',
      'views/cronjob.xml',
      'views/email_equipment.xml',
      'views/equipment_kanban.xml',
#       'views/maintenance_request.xml',
    ],
    "installable": True,
    "application": False,
}
