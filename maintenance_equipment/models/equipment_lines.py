from odoo import models, fields, api

class EquipmentLines(models.Model):
    _name = 'equipment.lines'

    equipment_id = fields.Many2one(
        comodel_name='maintenance.equipment',
        string='Equipment'
    )

    equipment_child = fields.Many2one(
        comodel_name='maintenance.equipment',
        string='Equipment'
    )


    @api.onchange('equipment_child')
    def change_asset_id(self):
        qq=0
        return {
            'domain': {
                'equipment_child': [('id', 'not in', self.equipment_id.equipment_lines.mapped('equipment_child').ids),('id', 'not in', self.env['maintenance.equipment'].search([('name','=',self.equipment_id.name)]).ids)]
            }
        }

class CustomEquipment(models.Model):
    _inherit = 'maintenance.equipment'

    equipment_lines = fields.One2many(
        comodel_name='equipment.lines',
        inverse_name='equipment_id',
        string='Equipment Ids'
    )
