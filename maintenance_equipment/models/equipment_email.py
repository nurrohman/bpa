from odoo import models, fields, api

class EquipmentEmail(models.Model):
    _inherit = 'maintenance.equipment'
    email_to = fields.Char(string='Email To')
