from odoo import models, fields, api


class EquipmentDocument(models.Model):
    _name = 'equipment.document'

    name = fields.Char(string='Description')
    file_f = fields.Binary(string='File')
    file_name = fields.Char('File Name')


    date_f = fields.Date(string='Date')
    document_id = fields.Many2one(
        comodel_name='maintenance.equipment',
        string='Document'
    )

    # equipment_name = fields.Char('sss')

    # equipment_name = fields.Char(
    #     string='Equipment',
    #     compute='get_equipment_name',
    # )
    #
    # @api.depends(equipment_name)
    # def get_equipment_name(self):
    #     if self.document_id.name:
    #         self.equipment_name = self.document_id.name




class CustomAsset(models.Model):
    _inherit = 'account.asset.asset'

    tesaja = fields.Char('Tes aja')
    custom_asset_id = fields.Many2one('maintenance.equipment', string='Products')


class EquipmentAssets(models.Model):
    _name = 'equipment.assets'

    eqipment_assets_id = fields.Many2one(
        comodel_name='maintenance.equipment',
        string='Equipment Assets'
    )

    asset_id = fields.Many2one(
        'account.asset.asset',
        string='Product Name'
    )

    @api.onchange('asset_id')
    def change_asset_id(self):
        return {
            'domain': {
                'asset_id': [('id', 'not in', self.eqipment_assets_id.custom_asset_ids.mapped('asset_id').ids)]
            }
        }


class CustomEquipment(models.Model):
    _inherit = 'maintenance.equipment'

    document_ids = fields.One2many(
        comodel_name='equipment.document',
        inverse_name='document_id',
        string='Document',
    )

    custom_asset_ids = fields.One2many(
        comodel_name='equipment.assets',
        inverse_name='eqipment_assets_id',
        string='Asset'
    )

    product_id = fields.Many2one("product.product", required=False)

    child_document_ids = fields.One2many(
        comodel_name='equipment.document',
        inverse_name='document_id',
        string='Document',
        compute='set_field'
    )

    @api.depends('child_document_ids')
    def set_field(self):
        # self.child_document_ids = self.env['equipment.document'].search([])
        self.child_document_ids = self.equipment_lines.mapped('equipment_child').mapped('document_ids')

        # qqq = '000'
        # name = self.env['res.partner'].search([])[0].name


