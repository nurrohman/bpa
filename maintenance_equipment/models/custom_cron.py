from odoo import models, fields, api

class CustomCron(models.Model):
    _name = 'custom.cron'

    def email_equipment_document_expired(self):
        from datetime import date
        expired_documents = self.env['equipment.document'].search([('date_f' , '<', date.today() )])
        for expired_document in expired_documents:

            local_context = self.env.context.copy()
            local_context.update({
                'email_to': expired_document.document_id.email_to,
                'email_from': expired_document.env.user.email,
                'email_subject': 'Dokumen telah expired',
                'document': expired_document.name,
                'expired_date': expired_document.date_f,
            })
            template = self.env.ref('maintenance_equipment.email_expired_document',False)
            template.with_context(local_context).send_mail(self.id, raise_exception=True)



