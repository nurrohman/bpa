from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError

class ProductInformation(models.Model):
    _inherit = 'maintenance.equipment'

    mesin = fields.Char(string='Mesin')
    kapasitas = fields.Char(string='Kapasitas')
    tahun = fields.Integer(string='Tahun')
    kode_alat = fields.Char(string='Kode alat')
    no_inventaris = fields.Char(string='No. Inventaris')
    
    no_seri = fields.Char(string='No. Seri')
    no_rangka = fields.Char(string='No. Rangka')
    no_mesin = fields.Char(string='No. Mesin')
    kubikasi = fields.Char(string='Kubikasi')
    bobot = fields.Char(string='Bobot')
