# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError


class EmployeeNoticeManagement(models.Model):
    _name = 'employee.notice.management'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'Employee Notice Management'

    name = fields.Char('Name', default=lambda self: _('New'), index=True, track_visibility='onchange', track_sequence=1)
    employee_id = fields.Many2one('hr.employee', 'Employee', index=True, track_visibility='onchange', track_sequence=2)
    manager_id = fields.Many2one('hr.employee', 'Manager', index=True, track_visibility='onchange', track_sequence=3)
    user_id = fields.Many2one('res.users', string='User', related='employee_id.user_id', store=True, compute_sudo=True, index=True, track_visibility='onchange', track_sequence=4)
    identification_no = fields.Char('Identification No')
    department_id = fields.Many2one('hr.department', 'Department')
    job_title = fields.Char('Job Title')
    create_date = fields.Date("Creation Date", index=True, readonly=True, default=fields.Date.context_today)
    create_uid = fields.Many2one('res.users', 'Created By', index=True, readonly=True, default=lambda self: self.env.user)
    offence_type = fields.Many2one('employee.notice.offence','Offence Type')
    warning_type = fields.Selection([('first_warning','First Warning'),
                                     ('second_warning','Second Warning'),
                                     ('final_warning','Final Warning')], 'Warning Type')
    company_id = fields.Many2one('res.company', string='Company', index=True, default=lambda self: self.env.user.company_id)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('confirmed', 'Confirmed'),
        ('done', 'Done'),
        ('cancel','Cancel'),
        ], string='State', default="draft", index=True, track_visibility='onchange', track_sequence=5)
    notes = fields.Text('Notes...')
    infraction_decision_warning = fields.Text('Decision of Infraction')
    action_improvement_warning = fields.Text('Action Improvement')
    future_infraction_warning = fields.Text('Consequence Future Infraction')

    @api.model
    def create(self, vals):
        vals['name'] = self.env['ir.sequence'].next_by_code('employee.notice.management') or _('New')
        return super(EmployeeNoticeManagement, self).create(vals)

    status = fields.Char('Status', compute='onchange_employee_id')

    @api.multi
    @api.onchange('employee_id')
    def onchange_employee_id(self):
        self.status = self.employee_id.status_id

        values = {
            'manager_id': self.employee_id.parent_id and self.employee_id.parent_id.id,
            'identification_no': self.employee_id.identification_id and self.employee_id.identification_id,
            'department_id': self.employee_id.department_id and self.employee_id.department_id.id,
            'job_title': self.employee_id.job_title and self.employee_id.job_title,
        }
        self.update(values)

    @api.multi
    def action_confirm(self):
        for record in self:
            record.state = 'confirmed'

    @api.multi
    def action_done(self):
        for record in self:
            record.state = 'done'

    @api.multi
    def action_cancel(self):
        for record in self:
            record.state = 'cancel'

    @api.multi
    def action_draft(self):
        for record in self:
            record.state = 'draft'


class EmployeeNoticeOffence(models.Model):
    _name = 'employee.notice.offence'
    _description = 'HR Employee Notice Offence'
    
    name = fields.Char('Name')
    active = fields.Boolean('Active', default=True, readonly=False)
    