# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError


class HrEmployee(models.Model):
    _inherit = 'hr.employee'

    @api.multi
    def _notice_count(self):
        notice_obj = self.env['employee.notice.management']
        for notice in self:
            notice.notice_count = notice_obj.search_count([('employee_id', 'in', notice.ids)])

    notice_count = fields.Integer(compute='_notice_count', string='Notices')

    @api.multi
    def open_employee_notice_details(self):
        xml_id = 'bi_employee_notice_management.employee_notice_mgmt_tree_view'
        tree_view_id = self.env.ref(xml_id).id
        xml_id = 'bi_employee_notice_management.employee_notice_mgmt_form_view'
        form_view_id = self.env.ref(xml_id).id
        return {
            'name': _('Notice'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'views': [(tree_view_id, 'tree'), (form_view_id, 'form')],
            'res_model': 'employee.notice.management',
            'domain': [('employee_id', 'in', self.ids)],
            'type': 'ir.actions.act_window',
        }