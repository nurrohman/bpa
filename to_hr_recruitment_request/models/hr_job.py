from odoo import models, fields, api


class HRJob(models.Model):
    _inherit = ['hr.job']

    recuitment_request_ids = fields.One2many('hr.recruitment.request', 'job_id', oldname='request_ids',
                                  string='Recruitment Requests',
                                  readonly=True)
    recuitment_requests_count = fields.Integer(string='Recruitment Requests', compute='_compute_recuitment_requests_count', store=True)

    quota_sudah_terpenuhi = fields.Boolean(default=False)

    @api.depends('recuitment_request_ids')
    def _compute_recuitment_requests_count(self):
        for r in self:
            r.recuitment_requests_count = len(r.recuitment_request_ids)

    @api.depends('recuitment_request_ids')
    def suggest_no_of_recruitment(self):
        for r in self:
            no_of_recruitment = 0
            for request_id in r.recuitment_request_ids:
                if request_id.state in ('accepted', 'recruiting'):
                    no_of_recruitment += request_id.expected_employees
            if r.no_of_recruitment < no_of_recruitment:
                r.no_of_recruitment = no_of_recruitment

    @api.multi
    def set_recruit(self):
        super(HRJob, self).set_recruit()
        self.suggest_no_of_recruitment()
        request_id = self.env['hr.recruitment.request'].sudo().search([('state', '=', 'accepted'), ('job_id', '=', self.id)], limit=1)
        if request_id:
            request_id.signal_workflow('recruit')
        return True

