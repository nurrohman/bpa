from odoo import models, fields


class HREmployee(models.Model):
    _inherit = 'hr.employee'

    request_id = fields.Many2one('hr.recruitment.request', string="Recruitment Request", index=True)

#     @api.model
#     def create(self, vals):
#         if 'job_id' in vals:
#             request = self.env['hr.recruitment.request'].sudo().search([('state', '=', 'recruiting'), ('job_id', '=', vals['job_id'])], limit=1)
#             if request:
#                 vals['request_id'] = request.id
#         return super(hr_employee, self).create(vals)
