from odoo import models, fields, api


class HRApplicant(models.Model):
    _inherit = ['hr.applicant']

    request_id = fields.Many2one('hr.recruitment.request', string="Recruitment Request", compute='_compute_request_id', store=True, index=True)

    @api.depends('job_id')
    def _compute_request_id(self):
        RequestSudo = self.env['hr.recruitment.request'].sudo()
        for r in self:
            if r.job_id:
                request = RequestSudo.search([
                    ('state', '=', 'in_recruitment'),
                    ('job_id', '=', r.job_id.id)], limit=1)
                # request.expected_employees
                # len(self.env['hr.employee'].search([('request_id','=',request.id)]).ids)
                if len(self.env['hr.employee'].search([('request_id','=',request.id)]).ids) >= request.expected_employees and request.expected_employees != 0:
                    request = False

                r.request_id = request or False
            else:
                r.request_id = False
