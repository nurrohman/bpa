{
    'name': "HR Recruitment Requests",

    'summary': """
        Allow Department Managers to request for new employee recruitment""",

    'description': """
This application allows Department Managers to submit requests for new employee recruitment. Once approved, the requests will be transfered to HR Department for executing recruitment

Key Features
============

Submission & Approval Workflow
------------------------------
- Department Managers can create a Recruitment Request to recruit additional employees of either an existing job position or new job position in his department
- Upon approved, new job position will be created if the request is for a new position, then activate the Odoo's recruitment process

Keep track of the progress
--------------------------
- Number of applicants
- Number of hired employees
- Percentage of the hired employees against the initial request

Editions Supported
==================
1. Community Edition
2. Enterprise Edition

    """,

    'author': "T.V.T Marine Automation (aka TVTMA)",
    'website': 'https://www.tvtmarine.com',
    'live_test_url': 'https://v12demo-int.erponline.vn',
    'support': 'support@ma.tvtmarine.com',

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Human Resources',
    'version': '1.0.0',

    # any module necessary for this one to work correctly
    'depends': ['hr_recruitment', 'hr_contract','bi_hr_custom'],

    # always loaded
    'data': [
             'data/module_data.xml',
             'security/recruitment_request_security.xml',
             'security/ir.model.access.csv',
             'data/recruitment_request_data.xml',
             'views/recruitment_request.xml',
             'views/hr_applicant_views.xml',
             'views/hr_job.xml',
    ],

    'installable': True,
    'auto_install': False,
    'application': True,
    'price': 45.9,
    'currency': 'EUR',
    'license': 'OPL-1',
}
