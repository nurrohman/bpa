# MuK Documents Attachment Rules

MuK Documents Attachment allows to store Odoo
Attachments inside the Document Management System. By creating rules
attachments can be placed automatically in the right directory.