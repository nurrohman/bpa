from odoo import models, fields, api, _
from odoo.exceptions import UserError


class MaintenanceEquipment(models.Model):
    _inherit = 'maintenance.equipment'
    
    @api.multi
    def _compute_working_hours(self):
        for equipment in self:
            equipment.working_hours = sum(equipment.timesheet_line_ids.mapped('time_total'))
            equipment.fuel_total = sum(equipment.timesheet_line_ids.mapped('amount_fuel'))
            
    @api.multi
    def _compute_oil(self):
        for equipment in self:
            equipment.oil_total = sum(equipment.oil_line_ids.mapped('qty'))
    
    location_id = fields.Many2one('project.project', string='Location')
    equipment_total = fields.Float('Total Equipment')
    working_hours = fields.Float('Total Equip Working Hours', compute='_compute_working_hours')
    fuel_total = fields.Float('Total Fuel', compute='_compute_working_hours')
    
    oil_total = fields.Float('Total Oil', compute='_compute_oil')
    
    oil_line_ids = fields.One2many('maintenance.equipment.timesheet', 'equipment_id', string='Oil Timesheet Lines', domain=[('type', '=', 'oil'), ('state', '=', 'done')])
    part_line_ids = fields.One2many('maintenance.equipment.timesheet', 'equipment_id', string='Part Timesheet Lines', domain=[('type', '=', 'part'), ('state', '=', 'done')])
    
    timesheet_line_ids = fields.One2many('equipment.timesheet.line', 'equipment_id', string='Part Timesheet Lines', domain=[('state', '=', 'done')])
