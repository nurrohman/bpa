from odoo import models, fields, api, _
from odoo.exceptions import UserError


class EquipmentTimesheet(models.Model):
    _name = 'equipment.timesheet'
    _description = 'EquipmentTimesheets'
    _order = 'name desc, id'
        
    @api.one
    @api.depends('morning_line_ids.time_total', 'noon_line_ids.time_total', 'night_line_ids.time_total', 'morning_line_ids.amount_fuel', 'noon_line_ids.amount_fuel', 'night_line_ids.amount_fuel')
    def _compute_total(self):
        self.total_time = sum(self.morning_line_ids.mapped('time_total')) + sum(self.noon_line_ids.mapped('time_total')) + sum(self.night_line_ids.mapped('time_total'))
        self.total_fuel = sum(self.morning_line_ids.mapped('amount_fuel')) + sum(self.noon_line_ids.mapped('amount_fuel')) + sum(self.night_line_ids.mapped('amount_fuel'))
    
    @api.one
    @api.depends('oil_line_ids.qty', 'part_line_ids.qty')
    def compute_oil_part(self):
        self.oil_count = sum(self.oil_line_ids.mapped('qty'))
        self.part_count = sum(self.part_line_ids.mapped('qty'))
    
    name = fields.Char('Name', default='New', copy=False)
    date = fields.Datetime('Date', default=fields.Datetime.now)
    user_id = fields.Many2one('res.users', 'Responsible', default=lambda self: self.env.user)
    project_id = fields.Many2one('project.project', 'Work Location')
    equipment_id = fields.Many2one('maintenance.equipment', 'Equipment')
    operator_id = fields.Many2one('hr.employee', string='Operator')
    helper_id = fields.Many2one('hr.employee', string='Helper')
    equipment_total = fields.Float('Total Equipment')
    working_hours = fields.Float('Total Equip Working Hours')
    oil_total = fields.Float('Total Oil')
    fuel_total = fields.Float('Total Fuel')
    note = fields.Text('Note')
    
    total_time = fields.Float('Total Time', compute='_compute_total', store=True)
    total_fuel = fields.Float('Total Fuel', compute='_compute_total', store=True)
    
    state = fields.Selection([
        ('draft', 'Draft'),
        ('waiting', 'Waiting First Approval'),
        ('waiting2', 'Waiting Second Approval'),
        ('done', 'Done'),
        ('reject', 'Rejected'),
        ('cancel', 'Cancelled')], string='Status', default='draft')
    
    morning_line_ids = fields.One2many('equipment.timesheet.line', 'timesheet_id', string='Timesheet Lines', domain=[('time_type', '=', 'morning')])
    noon_line_ids = fields.One2many('equipment.timesheet.line', 'timesheet_id', string='Timesheet Lines', domain=[('time_type', '=', 'noon')])
    night_line_ids = fields.One2many('equipment.timesheet.line', 'timesheet_id', string='Timesheet Lines', domain=[('time_type', '=', 'night')])
    
    oil_line_ids = fields.One2many('maintenance.equipment.timesheet', 'timesheet_id', string='Oil Timesheet Lines', domain=[('type', '=', 'oil')])
    part_line_ids = fields.One2many('maintenance.equipment.timesheet', 'timesheet_id', string='Part Timesheet Lines', domain=[('type', '=', 'part')])
    oil_count = fields.Float('Oil Quantity', compute='compute_oil_part', store=True)
    part_count = fields.Float('Parts Quantity', compute='compute_oil_part', store=True)
    
    @api.onchange('equipment_id')
    def change_equipment_id(self):
        if not self.equipment_id:
            return {}
        self.oil_total = self.equipment_id.oil_total
        self.fuel_total = self.equipment_id.fuel_total
        self.equipment_total = self.equipment_id.equipment_total
        self.working_hours = self.equipment_id.working_hours
    
    @api.multi
    def button_confirm(self):
        self.state = 'waiting'
        return True
    
    @api.multi
    def button_approve(self):
        self.state = 'waiting2'
        return True

    @api.multi
    def button_approve2(self):
        self.state = 'done'
        return True
    
    @api.multi
    def button_reject(self):
        self.state = 'reject'
        return True
    
    @api.multi
    def button_reject2(self):
        self.state = 'reject'
        return True
    
    @api.multi
    def button_draft(self):
        self.state = 'draft'
        return True
    
    @api.model
    def create(self, vals):
        if vals.get('name', 'New') == 'New':
            vals['name'] = self.env['ir.sequence'].next_by_code('equipment.timesheet') or '/'
        return super(EquipmentTimesheet, self).create(vals)
    
    @api.multi
    def unlink(self):
        for rec in self:
            if rec.state != 'draft':                
                raise UserError(_('Only draft document can be deleted !')) 
        return super(EquipmentTimesheet, self).unlink()


class EquipmentTimesheetLine(models.Model):
    _name = 'equipment.timesheet.line'
    _description = 'Equipment Timesheet Lines'
    
    @api.depends('time_start', 'time_end')
    def _compute_total_time(self):
        for line in self:
            line.time_total = line.time_end - line.time_start
    
    timesheet_id = fields.Many2one('equipment.timesheet', 'Equipment Timesheet', copy=False, ondelete='cascade')
    equipment_id = fields.Many2one(related='timesheet_id.equipment_id', string='Equipment', store=True)
    
    time_start = fields.Float('Start')
    time_end = fields.Float('End')    
    time_total = fields.Float('Total', compute='_compute_total_time', store=True)
    time_type = fields.Selection([
        ('morning', 'Morning'),
        ('noon', 'Noon'),
        ('night', 'Night'),
    ], string='Time Type')
    task = fields.Char('Task/Result')
    note = fields.Char('Note')
    amount_fuel = fields.Float('Amount of fuel filling (Liter)')
    time = fields.Float('Time')
    state = fields.Selection(related='timesheet_id.state', string='State', store=True, default='draft')

    
class MaintenanceEquipmentTimesheet(models.Model):
    _name = 'maintenance.equipment.timesheet'
    _description = 'Maintenance Equipment Timesheet'
    
    timesheet_id = fields.Many2one('equipment.timesheet', 'Equipment Timesheet', copy=False, ondelete='cascade')
    equipment_id = fields.Many2one(related='timesheet_id.equipment_id', string='Equipment', store=True, copy=False, ondelete='cascade')
    
    type = fields.Selection([
        ('oil', 'Oil'),
        ('part', 'Part')
    ], string='Type')
    product_id = fields.Many2one('product.product', string='Product')
    qty = fields.Float('Quantity')
    uom_id = fields.Many2one('product.uom', string='Unit of Measures')
    name = fields.Char('Description')
    employee_id = fields.Many2one('hr.employee', 'Responsible')
    state = fields.Selection(related='timesheet_id.state', string='State', store=True, default='draft')

    @api.onchange('product_id')
    def change_product_id(self):
        if not self.product_id:
            return {}
        self.qty = 1
        self.name = self.product_id.name
        self.uom_id = self.product_id.uom_id
    
