# -*- coding: utf-8 -*-
{
    'name': 'Equipment Timesheet',
    'version': "11.1.0",
    'summary': 'Equipment Timesheet',
    'description': """Equipment Timesheet""",
    'category': 'custom',
    'author': 'PT. VISI',
    'website': 'www.visi.co.id',
    'depends': [
        'project',
        'maintenance'
    ],
    'data': [
        'security/ir.model.access.csv',
        'views/ir_sequence.xml',
        'views/equipment_timesheet_view.xml',
        'views/equipment_view.xml',
    ],
    "installable": True,
    "application": False,
}
