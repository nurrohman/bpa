odoo.define('hr_employee_dashboard.employee_dashboard', function (require) {
    'use strict';
    var AbstractAction = require('web.AbstractAction');
    var core = require('web.core');
    var ajax = require('web.ajax');
    
    var _t = core._t;
    var QWeb = core.qweb;


    var EmployeeDashBoard = AbstractAction.extend({
        events: {
            'click #leaves_left': 'action_leaves_left',
            'click #payslips': 'action_payslips',
            'click #leaves_approve': 'action_leaves_approve',
            'click #leaves_allocation_approve': 'action_leaves_allocations_approve',
            'click #download_pdf': 'action_download_pdf',
        },
        init: function(parent, action) {
            this._super.apply(this, arguments);
            var employee_data = [];
            var self = this;
            var def = self._rpc({
                model: 'hr.employee',
                method: 'get_employee_info',
            }, []).then(function (res) {
                self.employee_data = res[0];
            }).done(function(){
                self.render();
            });            
        },
        start: function() {
            var self = this;   
            return this._super();
        },
        render: function() {
            var super_render = this._super;
            var self = this;
            var hr_employee_dashboard = $(QWeb.render("DashboardPage",{widget: self})).appendTo(this.$el); 
            self.render_graphs();
            return hr_employee_dashboard
        }, 
        reload: function () {
            window.location.reload();
        },
        action_leaves_left: function(e) {
            var self = this;
            e.preventDefault();
            var action = {
                name: _t('Leaves Left'),
                type: 'ir.actions.act_window',
                view_type: 'form',
                view_mode: 'tree,form',
                res_model: 'hr.leave.report',
                search_view_id: self.employee_data.leave_report_search_view_id,
                domain: [['employee_id', '=', self.employee_data.employee_id],['state','=', 'validate'],['holiday_status_id.allocation_type', '!=', 'no']],
                context: {
                    'search_default_year': true,
                    'search_default_group_type': true,
                },
                views: [[false, 'list'], [false, 'form']],
            };
            this.do_action(action,{
                on_reverse_breadcrumb: function(){ return self.reload();}
            });
        },
        action_payslips: function(e){
            var self = this;
            e.preventDefault();
            var action = {
                name: _t('Payslips'),
                type: 'ir.actions.act_window',
                view_type: 'form',
                view_mode: 'tree,form',
                res_model: 'hr.payslip',
                search_view_id: self.employee_data.payslip_search_view_id,
                context: {
                    'search_default_employee_id': [self.employee_data.employee_id],
                    'default_employee_id': self.employee_data.employee_id,
                },
                views: [[false, 'list'], [false, 'form']],
            };
            this.do_action(action,{
                on_reverse_breadcrumb: function(){ return self.reload();}
            });
        },
        action_leaves_approve: function(e) {
            var self = this;
            e.preventDefault();
            var action = {
                name: _t('Leaves to Approve'),
                type: 'ir.actions.act_window',
                view_type: 'form',
                view_mode: 'tree,form',
                res_model: 'hr.leave',
                search_view_id: self.employee_data.leave_search_view_id,
                domain: [['state','=', 'confirm']],
                context: {
                    'search_default_approve': true,
                },
                views: [[false, 'list'], [false, 'form']],
            };
            this.do_action(action,{
                on_reverse_breadcrumb: function(){ return self.reload();}
            });
        },
        action_leaves_allocations_approve: function(e) {
            var self = this;
            e.preventDefault();
            var action = {
                name: _t('Leaves Allocations to Approve'),
                type: 'ir.actions.act_window',
                view_type: 'form',
                view_mode: 'tree,form',
                res_model: 'hr.leave.allocation',
                search_view_id: self.employee_data.leave_search_view_id,
                domain: [['state','=', 'confirm']],
                context: {
                    'search_default_approve': true,
                },
                views: [[false, 'list'], [false, 'form']],
            };
            this.do_action(action,{
                on_reverse_breadcrumb: function(){ return self.reload();}
            });
        },
        getbgcolor: function (length) {
            var colors = ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"];
            var color = [];
            var length = length;
            for (var i = 1; i < length; i++ ) {
                for (var j =1; j < colors.length;j++ ) {
                    color.push(colors[Math.abs(j-i)]);
                }
            }
            
            return color;
        },       
        render_graphs: function() {            
            var self = this;
            var male_count = self.employee_data.male_count;
            var female_count = self.employee_data.female_count;
            var top_nationalities = self.employee_data.top_nationalities;
            var top_nationalities_count = self.employee_data.top_nationalities_count;
            var department_name = self.employee_data.department_name;
            var department_count = self.employee_data.department_count;
            var headcount = self.employee_data.headcount;
            var todays_attendance = self.employee_data.todays_attendance;
            var position_name = self.employee_data.position_name;
            var position_count = self.employee_data.position_count;

            var GenderCanvas = this.$el.find("#GenderchartContainer");
            var GenderChart = new Chart(GenderCanvas, {
                type: 'pie',
                data: {
                labels: ["Male", "Female"],
                datasets: [{
                    backgroundColor: ["#3e95cd", "#8e5ea2"],
                    data: [male_count,female_count]
                }]
                },
                options: {
                    responsive: true,
                    legend: { display: false },
                }
            });


            var NationalityCanvas = this.$el.find("#NationalitychartContainer");
            var NationalityChart = new Chart(NationalityCanvas, {
                type: 'horizontalBar',
                data: {
                labels: top_nationalities,
                datasets: [{
                    backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
                    data: top_nationalities_count
                }]
                },
                options: {
                    responsive: true,
                    legend: { display: false },
                }
            });


            var DepartmentCanvas = this.$el.find("#DepatmentchartContainer");
            var DepartmentChart = new Chart(DepartmentCanvas, {
                type: 'bar',
                data: {
                    labels: department_name,
                    datasets: [
                        {                        
                        backgroundColor: self.getbgcolor(department_name.length),
                        data: department_count
                        }
                    ]
                },
                options: {
                    responsive: true,
                    legend: { display: false },
                }
            });            

            var AttendanceCanvas = this.$el.find("#AttendancechartContainer");
            var AttendanceChart = new Chart(AttendanceCanvas, {
                type: 'pie',
                data: {
                labels: ["Headcount", "Todays Attendance"],
                datasets: [{
                    backgroundColor: ["#3cba9f","#e8c3b9"],
                    data: [headcount,todays_attendance]
                }]
                },
                options: {
                    responsive: true,
                    legend: { display: false },
                }
            });

            var PositionsCanvas = this.$el.find("#PositionschartContainer");
            var PositionsChart = new Chart(PositionsCanvas, {
                type: 'doughnut',
                data: {
                    labels: position_name,
                    datasets: [
                        {
                            backgroundColor: self.getbgcolor(position_name.length),
                            data: position_count
                        }
                    ]
                },
                options: {
                    responsive: true,
                    legend: { display: false },
                }
            });

        },
        action_download_pdf: function(){
            html2canvas(document.querySelector("#dashboard_canvas")).then(canvas => {                
                var dataURL = canvas.toDataURL();
                var pdf = new jsPDF('landscape');
                pdf.addImage(dataURL, 'JPEG',5, 10, 287, 200);
                pdf.save("CanvasJS Charts.pdf");
              });
        }        

    });

    core.action_registry.add('hr_employee_dashboard.employee_dashboard', EmployeeDashBoard);
    return EmployeeDashBoard;
});
