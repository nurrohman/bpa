
# -*- coding: utf-8 -*-
#################################################################################
# Author      : CodersFort (<https://codersfort.com/>)
# Copyright(c): 2017-Present CodersFort.
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://codersfort.com/>
#################################################################################
{
    "name": "HR Dashboard",
    "summary": "HR Employee Dashboard For HR Managers",
    "version": "12.0.1",
    "description": """HR Employee Dashboard For HR Managers""",
    "version" :  "1.0.0",
    "author": "Ananthu Krishna",
    "maintainer": "Ananthu Krishna",
    "website": "http://codersfort.com",
    "images": ["images/Banner.png"],
    "license" :  "Other proprietary",
    "category": "hr",
    "depends": [
        "base",
        "hr",
        "hr_attendance",
        "hr_holidays",
        "hr_payroll",
    ],
    "data": [
        "views/hr_employee_dashboard_view.xml",
        "views/assets.xml"
    ],
    "qweb": ["static/src/xml/hr_employee_dashboard.xml"],
    "installable": True,
    "application": True,
    "price"                :  17,
    "currency"             :  "EUR",
    "pre_init_hook"        :  "pre_init_check",
    
}
