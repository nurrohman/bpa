from odoo import _, api, fields, models
from dateutil.relativedelta import relativedelta
from datetime import datetime

class Employee(models.Model):
    _inherit = "hr.employee"

    @api.model
    def get_employee_info(self):
        cr = self.env.cr
        user_id = self.env.uid
        employee_id = self.env['hr.employee'].sudo().search([('user_id', '=', user_id)], limit=1)
        # leaves_to_approve = self.env['hr.leave'].sudo().search_count([('state', 'in', ['confirm', 'validate1'])])
        leaves_to_approve = self.env['hr.holidays'].sudo().search_count([('state', 'in', ['confirm', 'validate1'])])
        leaves_allowcations_to_approve = self.env['hr.leave.allocation'].sudo().search_count([('state', 'in', ['confirm', 'validate1'])])
        all_employees = self.env['hr.employee'].sudo().search([])
        todays_attendance = self.env['hr.attendance'].sudo().search_count([
            ('check_in', '>=',str(datetime.now().replace(hour=0, minute=0, second=0))),
            ('check_in', '<=', str(datetime.now().replace(hour=23, minute=59, second=59)))
        ])
        headcount = self.env['hr.employee'].sudo().search_count([])
        leave_search_view_id = self.env.ref('hr_holidays.view_hr_holidays_filter').id
        leave_report_search_view_id = self.env.ref('hr_holidays.view_hr_holidays_filter_report').id
        payslip_search_view_id = self.env.ref('hr_payroll.view_hr_payslip_filter').id
        male_count = self.env['hr.employee'].sudo().search_count([('gender', '=', 'male')])
        female_count = self.env['hr.employee'].sudo().search_count([('gender', '=', 'female')])
        
        
        #Age Demography
        age_les_24_m = 0            
        age_les_24_f = 0
        age_25_34_m = 0
        age_25_34_f = 0
        age_35_44_m = 0
        age_35_44_f = 0
        age_45_54_m = 0
        age_45_54_f = 0
        age_gre_54_m = 0            
        age_gre_54_f = 0

        for employee in all_employees:
            if employee.birthday:
                dob  = datetime.strptime(str(employee.birthday), "%Y-%m-%d")
                age_calc = (datetime.today() - dob).days/365
                age = round(age_calc)
                
                if age < 25 and employee.gender == 'male':
                    age_les_24_m += 1
                if age < 25 and employee.gender == 'female':
                    age_les_24_f += 1

                if 25 <= age <= 34 and employee.gender == 'male':
                    age_25_34_m += 1
                if 25 <= age <= 34 and employee.gender == 'female':
                    age_25_34_f += 1

                if 35 <= age <= 44 and employee.gender == 'male':
                    age_35_44_m += 1
                if 35 <= age <= 44 and employee.gender == 'female':
                    age_35_44_f += 1

                if 45 <= age <= 54 and employee.gender == 'male':
                    age_45_54_m += 1
                if 45 <= age <= 54 and employee.gender == 'female':
                    age_45_54_f += 1

                if age > 54 and employee.gender == 'male':
                    age_gre_54_m += 1
                if age > 54 and employee.gender == 'female':
                    age_gre_54_f += 1
                    

        
            
        #Top 5 Nationality Count
        top_nationality_querry = """
                select country.name as country_name, count(employee.id) as count from hr_employee as employee
                left join res_country as country on  country.id = employee.country_id
                where country.name is not null group by country.name order by count(employee.id) desc limit 5
                """
        cr.execute(top_nationality_querry)
        top_nationality_data = cr.dictfetchall()
        top_nationalities = []
        top_nationalities_count = []
        for record in top_nationality_data:
            top_nationalities.append(record['country_name'])
            top_nationalities_count.append(record['count'])


        #Department Count
        department_querry = """
                select department.name as department_name, count(employee.id) as count  from hr_employee as employee
                left join hr_department as department on  department.id = employee.department_id
                where department.name is not null group by department.name order by count(employee.id) desc
                """
        cr.execute(department_querry)
        department_data = cr.dictfetchall()
        department_name = []
        department_count = []
        for record in department_data:
            department_name.append(record['department_name'])
            department_count.append(record['count'])


        #Positions Count
        positions_querry = """
                select job.name as position_name, count(employee.id) as count from hr_employee as employee
                left join hr_job as job on  job.id = employee.job_id
                where job.name is not null group by job.name order by count(employee.id) desc
                """
        cr.execute(positions_querry)
        position_data = cr.dictfetchall()
        position_name = []
        position_count = []
        for record in position_data:
            position_name.append(record['position_name'])
            position_count.append(record['count'])

        dashboard_data = []
        dashboard_data.append({
            'employee_id': employee_id.id,
            'image':employee_id.image,
            'name' : employee_id.name,
            'job_name': employee_id.job_id.name,
            'leaves_count': str(employee_id.leaves_count),
            'payslip_count': str(employee_id.payslip_count), 
            'male_count':male_count,
            'female_count' : female_count, 
            'top_nationalities'  : top_nationalities ,
            'top_nationalities_count' : top_nationalities_count,
            'leaves_to_approve' : leaves_to_approve,
            'leaves_allowcations_to_approve' : leaves_allowcations_to_approve,
            'age_les_24_m' : age_les_24_m,
            'age_les_24_f' : age_les_24_f,
            'age_25_34_m' : age_25_34_m,
            'age_25_34_f' : age_25_34_f,
            'age_35_44_m' : age_35_44_m,
            'age_35_44_f' : age_35_44_f,
            'age_45_54_m': age_45_54_m,
            'age_45_54_f' : age_45_54_f,
            'age_gre_54_m': age_gre_54_m,
            'age_gre_54_f' : age_gre_54_f,
            'department_name' : department_name,
            'department_count' : department_count,
            'todays_attendance' :todays_attendance,
            'headcount': headcount,
            'position_name': position_name,
            'position_count': position_count,
            'leave_search_view_id': leave_search_view_id,
            'leave_report_search_view_id': leave_report_search_view_id,
            'payslip_search_view_id': payslip_search_view_id,

        })
        return dashboard_data