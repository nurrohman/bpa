from odoo import models, fields, api, _
from odoo.exceptions import UserError


class AllowanceWizard(models.TransientModel):
    _name = 'allowance.wizard'
    _description = 'Allowance Wizard'
    
    allowance_id = fields.Many2one('hr.allowance', string='Allowance')
    journal_id = fields.Many2one('account.journal', string='Payment Method')
    amount = fields.Float('Payment Amount')
    payment_date = fields.Date('Payment Date', default=fields.Date.today())
    
    @api.multi
    def first_move_line_get(self, move_id, company_currency, current_currency, amount, account_id):
        debit = 0.0
        credit = amount
        if amount < 0.0: 
            debit = -amount
            credit = 0.0
        sign = debit - credit < 0 and -1 or 1
        # set the first line of the voucher
        move_line = {
                'name': self.allowance_id.name,
                'debit': debit,
                'credit': credit,
                'account_id': account_id,
                'move_id': move_id,
                'journal_id': self.journal_id.id,
                'currency_id': company_currency != current_currency and current_currency or False,
                'amount_currency': (sign * abs(self.amount)
                    if company_currency != current_currency else 0.0),
                'date': self.payment_date,
            }
        return move_line

    @api.multi
    def account_move_get(self):
        if self.journal_id.sequence_id:
            if not self.journal_id.sequence_id.active:
                raise UserError(_('Please activate the sequence of selected journal !'))
            name = self.journal_id.sequence_id.with_context(ir_sequence_date=self.payment_date).next_by_id()
        else:
            raise UserError(_('Please define a sequence on the journal.'))
        move = {
            'name': name,
            'journal_id': self.journal_id.id,
            'date': self.payment_date,
            'ref': '%s (%s)' % (self.allowance_id.name, self.allowance_id.reference) if self.allowance_id.reference else self.allowance_id.name,
        }
        return move

    @api.multi
    def action_move_create(self, journal_id):
        '''
        Confirm the vouchers given in ids and create the journal entries for each of them
        ''' 
        if not self.env.user.company_id.allowance_account_id:
            raise UserError(_('Please define an allowance account... \n Go to Payroll - Setting !'))
        
        company_currency = self.journal_id.company_id.currency_id.id
        current_currency = self.journal_id.company_id.currency_id.id
        local_context = dict(self._context, force_company=self.journal_id.company_id.id)
        ctx = local_context.copy()
        ctx['date'] = self[0].payment_date
        ctx['check_move_validity'] = False
        move = self.env['account.move'].create(self[0].account_move_get())
        move_line = self.env['account.move.line']
        move_line |= self.env['account.move.line'].with_context(ctx).create(self[0].with_context(ctx).first_move_line_get(move.id, company_currency, current_currency, \
                                                                                                    self.amount, self.journal_id.default_credit_account_id.id))
        move_line |= self.env['account.move.line'].with_context(ctx).create(self[0].with_context(ctx).first_move_line_get(move.id, company_currency, current_currency, \
                                                                                                    -self.amount, self.journal_id.company_id.allowance_account_id.id))
        move.post()
        self.allowance_id.write({
            'move_id': move.id,
            'state': 'paid',
        })
        return True
