# -*- coding: utf-8 -*-
{
    'name': 'HR Allowance',
    'version': "1.0",
    'summary': 'HR Allowance',
    'description': """HR Allowance""",
    'category': 'custom',
    'author': 'PT. VISI',
    'website': 'www.visi.co.id',
    'depends': [
        'hr_payroll',
        'project',
        'bi_hr_custom'
    ],
    'data': [
        'security/ir.model.access.csv',
        'wizard/allowance_wizard_view.xml',
        'views/allowance_view.xml',
        'views/config_view.xml',
        'views/attendance_view.xml',
        'views/report_allowance.xml',
        'views/employee_view.xml',
        'views/ir_sequence.xml',
    ],
    "installable": True,
    "application": False,
}
