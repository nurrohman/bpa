from odoo import models, fields, api, _
from odoo.exceptions import UserError


class Employee(models.Model):
    _inherit = 'hr.employee'
    
    project_id = fields.Many2one('project.project', string='Work Location')
    
    
