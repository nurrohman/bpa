from odoo import models, fields, api, _
from odoo.exceptions import UserError


class Allowance(models.Model):
    _name = 'hr.allowance'
    _description = 'Allowances'
    _order = 'name desc, id'
    
    @api.one
    @api.depends('line_ids.price_subtotal')
    def _compute_amount(self):
        self.amount_total = sum(self.line_ids.mapped('price_subtotal'))
    
    name = fields.Char('Name', default='New')
    date_from = fields.Date('Date From')
    date_to = fields.Date('Date To')
    reference = fields.Char('Reference')
    date = fields.Datetime('Date', default=fields.Datetime.now)
    user_id = fields.Many2one('res.users', 'Responsible', default=lambda self: self.env.user)
    project_id = fields.Many2one('project.project', 'Project')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('waiting', 'Waiting Approval'),
        ('approved', 'Approved'),
        ('reject', 'Rejected'),
        ('paid', 'Paid'),
        ('cancel', 'Cancelled')], string='Status', default='draft')
    
    line_ids = fields.One2many('allowance.line', 'allowance_id', string='Allowance Lines')
    move_id = fields.Many2one('account.move', string='Journal Entries')
    move_line_ids = fields.One2many(related='move_id.line_ids', string='Move Lines')
    amount_total = fields.Float('Amount Total', compute='_compute_amount', store=True)
    
    @api.multi
    def button_submit(self):
        self.state = 'waiting'
        return True
    
    @api.multi
    def button_approve(self):
        self.state = 'approved'
        self.line_ids._insert_to_payslip()
        return True
    
    @api.multi
    def button_reject(self):
        self.state = 'reject'
        return True
    
    @api.multi
    def button_payment(self):
        data_obj = self.env['ir.model.data']
        view = data_obj.xmlid_to_res_id('hr_allowance.hr_allowance_wizard_form')
        return {
            'name': _('Register Payments'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'allowance.wizard',
            'target': 'new',
            'views': [(view, 'form')],
            'view_id': view,
            'context' : {
                'default_allowance_id': self.id,
                'default_amount': sum(self.line_ids.mapped('price_subtotal'))
            }
        }
        
    @api.multi
    def button_draft(self):
        self.state = 'draft'
        return True
    
    @api.model
    def create(self, vals):
        if vals.get('name', 'New') == 'New':
            vals['name'] = self.env['ir.sequence'].next_by_code('hr.allowance') or '/'
        return super(Allowance, self).create(vals)
    
    @api.multi
    def unlink(self):
        for rec in self:
            if rec.state != 'draft':                
                raise UserError(_('Only draft document can be deleted !')) 
        return super(Allowance, self).unlink()


class AllowanceLine(models.Model):
    _name = 'allowance.line'
    _description = 'Allowance Lines'
    
    @api.one
    @api.depends('allowance_id.date_from', 'allowance_id.date_to')
    def _compute_attendance(self):
        attendaces = self.env['hr.attendance'].search([
            ('employee_id', '=', self.employee_id.id),
            ('check_in', '>=', self.allowance_id.date_from),
            ('check_in', '<=', self.allowance_id.date_to),
        ])
        if attendaces:
            self.attendance_ids = attendaces.ids
            self.attendance_of_days = len(self.attendance_ids)
            self.meal_of_days = len(self.attendance_ids.filtered(lambda a:a.meal == True))
            self.price_subtotal = self.meal_of_days * self.meal_amount
            
    allowance_id = fields.Many2one('hr.allowance', 'Allowance', copy=False, ondelete='cascade')
    employee_id = fields.Many2one('hr.employee', 'Employee')
    job_id = fields.Many2one('hr.job', 'Job Position')
    job_level_id = fields.Many2one('hr.employee.level', 'Job Level')
    attendance_of_days = fields.Integer('Attendance of Days', compute='_compute_attendance', store=True)
    meal_of_days = fields.Integer('Meals of Days', compute='_compute_attendance', store=True)
    meal_amount = fields.Float('Meal / Day')
    price_subtotal = fields.Float('Subtotal', compute='_compute_attendance', store=True)
    attendance_ids = fields.Many2many('hr.attendance', compute='_compute_attendance', string='Attendance')
    
    @api.onchange('employee_id')
    def change_employee_id(self):
        if not self.employee_id:
            return
        self.job_id = self.employee_id.job_id.id
        self.job_level_id = self.employee_id.job_level_id.id
        if not self.employee_id.contract_ids:
            raise UserError(_('Employees %s do not have a contract !') % (self.employee_id.name)) 
        print(self.employee_id.contract_ids[-1].name)
        self.meal_amount = self.employee_id.contract_ids and self.employee_id.contract_ids[-1].meal or 0
        
    @api.multi
    def _insert_to_payslip(self):
        for line in self:
            if not line.employee_id.contract_ids:
                raise UserError(_('Employees %s do not have a contract... \n Please create contract first !') % (line.employee_id.name)) 
            payslips = line.employee_id.slip_ids.filtered(lambda s:s.state == 'draft')
            if not payslips:
                raise UserError(_('Employees %s do not have a open (draft) payslip... \n Please create payslip first !') % (line.employee_id.name)) 
            self.env['hr.payslip.input'].create({
                'payslip_id': payslips and payslips[0].id or False,
                'name': line.allowance_id.name,
                'code': 'ALW',
                'amount': line.price_subtotal,
                'contract_id': line.employee_id.contract_ids and line.employee_id.contract_ids[-1].id or False
            })
        return True
    
