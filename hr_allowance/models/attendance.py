from odoo import models, fields, api


class Attendance(models.Model):
    _inherit = 'hr.attendance'
    
    meal = fields.Boolean('Meal', default=False)

    @api.model
    def create(self, vals):   
        obj_employee = self.env['hr.employee'].search([('id', '=', vals['employee_id'])])
        if obj_employee.project_id.ho_checklist:

            import datetime
            # ambil weekday dari check in:
            datetime_checkin = datetime.datetime.strptime(vals['check_in'], "%Y-%m-%d %H:%M:%S")
            datetime_checkin = datetime_checkin + datetime.timedelta(hours=7)

            checkin_weekday = datetime_checkin.weekday()
            checkin_hour = datetime_checkin.hour
            checkin_minutes = datetime_checkin.minute
            checkin_hour_minute = checkin_hour + (checkin_minutes / 100)

            for i in obj_employee.resource_calendar_id.attendance_ids:
                if int(i.dayofweek) == checkin_weekday:

                    if checkin_hour_minute < i.hour_from:
                        # self.meal = True
                        vals['meal'] = True
                    else:
                        # self.meal = False
                        vals['meal'] = False

                else:
                    continue
        else:
            vals['meal'] = True

        res = super(Attendance, self).create(vals)
        return res

    @api.multi
    def write(self, vals):

        if self.employee_id.project_id.ho_checklist:

            import datetime
            # ambil weekday dari check in:
            datetime_checkin = datetime.datetime.strptime(self.check_in, "%Y-%m-%d %H:%M:%S")
            datetime_checkin = datetime_checkin + datetime.timedelta(hours=7)

            checkin_weekday = datetime_checkin.weekday()
            checkin_hour = datetime_checkin.hour
            checkin_minutes = datetime_checkin.minute
            checkin_hour_minute = checkin_hour + (checkin_minutes / 100)

            for i in self.employee_id.resource_calendar_id.attendance_ids:
                if int(i.dayofweek) == checkin_weekday:

                    if checkin_hour_minute < i.hour_from:
                        # self.meal = True
                        vals['meal'] = True
                    else:
                        # self.meal = False
                        vals['meal'] = False

                else:
                    continue

        res = super(Attendance, self).write(vals)

        return res
