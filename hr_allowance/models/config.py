from odoo import api, fields, models


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    allowance_account_id = fields.Many2one(
        related='company_id.allowance_account_id')


class ResCompany(models.Model):
    _inherit = 'res.company'

    allowance_account_id = fields.Many2one(
        'account.account', 'Alowance Account')

