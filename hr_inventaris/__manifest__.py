# -*- coding: utf-8 -*-
{
    'name': 'HR Inventaris',
    'version': "1.0",
    'summary': 'HR Inventaris',
    'description': """HR Inventaris""",
    'category': 'custom',
    'author': 'PT. VISI',
    'website': 'www.visi.co.id',
    'depends': [
        'hr',
        'bi_hr_custom'
    ],
    'data': [
        'security/ir.model.access.csv',
        'wizard/inventaris_wizard_view.xml',
        'views/inventaris_view.xml',
        'views/employee_view.xml',
        'views/product_view.xml',
        'views/ir_sequence.xml',
    ],
    "installable": True,
    "application": False,
}
