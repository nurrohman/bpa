from odoo import models, fields, api, _
from odoo.exceptions import UserError


class InventarisWizard(models.TransientModel):
    _name = 'inventaris.wizard'
    _description = 'Inventaris Wizard'
    
    action = fields.Selection([
        ('po', 'Purchase Order'),
        ('stock', 'Take from Stock'),
    ], string='Action')
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.user.company_id)
    partner_id = fields.Many2one('res.partner', 'Vendor')
    date_schedule = fields.Datetime('Schedule Date')
    
    picking_type_id = fields.Many2one('stock.picking.type', 'Picking Type')
    location_id = fields.Many2one('stock.location', 'Source Location')
    request_id = fields.Many2one('hr.inventaris.request', 'Request Reference')
    
    @api.onchange('picking_type_id')
    def change_picking_type(self):
        if not self.picking_type_id:
            return {}
        self.location_id = self.picking_type_id.default_location_src_id.id
        
    @api.multi
    def action_confirm(self):
        if self.action == 'po':
            self._create_po()
        else:
            self._create_picking()
            
    @api.multi
    def _create_po(self):
        PurchaseOrder = self.env['purchase.order']
        vals = {
            'partner_id': self.partner_id.id,
            'date_order': fields.Date.today(),
            'date_planned': self.date_schedule
        }
        purchase = PurchaseOrder.create(vals)
        lines = self._create_order_line(purchase)
        purchase.message_post_with_view('mail.message_origin_link',
            values={'self': purchase, 'origin': self.request_id},
            subtype_id=self.env.ref('mail.mt_note').id)
        self.request_id.write({
            'purchase_id': purchase.id,
            'state': 'done'
        })
        return True
    
    @api.multi
    def _create_order_line(self, purchase):
        for line in self.request_id.line_ids:
            template = {
                'name': (line.product_id.display_name or '')[:2000],
                'product_id': line.product_id.id,
                'product_uom': line.product_id.uom_id.id,
                'product_qty': line.qty,
                'date_planned': fields.Date.today(),
                'order_id': purchase.id,
                'taxes_id': [(6, 0, line.product_id.supplier_taxes_id.ids)],
                'price_unit': 0
            }
            self.env['purchase.order.line'].create(template)
    
    @api.model
    def _prepare_picking(self):
        if not self.env.user.partner_id.property_stock_supplier.id:
            raise UserError(_("You must set a Vendor Location for this partner %s") % self.partner_id.name)
        return {
            'picking_type_id': self.picking_type_id.id,
            'partner_id': self.env.user.partner_id.id,
            'date': fields.Date.today(),
            'origin': self.request_id.name,
            'location_dest_id': self.env.user.partner_id.property_stock_customer.id,
            'location_id': self.location_id.id,
            'company_id': self.company_id.id,
        }

    @api.multi
    def _create_picking(self):
        StockPicking = self.env['stock.picking']
        if any([ptype in ['product', 'consu'] for ptype in self.request_id.line_ids.mapped('product_id.type')]):
            res = self._prepare_picking()
            picking = StockPicking.create(res)
            moves = self._create_stock_moves(picking)
            picking.action_confirm()
            picking.action_assign()
            picking.message_post_with_view('mail.message_origin_link',
                values={'self': picking, 'origin': self.request_id},
                subtype_id=self.env.ref('mail.mt_note').id)
        self.request_id.write({
            'picking_id': picking.id,
            'state': 'done'
        })
        return True
    
    @api.multi
    def _create_stock_moves(self, picking):
        for line in self.request_id.line_ids.filtered(lambda l:l.product_id.type != 'service'):
            template = {
                'name': (line.product_id.display_name or '')[:2000],
                'product_id': line.product_id.id,
                'product_uom': line.product_id.uom_id.id,
                'product_uom_qty': line.qty,
                'date': fields.Date.today(),
                'date_expected': fields.Date.today(),
                'location_id': self.location_id.id,
                'location_dest_id': self.env.user.partner_id.property_stock_customer.id,
                'picking_id': picking.id,
                'partner_id': self.env.user.partner_id.id,
                'state': 'draft',
                'company_id': self.company_id.id,
                'picking_type_id': self.picking_type_id.id,
                'origin': self.request_id.name,
                'warehouse_id': self.picking_type_id.warehouse_id.id,
            }
            self.env['stock.move'].create(template)
    
