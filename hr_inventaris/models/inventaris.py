from odoo import models, fields, api, _
from odoo.exceptions import UserError


class InventarisRequest(models.Model):
    _name = 'hr.inventaris.request'
    _description = 'Inventaris'
    _order = 'name desc, id'
    
    @api.model
    def _default_employee(self):
        return self.env['hr.employee'].search([('user_id', '=', self.env.uid)], limit=1)

    name = fields.Char('Name', default='New')
    date = fields.Date('Date', default=fields.Date.today)
    employee_id = fields.Many2one('hr.employee', string='Employee', default=_default_employee)
    user_id = fields.Many2one('res.users', 'Responsible', default=lambda self: self.env.user)
    project_id = fields.Many2one('project.project', 'Project')
    department_id = fields.Many2one('hr.department', string='Department')
    job_id = fields.Many2one('hr.job', string='Job Position')
    work_status = fields.Char('Work Status')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('waiting_manager', 'Waiting Manager'),
        ('waiting_safety', 'Waiting Safety'),
        ('waiting_equipment', 'Waiting Equipment'),
        ('waiting_ga', 'Waiting GA'),
        ('waiting_gm', 'Waiting GM'),
        ('approved', 'Approved'),
        ('reject', 'Rejected'),
        ('done', 'Done'),
        ('cancel', 'Cancelled')], string='Status', default='draft')
    
    type = fields.Selection([
        ('uniform', 'Uniform'),
        ('shoes', 'Shoes'),
        ('other', 'Other')
    ], string='Type', default='uniform')
    
    action = fields.Selection([
        ('po', 'Purchase Order'),
        ('stock', 'Take from Stock'),
    ], string='Action')
    
    line_ids = fields.One2many('hr.inventaris', 'request_id', string='Inventaris Lines')
    picking_id = fields.Many2one('stock.picking', 'Picking')
    purchase_id = fields.Many2one('purchase.order', 'Purchase')
        
    @api.onchange('employee_id')
    def change_employee(self):
        if not self.employee_id:
            return
        self.department_id = self.employee_id.department_id.id
        self.job_id = self.employee_id.job_id.id
        self.project_id = self.employee_id.project_id.id
        self.work_status = self.employee_id.status_id
    
    @api.multi
    def button_to_manager(self):
        self.state = 'waiting_manager'
        return True
    
    @api.multi
    def button_approve_manager(self):
        if self.env.user != self.department_id.manager_id.user_id:
            raise UserError(_('Only manager of department can approve this request !')) 
        if self.type == 'uniform':
            self.state = 'waiting_ga'
        elif self.type == 'shoes':
            self.state = 'waiting_safety'
        else:
            self.state = 'waiting_equipment'
            
        return True
    
    @api.multi
    def button_approve_safety(self):
        self.state = 'waiting_ga'
        return True
    
    @api.multi
    def button_approve_equipment(self):
        self.state = 'waiting_ga'
        return True
    
    @api.multi
    def button_approve_ga(self):
        self.state = 'waiting_gm'
        return True
    
    @api.multi
    def button_approve_ga(self):
        self.state = 'waiting_gm'
        return True
    
    @api.multi
    def button_approve_gm(self):
        self.state = 'approved'
        return True
    
    @api.multi
    def button_wizard(self):
        view = self.env.ref('hr_inventaris.inventaris_wizard_form').id
        title = 'Purchase Order' if self.action == 'po' else 'Internal Transfer'
        return {
            'name': _(title),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'inventaris.wizard',
            'target': 'new',
            'views': [(view, 'form')],
            'view_id': view,
            'context' : {
                'default_action': 'po' if self.action == 'po' else 'stock',
                'default_request_id': self.id
            }        
        }    
    
    @api.multi
    def button_reject(self):
        self.state = 'reject'
        return True
    
    @api.multi
    def button_done(self):
        self.state = 'done'
        return True
        
    @api.multi
    def button_draft(self):
        self.state = 'draft'
        return True
    
    @api.model
    def create(self, vals):
        if vals.get('name', 'New') == 'New':
            vals['name'] = self.env['ir.sequence'].next_by_code('hr.inventaris.request') or '/'
        return super(InventarisRequest, self).create(vals)
    
    @api.multi
    def unlink(self):
        for rec in self:
            if rec.state != 'draft':                
                raise UserError(_('Only draft document can be deleted !')) 
        return super(InventarisRequest, self).unlink()
    
    @api.multi
    def action_view_picking(self):
        action = self.env.ref('stock.action_picking_tree_all')
        result = action.read()[0]

        result['context'] = {}
        pick_ids = self.picking_id
        # choose the view_mode accordingly
        if not pick_ids or len(pick_ids) > 1:
            result['domain'] = "[('id','in',%s)]" % (pick_ids.ids)
        elif len(pick_ids) == 1:
            res = self.env.ref('stock.view_picking_form', False)
            result['views'] = [(res and res.id or False, 'form')]
            result['res_id'] = pick_ids.id
        return result
    
    @api.multi
    def action_view_purchase(self):
        action = self.env.ref('purchase.purchase_rfq')
        result = action.read()[0]

        result['context'] = {}
        po_ids = self.purchase_id
        # choose the view_mode accordingly
        if not po_ids or len(po_ids) > 1:
            result['domain'] = "[('id','in',%s)]" % (po_ids.ids)
        elif len(po_ids) == 1:
            res = self.env.ref('purchase.purchase_order_form', False)
            result['views'] = [(res and res.id or False, 'form')]
            result['res_id'] = po_ids.id
        return result


class Inventaris(models.Model):
    _name = 'hr.inventaris'
    _description = 'Inventaris'
    
    @api.one
    @api.depends('uniform_size', 'shoes_size')
    def _get_size(self):
        if self.type == 'uniform':
            self.size = self.uniform_size
        elif self.type == 'shoes':
            self.size = self.shoes_size
        else:
            self.size = ''
        
    request_id = fields.Many2one('hr.inventaris.request', string='Inventaris Request', readonly=True, ondelete='cascade')
    product_id = fields.Many2one('product.product', string='Product')
    employee_id = fields.Many2one('hr.employee', string='Employee')
    state = fields.Selection(related='request_id.state', default='draft', store=True, string='Status')
    qty = fields.Integer('Quantity', default=1)
    uniform_size = fields.Selection([
        ('XXL', 'XXL'),
        ('XL', 'XL'),
        ('L', 'L'),
        ('M', 'M'),
        ('S', 'S'),
    ], defaultt='L', string='Uniform Size', default='L')
    shoes_size = fields.Integer('Shoes Size')
    name = fields.Char('Description')
    type = fields.Selection([
        ('uniform', 'Uniform'),
        ('shoes', 'Shoes'),
        ('other', 'Others'),
    ], default='uniform', string='Type')

    size = fields.Char('Size', compute='_get_size', store=True)
    action = fields.Selection([
        ('po', 'Purchase Order'),
        ('stock', 'Take from Stock'),
    ], string='Action')
    
    @api.onchange('product_id')
    def change_product_id(self):
        if not self.product_id:
            return
        self.name = self.product_id.display_name
        
