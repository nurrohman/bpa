from odoo import models, fields, api, _
from odoo.exceptions import UserError


class Employee(models.Model):
    _inherit = 'hr.employee'
    
    inventaris_ids = fields.One2many('hr.inventaris', 'employee_id', string='Inventaris', domain=[('state', '=', 'done')])
