from odoo import models, fields, api, _
from odoo.exceptions import UserError


class ProductTemplate(models.Model):
    _inherit = 'product.template'
    
    inventaris = fields.Boolean('Inventaris')


class ProductProduct(models.Model):
    _inherit = 'product.product'
    
    inventaris = fields.Boolean(related='product_tmpl_id.inventaris', string='Inventaris', store=True)
