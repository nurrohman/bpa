# -*- coding: utf-8 -*-

from odoo import models


class AcccountInvoice(models.Model):
    _inherit = 'account.invoice'

    def _prepare_invoice_line_from_po_line(self, line):
        res = super(AcccountInvoice, self)._prepare_invoice_line_from_po_line(line)
        res.update({
            'job_cost_id' : line.job_cost_id.id,
            'job_cost_line_id' : line.job_cost_line_id.id
        })
        return res
