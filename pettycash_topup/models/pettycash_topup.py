from odoo import models, fields, api, _
from odoo.exceptions import UserError
from odoo.tools.float_utils import float_is_zero
from _collections import defaultdict


class PettycashTopup(models.Model):
    _name = "pettycash.topup"
    _description = 'Pettycash Topup'
    
    @api.onchange('pettycash_account_id', 'date_start', 'date_end', 'type')
    def _get_aml(self):
        if self.type != 'topup':
            return
        if not self.pettycash_account_id or not self.date_start or not self.date_end:
            return
        begining_balance = 0
        for move_line in self.env['account.move.line'].search([('account_id', '=', self.pettycash_account_id.id),
                                                           ('date', '<', self.date_start)]):
            begining_balance += move_line.debit - move_line.credit

        move_lines = self.env['account.move.line'].search([('account_id', '=', self.pettycash_account_id.id),
                                                           ('date', '>=', self.date_start),
                                                           ('date', '<=', self.date_end)])
        ending_balance = begining_balance
        for move_line in move_lines:
            ending_balance += move_line.debit - move_line.credit
            
        self.begining_balance = begining_balance
        self.ending_balance = ending_balance
        self.aml_ids = move_lines
    
    name = fields.Char('Number', required=True, copy=False, default='New')
    journal_id = fields.Many2one('account.journal', 'Payment Method')
    bank_acc_number = fields.Char(related='journal_id.bank_acc_number', string='Bank Number')
    bank_id = fields.Many2one(related='journal_id.bank_id', string='Bank Name')
    pettycash_account_id = fields.Many2one('account.account', 'Pettycash Account')
    source_account_id = fields.Many2one('account.account', 'Source Account')
    transit_account_id = fields.Many2one('account.account', 'Transit Account')
    type = fields.Selection([
        ('begining', 'Modal Awal'),
        ('topup', 'Top Up')
    ], string='Request Type', default='topup')
    priority = fields.Selection([
        ('regular', 'Regular'),
        ('urgent', 'Urgent')
    ], string='Priority', default='regular')
    amount = fields.Monetary('Amount', currency_field='currency_id', required=True, default=0)
    begining_balance = fields.Monetary('Begining Balance', currency_field='currency_id', default=0)
    ending_balance = fields.Monetary('Ending Balance', currency_field='currency_id', default=0)
    
    company_id = fields.Many2one('res.company', 'Company', default=lambda self:self.env.user.company_id, required=True)
    currency_id = fields.Many2one(related='company_id.currency_id', readonly="1")

    communication = fields.Char('Memo')
    user_id = fields.Many2one('res.users', 'Responsible', default=lambda self: self.env.user)
    
    date_request = fields.Date('Request Date', required=True, default=fields.Date.today())
    date_start = fields.Date('Start Date', required=True)
    date_end = fields.Date('End Date')
    project_id = fields.Many2one('account.analytic.account', string='Project')
    aml_ids = fields.One2many('account.move.line', string='Journal Items', compute='_get_aml')
    transfered_move_id = fields.Many2one('account.move', 'Transfered Journal')
    received_move_id = fields.Many2one('account.move', 'Received Journal')
    released_move_id = fields.Many2one('account.move', 'Released Journal')
    
    state = fields.Selection([
        ('draft', 'New'),
        ('waiting_am', 'To Approve Accounting Manager'),
        ('waiting_gm', 'To Approve General Manager'),
        ('waiting_director', 'To Approve Director'),
        ('waiting_presdir', 'To Approve Presiden Director'),
        ('approved', 'Approved'),
        ('transfered', 'Transfered'),
        ('received', 'Received'),
        ('released', 'Released'),
        ('cancelled', 'Cancelled'),
        ], string='Status', readonly=True, copy=False, index=True, default='draft')
    
    @api.onchange('journal_id')
    def change_journal_id(self):
        if not self.journal_id:
            return
        self.source_account_id = self.journal_id.default_credit_account_id.id
    
    @api.multi
    def account_move_get(self):
        journal = self.journal_id
        if journal.sequence_id:
            if not journal.sequence_id.active:
                raise UserError(_('Please activate the sequence of selected journal !'))
            name = journal.sequence_id.with_context(ir_sequence_date=fields.Date.today()).next_by_id()
        else:
            raise UserError(_('Please define a sequence on the journal.'))

        move = {
            'name': name,
            'journal_id': self.journal_id.id,
            'narration': self.communication,
            'date': fields.Date.today(),
            'ref': self.name,
        }
        return move
    
    @api.multi
    def first_move_line_get(self, move_id, company_currency, current_currency, amount, account_id):
        amount = self._convert_amount(amount)
        # set the first line of the voucher
        move_line = {
                'name': self.name or '/',
                'debit': abs(amount),
                'credit': 0.0,
                'account_id': account_id,
                'move_id': move_id,
                'journal_id': self.journal_id.id,
                'partner_id': self.user_id.partner_id.id,
                'currency_id': company_currency != current_currency and current_currency or False,
                'date': fields.Date.today(),
            }
        return move_line
    
    @api.multi
    def _convert_amount(self, amount):
        for rec in self:
            return rec.currency_id.compute(amount, rec.company_id.currency_id)
        
    @api.multi
    def move_line_create(self, move_id, company_currency, current_currency):
        amount = self._convert_amount(self.amount)
        move_line = {
            'journal_id': self._context.get('journal_id', False),
            'name': self.name,
            'account_id': self._context.get('credit_account_id', False),
            'move_id': move_id,
            'partner_id': self.user_id.partner_id.id,
            'analytic_account_id': False,
            'quantity': 1,
            'credit': abs(amount),
            'debit': 0.0,
            'date': fields.Date.today(),
        }
        self.env['account.move.line'].create(move_line)
    
    @api.multi
    def action_move_create(self):
        company_currency = self[0].journal_id.company_id.currency_id.id
        current_currency = self[0].currency_id.id or company_currency
        ctx = self._context.copy()
        ctx['date'] = fields.Date.today()
        ctx['check_move_validity'] = False
        move = self.env['account.move'].create(self[0].account_move_get())
        move_line = self.env['account.move.line']
        move_line |= self.env['account.move.line'].with_context(ctx).\
            create(self[0].with_context(ctx).first_move_line_get(move.id, company_currency, current_currency, \
                                                                self.amount, self._context.get('debit_account_id')))
        abc = self.with_context(ctx).move_line_create(move.id, company_currency, current_currency)
        move.post()
        return move
    
    @api.model
    def create(self, vals):   
        if vals.get('name', 'New'):
            vals['name'] = self.env['ir.sequence'].next_by_code('pettycash.topup')
        return super(PettycashTopup, self).create(vals)
    
    @api.multi
    def action_confirm(self):
        self.state = 'waiting_am'
        
    @api.multi
    def action_approve_am(self):
        self.state = 'waiting_gm'
        
    @api.multi
    def action_approve_gm(self):
        self.state = 'waiting_director'
        
    @api.multi
    def action_approve_director(self):
        self.state = 'waiting_presdir'
    
    @api.multi
    def action_approve_presdir(self):
        self.state = 'approved'
    
    @api.multi
    def action_transfer(self):
        ctx = self._context.copy()
        ctx.update({
            'journal_id': self.journal_id.id,
            'debit_account_id': self.company_id.transfer_account_id.id,
            'credit_account_id': self.source_account_id.id
        })
        move = self.with_context(ctx).action_move_create()
        self.write({'transfered_move_id': move.id, 'state': 'transfered'})
        
    @api.multi
    def action_receive(self):
        ctx = self._context.copy()
        ctx.update({
            'journal_id': self.journal_id.id,
            'debit_account_id': self.source_account_id.id,
            'credit_account_id': self.company_id.transfer_account_id.id
        })
        move = self.with_context(ctx).action_move_create()
        self.write({'received_move_id': move.id, 'state': 'received'})
    
    @api.multi
    def action_release(self):
        ctx = self._context.copy()
        ctx.update({
            'journal_id': self.journal_id.id,
            'debit_account_id': self.pettycash_account_id.id,
            'credit_account_id': self.source_account_id.id
        })
        move = self.with_context(ctx).action_move_create()
        self.write({'released_move_id': move.id, 'state': 'released'})
    
    @api.multi
    def action_cancel(self):
        self.state = 'cancelled'
    
    @api.multi
    def action_draft(self):
        self.state = 'draft'
    
    @api.multi
    def unlink(self):
        for rec in self:
            if rec.state not in ('draft', 'cancel'):
                raise UserError(_('In order to delete pettycash, you must cancel first !'))   
        return super(PettycashTopup, self).unlink()             
        
