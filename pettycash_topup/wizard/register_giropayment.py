from odoo import models, fields, api, _
from odoo.exceptions import UserError
from odoo.tools.float_utils import float_is_zero
from _collections import defaultdict


MAP_INVOICE_TYPE_PAYMENT_SIGN = {
    'out_invoice': 1,
    'in_refund': 1,
    'in_invoice':-1,
    'out_refund':-1,
}

class account_register_giropayments(models.TransientModel):
    _name = "account.register.giropayments"
    _description = 'Wizard Register Giro'
    
    @api.multi
    @api.depends('partner_id', 'bank_account')
    def _warning(self):
        for wiz in self:
            if wiz.partner_id and wiz.bank_account and \
            (wiz.bank_account not in [wiz.partner_id.bank1_no, wiz.partner_id.bank2_no] or \
             wiz.bank_id.id not in [wiz.partner_id.bank1_id.id, wiz.partner_id.bank2_id.id]):
                wiz.warning = 'No Rekening yang diinput berbeda dengan yang terdaftar' 
    
    warning = fields.Char('Warning', compute='_warning')
    dir_type = fields.Selection([
        ('in', 'Penerimaan'),
        ('out', 'Pengeluaran')
    ], string='Tipe', default='in')
    partner_id = fields.Many2one('res.partner', 'Partner')
    amount = fields.Monetary('Jumlah', default=0)
    company_id = fields.Many2one('res.company', 'Company', default=lambda self:self.env.user.company_id, required=True)
    currency_id = fields.Many2one(related='company_id.currency_id')
    name = fields.Char('No. Giro')
    bank_id = fields.Many2one('res.bank', 'Bank')
    bank_account = fields.Char('Bank Account')
    date = fields.Date('Date')
    date_due = fields.Date('Due Date')
    communication = fields.Char('Memo')
    journal_id = fields.Many2one('account.journal', 'Bank Disetor')
    
    @api.multi
    def settlement(self):
        giro = self.env['account.giropayment'].browse(self._context.get('active_ids'))
        giro.write({
            'date_reconciled' : self.date,
            'state' : 'done'
        })
        giro.invoice_ids.write({'running_giro_id': False})
        giro.create_post_journal(self.date)
