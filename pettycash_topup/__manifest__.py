# -*- coding: utf-8 -*-
{
    'name': 'Pettycash Top UP',
    'version': '11.01',
    'summary': 'Pettycash Top UP',
    'description': 'Pettycash Top UP',
    'category': 'Custom',
    'author': 'PT. VISI',
    'website': "https://www.visi.com",
    'company': 'PT. VISI',
    'depends': [
                'account_payment_request',
                'account_accountant'
                ],
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'views/pettycash_topup_view.xml',
    ],
    'images': [],
    'license': 'AGPL-3',
    'installable': True,
    'auto_install': False,
    'application': True,
}
