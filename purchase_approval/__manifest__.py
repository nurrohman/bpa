# -*- coding: utf-8 -*-
# © 2004-2009 Tiny SPRL (<http://tiny.be>).
# © 2014-2017 Tecnativa - Pedro M. Baeza
# © 2016 ACSONE SA/NV (<http://acsone.eu>)
# License AGPL-3 - See http://www.gnu.org/licenses/agpl-3.0.html
{
    "name": "Purchase Approval",
    "author": "PT.VISI",
    "version": "11.0.1.0.0",
    "category": "Custom",
    "depends": ["bpa"],
    "data": [
        "security/purchase_security.xml",
        "views/purchase_order_view.xml",
        'views/material_purchase_requisition_view.xml',
        "views/account_invoice_view.xml",
    ],
    "license": 'AGPL-3',
    'installable': True,
}
