# -*- coding: utf-8 -*-
# Copyright 2004-2009 Tiny SPRL (<http://tiny.be>).
# Copyright 2016 ACSONE SA/NV (<http://acsone.eu>)
# Copyright 2015-2017 Tecnativa - Pedro M. Baeza
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

from odoo import api, fields, models, _
import odoo.addons.decimal_precision as dp


class Company(models.Model):
    _inherit = 'res.company'

    po_double_validation_max_amount = fields.Monetary(string='Double validation max amount', default=100000000,
        help="Maximum amount for which a double validation is required")
    
class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'
    
    po_double_validation_max_amount = fields.Monetary(related='company_id.po_double_validation_max_amount', string="Maximum Amount", currency_field='company_currency_id')

class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'
    
    state = fields.Selection([
        ('draft', 'RFQ'),
        ('sent', 'RFQ Sent'),
        ('to approve', 'To Dept Head Approve'),
        ('to gm approve', 'To GM Approve'),
        ('purchase', 'Purchase Order'),
        ('done', 'Locked'),
        ('cancel', 'Cancelled')
        ], string='Status', readonly=True, index=True, copy=False, default='draft', track_visibility='onchange')
    
    @api.multi
    def button_confirm(self):
        for order in self:
            if order.state not in ['draft', 'sent']:
                continue
            order.write({'state': 'to approve'})
        return True
    
    @api.multi
    def button_approve_dept_head(self):
        for order in self:
            order._add_supplier_to_product()
            if order.amount_total >= self.env.user.company_id.currency_id.compute(order.company_id.po_double_validation_amount, order.currency_id):
                order.write({'state': 'to gm approve'})
            else:
                order.button_approve()
    
                
                
            