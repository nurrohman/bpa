from odoo import api, fields, models

class MasterType(models.Model):
    _name = 'master.type'
    name = fields.Char('Type',required=True)