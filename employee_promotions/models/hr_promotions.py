# -*- coding: utf-8 -*-
import logging
import time
from datetime import datetime
from datetime import time as datetime_time
from dateutil import relativedelta

import babel

from odoo import api, fields, models, tools, _
from odoo.addons import decimal_precision as dp
from odoo.exceptions import UserError, ValidationError

_logger = logging.getLogger(__name__)

class HrPromotions(models.Model):
    _name = 'hr.promotion'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'HR Promotion'
    _rec_name = 'employee_id'


    PROMOTION_STATES = [
        ('new', 'To Start'),
        ('confirm_hrd', 'To Confirm HRD'),
        ('approve_direksi', 'To Approve Direksi'),
        ('to_promote', 'To Promote'),
        ('done', "Promoted"),
        ('cancel', "Canceled")
    ]

    def _compute_selection(self):
        all_employee = self.env['hr.employee'].search([])
        selection_list = []
        for employee in all_employee:  
            selection_list.append((employee.id,employee.name))   
        return selection_list

    # @api.onchange('nik', 'employee_id')
    @api.onchange('namanya')
    def _onchange_nik(self):
        if self.namanya != False:
            self.employee_id = self.env['hr.employee'].search([('id','=', self.namanya)])

    nik = fields.Char(string='NIK', compute='_onchange_employee')

    @api.onchange('employee_id')
    def _onchange_employee(self):
        if self.employee_id.id != False:
            self.nik = self.employee_id.barcode
            hr_employee = self.env['hr.employee'].search([('id', '=', self.employee_id.id)])
            id_employee = hr_employee.id
            # barcode = hr_employee.barcode
            self.namanya = id_employee

    start = fields.Date('Start Date')
    end = fields.Date('End Date')

    # def _get_tipe_text(self):
    #     qq=0
        
    # teks_tipe_invisible = fields.Char(compute="_get_tipe_text")

    # master_type_id = fields.Many2one('master.type')
    master_type_id = fields.Selection([
        ('kontrak','Contract'),
        ('percobaan','Trial'),
        ('tetap','Permanent')
    ])

    namanya = fields.Selection('_compute_selection',string="NIK")
    ref = fields.Char('Reference No.')
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id)
    color = fields.Integer(string='Color Index', help='This color will be used in the kanban view.')
    employee_id = fields.Many2one('hr.employee', required=True, string='Employee', index=True)
    department_id = fields.Many2one('hr.department', related='employee_id.department_id', string='Department', store=True)
    # status_id = fields.Many2one('hr.employee', related='employee_id.status_id', string='Status',store=True)
    # status_idnya = fields.Char(string='Nick name', related='employee_id.status_id')


    # status_id = fields.Many2one('hr.employee', related='employee_id.status_id', string='Status')
    job_id = fields.Many2one('hr.job', related='employee_id.job_id', string='Job Position',store=True)
    coach_id = fields.Many2one('hr.employee', related='employee_id.coach_id', string='Coach',store=True)
    manager_id = fields.Many2one('hr.employee', related='employee_id.parent_id', string='Manager',store=True)
    # state = fields.Selection(PROMOTION_STATES, string='Status', track_visibility='onchange', required=True,readonly=True, copy=False, default='new', index=True)

    state = fields.Selection(PROMOTION_STATES, 
        string='Status', 
        required=True,
        readonly=True ,
        default='new',
        group_expand='_expand_states'
    )

    def _expand_states(self, states, domain, order):
        # return [key for key, val in type(self).staging_applicant.selection]
        # hidden state tertentu qq=0	        
        return [key for key, val in type(self).state.selection]

    meeting_id = fields.Many2one('calendar.event', string='Meeting')
    hr_contract_id = fields.Many2one('hr.contract',string="Contract")
    hr_struct_id = fields.Many2one('hr.payroll.structure', related='hr_contract_id.struct_id', string="Salary Structure")
    date_promotion = fields.Datetime(string='Payment Date', required=True, default=lambda *a: fields.Datetime.now())
    new_job_id = fields.Many2one('hr.job', string='Job Position',store=True)
    new_department_id = fields.Many2one('hr.department', string='Department', store=True)
    new_coach_id = fields.Many2one('hr.employee', string='Superior',store=True)
    new_manager_id = fields.Many2one('hr.employee', string='Manager', store=True)
    new_meeting_id = fields.Many2one('calendar.event', string='Meeting')
    new_hr_contract_id = fields.Many2one('hr.contract', string="Contract")
    new_hr_struct_id = fields.Many2one('hr.payroll.structure', related='new_hr_contract_id.struct_id',string="Salary Structure")
    image = fields.Binary(
        "Medium-sized photo", attachment=True,related='employee_id.image',
        help="Medium-sized photo of the employee. It is automatically "
             "resized as a 128x128px image, with aspect ratio preserved. "
             "Use this field in form views or some kanban views.")
    note = fields.Text('Manager Comments')

    @api.onchange('employee_id')
    def onchange_employee(self):
        if self.employee_id:
            for line in self:
                contract_id = self.env['hr.contract'].search([('employee_id','=',line.employee_id.id)], limit=1)
                for contract in contract_id:
                    line.hr_contract_id = contract.id

    @api.model
    def create(self, vals):
        # Add the followers at creation, so they can be notified
        employee_id = vals.get('employee_id')
        if vals.get('employee_id'):
            employee = self.env['hr.employee'].browse(vals['employee_id'])
            users = self._get_users_to_subscribe(employee=employee) - self.env.user
            vals['message_follower_ids'] = []
            for partner in users.mapped('partner_id'):
                vals['message_follower_ids'] += self.env['mail.followers']._add_follower_command(self._name, [], {partner.id: None}, {})[0]
        promotion = super(HrPromotions, self).create(vals)
        employee = self.env['hr.employee'].sudo().search([('id', '=', employee_id)])
        for emp in employee:
            managers = []
            managers.append(emp.parent_id.user_id.partner_id)
            users = self.env['res.users'].sudo().search([])
            for user in users:
                if user.has_group('employee_promotion.group_inteslar_hr_promotion_manager'):
                    manager_employee = self.env['hr.employee'].sudo().search([('user_id', '=', user.id)])
                    for employeem in manager_employee:
                        promotion.add_follower(employeem.id)
                        managers.append(employeem.user_id.partner_id.id)
        return promotion

    @api.multi
    def button_start_promotions(self):
        self.write({'state':'confirm_hrd'})

    # @api.multi
    # def button_refused(self):
    #     self.write({'state':'cancel'})

    @api.multi
    def button_confirm_hrd(self):
        self.write({'state':'approve_direksi'})    

    @api.multi
    def button_approve_direksi(self):
        self.write({'state':'to_promote'})               

    @api.multi
    def _get_users_to_subscribe(self, employee=False):
        users = self.env['res.users']
        employee = employee or self.employee_id
        if employee.user_id:
            users |= employee.user_id
        if employee.parent_id:
            users |= employee.parent_id.user_id
        if employee.department_id and employee.department_id.manager_id and employee.parent_id != employee.department_id.manager_id:
            users |= employee.department_id.manager_id.user_id
        return users

    # @api.model
    # def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
    #     """ Override read_group to always display all states and order them appropriatly. """
    #     if groupby and groupby[0] == "state":
    #         states = [('new', _('To Start')), ('pending', _('Decisions Pending')), ('done', _('Promoted')), ('cancel', _('Cancelled'))]
    #         read_group_all_states = [{
    #             '__context': {'group_by': groupby[1:]},
    #             '__domain': domain + [('state', '=', state_value)],
    #             'state': state_value,
    #             'state_count': 0,
    #         } for state_value, state_name in states]
    #         # Get standard results
    #         read_group_res = super(HrPromotions, self).read_group(domain, fields, groupby, offset=offset, limit=limit, orderby=orderby)
    #         # Update standard results with default results
    #         result = []
    #         for state_value, state_name in states:
    #             res = [x for x in read_group_res if x['state'] == state_value]
    #             if not res:
    #                 res = [x for x in read_group_all_states if x['state'] == state_value]
    #             res[0]['state'] = state_value
    #             if res[0]['state'][0] == 'done' or res[0]['state'][0] == 'cancel':
    #                 res[0]['__fold'] = True
    #             result.append(res[0])
    #         return result
    #     else:
    #         return super(HrPromotions, self).read_group(domain, fields, groupby, offset=offset, limit=limit, orderby=orderby)

    @api.multi
    def add_follower(self, employee_id):
        employee = self.env['hr.employee'].browse(employee_id)
        if employee.user_id:
            self.message_subscribe_users(user_ids=employee.user_id.ids)

    @api.multi
    def button_done_promotions(self):
        history_pool = self.env['promotion.history']
        payslips = self.env['hr.payslip'].search([('employee_id','=', self.employee_id.id)], order="date_to desc, id desc", limit=1)
        if payslips:
            for payslip in payslips:
                val =[(0, 0, {'employee_id': self.employee_id.id,
                         'date_promotion': self.date_promotion,
                         'job_position':  self.employee_id.job_id.name,
                         'department': self.employee_id.department_id.name,
                         'coach': self.employee_id.coach_id.name,
                         'manager': self.employee_id.parent_id.name,
                         'payslip_id': payslip.id})]
                _logger.info("WIth Payslip")
                _logger.info(val)
        else:
            val =[(0, 0, {'employee_id': self.employee_id.id,
                   'date_promotion': self.date_promotion,
                   'job_position': self.employee_id.job_id.name,
                   'department': self.employee_id.department_id.name,
                   'coach': self.employee_id.coach_id.name,
                   'manager': self.employee_id.parent_id.name,
                   'payslip_id': ''})]
            _logger.info("WIthout Payslip")
            _logger.info(val)

        history_pro = {'promotion_history_id': val}

        self.employee_id.write(history_pro)

        self.employee_id.department_id = self.new_department_id
        self.employee_id.job_id = self.new_job_id
        self.employee_id.coach_id = self.new_coach_id
        self.employee_id.parent_id = self.new_manager_id


        if self.new_manager_id:
            self.add_follower(self.employee_id.parent_id.id)

        body = (_("%s has been promoted to %s.") % (self.employee_id.name, self.employee_id.job_id.name))

        self.message_post(body=body, message_type='comment',
                subtype='mail.mt_comment', author_id=self.env.user.partner_id.id, date=datetime.today(), partner_ids=self.message_follower_ids)

        self.write({'state':'done'})

    @api.multi
    def button_cancel_promotions(self):
        self.write({'state':'cancel'})

class Employee(models.Model):
    _inherit = 'hr.employee'

    promotion_history_id = fields.One2many('promotion.history', 'employee_id', 'Promotion History')

class PromotionHistory(models.Model):
    _name = 'promotion.history'
    _description = 'Promotion History'

    employee_id = fields.Many2one('hr.employee', 'Employee')
    date_promotion = fields.Date('Promotion Date')
    job_position = fields.Char('Job Position')
    department = fields.Char('Department')
    coach = fields.Char('Coach')
    manager = fields.Char('Manager')
    payslip_id = fields.Many2one('hr.payslip','Last Payslip')