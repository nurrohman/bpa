# -*- coding: utf-8 -*-
{
    'name': 'Employee Promotion',
    'version': '1.0',
    'category': 'HR',
    'price': 35.00,
    'currency': 'EUR',
    'license': 'OPL-1',
    'website': 'https://www.inteslar.com',
    'images': ['static/description/main_screenshot.png'],
    'author': 'inteslar',
    'summary': 'Employee Promotion By Inteslar',
    'description': """
    Key Features
    ------------
    * Employee Promotions
                    """,
    'depends': ['base','hr','hr_contract','hr_payroll'],
    'data': [
        'security/hr_promotion_security.xml',
        'security/ir.model.access.csv',
        'views/hr_promotions_view.xml',
    ],
    'installable': True,
    'auto_install': False,
}