# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import UserError


class HrAppraisalEmployees(models.TransientModel):
    _name = 'hr.appraisal.employees'
    _description = 'Generate appraisal for all selected employees'

    employee_ids = fields.Many2many('hr.employee', 'hr_employee_appraisal_group_rel', 'payslip_id', 'employee_id', 'Employees')

    @api.multi
    def compute_sheet(self):
        appraisal = self.env['inteslar.hr.appraisal']
        [data] = self.read()
        active_id = self.env.context.get('active_id')
        if active_id:
            [run_data] = self.env['hr.appraisal.run'].browse(active_id).read(['date_close'])
        to_date = run_data.get('date_close')
        if not data['employee_ids']:
            raise UserError(_("You must select employee(s) to generate appraisal(s)."))
        for employee in self.env['hr.employee'].browse(data['employee_ids']):
            res = {
                'employee_id': employee.id,
                'date_close' : to_date,
                'appraisal_run_id' : active_id
            }
            appraisal += self.env['inteslar.hr.appraisal'].create(res)
        for appr in appraisal:
            appr.get_line_manager()
        return {'type': 'ir.actions.act_window_close'}
