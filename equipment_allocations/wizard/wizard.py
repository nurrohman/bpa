# -*- coding: utf-8 -*-
##########################################################################
# 2010-2017 Webkul.
#
# NOTICE OF LICENSE
#
# All right is reserved,
# Please go through this link for complete license : https://store.webkul.com/license.html
#
# DISCLAIMER
#
# Do not edit or add to this file if you wish to upgrade this module to newer
# versions in the future. If you wish to customize this module for your
# needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
#
# @Author        : Webkul Software Pvt. Ltd. (<support@webkul.com>)
# @Copyright (c) : 2010-2017 Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
# @License       : https://store.webkul.com/license.html
#
##########################################################################


import logging

from odoo import api, fields, models
from odoo import tools, _
from odoo.exceptions import ValidationError, UserError

_logger = logging.getLogger(__name__)


class ReasonWizard(models.TransientModel):

    _name = "reason.wizard"
    _description = "Wizard for getting reason from use on cancel an allocation request."

    reason = fields.Text(string="Reason", required=True)

    @api.multi
    def get_reason(self):
        record = self.env['allocation.request'].browse(
            self._context.get('active_id'))
        ctx = self._context.copy()
        ctx.update({'cancel': True,'reason':self.reason})
        record.with_context(ctx).write({'state': 'cancel'})
        if record.type == "permanent":
            record.equipment_id.sudo().equipment_assign_to = "other"
            record.equipment_id.sudo().owner_user_id = False
        # record.message_post(
        #         body=self.reason,
        #         subject="Your Request for %s has been cancelled"%record.name,
        #         subtype='mail.mt_comment',
        #     )
