# -*- coding: utf-8 -*-
##########################################################################
# 2010-2017 Webkul.
#
# NOTICE OF LICENSE
#
# All right is reserved,
# Please go through this link for complete license : https://store.webkul.com/license.html
#
# DISCLAIMER
#
# Do not edit or add to this file if you wish to upgrade this module to newer
# versions in the future. If you wish to customize this module for your
# needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
#
# @Author        : Webkul Software Pvt. Ltd. (<support@webkul.com>)
# @Copyright (c) : 2010-2017 Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
# @License       : https://store.webkul.com/license.html
#
##########################################################################


import logging

from odoo import api, fields, models
from odoo import tools, _
from odoo.exceptions import ValidationError, UserError

_logger = logging.getLogger(__name__)


class RequestAllocatedWizard(models.TransientModel):

    _name = "request.allocated.wizard"
    _description = "Wizard for allocated equipment to other user"

    message = fields.Text(string="Message")

    @api.multi
    def approve_equipment(self):
        record = self.env['allocation.request'].browse(
            self._context.get('active_id'))
        ctx = self._context.copy()
        ctx.update({'approved': True})
        if record.state == "new":
            request_id = self.env['allocation.request'].search(
                [('equipment_id', '=', record.equipment_id.id),
                 ('state', 'in', ['approved'])], limit=1)
            if request_id:
                vals = {
                    'reason': "Approved for another request so cancelled it."}
                wizard = self.env['reason.wizard'].create(vals)
                context = ctx.copy()
                context.update({'active_id': request_id.id})
                wizard.with_context(context).get_reason()
            else:
                request_id = self.env['allocation.request'].search(
                    [('equipment_id', '=', record.equipment_id.id),
                     ('state', 'in', ['allocated'])], limit=1)
                if request_id:
                    request_id.set_returned()

        record.with_context(ctx).write({'state': 'approved'})
