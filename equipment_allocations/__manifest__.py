# -*- coding: utf-8 -*-
##########################################################################
# 2010-2017 Webkul.
#
# NOTICE OF LICENSE
#
# All right is reserved,
# Please go through this link for complete license : https://store.webkul.com/license.html
#
# DISCLAIMER
#
# Do not edit or add to this file if you wish to upgrade this module to newer
# versions in the future. If you wish to customize this module for your
# needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
#
# @Author        : Webkul Software Pvt. Ltd. (<support@webkul.com>)
# @Copyright (c) : 2010-2017 Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
# @License       : https://store.webkul.com/license.html
#
##########################################################################

{
    "name": "Equipment Allocations",
    "summary":  "This module helps to manage Equipments Allocation Requests.",
    "category":  "Human Resources",
    "version":  "1.0.0",
    "sequence":  1,
    "author":  "Webkul Software Pvt. Ltd.",
    "license":  "Other proprietary",
    'website': 'https://store.webkul.com/Odoo-Equipment-Allocations.html',
    "description": """
        This module helps to manage Equipments Allocation Requests.
    """,
    "live_test_url": 'http://odoodemo.webkul.com/?module=equipment_allocations&version=11.0',
    'depends': ['stock', 'maintenance_equipment'],

    'data': [
        'edi/mail_template.xml',
        'security/maintenance_security.xml',
        'security/ir.model.access.csv',
        'wizard/wizard_view.xml',
        'wizard/allocation_wizard_view.xml',
        'wizard/replace_equipment_view.xml',
        'views/wk_maintenance.xml',
    ],
    'demo': [
        'data/demo.xml',
    ],
    "installable": True,
    "application": True,
    "auto_install": False,
    "price": 45,
    "currency": 'EUR',
    "pre_init_hook": "pre_init_check",
    "images": ['static/description/Banner.png'],

}

