# -*- coding: utf-8 -*-
##########################################################################
# 2010-2017 Webkul.
#
# NOTICE OF LICENSE
#
# All right is reserved,
# Please go through this link for complete license : https://store.webkul.com/license.html
#
# DISCLAIMER
#
# Do not edit or add to this file if you wish to upgrade this module to newer
# versions in the future. If you wish to customize this module for your
# needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
#
# @Author        : Webkul Software Pvt. Ltd. (<support@webkul.com>)
# @Copyright (c) : 2010-2017 Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
# @License       : https://store.webkul.com/license.html
#
##########################################################################

import logging
import calendar
import pytz

from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from odoo import api, fields, models
from odoo import tools, _
from odoo.exceptions import ValidationError, UserError

_logger = logging.getLogger(__name__)


class MaintenanceEquipment(models.Model):
    _inherit = 'maintenance.equipment'

    @api.one
    @api.depends('allocation_ids.state')
    def _compute_allocation_count(self):
        for obj in self:
            obj.allocation_count = len(obj.allocation_ids)
            count = 0
            for allocation in obj.allocation_ids:
                if allocation.state in ['allocated']:
                    count += 1
            obj.allocation_open_count = count

    @api.depends('warranty_period', 'purchase_date')
    def get_warranty_last_date(self):
        for obj in self:
            if obj.purchase_date:
                if obj.warranty_period:
                    obj.warranty = datetime.strptime(
                        obj.purchase_date, '%Y-%m-%d') + relativedelta(months=obj.warranty_period, days=-1)
                else:
                    obj.warranty = obj.purchase_date
            else:
                obj.warranty = False

    @api.multi
    def name_get(self):
        result = []
        for record in self:
            if record.name and record.serial_no and record.partner_ref:
                result.append(
                    (record.id, record.name + '/' + record.serial_no + '/' + record.partner_ref))
            elif record.name and record.partner_ref and not record.serial_no:
                result.append(
                    (record.id, record.name + '/' + record.partner_ref))
            elif record.name and record.serial_no and not record.partner_ref:
                result.append(
                    (record.id, record.name + '/' + record.serial_no))
            elif record.name and not record.serial_no:
                result.append((record.id, record.name))
        return result

    product_id = fields.Many2one(
        'product.product', string='Main Product', domain="[('type', '=', 'product')]")
    allocation_ids = fields.One2many('allocation.request', 'equipment_id')
    allocation_count = fields.Integer(
        compute='_compute_allocation_count', string="Allocation")
    allocation_open_count = fields.Integer(
        compute='_compute_allocation_count', string="Current Allocation")
    state = fields.Selection(
        [('available', 'Available'), ('not-available', 'Not Available')],
        string="State", default="available")
    owner_user_id = fields.Many2one(
        'res.users', string='Owner', track_visibility='onchange',
        readonly=False)
    purchase_date = fields.Date(
        string="Purchase Date", track_visibility='onchange')
    warranty_period = fields.Integer(string="Warranty Period")
    warranty = fields.Date(
        'Warranty', compute="get_warranty_last_date", store=True)

    @api.onchange('equipment_assign_to')
    def _onchange_equipment_assign_to(self):
        if self.equipment_assign_to == 'employee':
            self.department_id = False
        if self.equipment_assign_to == 'department':
            self.owner_user_id = False
        if self.equipment_assign_to == 'other':
            self.department_id = False
            self.owner_user_id = False
        self.assign_date = fields.Date.context_today(self)

    @api.one
    @api.depends('employee_id', 'department_id', 'equipment_assign_to')
    def _compute_owner(self):
        self.owner_user_id = self.env.user.id
        if self.equipment_assign_to == 'employee':
            self.owner_user_id = self.employee_id.user_id.id
        elif self.equipment_assign_to == 'department':
            self.owner_user_id = self.department_id.sudo().manager_id.user_id.id
        elif self.equipment_assign_to == 'other':
            self.owner_user_id = False

    @api.model
    def create(self, vals):
        if vals.get('warranty_period') and vals.get('warranty_period') < 0:
            raise UserError(
                _('Warranty Period of an Equipment must be greater than or equal to 0.'))
        return super(MaintenanceEquipment, self).create(vals)

    @api.multi
    def write(self, vals):
        if vals.get('warranty_period') and vals.get('warranty_period') < 0:
            raise UserError(
                _('Warranty Period of an Equipment must be greater than or equal to 0.'))
        return super(MaintenanceEquipment, self).write(vals)


class AllocationRequest(models.Model):

    _name = "allocation.request"
    _inherit = ['mail.thread']
    _description = 'Allocation Requests'
    _order = "id desc"

    @api.multi
    def unlink(self):
        for record_obj in self:
            if record_obj.state != 'cancel':
                raise UserError(
                    _("You can ONLY delete the Request(s) that are in cancelled state !!!"))
        return super(AllocationRequest, self).unlink()

    def get_sign_in_time(self, sign_in):
        utc = pytz.utc
        current_timezone = 'Asia/Kolkata'
        local_timezone = pytz.timezone(current_timezone)
        now = datetime.today().replace(tzinfo=utc).astimezone(
            local_timezone).replace(tzinfo=None)
        dif = now - datetime.now()
        time_start = datetime.strptime(
            sign_in, '%Y-%m-%d %H:%M:%S') + dif
        return time_start + timedelta(seconds=1)

    def get_date(self, sign_in):
        return self.get_sign_in_time(sign_in).strftime('%d-%m-%Y')

    def get_time(self, sign_in):
        return self.get_sign_in_time(sign_in).strftime('%H:%M:%S')

    @api.model
    def get_default_department(self):
        if self.request_user_id and self.request_user_id.sudo().employee_ids:
            if self.request_user_id.sudo().employee_ids[0].department_id:
                return self.request_user_id.sudo().employee_ids[0].department_id.id
        else:
            user = self.env['res.users'].browse(self._uid)
            if user.sudo().employee_ids and user.sudo().employee_ids[0].department_id:
                return user.sudo().employee_ids[0].department_id.id
        return False

    @api.model
    def get_equipment_id_doamin(self):
        department_id = self.get_default_department()
        if not self.env.user._is_admin():
            if not department_id:
                raise UserError(
                    _("Employee Department not set. Please contact Hr Department."))
        equipment_ids = []
        if self.category_id:
            equipment_ids += self.env['maintenance.equipment'].search(
                [('category_id', '=', self.category_id.id),
                 ('department_id', '=', department_id)]).ids
            equipment_ids += self.env['maintenance.equipment'].search(
                [('category_id', '=', self.category_id.id),
                 ('owner_user_id', '=', False), ('department_id', '=', False)]).ids
        return equipment_ids

    @api.multi
    @api.depends('category_id', 'request_user_id')
    def equipment_domain(self):
        for obj in self:
            obj.equipment_ids = [(6, 0, obj.get_equipment_id_doamin())]

    @api.model
    def get_user_domain(self):
        groups = self.env['res.users'].browse(self._uid).groups_id.ids
        manager_group = self.env['ir.model.data'].get_object_reference(
            'maintenance', 'group_equipment_user')[1]
        if manager_group not in groups:
            users = []
            employees = self.env['hr.employee'].sudo().search(
                [('department_id', '=', self.get_default_department())])
            for employee in employees:
                if employee.user_id:
                    users.append(employee.user_id.id)
            return users
        return self.env["res.users"].sudo().search([('employee_ids', '!=', False)]).ids
    
    @api.one
    @api.depends('equipment_id')
    def _get_equipment_child(self):
        self.equipment_child_ids = self.equipment_id.equipment_lines

    name = fields.Char('Subjects', required=True, track_visibility='always', default='New')
    description = fields.Text('Description')
    request_date = fields.Datetime(
        'Request Date', track_visibility='onchange',
        default=fields.Datetime.now,
        help="Requested date for the Allocation.", copy=False, readonly=True,
        states={'new': [('readonly', False)]}, required=True)
    request_user_id = fields.Many2one(
        'res.users', string='Allocated To', default=lambda s: s.env.uid,
        track_visibility='onchange', copy=False, readonly=True,
        states={'new': [('readonly', False)]}, required=True, domain=lambda self: [('id', 'in', self.get_user_domain())])
    approved_by = fields.Many2one(
        'res.users', string='Approved By', track_visibility='onchange',
        copy=False)
    return_to = fields.Many2one(
        'res.users', string='Return To', track_visibility='onchange',
        copy=False)
    rejected_by = fields.Many2one(
        'res.users', string='Rejected By', track_visibility='onchange',
        copy=False)
    category_id = fields.Many2one('maintenance.equipment.category',
                                  string='Category', required=True,
                                  readonly=True,
                                  states={'new': [('readonly', False)]})
    equipment_id = fields.Many2one(
        'maintenance.equipment', string='Equipment', index=True, copy=False,
        domain="[('id', 'in', equipment_ids[0][2])]", readonly=True,
        states={'new': [('readonly', False)]})
    product_id = fields.Many2one('product.product',
                                 string='Product', readonly=True)
    equipment_ids = fields.Many2many("maintenance.equipment", 'request_id',
                                     'equipment_id',
                                     compute="equipment_domain",
                                     string="Equipments")
    state = fields.Selection([
        ('new', 'New'), ('submit', 'Submitted'),
        ('approve', 'Approved'), ('transfer', 'Transfered'),
        ('cancel', 'Cancelled'), ('reject', 'Rejected')], string='Status',
        track_visibility='onchange', default='new', copy=False)
    priority = fields.Selection(
        [('0', 'Very Low'), ('1', 'Low'), ('2', 'Normal'),
         ('3', 'High')], string='Priority', default=0)
    color = fields.Integer('Color Index')
    close_date = fields.Datetime(
        'Transfered Date', help="Date on which the equipment has been transfered.", copy=False)
    archive = fields.Boolean(
        default=False, copy=False,
        help="Set archive to true to hide the Allocation request without deleting it.")
    duration = fields.Float(
        help="Duration in hours and minutes.", copy=False)
    type = fields.Selection(
        [('on-demand', 'On-demand'), ('permanent', 'Permanent')], string="Allocation Type", default="on-demand", help="The current allocation of the equipment belongs to: \n"
        "- On-demand: Available when needed. Employee request for an equipment for a short or particular duration of time.\n"
        "- Permanent: The Equipment permanently get allocated to the employee.\n", readonly=True,
        states={'new': [('readonly', False)]}, copy=False)
    
    project_src_id = fields.Many2one('project.project', string='Source Project')
    project_dest_id = fields.Many2one('project.project', string='Destination Project')
    
    equipment_child_ids = fields.Many2many('equipment.lines', string='Equipment Childs', compute='_get_equipment_child', store=True)
    
    @api.onchange('equipment_id')
    def onchange_equipment_id(self):
        if not self.equipment_id:
            return {}
        self.project_src_id = self.equipment_id.location_id.id
        
    @api.onchange('category_id', 'request_user_id')
    def onchange_category_id(self):
        self.equipment_id = False
        equipment_ids = self.get_equipment_id_doamin()
        return {'domain': {'equipment_id': [('id', 'in', equipment_ids)]}}

    @api.model
    def create(self, vals):
        today = datetime.strftime(datetime.now(), '%Y-%m-%d')
        if not vals.get('type'):
            vals.update({'type': 'on-demand'})
        if vals.get('type') == 'on-demand':
            if vals.get('request_date') and vals.get('request_date') < today:
                raise UserError(_(
                    "Scheduled date for an equipment request must be of future."))
        vals['name'] = self.env['ir.sequence'].next_by_code('equipment.transfer') or '/'
        res = super(AllocationRequest, self).create(vals)
        res.get_equipment_id_doamin()
        res.message_subscribe_users(
            user_ids=[res.request_user_id.id])
        res.message_subscribe(
            partner_ids=res.category_id.message_partner_ids.ids)
        template = self.env['ir.model.data'].xmlid_to_object(
            'equipment_allocations.email_equipment_allocation_request')
        Template = self.env['mail.template']
        if res.type == "on-demand":
            if template:
                # template.send_mail(res.id, True)
                partner = []
                template = template.get_email_template(res.id)
                body_html = Template.with_context(template._context).render_template(
                    template.body_html, 'allocation.request', res.id)
                subject_html = Template.with_context(template._context).render_template(
                    template.subject, 'allocation.request', res.id)
                res.message_post(
                    body=body_html,
                    subject=subject_html,
                    subtype='mail.mt_comment',
                    partner_ids=res.category_id.technician_user_id.partner_id.ids + partner + res.category_id.message_partner_ids.ids,
                    message_type='comment',
                    email_from="no-reply@webkul.com"
                )
        return res

    @api.multi
    def message_get_message_notify_values(self, message, message_values):
        """ Override to avoid keeping all notified recipients of a comment.
        We avoid tracking needaction on post comments. Only emails should be
        sufficient. """
        if message.message_type == 'comment':
            return {
                'needaction_partner_ids': [],
            }
        return {}

    def check_authority(self):
        groups = self.env['res.users'].browse(self._uid).groups_id.ids
        manager_group = self.env['ir.model.data'].get_object_reference(
            'maintenance', 'group_equipment_user')[1]
        if manager_group not in groups:
            return False
        return True

    def check_allowed_equipment(self):
        msg = ""
        request_id = False
        if self.state == "new":
            request_id = self.search(
                [('equipment_id', '=', self.equipment_id.id),
                 ('state', 'in', ['approved'])], limit=1)
            if request_id:
                msg = 'This equipment is currently approved for <b>(%s)</b> request. <br/><i class="fa fa-hand-o-right text-danger" aria-hidden="true"/> <b> Note :</b> You cannot allocate the same equipment to multiple requests.<br/> So, If you want to <b>Cancel</b> the previous approved request and <b>Approve</b> the current request then click on <b>"Update Now"</b> button else click on <b>"Cancel"</b>.' % (
                    request_id.display_name)
            else:

                request_id = self.search(
                    [('equipment_id', '=', self.equipment_id.id), ('state', 'in', ['allocated'])], limit=1)
                if request_id:
                    msg = 'This equipment is currently allocated for <b>(%s)</b> request.<br/><i class="fa fa-hand-o-right text-danger" aria-hidden="true"/> <b> Note :</b> You cannot allocate the same equipment to multiple requests.<br/> So, If you want to <b>Return</b> the previous allocated request and <b>Approve</b> the current request then click on <b>"Update Now"</b> button else click on <b>"Cancel"</b>.' % (
                        request_id.display_name)
                else:
                    return True
        wizard_id = self.env['request.allocated.wizard'].create(
            {'message': msg})
        return {
            'name': "Message",
            'view_mode': 'form',
            'view_id': False,
            'view_type': 'form',
            'res_model': 'request.allocated.wizard',
            'res_id': wizard_id.id,
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'new',
            'domain': '[]',
        }
        return False

    @api.multi
    def check_equipment_availablity(self):
        if self.equipment_id.state != 'available':
            request_id = self.search(
                [('equipment_id', '=', self.equipment_id.id), ('state', 'in', ['approved', 'allocated'])])
            return request_id
        return False
        # raise UserError(_("Curently this equipment is not avaliable."))

    @api.multi
    def release_equipment(self):
        for obj in self:
            if obj.equipment_id.state == "not-available":
                obj.sudo().equipment_id.state = "available"

    @api.multi
    def allocate_resource(self):
        for obj in self:
            if obj.equipment_id.state == "available":
                obj.equipment_id.sudo().state = "not-available"

    @api.multi
    def button_submit(self):
        for obj in self:
            if not obj.equipment_id:
                raise UserError(
                    _("Firstly, please select a Equipment."))
            obj.state = 'submit'

    @api.multi
    def button_approve(self):
        for obj in self:
            obj.write({
                'approved_by': self.env.uid,
                'state': 'approve'
            })

    @api.multi
    def button_transfer(self):
        for obj in self:
            if not obj.cargo_id:
                raise UserError(_('SPB belum terdapat dalam Manifest !'))
            if obj.cargo_id.state != 'approved':
                raise UserError(_('Manifest %s belum disetujui !'))
            obj.write({
                'state': 'transfer'
            })
            obj.equipment_id.location_id = obj.project_dest_id.id

    @api.multi
    def button_draft(self):
        for obj in self:
            obj.state = 'new'

    @api.multi
    def button_reject(self):
        for obj in self:
            obj.state = 'reject'
            
    @api.multi
    def button_cancel(self):
        for obj in self:
            obj.state = 'cancel'


class ProductProduct(models.Model):
    _inherit = 'product.product'

    @api.multi
    def get_equipment_count(self):
        for obj in self:
            obj.equipment_counts = self.env['maintenance.equipment'].search_count(
                [('product_id', '=', obj.id)])

    equipment_counts = fields.Integer(compute="get_equipment_count")
