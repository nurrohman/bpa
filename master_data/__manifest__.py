# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': 'Master Data',
    'version': '1.1',
    'category': 'Custom',
    'sequence': 35,
    'summary': 'Master Data',
    'description': """
        Master Data
    """,
    'website': 'PT. VISI',
    'depends': [
        'base', 
        'sale',
        'fleet'
    ],
    'data': [
        'security/ir.model.access.csv',
        'views/partner_view.xml',
#         'views/fleet_view.xml',
        'views/job_view.xml',
        'views/ir_sequence.xml',
    ],
    'test': [],
    'installable': True,
    'auto_install': False,
    'application': True,
}
