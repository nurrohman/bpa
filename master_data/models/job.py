# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api, _
from odoo.exceptions import UserError, AccessError


class JobStall(models.Model):
    _name = 'job.stall'
    
    name = fields.Char('Name', required=True)
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id)

# class AccountAnalyticAccount(models.Model):
#     _inherit = 'account.analytic.account'
# 
#     def _compute_job_count(self):
#         for account in self:
#             account.job_count = len(account.with_context(active_test=False).job_ids)
#             
#     job_ids = fields.One2many('project.project', 'analytic_account_id', string='Jobs')
#     job_count = fields.Integer(compute='_compute_job_count', string='Job Count')

    
class JobJob(models.Model):
    _name = 'job.job'
    _order = 'order'
    _description = 'Master Job'
    
    @api.one
    @api.depends('part_ids', 'part_ids.product_id', 'part_ids.product_qty')
    def _compute_estimated_amount(self):
        self.estimated_amount = sum(self.part_ids.mapped('price_unit'))
    
    name = fields.Char('Job Name', required=True)
    code = fields.Char('Job Code')
    order = fields.Integer('Order')
    note = fields.Text('Notes')
    top = fields.Boolean('Top Jobs')
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id)
    state = fields.Selection([('active', 'Active'), ('inactive', 'Inactive')], default='active', string='Status')
#     analytic_account_id = fields.Many2one(
#         'account.analytic.account', string='Contract/Analytic',
#         help="Link this project to an analytic account if you need financial management on projects. "
#              "It enables you to connect projects with budgets, planning, cost and revenue analysis, timesheets on projects, etc.",
#         ondelete="cascade", auto_join=True)
    estimated_amount = fields.Float('Estimated Price', compute='_compute_estimated_amount', store=True)
    part_ids = fields.One2many('job.part', 'job_id', 'Jobs & Parts')
    
    _sql_constraints = [
        ('code_uniq', 'unique (code)', 'Kode job tidak boleh sama !')
    ]
    
    @api.multi
    def unlink(self):
        if not self.user_has_groups('stock.group_stock_manager'):
            raise UserError(_('Unable to delete job. Please contact Warehouse Manager or Administrator'))
        return super(JobJob, self).unlink()

    
class JobPart(models.Model):
    _name = 'job.part'
    _description = 'Job Parts'
    
    job_id = fields.Many2one('job.job', 'Job', ondelete='cascade')
    product_id = fields.Many2one('product.product', 'Product')
    name = fields.Char('Description')
    product_qty = fields.Float('Quantity', default=1)
    product_uom_id = fields.Many2one('product.uom', 'Unit of Measures')
    price_unit = fields.Float('Estimated Price')
    
    @api.onchange('product_id')
    def product_id_change(self):
        if not self.product_id:
            return
        self.name = self.product_id.name
        self.product_uom_id = self.product_id.uom_id.id
        self.price_unit = self.product_id.sale_price
        
