from odoo import fields, models, api, _
from odoo.exceptions import UserError


class ProductProduct(models.Model):
    _inherit = 'product.product'
    
    vehicle_type_ids = fields.Many2many('fleet.vehicle.type', 'vechile_type_product_rel', 'product_id', 'vehicle_type_id', string="Vehicle Types", copy=False, readonly=False)
    
