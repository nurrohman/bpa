# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api

class ResCountryState(models.Model):
    _inherit = 'res.country.state'

    city_ids = fields.One2many('res.country.city', 'state_id', 'Cities')
    
class ResCountryCity(models.Model):
    _name = 'res.country.city'
    _description = 'City (Kota)'
    
    name = fields.Char('Name')
    code = fields.Char('Code')
    state_id = fields.Many2one('res.country.state', string='Province')
    district_ids = fields.One2many('res.country.district', 'city_id', string='Districs')
    
class ResCountryDistrict(models.Model):
    _name = 'res.country.district'
    _description = 'District (Kecamatan)'
    
    city_id = fields.Many2one('res.country.city', string='City/Region')
    name = fields.Char('Name')
    code = fields.Char('Code')
    village_ids = fields.One2many('res.country.village', 'district_id', string='Villages')
    
class ResCountryVillage(models.Model):
    _name = 'res.country.village'
    _description = 'Village (Desa)'
    
    district_id = fields.Many2one('res.country.district', string='District')
    name = fields.Char('Name')
    code = fields.Char('Code')
    
class ResPartner(models.Model):
    _inherit = 'res.partner'

    nik = fields.Char('NIK')
    npwp = fields.Char('NPWP', default='000000000000000')
    gender = fields.Selection([('m', 'Male'), ('f', 'Female')], default='m', string='Gender')
    city_id = fields.Many2one('res.country.city', 'City')
    city = fields.Char(related='city_id.name', string='City', store=True)
    district_id = fields.Many2one('res.country.district', 'District')
    village_id = fields.Many2one('res.country.village', 'Village')
    birth_date = fields.Date('Birthdate')
    affiliate = fields.Boolean('Is a Affiliate')
    vehicle_ids = fields.One2many('fleet.vehicle', 'partner_id', string='Vehicles')
    
    @api.onchange('village_id')
    def change_village_id(self):
        if not self.village_id:
            return
        self.district_id = self.village_id.district_id.id
        
    @api.onchange('district_id')
    def change_district_id(self):
        if not self.district_id:
            return
        self.city_id = self.district_id.city_id.id
        
    @api.onchange('city_id')
    def change_city_id(self):
        if not self.city_id:
            return
        self.state_id = self.city_id.state_id.id
        
    @api.onchange('state_id')
    def change_state_id(self):
        if not self.state_id:
            return
        self.country_id = self.state_id.country_id.id
        
        
        
        