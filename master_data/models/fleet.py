# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api, _
from odoo.exceptions import UserError


class FleetVehicleModelBrand(models.Model):
    _inherit = 'fleet.vehicle.model.brand'

    code = fields.Char('Code', required=False)
    
    @api.model
    def create(self, vals):
        vals['code'] = self.env['ir.sequence'].next_by_code('fleet.vehicle.brand')
        return super(FleetVehicleModelBrand, self).create(vals)

    
class FleetVehicleColor(models.Model):
    _name = 'fleet.vehicle.color'

    name = fields.Char('Color', required=True)

    
class FleetVehicleYear(models.Model):
    _name = 'fleet.vehicle.year'

    name = fields.Char('Year', required=True)

    
class FleetVehicleModel(models.Model):
    _inherit = 'fleet.vehicle.model'

    code = fields.Char('Code', required=False)
    
    @api.model
    def create(self, vals):
        vals['code'] = self.env['ir.sequence'].next_by_code('fleet.vehicle.model')
        return super(FleetVehicleModel, self).create(vals)


class FleetVehicleTypeID(models.Model):
    _name = 'fleet.vehicle.type_id'
    
    code = fields.Char('Code', required=False)
    name = fields.Char('Name', required=False)
    
    @api.model
    def create(self, vals):
        vals['code'] = self.env['ir.sequence'].next_by_code('fleet.vehicle.type_id')
        return super(FleetVehicleTypeID, self).create(vals)


class FleetVehicleType(models.Model):
    _name = 'fleet.vehicle.type'

    @api.multi
    @api.depends('machine_id', 'front_brake_id', 'rear_brake_id', 'brake_tech_ids', 'fuel_type_id', 'fuel_system_id', 'kopling_id',
                 'drivetrain_id', 'safety_system_id', 'steering_system_id', 'power_steering_id', 'front_suspension_id',
                 'rear_suspension_id', 'front_shockabsorber_id', 'rear_shockabsorber_id')
    def _jobs(self):
        for type in self:
            type.job_ids |= type.machine_id.job_ids + type.front_brake_id.job_ids + type.rear_brake_id.job_ids + \
                type.brake_tech_ids.mapped('job_ids') + type.fuel_type_id.job_ids + type.fuel_system_id.job_ids + \
                type.kopling_id.job_ids + type.drivetrain_id.job_ids + type.safety_system_id.job_ids + \
                type.steering_system_id.job_ids + type.power_steering_id.job_ids + type.front_suspension_id.job_ids + \
                type.rear_suspension_id.job_ids + type.front_shockabsorber_id.job_ids + type.rear_shockabsorber_id.job_ids
    
    @api.multi
    @api.depends('vehicle_type_id', 'model_id', 'model_id.brand_id', 'year_id')
    def _get_vehicle_code(self):
        for type in self:
            if type.model_id and type.year_id:
                type.name = type.model_id.brand_id.code + type.model_id.code + type.year_id.name[-2:] + type.vehicle_type_id.code + '0'
    
    @api.multi
    @api.depends('model_name', 'brand_name', 'vehicle_type_id.name', 'year_id.name')
    def _complete_name(self):
        for type in self:
            type.complete_name = "%s %s %s %s" % (type.model_name, type.brand_name, type.vehicle_type_id.name, type.year_id.name)
            
    image = fields.Binary("Medium-sized image", attachment=True,
        help="Medium-sized logo of the brand. It is automatically "
             "resized as a 128x128px image, with aspect ratio preserved. "
             "Use this field in form views or some kanban views.")
    vehicle_type_id = fields.Many2one('fleet.vehicle.type_id', 'Type ID', required=True)
    name = fields.Char('Vehicle Code', compute='_get_vehicle_code', store=True)
    model_id = fields.Many2one('fleet.vehicle.model', 'Model', required=True)
    model_name = fields.Char(related='model_id.name', string='Model', store=True)
    brand_name = fields.Char(related='model_id.brand_id.name', string='Brand', store=True)
    category_id = fields.Many2one('fleet.vehicle.category', 'Category')
    grade = fields.Selection([('a', 'Grade A'), ('b', 'Grade B'), ('c', 'Grade C'), ('d', 'Grade D'), ('e', 'Grade E')], string='Grade')
    
    machine_id = fields.Many2one('fleet.vehicle.feature', 'Machine')
    front_brake_id = fields.Many2one('fleet.vehicle.feature', string='Front Brake')
    rear_brake_id = fields.Many2one('fleet.vehicle.feature', string='Rear Brake')
    brake_tech_ids = fields.Many2many('fleet.vehicle.feature', string='Brake Tech')
    
    fuel_type_id = fields.Many2one('fleet.vehicle.feature', string='Fuel Type')
    fuel_system_id = fields.Many2one('fleet.vehicle.feature', string='Fuel System')
    
    kopling_id = fields.Many2one('fleet.vehicle.feature', string='Kopling')
    drivetrain_id = fields.Many2one('fleet.vehicle.feature', string='Drivetrain')
    
    safety_system_id = fields.Many2one('fleet.vehicle.feature', string='Safety System')
    steering_system_id = fields.Many2one('fleet.vehicle.feature', string='Steering System')
    power_steering_id = fields.Many2one('fleet.vehicle.feature', string='Power Steering')
    
    front_suspension_id = fields.Many2one('fleet.vehicle.feature', 'Front Suspension')
    rear_suspension_id = fields.Many2one('fleet.vehicle.feature', 'Rear Suspension')
    
    front_shockabsorber_id = fields.Many2one('fleet.vehicle.feature', 'Front Shockabsorber')
    rear_shockabsorber_id = fields.Many2one('fleet.vehicle.feature', 'Rear Shockabsorber')
    production_year = fields.Char('Year of Made', help="Deprecated, change to year_id")
    year_id = fields.Many2one('fleet.vehicle.year', 'Year of Made', required=True)
    color_id = fields.Many2one('fleet.vehicle.color', 'Color')
    
    job_ids = fields.Many2many('job.job', string='Related Jobs', compute='_jobs', store=True)
#     complete_name = fields.Char('Name Search', compute='_complete_name', store=True)
    product_ids = fields.Many2many('product.product', 'vechile_type_product_rel', 'vehicle_type_id', 'product_id', string="Products", copy=False, readonly=False)
    
    _sql_constraints = [
        ('name', 'UNIQUE (name)', 'Duplicate vehicle code !')
    ]
    
    @api.multi
    @api.depends('name', 'model_id')
    def name_get(self):
        res = []
        for record in self:
            name = record.name
            if record.model_id.name and record.model_id.brand_id.name:
                name = '%s / %s / %s' % (name, record.model_id.name, record.model_id.brand_id.name)
            res.append((record.id, name))
        return res
    
    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        args = args or []
        recs = self.browse()
        if name:
            recs = self.search([('complete_name', operator, name)] + args, limit=limit)
        if not recs:
            recs = self.search([('name', operator , name)] + args, limit=limit)
        return recs.name_get()

    
class FleetVehicleCategory(models.Model):
    _name = 'fleet.vehicle.category'

    name = fields.Char('Name')


class FleetVehicleMachine(models.Model):
    _name = 'fleet.vehicle.feature'

    name = fields.Char('Name', required=True)
    type = fields.Selection([('machine', 'Machine'), ('fuel', 'Fuel'), ('brake', 'Brake'), ('transmission', 'Transmission'),
                             ('suspension', 'Suspension'), ('shockabsorber', 'Shockabsorber'),
                             ('steering', 'Steering')], required=True, string='Type')
    job_ids = fields.Many2many('job.job', string="Jobs", copy=False, readonly=False)


class FleetVehice(models.Model):
    _inherit = 'fleet.vehicle'
    
    @api.depends('type_id', 'license_plate')
    def _compute_vehicle_name(self):
        for record in self:
            record.name = record.display_name
            
    @api.multi
    @api.depends('license_plate', 'type_id')
    def _get_display_name(self):
        for fleet in self:
            fleet.display_name = '%s / %s / %s / %s / %s' % (fleet.license_plate, fleet.type_id.model_id.brand_id.name, fleet.type_id.model_id.name, fleet.type_id.year_id.name, fleet.type_id.vehicle_type_id.name)
            
    type_id = fields.Many2one('fleet.vehicle.type', 'Type', required=False)
    customer_id = fields.Many2one('res.partner', 'Customer No Used')
    partner_id = fields.Many2one('res.partner', 'Customer')
    image_medium = fields.Binary(related='type_id.image', string="Logo (medium)")
#     model_id = fields.Many2one(related='type_id.model_id', string='Model', store=True)
    display_name = fields.Char(string='Display Name', compute='_get_display_name', store=True)
    odometer_unit = fields.Selection([
        ('kilometers', 'Kilometers'),
        ('miles', 'Miles')
        ], 'Odometer Unit', default='kilometers', help='Unit of the odometer ', required=False)
    model_id = fields.Many2one('fleet.vehicle.model', 'Model', required=False, help='Model of the vehicle')
    color_id = fields.Many2one('fleet.vehicle.color', 'Color')
    
    _sql_constraints = [
        ('license_plate_uniq', 'unique(license_plate)', 'Nomor Polisi tidak boleh sama !')
    ]
        
