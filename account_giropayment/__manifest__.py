# -*- coding: utf-8 -*-
{
    'name': 'Giro Payments',
    'version': '11.01',
    'summary': 'Giro Payments',
    'description': 'Giro Payments',
    'category': 'Custom',
    'author': 'PT. VISI',
    'website': "https://www.visi.com",
    'company': 'PT. VISI',
    'depends': [
                'account_invoicing',
                ],
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'views/account_giropayment_view.xml',
        'views/account_invoice_view.xml',
        'wizard/register_giropayment_view.xml'
    ],
    'images': [],
    'license': 'AGPL-3',
    'installable': True,
    'auto_install': False,
    'application': True,
}
