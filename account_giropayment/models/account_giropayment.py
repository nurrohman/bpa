from odoo import models, fields, api, _
from odoo.exceptions import UserError
from odoo.tools.float_utils import float_is_zero
from _collections import defaultdict


class AccountGiropayment(models.Model):
    _name = "account.giropayment"
    _description = 'Payment Giro'
    
    @api.multi
    def _move_count(self):
        for giro in self:
            giro.move_count = len(giro.move_id.line_ids)
        
    move_count = fields.Integer('Jumlah Jurnal Entri', compute='_move_count')
    partner_id = fields.Many2one('res.partner', 'Customer')
    dir_type = fields.Selection([
        ('in', 'Giro Masuk'),
        ('out', 'Giro Keluar')
    ], string='Type', default='in')
    amount = fields.Monetary('Amount', required=True, default=0)
    company_id = fields.Many2one('res.company', 'Company', default=lambda self:self.env.user.company_id, required=True)
    currency_id = fields.Many2one(related='company_id.currency_id', readonly="1")
    number = fields.Char('Number', default='New')
    name = fields.Char('Giro Number', required=True, copy=False)
    communication = fields.Char('Memo')
    giro_journal_id = fields.Many2one('account.journal', 'Bank to Settle')
    bank_id = fields.Many2one('res.bank', 'Bank', required=False)
    bank_account = fields.Char('Bank Account', required=False)
    date = fields.Date('Giro Date', required=True)
    date_due = fields.Date('Due Date', required=True)
    date_reconciled = fields.Date('Reconciled Date')
    date_cancelled = fields.Date('Cancelled Date')
    invoice_ids = fields.Many2many('account.invoice', 'account_invoice_giropayment_rel', 'giropayment_id', 'invoice_id', string="Invoices", copy=False, readonly=False)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('open', 'Open'),
        ('cancel', 'Cancell'),
        ('done', 'Reconciled'),
        ], string='Status', readonly=True, copy=False, index=True, default='draft')
    move_id = fields.Many2one('account.move', 'Journal Entry')
    
    @api.model
    def create(self, vals):   
        res = super(AccountGiropayment, self).create(vals)
        if vals.get('number', 'New'):
            vals['number'] = self.env['ir.sequence'].next_by_code('account.giropayment')
        return res
    
    @api.multi
    def action_giro_validate(self):        
        self.write({'state':'open'})
        self.invoice_ids.write({'running_giro_id': self.id})
        
    @api.multi
    def action_draft(self):
        self.write({'state':'draft'})

    @api.multi
    def action_cancel(self):
        if self.state == 'done':
            raise UserError(_('Unable to cancel reconciled giro'))  
        self.invoice_ids.write({'running_giro_id': False})
        self.write({'state':'draft', 'invoice_ids': (6, 0, [])})
        
    @api.multi
    def action_done(self):
        return {
            'name': 'Reconciled Giro',
            'view_type': 'form',
            'view_mode': 'form',
            'target' : 'new',
            'res_model': 'account.register.giropayments',
            'type': 'ir.actions.act_window',
            'context' : {
                'active_ids': self.ids,
                'form_view_ref' : 'account_giropayment.view_account_register_giropayment_settlement'
            }
        }
        
    @api.multi
    def create_post_journal(self, date):
        for giro in self:
            giro.move_id = self.env['account.move'].create({
                'journal_id' : giro.giro_journal_id.id,
                'date' : date,
                'ref' : giro.communication}).id
            self.env['account.move.line'].with_context(check_move_validity=False).create({
                'move_id':giro.move_id.id,
                'name': giro.name,
                'partner_id' : giro.partner_id.id,
                'account_id': giro.giro_journal_id.default_debit_account_id.id  if giro.dir_type == 'in' else \
                                giro.giro_journal_id.default_credit_account_id.id,
                'currency_id': False,
                'debit' : giro.amount if giro.dir_type == 'in' else 0,
                'credit': 0 if giro.dir_type == 'in' else giro.amount
            })
            line_to_reconcile = self.env['account.move.line'].with_context(check_move_validity=False).create({
                'move_id':giro.move_id.id,
                'name': giro.name,
                'partner_id' : giro.partner_id.id,
                'account_id': giro.invoice_ids.mapped('account_id').ids[0] if giro.invoice_ids else giro.partner_id.property_account_receivable_id.id,
                'currency_id': False,
                'debit': 0  if giro.dir_type == 'in' else giro.amount,
                'credit' : giro.amount if giro.dir_type == 'in' else 0,
            })
            if giro.invoice_ids:
                giro.invoice_ids.register_payment(line_to_reconcile)
            giro.move_id.post()
                
    @api.multi
    def action_view_move_line(self):
        return {
            'name': _('Journal Item'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.move.line',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'domain': [('id', 'in', (self.mapped('move_id').mapped('line_ids')).ids)],
        }
    
    @api.multi
    def unlink(self):
        for giro in self:
            if giro.state not in ('draft','cancel'):
                raise UserError(_('In order to delete giro, you must cancel first !'))   
        return super(AccountGiropayment, self).unlink()             

        
class AccountInvoice(models.Model):
    _inherit = 'account.invoice'
    
    giro_ids = fields.Many2many('account.giropayment', 'account_invoice_giropayment_rel', 'invoice_id', 'giropayment_id', string="Payments", copy=False, readonly=True)
    running_giro_id = fields.Many2one('account.giropayment', string='Open Giro', copy=False)
    
    
# SELECT "account_move_line"."id" as "id","account_move_line"."debit_ca
#  sh_basis" as "debit_cash_basis","account_move_line"."move_id" as "move_id","account_move_line"."analytic_account_id" as "analytic_account_id","account_move_line"."start_
#  ate" as "start_date","account_move_line"."tax_base_amount" as "tax_base_amount","account_move_line"."product_id" as "product_id","account_move_line"."credit_cash_basis"
#  s "credit_cash_basis","account_move_line"."ref" as "ref","account_move_line"."payment_id" as "payment_id","account_move_line"."tax_exigible" as "tax_exigible","account_m
#  ve_line"."quantity" as "quantity","account_move_line"."end_date" as "end_date","account_move_line"."name" as "name","account_move_line"."create_uid" as "create_uid","acc
#  unt_move_line"."date" as "date","account_move_line"."balance_cash_basis" as "balance_cash_basis","account_move_line"."product_uom_id" as "product_uom_id","account_move_l
#  ne"."debit" as "debit","account_move_line"."statement_line_id" as "statement_line_id","account_move_line
